﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KTDB;
using ADC102.VM;

namespace ADC102.Models
{
    public class mdPRS002Service
    {
        private KTConnectionControler KTCC;

        public string sqlStr = string.Empty;

        public mdPRS002Service(KTConnectionControler _KTCC)
        {
            KTCC = _KTCC;
        }

        //取得單位代碼及名稱
        public IEnumerable<T> GetPrs002Stkcod<T>()
        {
            sqlStr = @"SELECT DPTID,
                              FNAME
                         FROM PRS002
                        WHERE     DTFLG      = '2'
                              AND VISIBLEFLG = 'Y'
                       ORDER BY DPTID ASC
            ";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            var result = KTCC.DoSql<T>(cmd).ToList();

            return result;
        }
    }
}
