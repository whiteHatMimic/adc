﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

using KTDB;
using Dapper;
using ADC102.VM;

namespace ADC102.Models
{
    public class mdADC102Service
    {
        private KTConnectionControler KTCC;

        public string sqlStr = string.Empty;

        public mdADC102Service(KTConnectionControler _KTCC)
        {
            KTCC = _KTCC;
        }

        public DataTable GetADC102Drug(string stkcod)
        {
            DataTable dtADC102 = new DataTable();
            string wherekey = string.Empty;

            //如果護理站選擇全部，直接抓全部待審資料，否則多加條件找目的護理站待審資料
            if (stkcod == "全部")
            {
                wherekey = @"WHERE     AGREEFLG = 'N'
                                   AND AGREEDTE IS NULL
                                   AND AGREETME IS NULL
                ";
            }
            else
            {
                wherekey = @"WHERE     AGREEFLG = 'N'
                                   AND AGREEDTE IS NULL
                                   AND AGREETME IS NULL
                                   AND STKCOD = :STKCOD
                ";
            }

            sqlStr = @"SELECT ADC102.CRTNO,
                              ADC102.ACTNO,
                              ADC102.SRFLG,
                              ADC102.ORDID,
                              ADC102.ORDDTE,
                              ADC102.ORDTME,
                              ADC102.DOSAGE,
                              ADC102.STKCOD,
                              ADC102.FREQ,
                              ADC102.ORDAMT,
                              ADC102.VSCLIN,
                              ADC102.SCRNO,
                              ADC102.BIUNT,
                              ADC102.PKUNT,
                              PRS002.FNAME,
                              PRS003.OPNAME,
                              PRS003.OPID,
                              PRS006.ORDNME,
                              '同意' AS AGREE,
                              '不同意' AS DISAGREE
                         FROM ADC102
                              LEFT OUTER JOIN ( SELECT DPTID,
                                                       FNAME
                                                  FROM PRS002
                                                       WHERE     DTFLG = '2'
                                                             AND VISIBLEFLG = 'Y' ) PRS002
                                 ON ( ADC102.STKCOD = PRS002.DPTID )
                              LEFT OUTER JOIN ( SELECT ORDID,
                                                       ORDNME,
                                                       SDATE,
                                                       EDATE,
                                                       ACTION
                                                  FROM PRS006
                                                       WHERE NO = '0' ) PRS006
                                 ON ( ADC102.ORDID = PRS006.ORDID )
                              LEFT OUTER JOIN PRS003 ON ( ADC102.VSDR = PRS003.OPID )
            ";

            sqlStr += wherekey;

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            cmd.Parameters("STKCOD", stkcod);
            KTCC.DoSql(cmd, dtADC102);

            return dtADC102;
        }

        /// <summary>
        /// 檢查審核資料是否存在
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="crtno">病歷號</param>
        /// <param name="actno">流水號</param>
        /// <param name="srflg">來源</param>
        /// <param name="ordid">醫令碼</param>
        /// <param name="orddte">醫令日期</param>
        /// <param name="ordtme">醫令時間</param>
        /// <param name="scrno">序號</param>
        /// <param name="stkcod">單位/庫存</param>
        /// <returns></returns>
        public T InspactADC102<T>(ADC102Key key)
        {
            sqlStr = @"SELECT CRTNO,
                              ACTNO,
                              SRFLG,
                              ORDID,
                              ORDDTE,
                              ORDTME,
                              SCRNO,
                              STKCOD
                         FROM ADC102
                        WHERE     CRTNO  = :CRTNO
                              AND ACTNO  = :ACTNO
                              AND SRFLG  = :SRFLG
                              AND ORDID  = :ORDID
                              AND ORDDTE = :ORDDTE
                              AND ORDTME = :ORDTME
                              AND SCRNO  = :SCRNO
                              AND STKCOD = :STKCOD
            ";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            cmd.Parameters("CRTNO", key.CRTNO);
            cmd.Parameters("ACTNO", key.ACTNO);
            cmd.Parameters("SRFLG", key.SRFLG);
            cmd.Parameters("ORDID", key.ORDID);
            cmd.Parameters("ORDDTE", key.ORDDTE);
            cmd.Parameters("ORDTME", key.ORDTME);
            cmd.Parameters("SCRNO", key.SCRNO);
            cmd.Parameters("STKCOD", key.STKCOD);

            var result = KTCC.DoSql<T>(cmd).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 更新審核資料
        /// </summary>
        /// <param name="newObj"></param>
        /// <param name="oldObj"></param>
        public void UpdateADC102(ADC102Key key, newUpdateADC102VM newObj, oldUpdateADC102VM oldObj)
        {
            DynamicParameters param;
            string whereKey = @" WHERE     CRTNO  = :CRTNO
                                       AND ACTNO  = :ACTNO
                                       AND SRFLG  = :SRFLG
                                       AND ORDID  = :ORDID
                                       AND ORDDTE = :ORDDTE
                                       AND ORDTME = :ORDTME
                                       AND SCRNO  = :SCRNO
                                       AND STKCOD = :STKCOD
            ";

            object paramKey = new {
                CRTNO = key.CRTNO,
                ACTNO = key.ACTNO,
                SRFLG = key.SRFLG,
                ORDID = key.ORDID,
                ORDDTE = key.ORDDTE,
                ORDTME = key.ORDTME,
                SCRNO = key.SCRNO,
                STKCOD = key.STKCOD
            };

            KTCC.CreateUpdateSql(newObj, oldObj, "ADC102", out sqlStr, out param, whereKey, paramKey);
            KTCC.DoSql(sqlStr, param);
        }
    }
}
