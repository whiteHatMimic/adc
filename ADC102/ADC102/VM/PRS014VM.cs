﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADC102.VM
{
    public class PRS014VM
    {
        //病歷號
        public string CRTNO { get; set; }
        //流水號
        public string ACTNO { get; set; }
        //患者姓名
        public string PTNAME { get; set; }
        //性別
        public string PTSEX { get; set; }
        //生日
        public string BRDTE { get; set; }
        //時段: 1.早上 2.下午 3.晚上
        public string VSAPN { get; set; }
        //醫師代碼
        public string VSDR { get; set; }
        //門診日期
        public string VSDTE { get; set; }
        //科別名稱
        public string DPTNAME { get; set; }
    }
}
