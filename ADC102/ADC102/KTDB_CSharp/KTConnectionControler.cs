﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Collections;

#if !noDapper
using Dapper;
#endif


using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;

//1：System.Data.OracleClient （需要安裝客戶端）沒有帶批量方法（還區分x86和x64）。
//2：Oracle.DataAccess        （需要安裝客戶端）帶批量方法（也區分x86和x64）。
//3：Oracle.ManagedDataAccess （不需要安裝客户端）沒帶批量方法（不區分x86和x64，但僅支援.NET 4.0或以上）

/*  
 * ***********************************************************************************
 * [條件式編譯的符號] 於專案在發佈時，沒有使用到此定義。
 * 在專案屬性的 組態: [DeBug]、[Release]、[所有組態] 可以選擇 [所有組態]。
 * DeBug 是在開發專案下的模式，Release 是在發佈程式的執行模式。
 * 這樣在 DeBug 模式與 Release 下都能使用到 條件式編譯的符號。
 * 
 * 109/05/11 居易增加說明
 * 多種管控類型的條件式編譯符號以逗點區隔
 * Ｅｘ　　：OracleODAC, MySqlClient
 * 備註說明：OracleODAC 與 OracleClient 無法共用，會以 OracleODAC 為優先
 * ***********************************************************************************
 */
#if OracleODAC
//Official Oracle ODP.NET, Managed Driver 
//此 using 參數必須 參考 Oracle.ManagedDataAccess 才能使用
//但是一旦程式模組開放於共用的連線集合，會立即發生錯誤
//必須在 [專案] --> [屬性] --> [參考] 加入 Oracle.ManagedDataAccess.dll
//並修改專案屬性 建置 --> 
//1.組態: [DeBug]、[Release]、[所有組態] 可以選擇 [所有組態]。
//2.條件式編譯的符號 --> 增加常數 OracleODAC"

//共同使用目前的 his Oracle 環境，並安裝 x86 x64(both) 與註冊 GAC(true)
//C:\ap06bz\ETC\ODP.NET_Managed_ODAC122cR1\install_odpm.bat "C:\oracle\" both true
//或
//自訂 Oracle.ManagedDataAccess.dll 認識的 環境變數 TNS_ADMIN
//名稱 : TNS_ADMIN
//參數 : C:\oracle\NETWORK\ADMIN
using Oracle.ManagedDataAccess.Client;
#elif OracleClient
//此 using 參數必須 參考 System.Data.OracleClient 才能使用
//但是一旦程式模組開放於共用的連線集合，會立即發生錯誤
//必須在 [專案] --> [屬性] --> [參考] 加入 System.Data.OracleClient
//並修改專案屬性 建置 --> 
//1.組態: [DeBug]、[Release]、[所有組態] 可以選擇 [所有組態]。
//2.條件式編譯的符號 --> 增加常數 OracleClient"
using System.Data.OracleClient;
#endif

#if MySqlClient
//此功能只能作用在 .Net 4.0 以上
//此 using 參數必須 參考 MySql.Data 才能使用
//但是一旦程式模組開放於共用的連線集合，會立即發生錯誤
//必須在 [專案] --> [屬性] --> [參考] 加入 MySql.Data.MySqlClient
//並修改專案屬性 建置 --> 
//1.組態: [DeBug]、[Release]、[所有組態] 可以選擇 [所有組態]。
//2.條件式編譯的符號 --> 增加常數 MySqlClient"
using MySql.Data.MySqlClient;
#endif

namespace KTDB
{
    public partial class KTConnectionControler : IDisposable
    {

        public KTConnectionControler()
        {
            ConnectOk = false;
            DoSqlOK = false;
            RunExecute = true;
            ShowDosqlMsg = true;
            Rowindicator = 0;
            ErrorCode = "";
            UnLockAuto = false;
            DoSqlDisposeCommand = false;

            Cns = new KTConnections();

            DBConfig = DBProviderType.Oracle;
            ADOConfig = ADOProviderType.OracleClient;

            try
            {
                OracleHostDSN_ = GetHostDSN();
                GetNurseStation();
            }
            catch (Exception)
            { }
        }

        //連線集合
        private KTConnections Cns { get; set; }

        //##### 預設 連線環境 & 選擇連線 
        //##### 在 Form_Load 宣告類別的程式 可以 重新設定連線環境
        public DBProviderType DBConfig { get; set; }    //設定 是哪一種資料庫類別
        public ADOProviderType ADOConfig { get; set; }  //設定 Odbc 或 OleDB 或 SQLClient 或 Oracle

        //連線成功否 
        public bool ConnectOk { get; set; }

        //Sql 指令成功否
        public bool DoSqlOK { get; set; }

        //Oracle 錯誤代碼
        public string ErrorCode { get; set; }

        //Oracle 錯誤訊息
        private System.Text.StringBuilder ErrorMessage_ = new System.Text.StringBuilder();
        public string ErrorMessage
        {
            get { return ErrorMessage_.ToString(); }
        }

        //DoSql 錯誤時是否要顯示錯誤訊息(常駐程式使用)
        public bool ShowDosqlMsg { get; set; }

        //程式啟動時, 判斷是否從醫院系統登入
        public bool RunExecute { get; set; }

        //程式啟動時, 自動刪除此程式先前存在 Oracle 的 Session 連線
        public bool UnLockAuto { get; set; }

        public bool DoSqlDisposeCommand { get; set; }

        //資料筆數
        public long Rowindicator { get; set; }

        //public string DosqlGrpIp { get; set; }
        //public string MachineIp { get; set; }

        private string ProcessModuleName_ = "";

        //目前的連線是應用程式(exe)或網頁程式(Web)
        private static string ExeOrWeb_ = "";
        public static string ExeOrWeb
        {
            get
            {
                if (ExeOrWeb_ == "")
                {
                    string fileName = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName;
                    string[] fileNames = fileName.Split(new string[] { "\\" }, StringSplitOptions.None);
                    fileName = fileNames[fileNames.Length - 1];

                    switch (fileName)
                    {
                        case "w3wp.exe":
                        case "iisexpress.exe":
                            ExeOrWeb_ = "web";
                            break;

                        default:
                            fileNames = fileName.Split('.');
                            ExeOrWeb_ = fileNames[fileNames.Length - 1].ToLower();
                            break;
                    }
                }
                return ExeOrWeb_;
            }
        }

        //紀錄此程式的所在院區 Function GetHostName()
        private static string OracleHostDSN_ = "";
        public static string OracleHostDSN
        {
            get
            {
                //取得目前 Oracle 的連線環境(KTGH00, HPK210, TSHIS)
                //避免使用 GetHostDSN 會重複開啟 Oracle.ini 很多次
                if (string.IsNullOrEmpty(OracleHostDSN_))
                    OracleHostDSN_ = GetHostDSN();
                return OracleHostDSN_;
            }

            //不要亂指定，這裡是指 CNHome 的院區
            set { OracleHostDSN_ = value; }
        }

        //護理站樓層位置
        private string STNID_ = "";
        public string STNID
        {
            get
            {
                //取得護理站的位置 
                if (string.IsNullOrEmpty(STNID_))
                    GetNurseStation();
                return STNID_;
            }
        }

        //護理站庫別位置
        private string STKCOD_ = "";
        public string STKCOD
        {
            get
            {
                //取得護理站的位置 
                if (string.IsNullOrEmpty(STKCOD_))
                    GetNurseStation();
                return STKCOD_;
            }
        }

        //ConnectionString  1060808
        //新增 連線字串，給定義在 Web.Config 的開發專案使用
        //要更換主機只需要修改 Web.Config 參數即可
        //使用方式 
        //KTCC.ConnectionString = WebConfigurationManager.ConnectionStrings["ConnectionStringName"].ConnectionString
        //KTCC.Connect()            'CNHOME
        //KTCC.Connect("KTGH00")    '或是 傳入 自訂 DataSource Name

        //另外也可以定義在 applicationSettings 定義其 DataSource、DataBase、UserID、Password、Port等資訊，
        //再導入 KTCC.Connect(DataSource, DataBase, UserID, Password, Port)
        public string ConnectionString { get; set; }
        public void Connect(String sDataSource = "", String sDataBase = "", String sUserID = "", String sPassword = "", String sPort = "", bool CnHomeFlg = false, string UseCnKey = "")
        {
            //功能：連線到主機
            //參數：sDataSource 主機名稱(KTGH00, HPK210, TSHIS)
            //                 沒傳 sHostName 進來，就連到oracle.ini內設定的主機        '
            //       sDataBase 資料庫名稱, 在 SQLServer 以及 MySql 才需要此功能
            //                 SQLServer 一定要傳入..否則連線會有錯誤
            //
            //       sUserid   登入資料庫的使用者帳號
            //                 Oracle 不傳入 預設為 AP06BZ
            //                 SQLServer  不傳入 預設為 sa
            //                 MySQL  不傳入 預設為 root
            //                 Access, Excel 預設是沒有帳號 
            //                 -- 如需帳號密碼 : 修改 ReplaceUserid 與 ReplacePasword 
            //
            //       sPassword 登入資料庫的使用者密碼
            //                 Oracle 不傳入 預設為 AP06BZ
            //                 SQLServer 不傳入 預設為 ""
            //                 MySQL 不傳入 預設為 ""
            //                 目前遇到需指定 Port Number 的資料庫為 Mysql
            //
            //       CnHomeFlg 功能：切換院區，--> 預設 為 False
            //                 當 CnHomeFlg = True 時，
            //                 若 CNHOME 已經連線，會將目前的 CNHOME 連線 Close
            //                  並將 CNHOME 重新產生連線到 sHostName 的主機，不會另外產生新的連線
            //                 且 OracleHostDSN_ 會更新為 sHostName
            //
            //       UseCnKey  使用自訂的 連線交易參數
            //                 預設是是以 CNHOME 或 主機名稱 來當連線, 
            //                 但是若有特定用途時，仍然可以對同一台主機連線，而使用不同的連線交易參數
            // 

            if (!檢查連線參數環境是否可用())
                return;

            if (RunExecute)
                if (!檢查是否為醫院系統登入())
                    System.Environment.Exit(1);

            try
            {
                //如果要更改連線的參數，您必須在 KTCC.Connect("Machine") 之前就必須重新指定
                KTDBProvider Cn = new KTDBProvider(DBConfig, ADOConfig);
                string cnKey = "";
                if (!string.IsNullOrEmpty(ConnectionString))
                {
                    cnKey = "CNHOME";
                    if (!string.IsNullOrEmpty(sDataSource))
                        cnKey = sDataSource;

                    Cn.Connection.ConnectionString = ConnectionString;
                    Cn.Connection.Open();

                    //Cn.Connection.ConnectionString = "USER ID=AP06BZ;PASSWORD=AP06BZ;Data Source=(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = kt1.ktgh.com.tw)(PORT = 1526)))(CONNECT_DATA =(SERVICE_NAME = kt1)))";
                    //Cn.Connection.ConnectionString = "USER ID=AP06BZ;PASSWORD=AP06BZ;Data Source=//kt1.ktgh.com.tw:1526/kt1";
                    //Cn.Connection.Open();
                    //Oracle.ManagedDataAccess.Client.OracleConnection o = new Oracle.ManagedDataAccess.Client.OracleConnection();
                    //o.ConnectionString = Cn.Connection.ConnectionString;
                    //o.Open();

                    //DataSet ds = new DataSet();
                    //string sql = "select * from dual";
                    //Oracle.ManagedDataAccess.Client.OracleDataAdapter oda = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(sql, o);
                    //oda.Fill(ds);
                    //DataTable dt = ds.Tables[0];

                    //DataTable tbl  = new DataTable(); 
                    //Oracle.ManagedDataAccess.Client.OracleDataAdapter adrOracleClient = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(new Oracle.ManagedDataAccess.Client.OracleCommand();
                    //Rowindicator = adrOracleClient.Fill(tbl);
                    //adrOracleClient.Dispose();

                    ConnectionString = "";
                }
                else
                {
                    if (!string.IsNullOrEmpty(UseCnKey))
                        UseCnKey = UseCnKey.ToUpper().Trim();

                    if (string.IsNullOrEmpty(sDataSource))
                    {
                        sDataSource = OracleHostDSN_;     //Property OracleHostDSN() 讀取oracle.ini內的主機名稱
                        cnKey = "CNHOME";               //連線集合的key 
                        if (!string.IsNullOrEmpty(UseCnKey))
                            cnKey = UseCnKey;

                        if (string.IsNullOrEmpty(sDataSource))
                        {
                            ConnectOk = false;
                            throw new ApplicationException("沒有定義連線主機名稱(sDataSource)!!");
                        }
                    }
                    else
                    {
                        sDataSource = sDataSource.ToUpper();
                        if (sDataSource == "CNHOME")
                            sDataSource = OracleHostDSN_;

                        if (CnHomeFlg == true)
                        {
                            //注意：連線院區會更改為 sHostName
                            //      且 OracleHostDSN_ 也必須變動為 [sHostName]
                            //      預設連線參數的關鍵字仍然是 CNHOME
                            OracleHostDSN_ = sDataSource;
                            cnKey = "CNHOME";
                            if (!string.IsNullOrEmpty(UseCnKey))
                                cnKey = UseCnKey;
                            if (Cns.Contains(cnKey))
                                Close(cnKey);
                        }
                        else
                        {
                            cnKey = sDataSource;
                            //連線集合的key 
                            if (!string.IsNullOrEmpty(UseCnKey))
                                cnKey = UseCnKey;
                            if (cnKey == "CNHOME")
                                OracleHostDSN_ = sDataSource;
                        }
                    }

                    if (Cns.Contains(cnKey))
                    {
                        ConnectOk = true;
                        //throw new ApplicationException(sHostName + " 己經連線, 不須再連線!!");
                        return;
                    }

                    Cn.Connection.ConnectionString = Cn.ConnectionString(sDataSource, sDataBase, sUserID, sPassword, sPort);
                    Cn.Connection.Open();
                }

                string tmpDataSource = "";
                string tmpDataBase = "";
                ConnectionDataSource(Cn, out tmpDataSource, out tmpDataBase);

                if (string.IsNullOrEmpty(sDataSource))
                    sDataSource = tmpDataSource;

                Cn.CnKey = cnKey;
                Cn.DataSource = tmpDataSource;  //真正的 DataSource
                Cn.DataBase = tmpDataBase;
                Cn.HostName = sDataSource;      //使用 Command 時，需調用 sHostName 的連線環境參數
                Cns.Add(cnKey, Cn);             //將連線加到連線集合

                if (Cn.DBConfig == DBProviderType.Oracle)
                {
                    if (ExeOrWeb == "exe")
                        switch (sDataSource)
                        {
                            case "KTGH00":
                            case "HPK210":
                            case "TSHIS":
                            case "KTXS00":
                                try { SetTime(cnKey); }
                                catch (Exception) { }
                                break;
                        }

                    KillOldSession(cnKey);
                    //if (datatablefirstflag == "")
                    //{
                    //    datatablefirstflag = sDataSource;
                    //    if (Cn.ADOConfig == ADOProviderType.OracleClient) SelectDual();
                    //}
                }

                ConnectOk = true;
                isConnectedFlag = true;

            }
            catch (Exception E)
            {
                ConnectOk = false;
                KTEnvs.Error_Log(E.Source + " " + E.Message);
                ErrorCode = GetOracleErrorCode(E.Message);
                //讓外部去判斷是否使用 Try Catch 顯示 MessageBox.Show
                string errStr = "";
                if (ErrorCode == "ORA-12500" | ErrorCode == "ORA-00020")
                {
                    errStr = errStr + "KTCC.Connect(" + sDataSource + ")" + (ErrorCode == "ORA-12500" ? "UNIX" : "DataBase") + " Already Max Process" + KTConstant.vbCrLf + KTConstant.vbCrLf;
                    errStr = errStr + "連線時發生錯誤!!" + KTConstant.vbCrLf + KTConstant.vbCrLf;
                    errStr = errStr + "由於目前主機連線人數過多" + KTConstant.vbCrLf + KTConstant.vbCrLf;
                    errStr = errStr + "以致無法正常使用醫院系統" + KTConstant.vbCrLf + KTConstant.vbCrLf;
                    errStr = errStr + "請您稍後再試一次!!" + KTConstant.vbCrLf + KTConstant.vbCrLf;
                    throw new ApplicationException("不正確的操作方式" + KTConstant.vbCrLf + KTConstant.vbCrLf + "請由醫院系統登入" + KTConstant.vbCrLf + KTConstant.vbCrLf + "點選 [確定] 結束程式");
                }
                else
                {
                    errStr = "資料庫連線時發生錯誤!!" + KTConstant.vbCrLf + E.Message;
                    throw new ApplicationException(errStr);
                }
            }
            finally
            {
                //任何資料庫類型的連線之後，都回復為 Oracle DBConfig
                DBConfig = DBProviderType.Oracle;
                ADOConfig = ADOProviderType.OracleClient;
            }
        }


        string datatablefirstflag = "";
        private void SelectDual()
        {
            System.Threading.Thread ActiveThreadStart = new System.Threading.Thread(new System.Threading.ThreadStart(StartSelectDual));
            ActiveThreadStart.Start();
        }

        public void StartSelectDual()
        {
            string UseCnKey = datatablefirstflag + "UseCnKey";

            using (KTConnectionControler tempKTCC = new KTConnectionControler())
            {
                tempKTCC.datatablefirstflag = UseCnKey;
                //Oracle.ManagedDataAccess.Client 程式啟動的第一次，再載入物件與使用物件時相當慢
                //1.DataAdper 丟資料給 DataSet 或 DataTable 會很慢
                //2.Rollback 第一次執行也是很慢。
                //所以無論執行 Dosql DataTable 或 Rollback 都可以
                try
                {
                    tempKTCC.Connect(datatablefirstflag, UseCnKey: UseCnKey);
                }
                catch (Exception) { }
                //try
                //{
                //    tempKTCC.BeginTransaction(UseCnKey);
                //    tempKTCC.Rollback(UseCnKey);
                //} catch (Exception) {}
                try
                {
                    tempKTCC.DoSql("select * from dual", new DataTable(), UseCnKey);
                }
                catch (Exception) { }
            }

        }

        private void ConnectionDataSource(KTDBProvider iConn, out string DataSource, out string DataBase)
        {
            DataSource = "";
            DataBase = "";
            switch (iConn.ADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:
                    DataBase = ((System.Data.Odbc.OdbcConnection)iConn.Connection).Database;
                    DataSource = ((System.Data.Odbc.OdbcConnection)iConn.Connection).DataSource;
                    break;

                case ADOProviderType.Oledb:
                    DataBase = ((System.Data.OleDb.OleDbConnection)iConn.Connection).Database;
                    DataSource = ((System.Data.OleDb.OleDbConnection)iConn.Connection).DataSource;
                    break;

                case ADOProviderType.SQLClient:
                    DataBase = ((System.Data.SqlClient.SqlConnection)iConn.Connection).Database;
                    DataSource = ((System.Data.SqlClient.SqlConnection)iConn.Connection).DataSource;
                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:
                    DataBase = ((Oracle.ManagedDataAccess.Client.OracleConnection)iConn.Connection).Database;
                    DataSource = ((Oracle.ManagedDataAccess.Client.OracleConnection)iConn.Connection).DataSource;
                    break;
#elif OracleClient
                case ADOProviderType.OracleClient:
                    DataBase = ((System.Data.OracleClient.OracleConnection)iConn.Connection).Database;
                    DataSource = ((System.Data.OracleClient.OracleConnection)iConn.Connection).DataSource;
                    break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:
                    DataBase = ((MySql.Data.MySqlClient.MySqlConnection)iConn.Connection).Database;
                    DataSource = ((MySql.Data.MySqlClient.MySqlConnection)iConn.Connection).DataSource;
                    break;
#endif
            }
        }

        public string WhichHost()
        {
            //功能：回傳目前預設的連線環境
            //回傳：KTGH00 沙鹿 HIS主機
            //      HPK210 大甲HIS主機
            //      KTGH03 沙鹿測試主機
            if (string.IsNullOrEmpty(Cns["CNHOME"].HostName.ToUpper()))
            {
                throw new System.Exception("呼叫 WhichHost 判斷連線環境時，無法正確的做判斷");
            }
            else
            {
                return Cns["CNHOME"].HostName.ToUpper();
            }
        }


        private class setOracleDataTime
        {
            [System.Runtime.InteropServices.DllImport("kernel32.dll")]
            public extern static void GetSystemTime(ref SYSTEMTIME lpSystemTime);
            [System.Runtime.InteropServices.DllImport("kernel32.dll")]
            public extern static uint SetSystemTime(ref SYSTEMTIME lpSystemTime);

            public struct SYSTEMTIME
            {
                public ushort wYear;
                public ushort wMonth;
                public ushort wDayOfWeek;
                public ushort wDay;
                public ushort wHour;
                public ushort wMinute;
                public ushort wSecond;
                public ushort wMilliseconds;
            }
            public SYSTEMTIME systime = new SYSTEMTIME();

            public class SetTime_Model
            {
                public DateTime SvrDateTime { get; set; }
            }
        }

        public void SetTime(string TransHost = "")
        {
            //功能:將本機電腦和Oracle主機對時
            string sqlstr = "select to_char(sysdate,'yyyy/mm/dd hh24:mi:ss') as SvrDateTime from dual";

            setOracleDataTime systime = new setOracleDataTime();

            System.Data.DataTable tblSetTime = new System.Data.DataTable();
            List<setOracleDataTime.SetTime_Model> dy_SetTime = new List<setOracleDataTime.SetTime_Model>();

            try
            {

#if !noDapper
                dy_SetTime = DoSql<setOracleDataTime.SetTime_Model>(sqlstr, TransHost: TransHost).ToList();
#else
                DoSql(sqlstr.ToString(), tblSetTime, TransHost: TransHost);
                dy_SetTime = DataTableToList<setOracleDataTime.SetTime_Model>(tblSetTime).ToList();
#endif


                if (dy_SetTime.Count > 0)
                {
                    setOracleDataTime.GetSystemTime(ref systime.systime);   //不先取得時間就修改不了
                    dy_SetTime[0].SvrDateTime = dy_SetTime[0].SvrDateTime.AddHours(-8);   //國際標準時間 -8
                    systime.systime.wYear = (ushort)dy_SetTime[0].SvrDateTime.Year;
                    systime.systime.wMonth = (ushort)dy_SetTime[0].SvrDateTime.Month;
                    systime.systime.wDay = (ushort)dy_SetTime[0].SvrDateTime.Day;
                    systime.systime.wHour = (ushort)dy_SetTime[0].SvrDateTime.Hour;
                    systime.systime.wMinute = (ushort)dy_SetTime[0].SvrDateTime.Minute;
                    systime.systime.wSecond = (ushort)dy_SetTime[0].SvrDateTime.Second;
                    setOracleDataTime.SetSystemTime(ref systime.systime);
                }
            }
            catch { }
        }

        private class gvSession_Models
        {
            public decimal SID { get; set; }
            public decimal SERIAL { get; set; }
            public string PROCESS { get; set; }
            public string PROGRAM { get; set; }
            public string CLIENT_INFO { get; set; }
        }
        private void KillOldSession(string TransHost)
        {
            //1050103 居易
            //功能：檢查目前連線院區是否有相同電腦、相同程式未結束的Session
            //條件：KTCC.UnLockAuto = True 
            //排除：1.Web
            //      2.無 Connection
            //      3.isDllConnect  Dll 產生的連線尚未測試完成，暫時不執行，因為這種連線的原生程式名稱仍然是原呼叫程式
            //        ex：ORDP1000.exe 呼叫 PRSP1000.dll
            //        ORDP1000.exe 當下已經有 KTCC.BeginTransaction()，
            //        在呼叫 PRSP1000.dll 時，會由 PRSP1000.exe 自己再連一條線
            //        這時候會再來執行一次此 Function 這只是多做的，因為抓到的程式為 ORDP1000.exe
            //      4.不是 Oracle
            //備註：A電腦 Lock 病歷號，重新啟動相同程式會直接刪除先前該程式執行後殘留的 Session
            //      但是 如果 A 電腦因為當機而無法開機，到 B電腦啟動此程式，此功能是沒有作用的

            if (!UnLockAuto) return;
            if (ExeOrWeb != "exe") return;
            if (!AlreadyConnectOK(TransHost)) return;
            if (Cns[TransHost].isDllConnect == true) return;
            if (Cns[TransHost].DBConfig != DBProviderType.Oracle) return;

            try
            {

                System.Text.StringBuilder sqlstr = new System.Text.StringBuilder();

                System.Data.DataTable tblgvSession = new System.Data.DataTable();
                List<gvSession_Models> dy_gvSessions = new List<gvSession_Models>();

                sqlstr.Length = 0;
                sqlstr.AppendLine("Select SID, SERIAL# AS SERIAL, PROCESS, PROGRAM, Client_info From gv$Session ");
                sqlstr.AppendLine(" Where AUDSID = (Select USERENV('SESSIONID') From DUAL) ");
#if !noDapper
                dy_gvSessions = DoSql<gvSession_Models>(sqlstr.ToString(), TransHost: TransHost).ToList();
#else
                DoSql(sqlstr.ToString(), tblgvSession, TransHost: TransHost);
                dy_gvSessions = DataTableToList<gvSession_Models>(tblgvSession).ToList();
#endif

                if (dy_gvSessions.Count == 0) return;

                var gvSession = dy_gvSessions[0];

                if (gvSession.CLIENT_INFO == "") return;

                switch (gvSession.PROGRAM)
                {
                    case "W3WP.EXE":
                    case "TOAD.EXE":
                        return;
                }

                string[] Processs = gvSession.PROCESS.Split(':');
                gvSession.PROCESS = Processs[0];

                sqlstr.Length = 0;
                sqlstr.AppendLine("Select Sid, Serial# As Serial From gv$Session");
                sqlstr.AppendLine(" Where Client_info = '" + gvSession.CLIENT_INFO + "' ");

                //保留分號，避免 Process 有兩組 123 與 1234 Like 後會將 1234 也跟著刪除。
                sqlstr.AppendLine("   And PROCESS Not Like '" + gvSession.PROCESS + ":%' ");
                sqlstr.AppendLine("   And UPPER(Program) = '" + gvSession.PROGRAM + "' ");
#if !noDapper
                dy_gvSessions = DoSql<gvSession_Models>(sqlstr.ToString(), TransHost: TransHost).ToList();
#else
                DoSql(sqlstr.ToString(), tblgvSession, TransHost: TransHost);
                dy_gvSessions = DataTableToList<gvSession_Models>(tblgvSession).ToList();
#endif

                if (dy_gvSessions.Count == 0) return;

                for (int x = 0; x <= dy_gvSessions.Count - 1; x++)
                {
                    gvSession = dy_gvSessions[x];
                    sqlstr.Length = 0;
                    sqlstr.AppendLine(" ALTER SYSTEM DISCONNECT SESSION '" + gvSession.SID + "," + gvSession.SERIAL + "' IMMEDIATE ");
                    DoSql(sqlstr.ToString(), TransHost: TransHost);
                    KTDateTime.Delay(0.01);
                }

            }
            catch (Exception ex)
            {
                KTEnvs.Error_Log("KillOldSession Error:" + TransHost + KTConstant.vbCrLf + ex.Message);
            }
        }

        public KTDBProvider KTProvider(string TransHost = "")
        {
            return Cns[主機代碼(TransHost)];
        }

        public KTDBProvider ConnProvider(string TransHost = "")
        {
            return Cns[主機代碼(TransHost)];
        }

        public IDbConnection Connection(string TransHost = "")
        {
            return Cns[主機代碼(TransHost)].Connection;
        }

        public ConnectionState ConnectState(string TransHost = "")
        {
            TransHost = 主機代碼(TransHost);

            //連線失敗時，不會加入連線的集合
            if (Cns[TransHost].Connection == null)
                return ConnectionState.Closed;

            return Cns[TransHost].Connection.State;
        }

        public string DataBaseLink(string DBLink, string TableName, string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：自動判斷所屬的連線院區來區別，其 DataBaseLink 該連結到哪台主機
            //作者：吳居易
            //日期：094/08/23
            //更新：094/10/20 更新
            //說明：
            //參數：Hospital  --> 要連結的 院區，目前、沙鹿、大甲、"KTGH00"、"HPK210" 
            //      LinkTable --> 連結的 資料表格 
            //      TransHost --> 預設為 CNHOME，必須真的有CNHOME，否則必須丟實際交易主機
            //      sKTDB     --> 另外一個 KTCC 的宣告連線 ex: KTDB.Connect()，必須丟入 KTDB.KTProvider

            //傳回：Table + 連線院區
            //版本：
            //範例：1.若連線的主院區在沙鹿，而要抓大甲的資料
            //        DataBaseLink("大甲", "IPD014") --> IPD014@HPK210
            //        DataBaseLink("大甲", "PRS006") --> PRS006@HPK210
            //        DataBaseLink("大甲", "")       --> @HPK210
            //        DataBaseLink("沙鹿", "PRS006") --> PRS006
            //
            //      2.若連線的主院區在大甲，而要抓沙鹿的資料
            //        DataBaseLink("沙鹿", "IPD014") --> IPD014@KTGH00
            //        DataBaseLink("沙鹿", "PRS006") --> PRS006@KTGH00
            //
            //備註：1.測試環境 與 沙鹿環境是相同的
            //      2.如果預設連線環境不是 KTGH00, KTGH03, HPK210 
            //        輸出結果仍然是正常的，可以直接看答案輸出答案

            //只能丟單一個院區
            //否則出錯無法解決

            //強制將 Hospital 轉換成 沙鹿 或 大甲
            //掛號有使用 "目前" 院區

            if (string.IsNullOrEmpty(DBLink))
            {
                DBLink = "";
                return TableName;
            }

            DBLink = DBLink.Replace("@", "");

            string ConnectHost = "";

            if ((sKTDB != null))
            {
                ConnectHost = sKTDB.HostName;
                if (string.IsNullOrEmpty(DBLink.Trim()))
                    DBLink = sKTDB.HostName;

                if (DBLink == "CNHOME")
                    DBLink = sKTDB.HostName;

                //若為測試院區 就是 KTGH03 
                if (DBLink.IndexOf("目前") != -1)
                    DBLink = sKTDB.HostName;

            }
            else
            {
                TransHost = 主機代碼(TransHost);

                if (AlreadyConnectOK(TransHost))
                {
                    ConnectHost = Cns[TransHost].HostName;
                    if (string.IsNullOrEmpty(DBLink.Trim()))
                        DBLink = Cns[TransHost].HostName;

                    if (DBLink == "CNHOME")
                        DBLink = Cns[TransHost].HostName;

                    if (DBLink.IndexOf("目前") != -1)
                        DBLink = Cns[TransHost].HostName;
                    //若為測試院區 就是 KTGH03 

                }
                else
                {
                    //1020905。
                    //若 KTCC 沒有指定交易連線近來，而且連預設的連線都沒有，
                    //全部都使用 CNHOME 變數，讓 Return 值，全部都加上主機名稱。
                    ConnectHost = "CNHOME";
                }
            }

            if (DBLink.IndexOf("沙鹿") != -1)
                DBLink = "KTGH00";

            if (DBLink.IndexOf("大甲") != -1)
                DBLink = "HPK210";

            if (DBLink.IndexOf("通霄") != -1)
                DBLink = "TSHIS";

            if (DBLink.IndexOf("向上") != -1)
                DBLink = "KTXS00";

            if (DBLink.IndexOf("測試") != -1)
                DBLink = "KTGH03";

            //當我的預設連線院區是 KTGH00，那麼 DBLink 又是自己，所以不要 Link 到任何主機
            if (ConnectHost == DBLink)
                return TableName;

            //當我的預設連線院區是 KTGH00，那麼 DBLink 不是自己，要 Link 到該主機
            if (!string.IsNullOrEmpty(DBLink))
                return TableName + "@" + DBLink;

            return TableName;

        }

        public bool ConnectDll(DBProviderType xDBConfig, ADOProviderType xADOConfig, IDbConnection xConnection, string HostName = "", string xTransHost = "")
        {
            //0990902 居易
            //將執行檔編譯成 DLL，可以和主要呼叫程式共用一個 Connection Session
            //在主要呼叫程式的使用
            //假設表單 frmCallForm 是執行程式主表單，也編譯為 dll，供其他程式呼叫。
            //在程式表單上宣告  Public frmCn2Dll As New cls_Connect2Dll

            //欲呼叫的程式必須告訴表單 目前為 dll Connect
            //Dim f As New frmCallForm
            //f.frmCn2Dll.isDllConnect = True
            //f.frmCn2Dll.ADOConfig = KTCC.Cns("CNHOME").ADOConfig
            //f.frmCn2Dll.DBConfig = KTCC.Cns("CNHOME").DBConfig
            //f.frmCn2Dll.Connection = KTCC.Cns("CNHOME").Connection
            //f.frmCn2Dll.HostName = KTCC.Cns("CNHOME").HostName
            //f.ShowDialog()
            //f.Dispose()

            //於程式表單載入時，需執行 frmCn2Dll.Connect 
            //若不傳入 isDllConnect = True，則表示此為 exe 執行程式
            //若傳入 isDllConnect = True，並傳入 KTCC.Cns(Key) 的連線時，則連線會共用
            //或傳入 isDllConnect = True，但不傳入 KTCC.Cns(Key)，則表單會於自己 Connect

            //備註
            //A
            //要共用連線者，必須符合四個條件 DBConfig、ADOConfig、Connection、Connection Not BeginTransaction
            //所以 1.若沒有丟入 Config，會重新產生一條連線，
            //     2.Connection 是 Nothing 會重新產生一條連線
            //     3.Connection.State 是 Closed 會重新產生一條連線
            //     4.Connection 是 BeginTransaction 時，會重新產生一條連線(備註C)
            //而新連線的 v$session 看到的執行程式是主要呼叫程式

            //B
            //dll 執行的連線 TransactionHost 應該全部都是 CNHOME

            //C
            //主程式的 Connection 連線若有 BeginTransaction，尚未 Commit 或 Rollback 
            //則 dll 程式不能夠使用主程式的 Connection，必須重新連線

            //D
            //RunExecute = False
            //呼叫此 dll 連線時，全部都是正常執行過的程式，不需再判斷是否從醫院系統登錄

            RunExecute = false;

            xTransHost = (string.IsNullOrEmpty(xTransHost) ? "CNHOME" : xTransHost);

            if ((int)xDBConfig != -1 & (int)xADOConfig != -1)
            {
                if (!AlreadyConnectOK(xTransHost))
                {
                    DBConfig = xDBConfig;
                    ADOConfig = xADOConfig;

                    if (xConnection == null)
                    {
                        //自己連線可以被 Close 
                        if (!AlreadyConnectOK(xTransHost))
                            Connect(HostName, UseCnKey: xTransHost);

                    }
                    else if (xConnection.State == ConnectionState.Closed)
                    {
                        //自己連線可以被 Close 
                        if (!AlreadyConnectOK(xTransHost))
                            Connect(HostName, UseCnKey: xTransHost);

                    }
                    else
                    {
                        //沿用主程式的連線，不能被 Closed 必須記錄參數 isDllConnect = True
                        KTDBProvider KTDB = default(KTDBProvider);
                        string cnKey = "";

                        KTDB = new KTDBProvider(xDBConfig, xADOConfig);

                        if (string.IsNullOrEmpty(HostName))
                            HostName = OracleHostDSN_;

                        KTDB.HostName = HostName;
                        OracleHostDSN_ = HostName;

                        cnKey = (string.IsNullOrEmpty(xTransHost) ? "CNHOME" : xTransHost);

                        KTDB.Connection = xConnection;
                        KTDB.isDllConnect = true;
                        Cns.Add(cnKey, KTDB);
                        //Add 主程式的連線至 dll 

                        //判斷主程式進入之前是否有 BeginTransaction
                        //有 BeginTransaction 所以 DataReader 去 Dosql 一定會失敗
                        //這時候就 dll 程式就必須自己重新連線

                        //1050107 再次確認了已經有 BeginTransaction 是否會沿用的答案
                        //        如果以自身使用的單一執行程式是測試不出來的
                        //        必須使用 dll 測試，才能讓 iDataReader 得到 DoSqlOk = false

                        IDataReader rdr = null;
                        try
                        {

                            DoSql("Select * from dual", out rdr, TransHost: xTransHost);
                            if (!DoSqlOK)
                            {
                                Cns.Remove(cnKey);
                                Connect(HostName, UseCnKey: cnKey);
                            }
                        }
                        catch { }
                        if ((rdr != null)) { rdr.Close(); rdr.Dispose(); }
                    }
                }
            }
            else
            {
                //照常理來說, Connection 的連線是屬於什麼屬性, dll 就是接受該屬性連線
                //但是 KTCC 內的 DBConfig 與 ADOConfig 不明確, 所以最好獨自連線
                if (!AlreadyConnectOK(xTransHost))
                {
                    Connect(HostName, UseCnKey: xTransHost);
                }
            }

            return true;

        }

        //1091228 居易 
        //        增加曾經 Connect 成功註記
        //        避免 Close Connect 之後，重新 Connect 判斷沒有從醫院系統登入過的錯誤
        private bool isConnectedFlag = false;

        public bool 檢查是否為醫院系統登入()
        {
            //0980923 居易 
            //        因應 ISO 27001 變動
            //        程式的執行必須強制透過醫院系統登入
            //
            //1010406 居易 
            //        此 Class 這個地方要共用於 ASP.NET
            //        Asp.Net 不是執行檔程式，它是一個虛擬目錄或網頁程式
            //        沒有 Application 的名稱空間
            //        Asp.Net 的開發版本為 VB.NET 2008、VBC_VRE = 9.0
            //        且又要共用 VB.NET 2003，必須使用底下的定義

            RunExecute = false;

            if (ExeOrWeb != "exe")
            {
                //ASP.NET 版本不做醫院系統登錄限制
                RunExecute = false;
            }
            else
                if (System.AppDomain.CurrentDomain.BaseDirectory.ToLower().IndexOf("\\bin") > -1)
                {
                    //程式開發人員, 也要能夠方便開發
                    //所以如果是在 bin 資料夾啟動, 不判斷
                }
                else
                {
                    //Application.StartupPath.ToLower = "c:\ap06bz\exe"
                    //已經連線過. 會將 ap06bzPGM 刪除, 
                    //所以要再次連線時, 只要有連線數, 就可以連線
                    if (isConnectedFlag)
                        return true;

                    string ap06bzPGM = "C:\\AP06BZ\\INI\\06BZPGM.INI";
                    string nowProgram = "";
                    bool CanRunPgm = true;
                    if (System.IO.File.Exists(ap06bzPGM))
                    {
                        nowProgram = System.IO.File.ReadAllText(ap06bzPGM);
                        nowProgram = nowProgram.Replace(KTConstant.vbCrLf, "");
                        System.IO.File.Delete(ap06bzPGM);

                        if (string.IsNullOrEmpty(ProcessModuleName_))
                            ProcessModuleName_ = KTEnvs.getAppProgramName();

                        if (ProcessModuleName_.ToUpper() != nowProgram.ToUpper())
                            CanRunPgm = false;

                    }
                    else
                        CanRunPgm = false;

                    if (!CanRunPgm)
                    {
                        throw new ApplicationException("不正確的操作方式" + KTConstant.vbCrLf + KTConstant.vbCrLf + "請由醫院系統登入" + KTConstant.vbCrLf + KTConstant.vbCrLf + "點選 [確定] 結束程式");
                    }

                    //0981106 RunExecute = True 是需要判斷有沒有從醫院系統進入, 
                    //        但是已經連線過. 然後再斷線的. 不需要再判斷了
                    RunExecute = false;
                }

            return true;

        }

        private bool 檢查連線參數環境是否可用()
        {
            //吳居易 0950519 
            //      1020722 測試後修正備註
            //1.使用 OracleClient 須參考 System.Data.OracleClient 元件
            //  且現在的 OracleClient 的語系是 UTF8，不支援 US7Ascii
            //  若是套用到醫院系統 Oracle 8i，醫院系統的中文字體會是亂碼
            //  http://blogs.msdn.com/b/tomleetaiwan/archive/2007/12/19/net-managed-provider-for-oracle-us7ascii.aspx
            //
            //0950520
            //2.9i ODBC
            //  由於 9i 須另外安裝環境..會指定 DefaultHome 的路徑
            //  因此安裝9i 的 Home環境..若是每個人的DefauleHome命名不一樣
            //  該 9i DriverName 也會跟著不一樣，而不曉得哪一個固定的 Odbc Driver 可用
            //  因此目前上無法統一使用 DriverName 
            //
            //Q: 如果出現了 'OraOLEDB.Oracle' 提供者並未登錄於本機電腦上
            //A: regsvr32.exe C:\ORANT9i\bin\OraOLEDB.dll -> **** 註冊他
            //   參考: http://technet.microsoft.com/zh-tw/library/ms152516.aspx
            //
            //3.DSN (Data Source Name)
            //  不同的 Driver Name 只連線主機名稱設定正確
            //  DSN 就會透過檔案內的連線參數設定來連線
            //  只要使用 DSN 方式連線，就不需要設定一大堆 ODBC Driver 參數
            //  直接以主機(檔案)所需的連線版本來連線

            switch (DBConfig)
            {
                case DBProviderType.Oracle:
#if OracleODAC
#elif OracleClient
#else
					//部分SQL語法使用 DBLink 時會發生錯誤。
					//已經有部分程式使用  OracleClinet
					//但是 dll 或 EXE 沒開啟 OracleClient = True 時
					//就強迫使用 ADOProviderType.Oledb
					ADOConfig = ADOProviderType.Oledb;
#endif
                    break;

                case DBProviderType.MySql:
                    if (ADOConfig == ADOProviderType.MySqlClient)
                    {
#if MySqlClient
#else
                        string ConfigStr = "";
                        ConfigStr = ConfigStr + "使用 MySqlClient" + KTConstant.vbCrLf;
                        ConfigStr = ConfigStr + "必須在 [專案] --> [屬性] --> [參考] 加入 MySql.Data.MySqlClient" + KTConstant.vbCrLf;
                        ConfigStr = ConfigStr + "並修改專案屬性 建置 --> " + KTConstant.vbCrLf;
                        ConfigStr = ConfigStr + "1.組態: [DeBug]、[Release]、[所有組態] 可以選擇 [所有組態]。" + KTConstant.vbCrLf;
                        ConfigStr = ConfigStr + "2.條件式編譯的符號 --> 增加常數 MySqlClient" + KTConstant.vbCrLf;
                        //ConfigStr = ConfigStr + "再安裝 MySql Connectors，(1090120條清測試時，增加這個是錯誤的)";
                        throw new ApplicationException(ConfigStr);

#endif
                    }
                    break;

                case DBProviderType.Access:
                case DBProviderType.Access_accdb:
                case DBProviderType.Excel:
                case DBProviderType.Excel_xlsx:
                    ADOConfig = ADOProviderType.Oledb;
                    break;
            }

            switch (ADOConfig)
            {
                case ADOProviderType.Odbc:
                    switch (DBConfig)
                    {
                        case DBProviderType.MySql:
                            string ConfigStr = "";
                            ConfigStr = ConfigStr + "請選擇 MySql 資料庫的連線模式" + KTConstant.vbCrLf;
                            ConfigStr = ConfigStr + "ADOConfig = MySqlClient、MysqlOdbc52、MysqlOdbc35";
                            throw new ApplicationException(ConfigStr);

                        case DBProviderType.Postgre:
                            break;

                        default:
                            ADOConfig = ADOProviderType.Oledb; //Oracle 選擇 Odbc 自動改為 OleDB 
                            break;
                    }
                    break;
            }

            return true;

        }

        public bool CompactAccessDB(string MDBPath)
        {
            //功能：壓縮與修復 Access (MDB)
            //作者：吳居易
            //日期：094/09/08
            //說明：每當 Access 新增刪除之後都會增加 MDB 的檔案大小
            //      或是 MDB 檔已經損毀無法再開啟時
            //      必須使用 Access 的 工具 --> 資料庫公用程式 --> 壓縮及修復資料庫
            //參數：MDBPath --> 將要被壓縮的 MDB 路徑
            //傳回：Commands 字串
            //版本：只適用於 VB.NET 2003、2005、2008，2010 以上版本尚未測試
            //範例：CompactAccessDB(MDBPath)

            ADOProviderType tmpADOConfig;
            DBProviderType tmpDBConfig;
            //string ConnectString;

            if (!Cns.Contains(MDBPath))
            {
                throw new ApplicationException("請先連線 Address 資料庫：" + Environment.NewLine + Environment.NewLine + MDBPath);
            }
            else
            {
                tmpADOConfig = Cns[MDBPath].ADOConfig;
                tmpDBConfig = Cns[MDBPath].DBConfig;
                //ConnectString = Cns[MDBPath].Connection.ConnectionString;
                //ConnectString = ConnectString.Replace("Persist Security Info=False;", "");
                //ConnectString = KTString.Split(ConnectString, "Data Source=")[0] + "Data Source=";
                Close(MDBPath);
            }

            string TempMDB = "c:\\\\tempdb.mdb";
            if (System.IO.File.Exists(TempMDB)) System.IO.File.Delete(TempMDB);

            object objJRO = null;
            //object[] oParams = null;
            //switch (tmpDBConfig)
            //{
            //    case DBProviderType.Access:
            //        string ProviderString = ConnectString; //"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=";
            //        string SourceString = ProviderString + MDBPath;
            //        string TmpAccessMDB = ProviderString + TempMDB + ";Jet OLEDB:Engine Type=5";

            //        oParams = new object[] {
            //            SourceString,
            //            TmpAccessMDB
            //        };

            //        System.Reflection.Binder binder = null;
            //        objJRO = Activator.CreateInstance(Type.GetTypeFromProgID("JRO.JetEngine"));
            //        objJRO.GetType().InvokeMember("CompactDatabase", System.Reflection.BindingFlags.InvokeMethod, binder, objJRO, oParams);
            //        break;

            //    case DBProviderType.Access_accdb:
            //        break;
            //}          

            try
            {
                object[] oParams = new object[] { MDBPath, TempMDB, false };
                objJRO = Activator.CreateInstance(Type.GetTypeFromProgID("Access.Application"));
                objJRO.GetType().InvokeMember("CompactRepair", System.Reflection.BindingFlags.InvokeMethod, null, objJRO, oParams);

            }
            catch (Exception)
            {
                ADOConfig = tmpADOConfig;
                DBConfig = tmpDBConfig;
                Connect(MDBPath);
                //throw new ApplicationException("Access 資料庫壓縮與修復過程失敗" + Environment.NewLine + Environment.NewLine + "請檢查是否正在開啟 " + MDBPath);
                return false;
            }

            System.IO.File.Delete(MDBPath);
            System.IO.File.Move(TempMDB, MDBPath);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(objJRO);
            objJRO = Convert.DBNull;

            ADOConfig = tmpADOConfig;
            DBConfig = tmpDBConfig;
            Connect(MDBPath);

            return true;
        }

        public void BeginTransaction(string TransHost = "")
        {
            //功能：啟動指定的交易連線
            //參數：TransHost 交易連線(KTGH00, HPK210, TSHIS)

            TransHost = 主機代碼(TransHost);

            if (!Cns.Contains(TransHost))
                throw new ApplicationException(TransHost + "連線不存在，無法啟動交易");

            try
            {
                Cns[TransHost].Transaction = Cns[TransHost].Connection.BeginTransaction();
                Cns[TransHost].SetTransaction = true;
                Cns[TransHost].LastDoSqlTime = DateTime.Now;

            }
            catch (Exception ex)
            {
                KTEnvs.Error_Log("BeginTransaction:" + ex.Source + " " + ex.Message);
                ErrorCode = GetOracleErrorCode(ex.Message);
                ConnectOk = false;
                throw new ApplicationException(TransHost + " BeginTransaction 錯誤!!" + Environment.NewLine + Environment.NewLine + ex.Message);
            }
        }

        public IDbTransaction rtnBeginTransaction(string TransHost = "")
        {
            //功能：啟動指定的交易連線
            //參數：TransHost 交易連線(KTGH00, HPK210, TSHIS)

            TransHost = 主機代碼(TransHost);

            if (!Cns.Contains(TransHost))
                throw new ApplicationException(TransHost + "連線不存在，無法啟動交易");

            try
            {
                return Cns[TransHost].Transaction;
            }
            catch (Exception ex)
            {
                KTEnvs.Error_Log("BeginTransaction:" + ex.Source + " " + ex.Message);
                ErrorCode = GetOracleErrorCode(ex.Message);
                ConnectOk = false;
                throw new ApplicationException(TransHost + " BeginTransaction 錯誤!!" + Environment.NewLine + Environment.NewLine + ex.Message);
            }
        }

        public bool SetTransactionFlag(string TransHost = "")
        {
            //功能：確認交易
            //參數：sHostName 交易連線 (CNHOME、KTGH00、HPK210、...)

            if (TransHost == "") TransHost = "CNHOME";
            if (TransHost != "") TransHost = TransHost.ToUpper();

            if (!Cns.Contains(TransHost))
                throw new ApplicationException(TransHost + " 設定交易，連線不存在，無法使用交易");

            try
            {
                return Cns[TransHost].SetTransaction;
            }
            catch (Exception ex)
            {
                KTEnvs.Error_Log("SetTransactionFlag:" + ex.Source + " " + ex.Message);
                ErrorCode = GetOracleErrorCode(ex.Message);
                ConnectOk = false;
                throw new ApplicationException(TransHost + " SetTransactionFlag 錯誤!!" + Environment.NewLine + Environment.NewLine + ex.Message);
            }
        }

        public bool Commit(string TransHost = "")
        {
            //功能：確認交易
            //參數：TransHost 交易連線 (CNHOME、KTGH00、HPK210、TSHIS)

            TransHost = 主機代碼(TransHost);

            if (!Cns.Contains(TransHost))
                throw new ApplicationException(TransHost + "連線不存在，無法確認交易");

            try
            {
                Cns[TransHost].Transaction.Commit();
                Cns[TransHost].SetTransaction = false;
                Cns[TransHost].LastDoSqlTime = DateTime.Now;

                if (Cns[TransHost].DBConfig == DBProviderType.Oracle)
                    this.CloseDBLink(TransHost);

            }
            catch (System.Exception ex)
            {
                KTEnvs.Error_Log("Commit:" + ex.Source + " " + ex.Message);
                ErrorCode = GetOracleErrorCode(ex.Message);
                ConnectOk = false;
                throw new ApplicationException(TransHost + " Commit 錯誤!!" + Environment.NewLine + Environment.NewLine + ex.Message);
            }

            return true;
        }

        public void Rollback(string TransHost = "")
        {
            //功能：確認交易
            //參數：TransHost 交易連線 (CNHOME、KTGH00、HPK210、TSHIS)

            TransHost = 主機代碼(TransHost);

            if (!Cns.Contains(TransHost))
                throw new ApplicationException(TransHost + "連線不存在，無法回復交易");

            try
            {
                Cns[TransHost].Transaction.Rollback();
                Cns[TransHost].SetTransaction = false;
                Cns[TransHost].LastDoSqlTime = DateTime.Now;

            }
            catch (Exception ex)
            {
                KTEnvs.Error_Log("Rollback:" + ex.Source + " " + ex.Message);
                ErrorCode = GetOracleErrorCode(ex.Message);
                ConnectOk = false;
                throw new ApplicationException(TransHost + " Rollback 錯誤!!" + Environment.NewLine + Environment.NewLine + ex.Message);
            }
        }

        //public void 監測SQL作業(string TransHost)
        //{
        //    DataTable tbl = new DataTable();
        //    string SqlStr;
        //    DosqlGrpIp = "";
        //    SqlStr = " SELECT CODE FROM PUB002 ";
        //    SqlStr = SqlStr + " WHERE DTATYP='HL' ";
        //    DoSql(SqlStr, tbl, TransHost);
        //    for (int i = 0; i <= tbl.Rows.Count - 1; i++)
        //    {
        //        DosqlGrpIp = DosqlGrpIp + tbl.Rows[i]["CODE"] + ",";
        //    }
        //    MachineIp = KTEnvs.Get_IPAddress();
        //}

        public void DoSql(string SqlStr, out IDataReader KtReader, string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：執行SQL，不使用指定的Command物件
            //參數：Sqlstr    -> SQL
            //      KtReader  -> 儲存查詢結果的 DataReader(非Select 語法不用傳）
            //      TransHost -> 執行 SQL 的交易連線 (CNHOME、KTGH00、HPK210、TSHIS)
            //                   預設為 Oracle.ini 檔內設定的主機 CNHOME
            //備註：ODBC與OLEDB 的 DataReader 不要用的時候必須執行 DataReader.Close()
            //      其他的連線方式 OracleClient、OPD.NET(Oracle.ManagedDataAccess.Client) 不需要 Close()
            //                     SqlClient 尚未測試

            Rowindicator = 0;
            ErrorCode = "";
            DoSqlOK = false;

            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB, SqlStr);

            KtReader = default(IDataReader);

            try
            {
                if (!IsSelectingData(SqlStr))
                {
                    ErrorMessage_.Length = 0;
                    ErrorMessage_.AppendLine("非資料查詢(Select 語法)，不須輸入DataReader參數!!");
                    throw new ApplicationException(ErrorMessage_.ToString());
                }

                DataReaderIsClosed(tmpCN);

                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.SqlStr = SqlStr;
                tmpCN.Command.CommandText = SqlStr;
                tmpCN.Command.Connection = tmpCN.Connection;
                if (tmpCN.SetTransaction)
                {
                    tmpCN.Command.Transaction = tmpCN.Transaction;
                }

                KtReader = tmpCN.Command.ExecuteReader();
                tmpCN.DataReader = KtReader;
                tmpCN.SqlStr = SqlStr;

                Rowindicator = KtReader.RecordsAffected;

                DoSqlOK = true;
                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.LastDoSqlTime = DateTime.Now;
                tmpCN.Dispose();

            }
            catch (Exception ex)
            {
                ShowDoSql_MessageBox(ex, SqlStr);
                if ((sKTDB != null))
                {
                    sKTDB.ErrorCode = ErrorCode;
                    sKTDB.ErrorMessage = ErrorMessage;
                }
            }
        }

        private void DataReaderIsClosed(KTDBProvider sKTDB = null, int Sec = 5)
        {
            //檢查是否有 DataReader 沒有關閉，已經關閉 Return
            //超過定義的五秒鐘也是 Return，後續的 Dosql 有錯誤就算了

            try
            {
                if ((sKTDB.DataReader != null))
                {
                    if (sKTDB.DataReader.IsClosed)
                    {
                        return;
                    }
                    else
                    {
                        DateTime tmpTime = DateTime.Now;
                        while (!sKTDB.DataReader.IsClosed)
                        {
                            KTDateTime.Delay(0.1);
                            if (DateTime.Now.Subtract(tmpTime).Seconds > Sec)
                            {
                                KTEnvs.Error_Log(sKTDB.SqlStr + "," + Sec);
                                KTEnvs.Error_Log(sKTDB.SqlStr + "," + Sec);
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                KTEnvs.Error_Log(sKTDB.SqlStr + "," + Sec + "," + ex.Message);
                KTEnvs.Error_Log(sKTDB.SqlStr + "," + Sec + "," + ex.Message);
            }
        }

        public void DoSql(KTDBCommand cmd, out IDataReader KtReader, string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：Command 物件 CommandText 屬性內的sql指令
            //參數：Cmd       -> KTDBCommand 物件類別
            //                   不需指定何種 Command 類型(Odbc、Oledb、SqlClinet)
            //                   自動由 TransHost 所紀錄的連線模式來定義 Cmd
            //
            //      KtReader  -> 儲存查詢結果的 IDataReader 物件類別(非Select 語法不用傳）
            //
            //      TransHost -> 執行 SQL 的交易連線 (CNHOME、KTGH00、HPK210、TSHIS)
            //                   省略時為 Oracle.ini 檔內設定的主機 CNHOME

            Rowindicator = 0;
            ErrorCode = "";
            DoSqlOK = false;

            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB, cmd.CommandText);

            KtReader = default(IDataReader);

            try
            {
                if (!cmd.CreateCommand(tmpCN.DBConfig, tmpCN.ADOConfig))
                    return;

                if (!IsSelectingData(cmd.Command.CommandText))
                {
                    ErrorMessage_.Length = 0;
                    ErrorMessage_.AppendLine("非資料查詢(Select 語法)，不須輸入DataReader參數!!");
                    throw new ApplicationException(ErrorMessage_.ToString());
                }

                DataReaderIsClosed(tmpCN);

                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.SqlStr = cmd.Command.CommandText;
                tmpCN.Command.Connection = tmpCN.Connection;
                if (tmpCN.SetTransaction)
                {
                    cmd.Command.Transaction = tmpCN.Transaction;
                }

                KtReader = cmd.Command.ExecuteReader();
                Rowindicator = KtReader.RecordsAffected;

                DoSqlOK = true;
                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.LastDoSqlTime = DateTime.Now;
                tmpCN.Dispose();

            }
            catch (Exception ex)
            {
                ShowDoSql_MessageBox(ex, cmd.Command.CommandText);
                if ((sKTDB != null))
                {
                    sKTDB.ErrorCode = ErrorCode;
                    sKTDB.ErrorMessage = ErrorMessage;
                }
            }
            finally
            {
                //居易 1060517
                //DoSql 後預設會清除  Parameters 所有的欄位資料
                //當初剛開始撰寫此 KTCC 應該是改錯了。
                if (DoSqlDisposeCommand)
                    cmd.DisposeCommand();
                //需釋放先前 Add 的所有欄位名稱
            }

        }

        public void DoSql(KTDBCommand cmd, System.Data.DataTable DataTable = null, string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：Command 物件 CommandText 屬性內的sql指令
            //參數：Cmd      -> KTDBCommand 物件類別 
            //                  不需指定何種 Command 類型(Odbc、Oledb、SqlClinet)
            //                  自動由 TransHost 所紀錄的連線模式來定義 Cmd
            //
            //      tbl      -> 儲存查詢結果的 DataTable (非Select 語法不用傳）
            //                  先紀錄 tbl 所帶入的 Dim TableName = tbl.TableName 名稱
            //                  須由 DataAdapter Fill 至 DataSet，與 Rowindicator(資料筆數)
            //                  再由 DataSet 丟給 tbl，並且回存 tbl.TableName = TableName
            //
            //     TransHost -> 執行 SQL 的交易連線 (CNHOME、KTGH00、HPK210、TSHIS)
            //                  省略時為 Oracle.ini 檔內設定的主機

            Rowindicator = 0;
            ErrorCode = "";
            DoSqlOK = false;

            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB, cmd.CommandText);

            try
            {
                if (!cmd.CreateCommand(tmpCN.DBConfig, tmpCN.ADOConfig))
                    return;

                bool IsSelectFlag =  IsSelectingData(cmd.Command.CommandText);

                if (IsSelectFlag)
                {
                    if (DataTable == null)
                    {
                        ErrorMessage_.Length = 0;
                        ErrorMessage_.AppendLine("資料查詢(Select 語法)，須輸入DataTable參數或是DataTable的執行體尚未產生!!");
                        throw new ApplicationException(ErrorMessage_.ToString());
                    }
                }
                else
                {
                    if (DataTable != null)
                    {
                        ErrorMessage_.Length = 0;
                        ErrorMessage_.AppendLine("非資料查詢(Select 語法)，不須輸入DataTable參數!!");
                        throw new ApplicationException(ErrorMessage_.ToString());
                    }
                }

                DataReaderIsClosed(tmpCN);

                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.SqlStr = cmd.Command.CommandText;
                cmd.Command.Connection = tmpCN.Connection;
                if (tmpCN.SetTransaction)
                    cmd.Command.Transaction = tmpCN.Transaction;

                if (IsSelectFlag)
                {
                    tmpCN.DataAdapter.SelectCommand = cmd.Command;
                    string tablename = DataTable.TableName;
                    ConnDataAdapterToDataTable(tmpCN, DataTable);
                    if (!string.IsNullOrEmpty(tablename))
                        DataTable.TableName = tablename;

                }
                else
                {
                    //ExecuteNonQuery 會回傳影響到的資料數量
                    Rowindicator = cmd.Command.ExecuteNonQuery();

                    //#######################################################
                    //1051208 居易 絕對絕對不能使用 底下這段，好像錯第二次了
                    //ExecuteScalar 不會回傳受到影響的資料數量
                    //Rowindicator = cmd.Command.ExecuteScalar
                    //#######################################################
                }

                DoSqlOK = true;
                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.LastDoSqlTime = DateTime.Now;
                tmpCN.Dispose();

            }
            catch (Exception ex)
            {
                ShowDoSql_MessageBox(ex, cmd.Command.CommandText);
                if ((sKTDB != null))
                {
                    sKTDB.ErrorCode = ErrorCode;
                    sKTDB.ErrorMessage = ErrorMessage;
                }
            }
            finally
            {
                //居易 1060517
                //DoSql 後預設會清除  Parameters 所有的欄位資料
                //當初剛開始撰寫此 KTCC 應該是改錯了。
                if (DoSqlDisposeCommand)
                    cmd.DisposeCommand();
                //需釋放先前 Add 的所有欄位名稱
            }

        }

#if !noDapper

        public IEnumerable<T> DoSql<T>(KTDBCommand cmd, string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：Command 物件 CommandText 屬性內的sql指令
            //參數：Cmd       -> KTDBCommand 物件類別 
            //                   不需指定何種 Command 類型(Odbc、Oledb、SqlClinet)
            //                   自動由 TransHost 所紀錄的連線模式來定義 Cmd
            //      TransHost -> 執行 SQL 的交易連線 (CNHOME、KTGH00、HPK210、TSHIS)
            //                   省略時為 Oracle.ini 檔內設定的主機

            Rowindicator = 0;
            ErrorCode = "";
            DoSqlOK = false;

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB, cmd.CommandText);

            List<T> dy = new List<T>();

            try
            {
                if (!cmd.CreateCommand(tmpCN.DBConfig, tmpCN.ADOConfig))
                    return new List<T>().ToList();

                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.SqlStr = cmd.Command.CommandText;

                Dapper.DynamicParameters param = null;
                System.Data.IDataParameterCollection iParams = cmd.Command.Parameters;

                if (iParams.Count > 0)
                {
                    param = new Dapper.DynamicParameters();

                    for (int x = 0; x <= iParams.Count - 1; x++)
                    {
                        switch (tmpCN.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                            case ADOProviderType.MysqlOdbc35:
                            case ADOProviderType.MysqlOdbc52:
                                System.Data.Odbc.OdbcParameter iParamOdbc = (System.Data.Odbc.OdbcParameter)iParams[x];
                                param.Add(iParamOdbc.ParameterName, iParamOdbc.Value, iParamOdbc.DbType, iParamOdbc.Direction, iParamOdbc.Size);
                                break;

                            case ADOProviderType.Oledb:
                                System.Data.OleDb.OleDbParameter iParamOleDb = (System.Data.OleDb.OleDbParameter)iParams[x];
                                param.Add(iParamOleDb.ParameterName, iParamOleDb.Value, iParamOleDb.DbType, iParamOleDb.Direction, iParamOleDb.Size);
                                break;

                            case ADOProviderType.SQLClient:
                                System.Data.SqlClient.SqlParameter iParamSQLClient = (System.Data.SqlClient.SqlParameter)iParams[x];
                                param.Add(iParamSQLClient.ParameterName, iParamSQLClient.Value, iParamSQLClient.DbType, iParamSQLClient.Direction, iParamSQLClient.Size);
                                break;

#if OracleODAC
                            case ADOProviderType.OracleClient:
                                Oracle.ManagedDataAccess.Client.OracleParameter iParamOracleClient = (Oracle.ManagedDataAccess.Client.OracleParameter)iParams[x];
                                param.Add(iParamOracleClient.ParameterName, iParamOracleClient.Value, iParamOracleClient.DbType, iParamOracleClient.Direction, iParamOracleClient.Size);
                                break;
#elif OracleClient
                            case ADOProviderType.OracleClient:
                                System.Data.OracleClient.OracleParameter iParamOracleClient = (System.Data.OracleClient.OracleParameter)iParams[x];
                                param.Add(iParamOracleClient.ParameterName, iParamOracleClient.Value, iParamOracleClient.DbType, iParamOracleClient.Direction, iParamOracleClient.Size);
                                break;
#endif

#if MySqlClient
                            case ADOProviderType.MySqlClient:
                                MySql.Data.MySqlClient.MySqlParameter iParamMySqlClient = (MySql.Data.MySqlClient.MySqlParameter)iParams[x];
                                param.Add(iParamMySqlClient.ParameterName, iParamMySqlClient.Value, iParamMySqlClient.DbType, iParamMySqlClient.Direction, iParamMySqlClient.Size);
                                break;
#endif
                        }
                    }
                }

                //dy = (List<T>)cmd.Command.Connection.Query<T>(tmpCN.SqlStr, param, tmpCN.Transaction);
                dy = (List<T>)tmpCN.Connection.Query<T>(tmpCN.SqlStr, param, tmpCN.Transaction, commandType: cmd.CommandType);

                Rowindicator = dy.Count;

                DoSqlOK = true;
                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.LastDoSqlTime = DateTime.Now;
                tmpCN.Dispose();

            }
            catch (Exception ex)
            {
                ShowDoSql_MessageBox(ex, cmd.Command.CommandText);
                if ((sKTDB != null))
                {
                    sKTDB.ErrorCode = ErrorCode;
                    sKTDB.ErrorMessage = ErrorMessage;
                }
            }
            finally
            {
                //居易 1060517
                //DoSql 後預設會清除  Parameters 所有的欄位資料
                //當初剛開始撰寫此 KTCC 應該是改錯了。
                if (DoSqlDisposeCommand)
                    cmd.DisposeCommand();
                //需釋放先前 Add 的所有欄位名稱
            }

            return (List<T>)dy.ToList();
        }

        public IEnumerable<T> DoSql<T>(string SqlStr, object param = null, string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：執行SQL，不使用指定的Command物件
            //參數：Sqlstr    -> SQL
            //      param     -> Dapper.DynamicParameters 參數型別 或 new{}匿名型別。
            //      TransHost -> 執行 SQL 的交易連線 (CNHOME、KTGH00、HPK210、TSHIS...)
            //                   省略時為 Oracle.ini 檔內設定的主機 CNHOME

            Rowindicator = 0;
            ErrorCode = "";
            DoSqlOK = false;

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB, SqlStr);

            List<T> dy = new List<T>();

            try
            {
                if (tmpCN.DBConfig == DBProviderType.Oracle)
                {
                    if (SqlStr.IndexOf("@") > -1)
                    {
                        //UTF8語系，備註符號 "--" 跟著 符號 ":\" 會讓 
                        //Insert Into PRS003 Select * Form PRS003@HPK210 語法全部 Press 
                        //Application.ExecutablePath.Split("\")(Application.ExecutablePath.Split("\").Length - 1)

                        if (ExeOrWeb == "exe")
                        {
                            if (string.IsNullOrEmpty(ProcessModuleName_))
                                ProcessModuleName_ = KTEnvs.getAppProgramName();
                            SqlStr = SqlStr + " -----EXE_NAME_" + ProcessModuleName_;
                        }
                        else
                        {
                            ProcessModuleName_ = KTEnvs.Get_IPAddress();
                            SqlStr = SqlStr + " -----EXE_IP_" + ProcessModuleName_;
                        }
                    }
                }

                //switch (MachineIp)
                //{
                //    case "192.168.1.110":
                //    case "192.73.146.134":
                //    case "192.168.1.5":
                //        ALLSQLLOG(SqlStr);
                //        break;
                //}

                DataReaderIsClosed(tmpCN);

                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.SqlStr = SqlStr;

                DateTime DosqlSTime = DateTime.Now;

                if (IsSelectingData(SqlStr))
                {
                    //dy = (List<T>)tmpCN.Command.Connection.Query<T>(SqlStr, param, tmpCN.Transaction);
                    dy = (List<T>)tmpCN.Connection.Query<T>(SqlStr, param, tmpCN.Transaction);
                    Rowindicator = dy.Count;
                }
                else
                {
                    //Rowindicator = tmpCN.Command.Connection.Execute(SqlStr, param, tmpCN.Transaction);
                    Rowindicator = tmpCN.Connection.Execute(SqlStr, param, tmpCN.Transaction);
                }

                AppendDosqlLog記錄(SqlStr, DosqlSTime);

                DoSqlOK = true;
                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.LastDoSqlTime = DateTime.Now;
                tmpCN.Dispose();

            }
            catch (Exception ex)
            {
                ShowDoSql_MessageBox(ex, SqlStr);
                if ((sKTDB != null))
                {
                    sKTDB.ErrorCode = ErrorCode;
                    sKTDB.ErrorMessage = ErrorMessage;
                }
            }

            return (List<T>)dy.ToList();
        }
#endif

        public void AppendDosqlLog記錄(string SqlStr, System.DateTime STime)
        {
            if (ExeOrWeb == "web")
                return;

            double DosqlTime = DateTime.Now.Subtract(STime).TotalSeconds;

            if (DosqlTime < 1) return;

            //switch (MachineIp)
            //{
            //    case "10.20.1.23":
            //    case "10.20.1.24":
            //    case "10.20.1.60":
            //        if (DosqlTime < 0.2)
            //            return;
            //        break;

            //    default:
            //        if (DosqlTime < 1) return;
            //        break;
            //}

            string CommadStr = "";
            string PrgName = "";
            bool SpcFlg = false;

            if (DosqlTime > 6)
            {
                SpcFlg = false;
                PrgName = (System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

                CommadStr = PrgName + "!@#$%" + "SQL" + "!@#$%" + DosqlTime;
                CommadStr = CommadStr + "!@#$%" + SqlStr + "!@#$%";
                System.Diagnostics.Process p = new System.Diagnostics.Process();
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.FileName = "C:\\ap06bz\\exe\\prg2100.exe";
                p.StartInfo.Arguments = CommadStr;
                p.Start();

            }
            else if (DosqlTime > 3)
            {
                SpcFlg = false;
                PrgName = (System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

                PrgName = PrgName.ToUpper().Split(Convert.ToChar('.'))[0];
                switch (PrgName)
                {
                    case "PRG1100N":
                    case "ORDP1000":
                    case "NURP5000":
                        SpcFlg = true;
                        break;
                }
                if (SpcFlg)
                {
                    CommadStr = PrgName + "!@#$%" + "SQL" + "!@#$%" + DosqlTime;
                    CommadStr = CommadStr + "!@#$%" + SqlStr + "!@#$%";
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.FileName = "C:\\ap06bz\\exe\\prg2100.exe";
                    p.StartInfo.Arguments = CommadStr;
                    p.Start();
                }
            }

            //if (string.IsNullOrEmpty(MachineIp))
            //    return;
            //if (string.IsNullOrEmpty(DosqlGrpIp))
            //    return;
            //if (DosqlGrpIp.IndexOf(MachineIp) == -1)
            //    return;

            //string strFile = "C:\\ap06bz\\log\\Dosql" + "X" + DateTime.Today.ToString("dd-MMM-yyyy") + ".log";
            //System.IO.StreamWriter sw = default(System.IO.StreamWriter);
            //System.IO.FileStream fs = null;
            //if ((!System.IO.File.Exists(strFile)))
            //{
            //    try
            //    {
            //        fs = System.IO.File.Create(strFile);
            //        fs.Close();
            //        sw = System.IO.File.AppendText(strFile);
            //        sw.WriteLine(DateTime.Now + KTString.Space(2) + DosqlTime.ToString().Substring(0, 5).PadLeft(6, ' ') + KTString.Space(2) + SqlStr);
            //        sw.Close();
            //    }
            //    catch (Exception)
            //    {
            //        //MsgBox("Error Creating Log File")
            //    }
            //}
            //else
            //{
            //    try
            //    {
            //        sw = System.IO.File.AppendText(strFile);
            //        sw.WriteLine(DateTime.Now + KTString.Space(2) + DosqlTime.ToString().Substring(0, 5).PadLeft(6, ' ') + KTString.Space(2) + SqlStr);
            //        sw.Close();
            //        fs.Close();
            //    }
            //    catch (Exception)
            //    {
            //        //MsgBox("Error APPEND Log File")
            //    }
            //}
        }

        //private void ALLSQLLOG(string sqlstr)
        //{
        //    try
        //    {
        //        if (System.IO.Directory.Exists("C:\\AP06BZ\\LOG\\"))
        //            System.IO.Directory.CreateDirectory("C:\\AP06BZ\\LOG\\");
        //        if (System.IO.File.Exists("C:\\AP06BZ\\LOG\\ALLSQL.TXT") == false)
        //        {
        //            System.IO.StreamWriter sw = System.IO.File.CreateText("C:\\AP06BZ\\LOG\\ALLSQL.TXT");
        //            sw.Close();
        //        }
        //        System.IO.FileStream Target_File = new System.IO.FileStream("C:\\AP06BZ\\LOG\\ALLSQL.TXT", System.IO.FileMode.Append);
        //        System.IO.StreamWriter Target_Writer_File = new System.IO.StreamWriter(Target_File, System.Text.Encoding.Default);
        //        string Line_Text = "";

        //        try
        //        {
        //            Line_Text = DateTime.Now + KTString.Space(2) + KTString.Space(2) + sqlstr;
        //            Target_Writer_File.WriteLine(Line_Text);
        //            Target_Writer_File.Close();
        //        }
        //        catch (Exception)
        //        {
        //            try
        //            {
        //                Target_Writer_File.Close();
        //            }
        //            catch (Exception)
        //            {
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}

        private bool sqlError_ReConnect(string TransHost, string ErrCode)
        {
            //1060714 目前此 Function 沒有使用
            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = Cns[TransHost];

            switch (tmpCN.DBConfig)
            {
                case DBProviderType.Oracle:
                    switch (ErrCode)
                    {
                        case "ORA-12571":
                        case "ORA-12560":
                        case "ORA-03113":
                        case "ORA-03114":
                            //ODBC         ORA-12560, ORA-12571
                            //OLEDB        ORA-03114
                            //OracleClient ORA-03114
                            Close(tmpCN.CnKey);
                            try
                            {
                                Connect(tmpCN.HostName, UseCnKey: tmpCN.CnKey);
                                return true;
                            }
                            catch (Exception)
                            {
                                return false;
                            }
                    }

                    break;
            }

            return false;
        }

        //OverLoading(重載)
        //返回值相同，參數屬性不同
        //返回值相同，參數個數不同
        //參數不同，返回值不同(這裡就是參數不同)
        //泛型出現後，相同參數，相同返回值的方法也能構成成重載。

        //這裡無法使用 型別 DataTable 的原因，是因為與另一個 DoSql<T> 的強行別有衝突
        //因此知道雖然是 DataTable 但仍然必須共同使用 object 才能符合多載，
        //才能讓 DoSql 被多種情形使用。
        public void DoSql(string SqlStr, object DataTable = null, string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：執行SQL，不使用指定的Command物件
            //參數：Sqlstr    -> SQL
            //      DataTable -> Dapper.DynamicParameters 參數型別 或 new{}匿名型別。
            //      TransHost -> 執行 SQL 的交易連線 (CNHOME、KTGH00、HPK210、TSHIS)
            //                   省略時為 Oracle.ini 檔內設定的主機 CNHOME

            Rowindicator = 0;
            ErrorCode = "";
            DoSqlOK = false;

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB, SqlStr);

            try
            {
                if (tmpCN.DBConfig == DBProviderType.Oracle)
                {
                    if (SqlStr.IndexOf("@") > -1)
                    {
                        //UTF8語系，備註符號 "--" 跟著 符號 ":\" 會讓 
                        //Insert Into PRS003 Select * Form PRS003@HPK210 語法全部 Press 
                        //Application.ExecutablePath.Split("\")(Application.ExecutablePath.Split("\").Length - 1)

                        if (ExeOrWeb == "exe")
                        {
                            if (string.IsNullOrEmpty(ProcessModuleName_))
                                ProcessModuleName_ = KTEnvs.getAppProgramName();
                            SqlStr = SqlStr + " -----EXE_NAME_" + ProcessModuleName_;
                        }
                        else
                        {
                            ProcessModuleName_ = KTEnvs.Get_IPAddress();
                            SqlStr = SqlStr + " -----EXE_IP_" + ProcessModuleName_;
                        }
                    }
                }

                //switch (MachineIp)
                //{
                //    case "192.168.1.110":
                //    case "192.73.146.134":
                //    case "192.168.1.5":
                //        ALLSQLLOG(SqlStr);
                //        break;
                //}

                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.SqlStr = SqlStr;

                bool IsSelectFlag = IsSelectingData(SqlStr);

                if (IsSelectFlag)
                {
                    if ((DataTable == null))
                    {
                        ErrorMessage_.Length = 0;
                        ErrorMessage_.AppendLine("資料查詢(Select 語法)，須輸入DataTable參數或是DataTable的執行體尚未產生!!");
                        throw new ApplicationException(ErrorMessage_.ToString());
                    }
                }
                else
                {
                    if ((DataTable != null) && (DataTable.GetType().FullName == "System.Data.DataTable"))
                    {
                        ErrorMessage_.Length = 0;
                        ErrorMessage_.AppendLine("非資料查詢(Select 語法)，不須輸入DataTable參數!!");
                        throw new ApplicationException(ErrorMessage_.ToString());
                    }
                }

                DateTime DosqlSTime = DateTime.Now;

                tmpCN.Command.CommandText = SqlStr;
                tmpCN.Command.Connection = tmpCN.Connection;

                if (tmpCN.SetTransaction)
                    tmpCN.Command.Transaction = tmpCN.Transaction;

                if (IsSelectFlag)
                {
                    tmpCN.DataAdapter.SelectCommand = tmpCN.Command;

                    string tablename = ((DataTable)DataTable).TableName;
                    ConnDataAdapterToDataTable(tmpCN, ((DataTable)DataTable));
                    if (!string.IsNullOrEmpty(tablename))
                        ((DataTable)DataTable).TableName = tablename;
                }
                else
                {
#if !noDapper
                    Rowindicator = tmpCN.Connection.Execute(SqlStr, DataTable, tmpCN.Transaction);
#else
                    Rowindicator = tmpCN.Command.ExecuteNonQuery();
#endif
                }

                AppendDosqlLog記錄(SqlStr, DosqlSTime);

                DoSqlOK = true;
                tmpCN.DoSqlOK = DoSqlOK;
                tmpCN.LastDoSqlTime = DateTime.Now;
                tmpCN.Dispose();
            }
            catch (Exception ex)
            {
                ShowDoSql_MessageBox(ex, SqlStr);
                if ((sKTDB != null))
                {
                    sKTDB.ErrorCode = ErrorCode;
                    sKTDB.ErrorMessage = ErrorMessage;
                }
            }
        }

        private void ConnDataAdapterToDataTable(KTDBProvider iConn, DataTable tbl)
        {
            tbl.Clear();    //清掉DataTable 否則資料會累積

            switch (iConn.ADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:
                    System.Data.Odbc.OdbcDataAdapter adrOdbc = (System.Data.Odbc.OdbcDataAdapter)iConn.DataAdapter;
                    Rowindicator = adrOdbc.Fill(tbl);
                    adrOdbc.Dispose();
                    break;

                case ADOProviderType.Oledb:
                    System.Data.OleDb.OleDbDataAdapter adrOleDb = (System.Data.OleDb.OleDbDataAdapter)iConn.DataAdapter;
                    Rowindicator = adrOleDb.Fill(tbl);
                    adrOleDb.Dispose();
                    break;

                case ADOProviderType.SQLClient:
                    System.Data.SqlClient.SqlDataAdapter adrSqlClient = (System.Data.SqlClient.SqlDataAdapter)iConn.DataAdapter;
                    Rowindicator = adrSqlClient.Fill(tbl);
                    adrSqlClient.Dispose();
                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:
                    //這個方式由 DataReader 將資料全部載入 DataTable，第一次也是很慢 
                    Oracle.ManagedDataAccess.Client.OracleDataAdapter adrOracleClient = (Oracle.ManagedDataAccess.Client.OracleDataAdapter)iConn.DataAdapter;
                    Rowindicator = adrOracleClient.Fill(tbl);
                    adrOracleClient.Dispose();
                    break;
#elif OracleClient
                case ADOProviderType.OracleClient:
                    System.Data.OracleClient.OracleDataAdapter adrOracleClient = (System.Data.OracleClient.OracleDataAdapter)iConn.DataAdapter;
                    Rowindicator = adrOracleClient.Fill(tbl);
                    adrOracleClient.Dispose();
                    break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:
                    MySql.Data.MySqlClient.MySqlDataAdapter adrMySqlClient = (MySql.Data.MySqlClient.MySqlDataAdapter)iConn.DataAdapter;
                    Rowindicator = adrMySqlClient.Fill(tbl);
                    adrMySqlClient.Dispose();
                    break;
#endif
            }
        }

        public IEnumerable<T> DataReaderToList<T>(IDataReader iReader) where T : class, new()
        {

            List<T> dy = new List<T>();
            System.Reflection.PropertyInfo[] prop;

            T gType = default(T);
            gType = Activator.CreateInstance<T>();

            prop = gType.GetType().GetProperties();

            System.Collections.Generic.List<String> ColumnName = new System.Collections.Generic.List<String>();
            System.Collections.Generic.List<String> ColumnType = new System.Collections.Generic.List<String>();
            //System.Collections.Generic.List<bool> ColumnNull = new System.Collections.Generic.List<bool>();
            System.Collections.Generic.List<Int32> Columnidx = new System.Collections.Generic.List<Int32>();

            System.Collections.Generic.List<Int32> iReaderColumnidx = new System.Collections.Generic.List<Int32>();

            for (int i = 0; i <= prop.Length - 1; i++)
            {
                ColumnName.Add(prop[i].Name);
                try
                {
                    Int32 idx = iReader.GetOrdinal(ColumnName[ColumnName.Count - 1]);

                    Columnidx.Add(i);

                    iReaderColumnidx.Add(idx);

                    ColumnType.Add(prop[i].PropertyType.FullName);
                    if (ColumnType[ColumnType.Count - 1].ToString().IndexOf("System.Nullable") > -1)
                    {
                        ColumnType[ColumnType.Count - 1] = ColumnType[ColumnType.Count - 1].Split(new string[] { "[[" }, StringSplitOptions.None)[1].Split(',')[0];
                        //ColumnNull.Add(true);
                    }
                    else
                    {
                        //ColumnNull.Add(false);
                    }
                }
                catch (Exception)
                {
                    ColumnName.RemoveAt(ColumnName.Count - 1);
                }
            }

            object gData;
            while (iReader.Read())
            {
                gType = Activator.CreateInstance<T>();

                prop = gType.GetType().GetProperties();

                for (int i = 0; i <= ColumnName.Count - 1; i++)
                {
                    gData = iReader.GetValue(iReaderColumnidx[i]);

                    if (!Convert.IsDBNull(gData))
                    {
                        switch (ColumnType[i])
                        {
                            case "System.String":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToString(gData), null);
                                break;

                            case "System.Decimal":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToDecimal(gData), null);
                                break;

                            case "System.Single":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToSingle(gData), null);
                                break;

                            case "System.Double":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToDouble(gData), null);
                                break;

                            case "System.Short":
                            case "System.Int16":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToInt16(gData), null);
                                break;

                            case "System.UInt16":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToUInt16(gData), null);
                                break;

                            case "System.Integer":
                            case "System.Int32":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToInt32(gData), null);
                                break;

                            case "System.UInt":
                            case "System.UInt32":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToUInt32(gData), null);
                                break;

                            case "System.Long":
                            case "System.Int64":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToInt64(gData), null);
                                break;

                            case "System.UInt64":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToUInt64(gData), null);
                                break;

                            case "System.TimeSpan":
                            case "System.DateTime":
                            case "MySql.Data.Types.MySqlDateTime":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToDateTime(Convert.ToDateTime(gData).ToString("yyyy/MM/dd HH:mm:ss")), null);
                                break;

                            case "System.Byte":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToByte(gData), null);
                                break;

                            case "System.SByte":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToSByte(gData), null);
                                break;

                            case "System.Bool":
                            case "System.Boolean":
                                prop[Columnidx[i]].SetValue(gType, Convert.ToBoolean(gData), null);
                                break;

                            case "System.Char":
                                //長度必須剛好一個字元
                                prop[Columnidx[i]].SetValue(gType, Convert.ToChar(gData), null);
                                break;

                            default:
                                //這裡都是無法轉型的
                                //如果是陣列型態也是無法轉型成功 "System.Byte[]": "System.String[]":
                                prop[Columnidx[i]].SetValue(gType, gData, null);
                                break;
                        }
                    }
                    else
                    {
                        switch (ColumnType[i])
                        {
                            case "System.String":
                                prop[Columnidx[i]].SetValue(gType, "", null);
                                break;
                        }
                    }
                }
                dy.Add(gType);
            }
            return dy;
        }

        public IEnumerable<T> DataTableToList<T>(DataTable tbl) where T : class, new()
        {
            //建立一個回傳用的 List<T>
            List<T> T_List = new List<T>();

            //取得映射型別
            Type type = typeof(T);

            //儲存 DataTable 的欄位名稱
            List<System.Reflection.PropertyInfo> pr_List = new List<System.Reflection.PropertyInfo>();

            foreach (System.Reflection.PropertyInfo item in type.GetProperties())
            {
                if (tbl.Columns.IndexOf(item.Name) != -1)
                    pr_List.Add(item);
            }

            //逐筆將 DataTable 的值新增到 List<TResult> 中
            foreach (DataRow row in tbl.Rows)
            {
                T tr = new T();

                foreach (System.Reflection.PropertyInfo pr_Item in pr_List)
                {
                    if (row[pr_Item.Name] != DBNull.Value)
                        if (pr_Item.PropertyType.ToString().IndexOf("Nullable") == -1)
                            pr_Item.SetValue(tr, Convert.ChangeType(row[pr_Item.Name], pr_Item.PropertyType), null);
                        else
                            pr_Item.SetValue(tr, Convert.ChangeType(row[pr_Item.Name], pr_Item.PropertyType.GetGenericArguments()[0]), null);

                }

                T_List.Add(tr);
            }

            return T_List;
        }

        public DataTable ListToDataTable<T>(IEnumerable<T> ListValue, DataTable tbl) where T : class, new()
        {
            //建立一個回傳用的 DataTable
            DataTable dt = new DataTable();

            //取得映射型別
            Type type = typeof(T);

            //宣告一個 PropertyInfo 陣列，來接取 Type 所有的共用屬性
            System.Reflection.PropertyInfo[] PI_List = null;

            foreach (var item in ListValue)
            {
                //判斷 DataTable 是否已經定義欄位名稱與型態
                if (dt.Columns.Count == 0)
                {
                    //取得 Type 所有的共用屬性
                    PI_List = item.GetType().GetProperties();

                    //將 List 中的 名稱 與 型別，定義 DataTable 中的欄位 名稱 與 型別
                    foreach (var item1 in PI_List)
                    {
                        if (item1.PropertyType.ToString().IndexOf("Nullable") == -1)
                            dt.Columns.Add(item1.Name, item1.PropertyType);
                        else
                            dt.Columns.Add(item1.Name, item1.PropertyType.GetGenericArguments()[0]);
                    }
                }

                //在 DataTable 中建立一個新的列
                DataRow dr = dt.NewRow();

                //將資料逐筆新增到 DataTable 中
                foreach (var item2 in PI_List)
                    dr[item2.Name] = (item2.GetValue(item, null) == null ? DBNull.Value : item2.GetValue(item, null));

                dt.Rows.Add(dr);
            }

            dt.AcceptChanges();
            tbl = dt.Copy();
            return dt;
        }

        private KTDBProvider 取得CN連線(string TransHost, KTDBProvider sKTDB, string SqlStr = "")
        {
            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = default(KTDBProvider);
            try
            {
                if ((sKTDB != null))
                {
                    TransHost = sKTDB.HostName;
                    tmpCN = sKTDB;
                }
                else
                {
                    tmpCN = Cns[TransHost];
                }

                if (tmpCN == null)
                {
                    ErrorMessage_.Length = 0;
                    ErrorMessage_.AppendLine("尚未建立 " + TransHost + " 的連線!!");
                    if (SqlStr != "")
                    {
                        ErrorMessage_.AppendLine("執行DoSql時發生錯誤!!SQL語法:");
                        ErrorMessage_.AppendLine(SqlStr);
                        throw new ApplicationException(ErrorMessage_.ToString());
                    }
                }
                return tmpCN;
            }
            finally
            {
                if ((tmpCN != null))
                    tmpCN.Dispose();
            }
        }

        private string 主機代碼(string TransHost = "")
        {
            if (string.IsNullOrEmpty(TransHost))
                TransHost = "CNHOME";
            if (!string.IsNullOrEmpty(TransHost))
                TransHost = TransHost.ToUpper();

            return TransHost;
        }

        public DateTime GetLastDoSqlTime(string TransHost = "")
        {
            return Cns[主機代碼(TransHost)].LastDoSqlTime;
        }

        public void ShowDoSql_MessageBox(System.Exception e, string ErrSql)
        {
            //0960824 居易 
            //雖然 Dosql 不常異動，但是每次異動都要維護四個地方
            //故意挑出此段錯誤訊息

            KTEnvs.Error_Log(e.Source + " " + e.Message + " " + ErrSql);
            ErrorCode = GetOracleErrorCode(e.Message.ToString());

            string ErrMsg = GetOracleErrorMessage(e.Message.ToString());

            //Lock Message
            if (ErrorCode == "ORA-00054")
                return;

            //Update Lock Message
            if (ErrorCode == "ORA-20090")
                return;

            if (ShowDosqlMsg)
            {
                if (!string.IsNullOrEmpty(ErrorCode))
                {
                    //自訂 Oracle P/SQL 資料庫錯誤定義 的錯誤訊息抓不到
                    if (ErrorCode.Substring(0, 7) == "ORA-203")
                    {
                        throw new ApplicationException(ErrMsg);
                    }
                }

                ErrorMessage_.Length = 0;
                ErrorMessage_.AppendLine("執行DoSql時發生錯誤!!");
                ErrorMessage_.AppendLine("錯誤訊息：" + ErrorCode);
                ErrorMessage_.AppendLine(ErrMsg);
                ErrorMessage_.AppendLine("SQL語法：");
                ErrorMessage_.AppendLine(ErrSql);
                throw new ApplicationException(ErrorMessage_.ToString());
            }

            ErrorMessage_.Length = 0;
            ErrorMessage_.Append(ErrMsg);
        }

        public bool AlreadyConnectOK(string TransHost = "")
        {
            //功能：檢查目前的連線 Hosts 是否已經連線
            //參數：Hosts 欲連線的主機

            //回傳：True  已經連線  
            //      False 尚未連線

            TransHost = 主機代碼(TransHost);

            if (Cns.Contains(TransHost))
            {
                if (Cns[TransHost].Connection.State == ConnectionState.Closed)
                {
                    //1050308 居易
                    //連線集合有存在，但是連線中斷
                    //將連線集合關閉，讓外層呼叫程式自己再去連線一次。
                    Close(TransHost);
                    return false;
                }
                return true;
            }
            return false;
        }

        public void Close(string TransHost = "", bool ClearPool = true)
        {
            //功能：關閉連線
            //參數：TransHost 主機名稱(KTGH00, HPK210, TSHIS)
            //      沒傳 TransHost 進來，就連到oracle.ini內設定的主機

            TransHost = 主機代碼(TransHost);

            if (Cns.Contains(TransHost))
            {
                if (!Cns[TransHost].isDllConnect)
                {
                    //0990903 居易
                    //不是 dllConnect 連線，才可以被中斷，否則會砍到主程式的連線
                    if (Cns[TransHost].DBConfig == DBProviderType.Oracle)
                        CloseDBLink(Cns[TransHost].CnKey);

                    switch (Cns[TransHost].ADOConfig)
                    {
                        case ADOProviderType.SQLClient:
                            System.Data.SqlClient.SqlConnection sCN = (System.Data.SqlClient.SqlConnection)Cns[TransHost].Connection;
                            sCN.Close();

                            if (ClearPool)
                                System.Data.SqlClient.SqlConnection.ClearPool(sCN);

                            sCN.Dispose();
                            break;

#if OracleODAC
                        case ADOProviderType.OracleClient:
                            Oracle.ManagedDataAccess.Client.OracleConnection oCN = (Oracle.ManagedDataAccess.Client.OracleConnection)Cns[TransHost].Connection;
                            oCN.Close();

                            //if (ClearPool)
                            //    Oracle.ManagedDataAccess.Client.OracleConnection.ClearPool(oCN);

                            oCN.Dispose();
                            break;
#elif OracleClient
                        case ADOProviderType.OracleClient:
						    System.Data.OracleClient.OracleConnection oCN = (System.Data.OracleClient.OracleConnection) Cns[TransHost].Connection;
						    oCN.Close();

						    if (ClearPool)
							    System.Data.OracleClient.OracleConnection.ClearPool(oCN);

						    oCN.Dispose();
                            break;
#endif

#if MySqlClient
                        case ADOProviderType.MySqlClient:
                            MySql.Data.MySqlClient.MySqlConnection mCN = (MySql.Data.MySqlClient.MySqlConnection)Cns[TransHost].Connection;
                            mCN.Close();

                            if (ClearPool)
                                MySql.Data.MySqlClient.MySqlConnection.ClearPool(mCN);

                            mCN.Dispose();
                            break;
#endif

                        default:
                            Cns[TransHost].Connection.Close();
                            Cns[TransHost].Connection.Dispose();
                            break;
                    }

                    Cns[TransHost].Command.Dispose();
                    Cns[TransHost].Dispose();

                    Cns.Remove(TransHost);

                }
                else
                {
                    //1010223 居易
                    //雖然不是 dllConnect 連線的，但是必須移除 dll 的 Connect 集合，
                    //如果沒有移除Connect的集合，表示此dll的連線與主程式連線仍然還是同一條
                    //會造成主表單程式執行 BeginTransaction 後，再呼叫相同 dll 的其他表單程式，
                    //雖然指定的不是傳入主程式 Connect，但是在執行連線時，判斷上仍是使用上先前傳入的 dllConnect 連線
                    //會導致 交易 程序出問題。

                    //PS: 必須注意此動作只是移除連線的集合，不是 Connection Close
                    //Close 連線時，自動執行 CloseDBLink

                    if (Cns[TransHost].DBConfig == DBProviderType.Oracle)
                        this.CloseDBLink(TransHost);

                    Cns.Remove(TransHost);
                }
            }
        }

        public void CloseAll(bool ClearPool = true)
        {
            DictionaryEntry[] cn = new DictionaryEntry[Cns.Count + 1];
            int idx = 0;

            Cns.CopyTo(cn, 0);
            //最後一個是 Nothing
            for (idx = 0; idx <= cn.Length - 2; idx++)
            {
                Close(cn[idx].Key.ToString(), ClearPool);
            }
        }

        private class DBLink_Models
        {
            public string DB_LINK { get; set; }
            public string HETEROGENEOUS { get; set; }
            public decimal OPEN_CURSORS { get; set; }
            public string IN_TRANSACTION { get; set; }
            public string UPDATE_SENT { get; set; }
        }
        public void CloseDBLink(string TransHost = "")
        {
            //0990716 居易
            //功能：中斷目前交易連線所執行的 Remote DataBase Link 連線
            //參數：TransactionHost  要執行中斷 DBlink 的交易連線
            //參考網址：
            //http://www.dbmaker.com/reference/manuals/sql/SQLCLOSE_DATABASE_LINK.htm
            //http://psoug.org/reference/db_link.html
            //http://www.oracle-database-tips.com/troubleshoot_oracle_database_link_errors.html

            //程式執行的條件：
            //a.只有 oracle 需要這樣做
            //b.指定的連線交易環境(TransactionHost)，必須連線。
            //c.連線必須沒有等待交易
            //  要執行 KTCC.Commit、KTCC.Rollback 而未執行交易 KTCC.Commit、KTCC.Rollback
            //  若此開發模式下偵測正在等待中的交易，不繼續進行 CloseDBLink 的動作
            //d.
            //0990811 新增判斷條件, 
            //VIEW:   V$DBLINK 查看目前連線的 DBlink 位置, 若 DBLink 自己連線的院區, 也會在裡面

            //db_link           Db link name
            //owner_id          Owner name
            //logged_on         Is the database link currently logged on?
            //protocol          Dblink's communications protocol
            //open_cursors      Are there any cursors open for the db link ?
            //in_transaction    Is the db link part of a transaction which 
            //                  has not been commited or rolled back yet ?
            //update_sent       Was there an update on the db link ?

            //column: UPDATE_SENT = YES 表示有正在等待交易資訊 (Insert, Update, Delete, For Update)
            //        所以不可以做 CloseDBLink 之前的 Commit 或 RollBack 
            //       
            //        ORACLE 裡, 一執行 DataBaselink 即會產生 v$Transaction
            //        IN_TRANSACTION = 'YES' 表示有資料被讀取且已在 v$Transaction產生資訊, 
            //        但並不表示 正在等待交易 (Insert, Update, Delete, For Update)
            //        只是未執行 Rollback 或 Commit, 若執行 Rollback 或 Commit 則可正常 CloseDBLink
            //
            //        'TODO 測試的條件與程式執行時似乎有所不同
            //        ########################################################################################
            //        OPEN_CURSORS > 0  經過測試表示此 DBlink 是最新的交易資訊
            //        原因是該 DBLink 是最新一次有(Insert, Update, Delete)資料的連線,
            //        就算執行 Rollback 或 Commit 仍無法正常 CloseDBLink, 因為有 Cursosr 被開啟
            //
            //        若要解決OPEN_CURSORS > 0 必須, 再更新一筆別的院區的資料, 
            //        但是換別的院區 OPEN_CURSORS > 0
            //        1010829 居易
            //        HETEROGENEOUS : (接續 OPEN_CURSORS)
            //        當 OPEN_CURSORS > 0 但是 HETEROGENEOUS = 'YES' 時 使用 Function 呼叫 EIP 主機是這種情形)
            //        仍然可以關閉 v$DBLink 
            //        ########################################################################################
            //        
            //        程式 只要 UPDATE_SENT 沒有 = 'YES' 的資料，都可以關閉 v$DBLink
            //
            //備註：
            //A.有做過 Select DBLink、Update DBLink、Insert DBLink、Delete DBLink
            //  要執行之前，外部程式必須先執行 KTCC.Commit() 或 KTCC.Rollback()
            //  所以請務必確認 再呼叫此 Funcion 之前，是否必須執行而未執行 KTCC.Commit() 或 KTCC.Rollback()
            //
            //B.如果是使用 dataReader 讀取資料，ADO.NET 本來就不允許了
            //  因為 dataReader 還沒有 close 的時候不能做下一個 dosql
            //  在執行 KTCC.DoSql、KTCC.Commit、KTCC.Rollback 時就會被 ADO.NET 阻擋
            //  錯誤訊息關鍵字為 "Fetching"
            //
            //C.v$session.Taddr 與 v$transaction 存在 並不表示有正在等待交易結束 Commit, Rollback 的資料

            try
            {
                //不能執行 Close DataBase Link 就算了, 不提示任何訊息避免造成不必要的困擾

                TransHost = 主機代碼(TransHost);

                //沒有指定的交易連線
                if (!Cns.Contains(TransHost))
                    return;

                if (Cns[TransHost] == null)
                    return;

                if (Cns[TransHost].DBConfig != DBProviderType.Oracle)
                    return;

                //有未執行的交易(KTCC.Commit、KTCC.Rollback)
                if (Cns[TransHost].SetTransaction)
                    return;

                System.Text.StringBuilder sqlstr = new System.Text.StringBuilder();
                System.Data.DataTable tblDBLink = new System.Data.DataTable();
                List<DBLink_Models> dy_DBLinks = new List<DBLink_Models>();

                sqlstr.Length = 0;
                sqlstr.AppendLine("Select * From v$DBLink ");

#if !noDapper
                dy_DBLinks = DoSql<DBLink_Models>(sqlstr.ToString(), TransHost: TransHost).ToList();
#else
                DoSql(sqlstr.ToString(), tblDBLink, TransHost: TransHost);
                dy_DBLinks = DataTableToList<DBLink_Models>(tblDBLink).ToList();
#endif

                if (dy_DBLinks.Count == 0) return;

                //有正在執行的交易存在，在上面的  Cns[TransHost].SetTransaction 就會被 return 了
                if (dy_DBLinks.Where(D => D.UPDATE_SENT.Equals("YES")).ToList().Count > 0) return;

                //這裡必須使用 Commit 或 Rollback，才能夠接續 Close Database Link 連線
                //但是相對的 若有做 KTCC.BeginTransaction 而程式沒有 KTCC.Commit() 或 KTCC.Rollback()
                //則會被此段程式寫入, 所以上方已經做過阻擋
                DoSql(" Commit ", TransHost: TransHost);
                //Commit 就是要讓 IN_TRANSACTION = 'NO' 的動作

                for (int x = 0; x <= dy_DBLinks.Count - 1; x++)
                {
                    //不知道 DBlink 網路斷線，或主機當機時，執行 Alter Session Close DataBase Link 會發生什麼事情
                    //每一台機器都去執行 close database link，若有錯誤訊息 不呈現
                    //至於要使用 下列哪一種方式 close database link 都是可行的

                    sqlstr.Length = 0;
                    sqlstr.AppendLine("Begin DBMS_SESSION.Close_Database_Link('" + dy_DBLinks[x].DB_LINK + "'); End; ");
                    sqlstr.AppendLine("Alter Session Close DataBase Link " + dy_DBLinks[x].DB_LINK);
                    DoSql(sqlstr.ToString(), TransHost: TransHost);
                    if (!DoSqlOK)
                    {
                        //目前測試得知，Oracle.ManangedDataAccess 無法執行 CloseDBLink，
                        //會產生 ORA-02080: database link is in use

                        //ORA-02080: database link is in use
                        //ORA-02081: database link is not open
                        //這裡是錯誤訊息，常見的錯誤訊息就是這兩種
                        //還沒有判斷到當掛院區主機檔機時，close database link 時會出現什麼情況
                    }
                }
            }
            catch (Exception ex)
            {
                KTEnvs.Error_Log("CloseDBlink: " + KTConstant.vbCrLf + ex.Message);
            }
            finally
            {
            }
        }

        public void CloseDBLinkAll()
        {
            DictionaryEntry[] cn = new DictionaryEntry[Cns.Count + 1];
            int idx = 0;

            Cns.CopyTo(cn, 0);
            //最後一個是 Nothing
            for (idx = 0; idx <= cn.Length - 2; idx++)
            {
                CloseDBLink(cn[idx].Key.ToString());
            }
        }

        private bool IsSelectingData(string sSqlStr)
        {
            //功能：檢查是否為資料查詢(SELECT)語法
            //回傳：Select 語法 Return True,Else Return False
            if (sSqlStr.Length > 10)
            {
                //Mysql
                if (sSqlStr.Trim().ToUpper().Substring(0, 10).IndexOf("SELECT") > -1 | sSqlStr.Trim().ToUpper().Substring(0, 10).IndexOf("SHOW") > -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private string GetOracleErrorCode(string InErr)
        {
            //功能:從錯誤訊息中解析出oracle的錯誤代碼

            int errLength = -1;
            int sIndex = -1;
            int eindex = -1;

            sIndex = InErr.IndexOf("ORA-");
            eindex = InErr.IndexOf(":");

            if (eindex == -1)
                eindex = InErr.IndexOf(KTConstant.vbCrLf);
            //eindex = InErr.IndexOf(Constants.vbCr);

            errLength = eindex - sIndex;

            if (sIndex > -1)
            {
                return InErr.Substring(sIndex, errLength);
            }
            else
            {
                return "";
            }
        }

        private string GetOracleErrorMessage(string InErr)
        {

            int errLength = -1;
            int sIndex = -1;
            int eindex = -1;
            string ErrStr = "";
            string TmpErr = InErr;
            string ErrChar = "||@@||";

            try
            {
                TmpErr = TmpErr.Replace(KTConstant.vbCrLf, ErrChar); //.Replace(Constants.vbLf, ErrChar);
                TmpErr = TmpErr.Replace(ErrChar + ErrChar, ErrChar);

                sIndex = TmpErr.IndexOf("ORA-");
                eindex = TmpErr.IndexOf(":");

                if (eindex == -1)
                    eindex = TmpErr.IndexOf(ErrChar);

                errLength = eindex - sIndex;

                if (sIndex > -1)
                {
                    ErrStr = TmpErr.Substring(sIndex, errLength);
                    ErrStr = TmpErr.Substring(TmpErr.IndexOf(ErrStr) + ErrStr.Length + 2);
                    InErr = ErrStr.Replace(ErrChar, KTConstant.vbCrLf);

                }
                else
                {
                    return InErr;
                }

            }
            catch (Exception)
            {
                return InErr;
            }

            return InErr;

        }

        public static string GetHostDSN()
        {
            //功能：取得 C:\windows\oracle.ini 內的主機名稱
            //回傳：主機名稱，如 KTGH00, HPK210

            //Windows Server 2008 因權限過於嚴謹無法讀取  "C:\windows\Oracle.ini"
            //故在網站主機放置 "C:\ap06bz\ini\Oracle.ini"，
            //並設定安全性增加使用者 IIS_IUSRS，給予完全控制權限

            string[] Oraclefiles = {
			    "C:\\windows\\Oracle.ini",
			    "C:\\ap06bz\\ini\\Oracle.ini"
		    };

            string OracleIni = "";
            string[] ReadEnds = new string[0];

            string PgName = "";

            if (ExeOrWeb == "exe")
            {
                //網頁無法取得程式名稱
                //只能讀取程式網址位置 System.Web.HttpContext.Current.Request.FilePath
                PgName = KTEnvs.getAppProgramName();
            }

            for (int x = 0; x <= Oraclefiles.Length - 1; x++)
            {
                OracleIni = Oraclefiles[x];
                try
                {
                    //windows server 2008 檔案存在不表示允許存取
                    if (System.IO.File.Exists(OracleIni))
                    {
                        try
                        {
                            ReadEnds = System.IO.File.ReadAllLines(OracleIni);
                            break;
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.IndexOf("拒絕存取路徑") == -1)
                            {
                                throw new ApplicationException(PgName + " 拒絕存取路徑" + KTConstant.vbCrLf + KTString.errException(ex));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //網頁權限有異常
                    throw new ApplicationException(PgName + " 使用權限異常" + KTConstant.vbCrLf + KTString.errException(ex));
                }
            }

            if (ReadEnds.Length == 0)
            {
                OracleIni = String.Join("、", Oraclefiles);
                throw new ApplicationException("執行環境嚴重錯誤：找不到院區判別檔案 " + KTConstant.vbCrLf + KTConstant.vbCrLf + OracleIni + KTConstant.vbCrLf + KTConstant.vbCrLf + "請通知資訊室");
            }

            int Pos1 = 0;
            int Pos2 = 0;
            string ReadLine = "";

            for (int x = 0; x <= ReadEnds.Length - 1; x++)
            {
                ReadLine = ReadEnds[x];
                if (ReadLine.IndexOf(Convert.ToChar("#")) == -1)
                {
                    //local = KTGH00 or local = t:KTGHCC00:kt1  
                    if (ReadLine.ToUpper().IndexOf("LOCAL") > -1)
                    {
                        if (ReadLine.IndexOf(":") > -1)
                        {
                            Pos1 = ReadLine.IndexOf(':') + 1;
                            Pos2 = ReadLine.IndexOf(':', Pos1);
                            return ReadLine.Substring(Pos1, Pos2 - Pos1).Trim().ToUpper();
                        }
                        else
                        {
                            if (ReadLine.Split(Convert.ToChar("=")).Length == 2)
                            {
                                return ReadLine.Split(Convert.ToChar('='))[1].Trim().ToUpper();
                            }
                        }
                    }
                }
            }

            return "";

        }

        private void GetNurseStation()
        {
            //功能：取得 C:\ap06bz\exe\06bz.ini 的護理站代碼
            //回傳：護理站代碼，護理站名稱抓 PRS002.Dtflg "2"
            //      有 STNID 回傳 STNID 
            //      有 STKCOD 回傳 STKCOD

            string ini_06bz = "C:\\ap06bz\\exe\\06bz.ini";

            if (!System.IO.File.Exists(ini_06bz))
                return;

            string[] allLine = System.IO.File.ReadAllLines(ini_06bz);

            for (int x = 0; x <= allLine.Length - 1; x++)
            {
                if (!string.IsNullOrEmpty(allLine[x].ToString()))
                {
                    if (allLine[x].ToUpper().IndexOf("STKCOD") > -1)
                        STKCOD_ = allLine[x].Split(Convert.ToChar('='))[1].ToUpper();
                    else if (allLine[x].ToUpper().IndexOf("STNID") > -1)
                        STNID_ = allLine[x].Split(Convert.ToChar('='))[1].ToUpper();
                }
            }

            //以前的護理站樓層都是 STKCOD 
            //是之後才有增加一個 STNID
            if (string.IsNullOrEmpty(STNID_))
                STNID_ = STKCOD_;

        }

        public string CheckBedno(string bedno, string Host = "")
        {
            //處理SI,MI,PI,CNC,111D
            string STNIDStr = "";
            bedno = bedno.ToUpper().Trim();
            //1.
            if (string.IsNullOrEmpty(bedno))
                return STNID_;

            if (string.IsNullOrEmpty(Host))
                Host = OracleHostDSN_;

            //2.
            if (bedno.Length == 8)
                return bedno;

            //3.
            string icuChar = bedno.Substring(0, 2);
            string bedChar = bedno.Substring(2);

            switch (icuChar)
            {
                case "SI":
                case "MI":
                case "PI":
                case "NC":
                    int BedNum;
                    if (int.TryParse(bedChar, out BedNum))
                        bedno = bedChar.PadLeft(2, '0');
                    else
                        bedno = "0" + bedChar;
                    break;
            }

            switch (icuChar)
            {
                case "SI":
                    switch (Host)
                    {
                        case "KTGH00":
                            bedno = "003SSI" + bedno;
                            break;
                        default:
                            bedno = "103SSI" + bedno;
                            break;
                    }
                    return bedno;

                case "MI":
                    switch (Host)
                    {
                        case "KTGH00":
                            bedno = "003MMI" + bedno;
                            break;
                        default:
                            bedno = "106MMI" + bedno;
                            break;
                    }
                    return bedno;

                case "PI":
                    switch (Host)
                    {
                        case "KTGH00":
                            bedno = "007PPI" + bedno;
                            break;
                        default:
                            bedno = "105PPI" + bedno;
                            break;
                    }
                    return bedno;

                case "NC":
                    switch (Host)
                    {
                        case "KTGH00":
                            bedno = "007CNC" + bedno;
                            break;
                        default:
                            bedno = "105CNC" + bedno;
                            break;
                    }
                    return bedno;
            }

            //4.
            switch (bedno.Length)
            {
                case 1:
                    //只輸入一個碼的時候
                    bedno = "0" + bedno;
                    break;

                case 2:
                    break;

                case 3:
                    //Interaction.MsgBox("請輸輸入最少輸入 4 碼的床位資料");
                    return "";

                case 4:
                    if (string.IsNullOrEmpty(STNID_))
                    {
                        //Interaction.MsgBox("您目前所使用的機器非護理站電腦，因此所輸入的床位號碼無法判斷");
                        return "";
                    }
                    break;

                case 8:
                    return bedno;

                default:
                    break;

                //先不鳥他
                //If NurseStation_ = "" Then
                //    MsgBox("您目前所使用的機器非護理站電腦，因此所輸入的床位號碼無法判斷")
                //    Return ""
                //End If

                //Select Case NurseStation_.Substring(0, 2)
                //    Case "10"
                //        bedno = "10" & bedno.Substring(0, 2) & bedno.Substring(2, 4)
                //    Case "11"
                //End Select
            }

            switch (STNID_)
            {
                case "106M":
                case "003M":
                    STNIDStr = STNID_ + "MI";
                    break;
                case "103S":
                case "003S":
                    STNIDStr = STNID_ + "SI";
                    break;
                case "105D":
                    STNIDStr = STNID_ + "DR";
                    break;
                case "111D":
                    STNIDStr = STNID_ + "AY";
                    break;
                case "106C":
                    //0950925 CCU
                    STNIDStr = STNID_ + "CU";
                    break;

                case "202R":
                    //0950925 RCW
                    STNIDStr = STNID_ + "CW";
                    break;
            }

            return STNIDStr + bedno.PadLeft(2, '2').Substring(0, 2);
        }

        public string CreateInsertSqlSize(System.Data.DataRow R, string TableName = "", string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：針對 Row 不是 DBNull 的欄位 自動產生 Insert SQL
            //參數：R          : DataRow
            //      TableName  : 要新增的資料表名稱
            //      DBLink     : 若是 Oracle 則使用 @DBLink
            //                   若是 SqlServer 則使用 DBLink.TableName (DBLink 是 DataBaseName)
            //      TransHost  : 交易連線主機，預設 CNHOME
            //                   主要處理 A.Oracle、SqlServer 的 DataBase 
            //                            B.資料庫格式的時間函式
            //      chkColSize : 檢查欄位大小是否太長，預設 false
            //
            //回傳：Sql 語法字串
            //
            //範例：rowPRS014 = tblPRS014.NewRow --> 此時的 rowPRS014 Column 都是 DBNull
            //      將部分需要新增的欄位寫入 rowPRS014 並帶入此 Function 即可使用
            //      rowPRS014("CRTNO") = '000000004'
            //      rowPRS014("ACTNO") = '0A04'
            //      rowPRS014("Vsdte") = '0950404'
            //      rowPRS014("Vsclin") = '444'
            //      rowPRS014("Vsapn") = '4'
            //
            //      InsertSql = KTCC.CreateInsertSqlSize(rowPRS014, "PRS014")
            //
            //備註：只增加不是 DBNull 的欄位內容
            //      所以，當整個 DataRow 都是 DBNull 值，則 回傳的 SQL 是空字串

            if (string.IsNullOrEmpty(TableName))
                TableName = R.Table.TableName;

            if (string.IsNullOrEmpty(TableName) | TableName == "TABLE")
            {
                throw new ApplicationException("呼叫 CreateInsertSqlSize 自動產生新增SQL(Insert SQL)時發生錯誤!!" + KTConstant.vbCrLf + KTConstant.vbCrLf + "要更新的資料表名稱(TableName參數)不可以是空字串!!");
            }

            if (!string.IsNullOrEmpty(DBLink))
                DBLink = DBLink.ToUpper();

            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB);

            System.Text.StringBuilder InsertsqlStr = new System.Text.StringBuilder();
            System.Text.StringBuilder ColumnStr = new System.Text.StringBuilder();
            System.Text.StringBuilder ValuesStr = new System.Text.StringBuilder();
            string ColumnToLong = "";

            ColumnToLong = ColumnsOverLong(R, TableName, DBLink, TransHost, sKTDB);
            if (!string.IsNullOrEmpty(ColumnToLong))
            {
                TableCns_[DBLink + TransHost].Table.Remove(TableName);
                ColumnToLong = ColumnsOverLong(R, TableName, DBLink, TransHost, sKTDB);
                if (!string.IsNullOrEmpty(ColumnToLong))
                    ColumnToLong = ColumnToLong + KTConstant.vbCrLf + KTConstant.vbCrLf;
            }

            switch (tmpCN.DBConfig)
            {
                case DBProviderType.Oracle:
                    TableName = DataBaseLink(DBLink, TableName, TransHost, sKTDB);
                    break;

                case DBProviderType.SQLServer:
                    //0960907 居易
                    //SqlServer 也可以跨資料庫 查詢，交易(只要權限足夠 @@)
                    //目前僅知到單一主機有多資料庫的時候可以這樣做
                    //跨主機的部份需要再去找資料
                    if (!string.IsNullOrEmpty(DBLink))
                        TableName = DBLink + "." + TableName;
                    break;

                case DBProviderType.Excel:
                case DBProviderType.Excel_xlsx:
                    string tmpTableName = TableName;
                    if (tmpTableName.IndexOf("$") == -1)
                        tmpTableName = "[" + tmpTableName + "$]";
                    if (tmpTableName.IndexOf("[") == -1)
                        tmpTableName = "[" + tmpTableName + "]";
                    TableName = tmpTableName;
                    break;
            }

            InsertsqlStr.Append("Insert Into " + TableName);

            foreach (DataColumn C in R.Table.Columns)
            {

                if (!Convert.IsDBNull(R[C]))
                {
                    if (ColumnStr.ToString() != string.Empty)
                        ColumnStr.Append(",");
                    ColumnStr.Append(SpecialColumnName(C.ColumnName, tmpCN.DBConfig));

                    if (ValuesStr.ToString() != string.Empty)
                        ValuesStr.Append(",");

                    switch (C.DataType.ToString())
                    {
                        case "System.String":
                            ValuesStr.Append(" '" + R[C].ToString().Replace("'", "''") + "' ");
                            break;

                        case "System.Decimal":
                        case "System.Short":
                        case "System.Integer":
                        case "System.Long":
                        case "System.Single":
                        case "System.Double":
                        case "System.Int16":
                        case "System.Int32":
                        case "System.Int64":
                            ValuesStr.Append(" '" + R[C].ToString().Replace("'", "''") + "' ");
                            break;

                        case "System.DateTime":
                        case "MySql.Data.Types.MySqlDateTime":
                            //若只丟日期進去 格式化之後的時間為 00:00:00 這是正常的，寫入資料庫之後就會不見了
                            switch (tmpCN.DBConfig)
                            {
                                case DBProviderType.Oracle:
                                    ValuesStr.Append(" To_Date('" + Convert.ToDateTime(R[C]).ToString("yyyy/MM/dd HH:mm:ss") + "', 'YYYY/MM/DD HH24:MI:SS') ");
                                    break;

                                case DBProviderType.Access:
                                case DBProviderType.Access_accdb:
                                    ValuesStr.Append(" #'" + Convert.ToDateTime(R[C]).ToString("yyyy/MM/dd HH:mm:ss") + "'# ");
                                    break;

                                default:
                                    ValuesStr.Append(" '" + Convert.ToDateTime(R[C]).ToString("yyyy/MM/dd HH:mm:ss") + "' ");
                                    break;
                            }
                            break;

                        case "System.TimeSpan":
                            ValuesStr.Append(" '" + R[C].ToString().Replace("'", "''") + "' ");
                            break;

                        default:
                            InsertsqlStr.Append("Function CreateInsertSqlSize" + KTConstant.vbCrLf);
                            InsertsqlStr.Append("自動產生新增SQL(Insert SQL)時發生錯誤!!" + KTConstant.vbCrLf);
                            InsertsqlStr.Append("資料型態超出範圍(不是字串也不是數字)");
                            throw new ApplicationException(InsertsqlStr.ToString());
                    }
                }
            }

            if (string.IsNullOrEmpty(ColumnStr.ToString()) | string.IsNullOrEmpty(ValuesStr.ToString()))
            {
                return "";
            }
            else
            {
                if (!string.IsNullOrEmpty(ColumnToLong))
                    ColumnToLong = ColumnToLong + KTConstant.vbCrLf + KTConstant.vbCrLf;
                return ColumnToLong + InsertsqlStr.ToString() + "(" + ColumnStr.ToString() + ") Values ( " + ValuesStr.ToString() + ")";
            }
        }

#if !noDapper
        public void CreateInsertSql(object obj, string tableName, out string sqlstr,
                                      bool allColunmFlag = false, string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            //下列資料庫連線類型，可以整批更新，且不需透過 Dapper.DynamicParameters()
            //System.Data.OracleClient、Oracle.ManagedDataAccess.Client
            //System.Data.SqlClient、MySql.Data.MySqlClient

            Dapper.DynamicParameters dy;  //不取得 DynamicParameters 就表示目前使用上述的 Provider 
            CreateInsertSql(obj, tableName, out sqlstr, out dy, allColunmFlag, DBLink, TransHost, sKTDB);
        }

        public void CreateInsertSql(object obj, string tableName, out string sqlstr, out Dapper.DynamicParameters Parameter,
                                      bool allColunmFlag = false, string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            if (tableName == "") tableName = obj.GetType().Namespace.Split(new string[] { "." }, StringSplitOptions.None)[0];

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB);

            switch (tmpCN.DBConfig)
            {
                case DBProviderType.Oracle:
                    tableName = DataBaseLink(DBLink, tableName, sKTDB: tmpCN);
                    break;

                case DBProviderType.MySql:
                    tableName = "`" + tableName + "`";
                    break;
            }

            //1060727 居易
            //DB Table 資料結構不能包含其他額外的自訂欄位
            //若要使用自訂欄位，必須使用 partial(部分類別) 再包一層 public Class 來當作自訂欄位
            //自訂欄位就不會被自動產生SQL語法抓到。
            //obj.GetType().GetNestedTypes().Count()              //還有多少子類別
            //obj.GetType().GetNestedTypes()[0].GetProperties()   //子類別的資料屬性
            System.Reflection.PropertyInfo[] props = obj.GetType().GetProperties();
            System.Reflection.PropertyInfo prop = null;

            //System.Data.OleDb 一定要透過 Dapper.DynamicParameters 才能新增

            //下列資料庫連線類型，可以整批更新，且不需透過 Dapper.DynamicParameters()
            //System.Data.OracleClient、Oracle.ManagedDataAccess.Client
            //System.Data.SqlClient、MySql.Data.MySqlClient

            //obj 有 null 值
            //排除 null 值 Parameter 重新產生參數值，sqlstr 也不產生 null 的欄位語法
            Parameter = new Dapper.DynamicParameters();

            sqlstr = "";
            string PropertieName = "";
            string InsertStr = "Insert into " + tableName;
            System.Text.StringBuilder ColumnStr = new System.Text.StringBuilder();
            System.Text.StringBuilder ValueStr = new System.Text.StringBuilder();

            for (int i = 0; i <= props.Count() - 1; i++)
            {
                prop = props[i];

                if (prop.PropertyType.Namespace == "System")
                {
                    PropertieName = prop.Name;
                    object cGetValue = prop.GetValue(obj, null);
                    if (((prop != null) && (cGetValue != null)) || allColunmFlag)
                    {

                        if (ColumnStr.Length != 0) { ColumnStr.Append(", "); }
                        ColumnStr.Append(SpecialColumnName(PropertieName, tmpCN.DBConfig));

                        if (ValueStr.Length != 0) { ValueStr.Append(", "); }

                        switch (tmpCN.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                            case ADOProviderType.MysqlOdbc35:
                            case ADOProviderType.MysqlOdbc52:
                            case ADOProviderType.Oledb:
                                ValueStr.Append("?");
                                break;

                            case ADOProviderType.SQLClient:
                            case ADOProviderType.MySqlClient:
                                ValueStr.Append("@" + PropertieName);
                                break;

                            case ADOProviderType.OracleClient:
                                ValueStr.Append(":" + PropertieName);
                                break;
                        }

                        Parameter.Add(PropertieName, cGetValue);
                    }
                }
            }
            if ((ColumnStr.Length != 0) && (ValueStr.Length != 0))
            {
                sqlstr = InsertStr + "(" + ColumnStr.ToString() + ") values (" + ValueStr.ToString() + ")";
            }
        }
#endif

        public KTDBCommand CreateInsertCommandSize(System.Data.DataRow R, string tableName = "", string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：針對異動的欄位自動產生更新 Update SQL，沒異動的欄位不更新
            //參數：R          : DataRow
            //      TableName  : 要新增的資料表名稱
            //      DBLink     : 若是 Oracle 則使用 @DBLink
            //                   若是 SqlServer 則使用 DBLink.TableName (DBLink 是 DataBaseName)
            //      TransHost  : 交易連線主機，預設 CNHOME
            //                   主要處理 A.Oracle、SqlServer 的 DataBase 
            //                            B.資料庫格式的時間函式
            //回傳：Sql 語法字串
            //
            //注意：執行本程式後，若沒有欄位需要更新，則 ColumnsToBeUpdate 會設定為 = 0
            //      當 ColumnsToBeUpdate = 0 時，傳回空字串
            //      所以使用者須自行判斷，若回傳直為空字串則不要執行更新

            if (string.IsNullOrEmpty(tableName))
                tableName = R.Table.TableName;

            if (string.IsNullOrEmpty(tableName) | tableName == "TABLE")
            {
                throw new ApplicationException("呼叫 CreateInsertCommandSize 自動產生新增SQL(Insert SQL)時發生錯誤!!" + KTConstant.vbCrLf + KTConstant.vbCrLf + "要更新的資料表名稱(TableName參數)不可以是空字串!!");
            }

            if (!string.IsNullOrEmpty(DBLink))
                DBLink = DBLink.ToUpper();

            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB);

            System.Text.StringBuilder InsertsqlStr = new System.Text.StringBuilder();
            System.Text.StringBuilder ColumnStr = new System.Text.StringBuilder();
            System.Text.StringBuilder ValuesStr = new System.Text.StringBuilder();
            string ColumnToLong = "";

            ColumnToLong = ColumnsOverLong(R, tableName, DBLink, TransHost, sKTDB);
            if (!string.IsNullOrEmpty(ColumnToLong))
            {
                TableCns_[DBLink + TransHost].Table.Remove(tableName);
                ColumnToLong = ColumnsOverLong(R, tableName, DBLink, TransHost, sKTDB);
                if (!string.IsNullOrEmpty(ColumnToLong))
                    ColumnToLong = ColumnToLong + KTConstant.vbCrLf + KTConstant.vbCrLf;
            }

            switch (tmpCN.DBConfig)
            {
                case DBProviderType.Oracle:
                    tableName = DataBaseLink(DBLink, tableName, TransHost, sKTDB);
                    break;

                case DBProviderType.SQLServer:
                    //0960907 居易
                    //SqlServer 也可以跨資料庫 查詢, 交易(只要權限夠 @@)
                    //目前僅知到單一主機有多資料庫的時候可以這樣做
                    //跨主機的部份需要再去找資料
                    if (!string.IsNullOrEmpty(DBLink))
                        tableName = DBLink + "." + tableName;
                    break;
            }

            InsertsqlStr.Append("Insert Into " + tableName);

            foreach (System.Data.DataColumn C in R.Table.Columns)
            {

                if (!Convert.IsDBNull(R[C]))
                {
                    switch (C.DataType.ToString())
                    {
                        case "System.String":
                            break;

                        case "System.Decimal":
                        case "System.Short":
                        case "System.Integer":
                        case "System.Long":
                        case "System.Single":
                        case "System.Double":
                        case "System.Int16":
                        case "System.Int32":
                        case "System.Int64":
                            break;

                        case "System.DateTime":
                        case "MySql.Data.Types.MySqlDateTime":
                        case "System.TimeSpan":
                            break;

                        case "System.Byte[]":
                            break;

                        default:
                            InsertsqlStr.Append("Function CreateInsertCommandSize " + KTConstant.vbCrLf);
                            InsertsqlStr.Append("自動產生新增SQL(Insert SQL)時發生錯誤!!" + KTConstant.vbCrLf);
                            InsertsqlStr.Append("資料型態超出範圍(不是字串也不是數字)");
                            throw new ApplicationException(InsertsqlStr.ToString());
                    }

                    if (ColumnStr.Length != 0) { ColumnStr.Append(", "); }
                    ColumnStr.Append(SpecialColumnName(C.ColumnName, tmpCN.DBConfig));

                    if (ValuesStr.Length != 0) { ValuesStr.Append(", "); }
                    switch (tmpCN.DBConfig)
                    {
                        case DBProviderType.Oracle:
                            ValuesStr.Append(":" + C.ColumnName);
                            break;

                        case DBProviderType.SQLServer:
                        case DBProviderType.MySql:
                            ValuesStr.Append("@" + C.ColumnName);
                            break;

                        default:
                            ValuesStr.Append("?");
                            break;
                    }
                }
            }

            if (string.IsNullOrEmpty(ColumnStr.ToString()) | string.IsNullOrEmpty(ValuesStr.ToString()))
            {
                //No Column Need to be insert 
                return null;
            }

            //判斷 CLOB Column
            System.Collections.Specialized.StringCollection CLOBColumn = null;
            string[] CLOBColumns = new string[0];
            if (TableCns_.ContainsKey(DBLink + TransHost))
            {
                if (TableCns_[DBLink + TransHost].Table.ContainsKey(tableName))
                {
                    DataTable tblTableSchema = TableCns_[DBLink + TransHost].Table[tableName].TableSchema;
                    DataRow[] rowTableSchemas = tblTableSchema.Select("ColumnSize = -1");
                    if (rowTableSchemas.Length > 0)
                    {
                        CLOBColumns = new string[rowTableSchemas.Length];
                        CLOBColumn = new System.Collections.Specialized.StringCollection();
                        for (int x = 0; x <= rowTableSchemas.Length - 1; x++)
                        {
                            CLOBColumn.Add(rowTableSchemas[x][0].ToString());
                        }
                        CLOBColumn.CopyTo(CLOBColumns, 0);
                    }
                }
            }

            KTDBCommand KTCm = new KTDBCommand();
            KTCm.KTDBProvider = tmpCN;
            KTCm.DataRow = R;
            KTCm.TableName = tableName;
            KTCm.CommandText = ColumnToLong + InsertsqlStr.ToString() + "(" + ColumnStr.ToString() + ") Values ( " + ValuesStr.ToString() + ")";
            if (CLOBColumns.Length > 0)
                KTCm.CLOBColumn = String.Join(",", CLOBColumns);
            KTCm.InsertParameters();
            return KTCm;
        }

        private string SpecialColumnName(string ColumnName, DBProviderType DBConfig = DBProviderType.Oracle)
        {
            string colStr = "";

            switch (DBConfig)
            {
                case DBProviderType.Oracle:
                    colStr = colStr + ColumnName;
                    break;

                case DBProviderType.Access:
                case DBProviderType.Access_accdb:
                    //Access 可以讓所有的欄位都當作特殊欄位
                    colStr = colStr + "[" + ColumnName + "]";
                    break;

                case DBProviderType.SQLServer:
                    if (ColumnName.IndexOf("/") > -1 | ColumnName.IndexOf(" ") > -1)
                        colStr = colStr + "[" + ColumnName + "]";
                    else
                        colStr = colStr + ColumnName;
                    break;

                case DBProviderType.MySql:
                    colStr = colStr + "`" + ColumnName + "`";
                    break;

                default:
                    //預設使用中括弧標示出來，其他資料庫有不同之處，再額外處理
                    if (ColumnName.IndexOf("/") > -1 | ColumnName.IndexOf(" ") > -1)
                        colStr = colStr + "[" + ColumnName + "]";
                    else
                        colStr = colStr + ColumnName;
                    break;
            }

            return colStr;
        }

        public string CreateUpdateSqlSize(System.Data.DataRow R, string sWhereStr, string TableName = "", string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：針對異動的欄位自動產生更新 Update SQL，沒異動的欄位不更新
            //參數：R          : DataRow
            //     sWhereStr  : 要更新的 Where Sql 語法
            //     TableName  : 要更新的資料表名稱
            //     DBLink     : 若是 Oracle 則使用 @DBLink
            //                  若是 SqlServer 則使用 DBLink.TableName (DBLink 是 DataBaseName)
            //     TransHost  : 交易連線主機，預設 CNHOME
            //                  主要處理 A.Oracle、SqlServer 的 DataBase 
            //                            B.資料庫格式的時間函式
            //回傳：Sql 語法字串
            //
            //注意：執行本程式後，若沒有欄位需要更新，則傳回空字串
            //     所以必須須自行判斷，若回傳直為空字串則不要執行更新

            if (string.IsNullOrEmpty(TableName))
                TableName = R.Table.TableName;

            if (string.IsNullOrEmpty(TableName) | TableName == "TABLE")
            {
                throw new ApplicationException("呼叫 CreateUpdateSqlSize 自動產生更新SQL(Update SQL)時發生錯誤!!" + KTConstant.vbCrLf + KTConstant.vbCrLf + "要更新的資料表名稱(TableName參數)不可以是空字串!!");
            }

            if (string.IsNullOrEmpty(sWhereStr))
            {
                throw new ApplicationException("呼叫 CreateUpdateSqlSize 自動產生更新SQL(Update SQL)時發生錯誤!!" + KTConstant.vbCrLf + KTConstant.vbCrLf + "更新條件(sWhereStr參數)不可以為空字串!!");
            }

            if (!string.IsNullOrEmpty(DBLink))
                DBLink = DBLink.ToUpper();

            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB);

            string UpdateSqlStr = "";
            string ColumnStr = "";
            string ValuesStr = "";
            string ColumnToLong = "";
            int ColumnsToBeUpdate = 0;

            ColumnToLong = ColumnsOverLong(R, TableName, DBLink, TransHost, sKTDB);
            if (!string.IsNullOrEmpty(ColumnToLong))
            {
                TableCns_[DBLink + TransHost].Table.Remove(TableName);
                ColumnToLong = ColumnsOverLong(R, TableName, DBLink, TransHost, sKTDB);
                if (!string.IsNullOrEmpty(ColumnToLong))
                    ColumnToLong = ColumnToLong + KTConstant.vbCrLf + KTConstant.vbCrLf;
            }

            switch (tmpCN.DBConfig)
            {
                case DBProviderType.Oracle:
                    TableName = DataBaseLink(DBLink, TableName, TransHost, sKTDB);
                    break;

                case DBProviderType.SQLServer:
                    //0960907 居易
                    //SqlServer 也可以跨資料庫 查詢, 交易(只要權限夠 @@)
                    //目前僅知到單一主機有多資料庫的時候可以這樣做
                    //跨主機的部份需要再去找資料
                    if (!string.IsNullOrEmpty(DBLink))
                        TableName = DBLink + "." + TableName;
                    break;
            }

            ColumnsToBeUpdate = 0;

            UpdateSqlStr = "Update " + TableName + " set ";

            foreach (DataColumn C in R.Table.Columns)
            {

                //該欄位被異動過
                //if (R[C] + "" != R[C, DataRowVersion.Original] + "") {
                if (!R[C].Equals(R[C, DataRowVersion.Original]))
                {
                    ValuesStr = "";

                    switch (C.DataType.ToString())
                    {
                        case "System.String":
                            ValuesStr = " '" + R[C].ToString().Replace("'", "''") + "' ";
                            break;

                        case "System.Decimal":
                        case "System.Short":
                        case "System.Integer":
                        case "System.Long":
                        case "System.Single":
                        case "System.Double":
                        case "System.Int16":
                        case "System.Int32":
                        case "System.Int64":
                            //數字型態更新為 Null 時，SQL 語法會發生失敗，必須產生 0 值
                            //利用 Val() 排除 0 的紀錄
                            //if (Conversion.Val(R[C].ToString) != Conversion.Val(R[C, DataRowVersion.Original].ToString())) {
                            //    ValuesStr = R[C].ToString();
                            //    if (string.IsNullOrEmpty(ValuesStr))
                            //        ValuesStr = "0";
                            //}
                            //break;
                            double sOut;
                            double oOut;
                            if ((Double.TryParse(R[C].ToString(), out sOut)) && (Double.TryParse(R[C, DataRowVersion.Original].ToString(), out oOut)))
                            {
                                ValuesStr = R[C].ToString();
                                if (string.IsNullOrEmpty(ValuesStr))
                                    ValuesStr = "0";
                            }
                            break;

                        case "System.DateTime":
                        case "MySql.Data.Types.MySqlDateTime":
                            switch (tmpCN.DBConfig)
                            {
                                case DBProviderType.Oracle:
                                    ValuesStr = " To_Date('" + Convert.ToDateTime(R[C]).ToString("yyyy/MM/dd HH:mm:ss") + "', 'YYYY/MM/DD HH24:MI:SS') ";
                                    break;

                                case DBProviderType.Access:
                                case DBProviderType.Access_accdb:
                                    ValuesStr = " #'" + Convert.ToDateTime(R[C]).ToString("yyyy/MM/dd HH:mm:ss") + "'# ";
                                    break;

                                default:
                                    ValuesStr = " '" + Convert.ToDateTime(R[C]).ToString("yyyy/MM/dd HH:mm:ss") + "' ";
                                    break;
                            }
                            break;

                        case "System.TimeSpan":
                            ValuesStr = " '" + R[C].ToString() + "' ";
                            break;

                        default:
                            UpdateSqlStr = "Function CreateUpdateSqlSize" + KTConstant.vbCrLf;
                            UpdateSqlStr = UpdateSqlStr + "自動產生新增SQL(Update SQL)時發生錯誤!!" + KTConstant.vbCrLf;
                            UpdateSqlStr = UpdateSqlStr + "資料型態超出範圍(不是字串也不是數字)";
                            throw new ApplicationException(UpdateSqlStr);
                    }

                    if (!string.IsNullOrEmpty(ValuesStr))
                    {
                        ColumnsToBeUpdate = ColumnsToBeUpdate + 1;
                        if (!string.IsNullOrEmpty(ColumnStr))
                            ColumnStr = ColumnStr + ",";

                        ColumnStr = ColumnStr + SpecialColumnName(C.ColumnName, tmpCN.DBConfig) + "=";
                        ColumnStr = ColumnStr + ValuesStr;
                    }

                }
            }

            if (ColumnsToBeUpdate == 0)
            {
                //No Column Need to be updated
                return "";
            }
            else
            {
                return ColumnToLong + UpdateSqlStr + " " + ColumnStr + " " + sWhereStr;
            }
        }

#if !noDapper
        public void CreateUpdateSql(object Newobj, object Oldobj, string tableName,
                                out string sqlstr, out Dapper.DynamicParameters Parameter,
                                    string whereKey, object iParam = null,
                                    string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {

            if (tableName == "")
            {
                if (Newobj.GetType().Namespace.Split(('.'))[0] == Oldobj.GetType().Namespace.Split(('.'))[0])
                    tableName = Newobj.GetType().Namespace.Split('.')[0];
                else
                    throw new ApplicationException("呼叫 CreateUpdateSql 自動產生更新SQL(Update SQL)時發生錯誤!!" + KTConstant.vbCrLf + KTConstant.vbCrLf + "要更新的資料表名稱(TableName參數)不可以是空字串!!");
            }

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB);

            switch (tmpCN.ADOConfig)
            {
                case ADOProviderType.OracleClient:
                    tableName = DataBaseLink(DBLink, tableName, sKTDB: tmpCN);
                    break;

                case ADOProviderType.MySqlClient:
                    tableName = "`" + tableName + "`";
                    break;
            }

            Parameter = new Dapper.DynamicParameters();

            //1060727 居易
            //DB Table 資料結構不能包含其他額外的自訂欄位
            //若要使用自訂欄位，必須使用 partial(部分類別) 再包一層 public Class 來當作自訂欄位
            //自訂欄位就不會被自動產生SQL語法抓到。
            //Newobj.GetType().GetNestedTypes().Count()              //還有多少子類別
            //Newobj.GetType().GetNestedTypes()[0].GetProperties()   //子類別的資料屬性
            System.Reflection.PropertyInfo[] Nprops = Newobj.GetType().GetProperties();
            System.Reflection.PropertyInfo[] Oprops = Oldobj.GetType().GetProperties();
            System.Reflection.PropertyInfo Nprop;
            System.Reflection.PropertyInfo Oprop;

            sqlstr = "";
            string PropertieName = "";

            string UpdateStr = "Update " + tableName + " set ";
            bool dataChange = false;
            System.Text.StringBuilder ColumnValueStr = new System.Text.StringBuilder();

            for (int i = 0; i <= Nprops.Count() - 1; i++)
            {
                dataChange = false;

                Nprop = Nprops[i];
                Oprop = Oprops[i];

                if (Nprop.PropertyType.Namespace == "System" && Oprop.PropertyType.Namespace == "System")
                {
                    PropertieName = Nprop.Name;

                    if ((Nprop != null) && (Oprop != null))
                    {
                        object NGetValue = Nprop.GetValue(Newobj, null);
                        object OGetValue = Oprop.GetValue(Oldobj, null);

                        if ((NGetValue != null) && (OGetValue != null))
                        {
                            if (!NGetValue.Equals(OGetValue))
                                dataChange = true;
                        }
                        else if ((NGetValue != null) && (OGetValue == null))
                        {
                            dataChange = true;
                        }
                        else if ((NGetValue == null) && (OGetValue != null))
                        {
                            dataChange = true;
                        }

                        if (dataChange)
                        {
                            if (ColumnValueStr.Length != 0) { ColumnValueStr.Append(", "); }
                            ColumnValueStr.Append(SpecialColumnName(PropertieName, tmpCN.DBConfig) + " = ");

                            switch (tmpCN.ADOConfig)
                            {
                                case ADOProviderType.Odbc:
                                case ADOProviderType.MysqlOdbc35:
                                case ADOProviderType.MysqlOdbc52:
                                case ADOProviderType.Oledb:
                                    ColumnValueStr.Append("?");
                                    break;

                                case ADOProviderType.SQLClient:
                                case ADOProviderType.MySqlClient:
                                    ColumnValueStr.Append("@" + PropertieName);
                                    break;

                                case ADOProviderType.OracleClient:
                                    ColumnValueStr.Append(":" + PropertieName);
                                    break;
                            }

                            Parameter.Add(PropertieName, NGetValue);
                        }
                    }
                }
            }

            if ((ColumnValueStr.Length != 0))
            {
                if (whereKey.Substring(0, 1) != " ") { whereKey = " " + whereKey; }

                sqlstr = UpdateStr + ColumnValueStr.ToString() + whereKey;

                //當資料確定要被更新時，且 Where 語法有匿名參數，必須到最後才做與處理判斷。
                //OleDb 的Parameter參數是順序式給值，並非以名稱給值。
                if (iParam != null)
                {
                    System.Reflection.PropertyInfo[] prop = iParam.GetType().GetProperties();
                    for (int i = 0; i <= prop.Length - 1; i++)
                    {
                        PropertieName = prop[i].Name;
                        System.Reflection.PropertyInfo c = prop[i];
                        Parameter.Add(PropertieName, c.GetValue(iParam, null));
                    }
                }
            }
        }
#endif

        public KTDBCommand CreateUpdateCommandSize(System.Data.DataRow R, string sWhereStr, string tableName = "", string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：針對異動的欄位自動產生更新 Update SQL，沒異動的欄位不更新
            //參數：R          : DataRow
            //      TableName  : 要新增的資料表名稱
            //      TableName  : 要更新的資料表名稱
            //      DBLink     : 若是 Oracle 則使用 @DBLink
            //                   若是 SqlServer 則使用 DBLink.TableName (DBLink 是 DataBaseName)
            //      TransHost  : 交易連線主機，預設 CNHOME
            //                   主要處理 A.Oracle、SqlServer 的 DataBase 
            //                            B.資料庫格式的時間函式
            //回傳：Sql 語法字串
            //
            //注意：執行本程式後，若沒有欄位需要更新，則傳回空字串
            //     所以必須須自行判斷，若回傳直為空字串則不要執行更新

            if (string.IsNullOrEmpty(tableName))
                tableName = R.Table.TableName;

            if (string.IsNullOrEmpty(tableName) | tableName == "TABLE")
            {
                throw new ApplicationException("呼叫 CreateUpdateCommandSize 自動產生更新SQL(Update SQL)時發生錯誤!!" + KTConstant.vbCrLf + KTConstant.vbCrLf + "要更新的資料表名稱(TableName參數)不可以是空字串!!");
            }

            if (string.IsNullOrEmpty(sWhereStr))
            {
                throw new ApplicationException("呼叫 CreateUpdateCommandSize 自動產生更新SQL(Update SQL)時發生錯誤!!" + KTConstant.vbCrLf + KTConstant.vbCrLf + "更新條件(sWhereStr參數)不可以為空字串!!");
            }

            //System.Text.StringBuilder UpdateSqlStr = new System.Text.StringBuilder();
            //System.Text.StringBuilder ColumnStr = new System.Text.StringBuilder();

            string ColumnToLong = "";

            if (!string.IsNullOrEmpty(DBLink))
                DBLink = DBLink.ToUpper();

            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB);

            ColumnToLong = ColumnsOverLong(R, tableName, DBLink, TransHost, sKTDB);
            if (!string.IsNullOrEmpty(ColumnToLong))
            {
                TableCns_[DBLink + TransHost].Table.Remove(tableName);
                ColumnToLong = ColumnsOverLong(R, tableName, DBLink, TransHost, sKTDB);
                if (!string.IsNullOrEmpty(ColumnToLong))
                    ColumnToLong = ColumnToLong + KTConstant.vbCrLf + KTConstant.vbCrLf;
            }

            switch (tmpCN.DBConfig)
            {
                case DBProviderType.Oracle:
                    tableName = DataBaseLink(DBLink, tableName, TransHost, sKTDB);
                    break;

                case DBProviderType.SQLServer:
                    //0960907 居易
                    //SqlServer 也可以跨資料庫 查詢, 交易(只要權限夠 @@)
                    //目前僅知到單一主機有多資料庫的時候可以這樣做
                    //跨主機的部份需要再去找資料
                    if (!string.IsNullOrEmpty(DBLink))
                        tableName = DBLink + "." + tableName;
                    break;
            }

            string UpdateStr = "Update " + tableName + " set ";
            System.Text.StringBuilder ColumnValueStr = new System.Text.StringBuilder();

            foreach (System.Data.DataColumn C in R.Table.Columns)
            {

                if (R[C].ToString() + "" != R[C, DataRowVersion.Original].ToString())
                {
                    switch (C.DataType.ToString())
                    {
                        case "System.String":
                            break;

                        case "System.Decimal":
                        case "System.Short":
                        case "System.Integer":
                        case "System.Long":
                        case "System.Single":
                        case "System.Double":
                        case "System.Int16":
                        case "System.Int32":
                        case "System.Int64":
                            break;

                        case "System.TimeSpan":
                        case "System.DateTime":
                        case "MySql.Data.Types.MySqlDateTime":
                            break;

                        case "System.Byte[]":
                            break;

                        default:
                            ColumnValueStr.Append("Function CreateUpdateCommandSize" + KTConstant.vbCrLf);
                            ColumnValueStr.Append("自動產生新增SQL(Update SQL)時發生錯誤!!" + KTConstant.vbCrLf);
                            ColumnValueStr.Append("資料型態超出範圍(不是字串也不是數字)");
                            throw new ApplicationException(ColumnValueStr.ToString());
                    }

                    if (ColumnValueStr.Length != 0) { ColumnValueStr.Append(", "); }
                    ColumnValueStr.Append(SpecialColumnName(C.ColumnName, tmpCN.DBConfig) + " = ");

                    switch (tmpCN.DBConfig)
                    {
                        case DBProviderType.Oracle:
                            ColumnValueStr.Append(":" + C.ColumnName);
                            break;

                        case DBProviderType.SQLServer:
                        case DBProviderType.MySql:
                            ColumnValueStr.Append("@" + C.ColumnName);
                            break;

                        default:
                            ColumnValueStr.Append("?");
                            break;
                    }
                }
            }

            if (ColumnValueStr.Length == 0)
            {
                //No Column Need to be updated
                return null;
            }

            if (sWhereStr.Substring(0, 1) != " ") sWhereStr = " " + sWhereStr;

            //判斷 CLOB Column
            System.Collections.Specialized.StringCollection CLOBColumn = null;
            string[] CLOBColumns = new string[0];
            if (TableCns_.ContainsKey(DBLink + TransHost))
            {
                if (TableCns_[DBLink + TransHost].Table.ContainsKey(tableName))
                {
                    DataTable tblTableSchema = TableCns_[DBLink + TransHost].Table[tableName].TableSchema;
                    DataRow[] rowTableSchemas = tblTableSchema.Select("ColumnSize = -1");
                    if (rowTableSchemas.Length > 0)
                    {
                        CLOBColumns = new string[rowTableSchemas.Length];
                        CLOBColumn = new System.Collections.Specialized.StringCollection();
                        for (int x = 0; x <= rowTableSchemas.Length - 1; x++)
                        {
                            CLOBColumn.Add(rowTableSchemas[x][0].ToString());
                        }
                        CLOBColumn.CopyTo(CLOBColumns, 0);
                    }
                }
            }

            KTDBCommand KTCm = new KTDBCommand();
            KTCm.KTDBProvider = tmpCN;
            KTCm.DataRow = R;
            KTCm.TableName = tableName;
            KTCm.CommandText = ColumnToLong + UpdateStr.ToString() + ColumnValueStr.ToString() + sWhereStr;
            if (CLOBColumns.Length > 0)
                KTCm.CLOBColumn = String.Join(",", CLOBColumns);
            KTCm.UpdateParameters();
            return KTCm;

        }

        #region " TableCns 集合，DataTable與DataRow 才有使用到 "

        //主機的 Table 集合
        private System.Collections.Generic.Dictionary<string, nsTableProvider.TableProvider> TableCns_ = new System.Collections.Generic.Dictionary<string, nsTableProvider.TableProvider>();
        private System.Collections.Generic.Dictionary<string, nsTableProvider.TableProvider> TableCns
        {
            get { return TableCns_; }
            set { TableCns_ = value; }
        }

        private DataTable TableSchemaCollection(string tableName, string DBLink, string TransHost, KTDBProvider sKTDB = null)
        {
            //作者：吳居易
            //日期：101-04-04
            //功能：抓取交易連線中 Table 的結構
            //參數：TableName : 要抓取的 TableName
            //      TransHost : 要抓取的 交易連線主機
            //回傳：傳回型態為 DataTable，是指定 TableName 的結構
            //      內容包含 ColumnName、ColumnSize、DataType、NumericPrecision、NumericScale
            //備註：使用 DataReader 擷取 Table 的結構值
            //      每一條交易連線主機會有不同的 TableName
            //      為了避免 TableName 衝突 (如: 同時連線多個 mdb，mdb 有多個 TableName 相同)
            //      故使用 Collections 集合來記錄不同連線主機所記錄的 TableName 結構。
            //
            //集合：其中定義了四個泛型集合 ColumnSize、DataType、NumericPrecision、NumericScale
            //      集合的處理，每 10000 筆紀錄 比 DataTable.Select 快了 8 秒
            //
            //使用方式：參考 CreateUpdateSqlSize、InsertIntoSqlSize

            DataTable tblSchema = new DataTable();

            //假設程式設計師都知道目前的交易連線，與正要運作的 TableName
            //如果丟了錯誤的 TableName 則會產生錯誤

            if (!string.IsNullOrEmpty(tableName) & !string.IsNullOrEmpty(TransHost))
            {
                string TrueDBHost = DBLink + TransHost;
                string ColumnName = "";
                if (!TableCns_.ContainsKey(TrueDBHost))
                {
                    //交易連線必須 <> "" 才能夠使用，就算是 CNHOME，也要丟入 CNHOME，否則預設的話會出錯
                    nsTableProvider.TableProvider TransHostCns = new nsTableProvider.TableProvider();
                    TransHostCns.TransHost = TransHost;
                    TableCns_.Add(TrueDBHost, TransHostCns);
                    TransHostCns.Dispose();
                }

                if (!TableCns_[TrueDBHost].Table.ContainsKey(tableName))
                {
                    tblSchema = GetTableSchema(tableName, DBLink, TransHost, sKTDB);

                    if (tblSchema.Columns.Count == 0)
                    {
                        TableCns_[TrueDBHost].Table.Add(tableName, new nsTableProvider.TableProvider());
                    }

                    if (tblSchema.Rows.Count > 0)
                    {
                        nsTableProvider.TableProvider TableNameCns = new nsTableProvider.TableProvider();

                        System.Collections.Generic.Dictionary<string, string> DataType = new System.Collections.Generic.Dictionary<string, string>();
                        System.Collections.Generic.Dictionary<string, Int32> ColumnSize = new System.Collections.Generic.Dictionary<string, Int32>();
                        System.Collections.Generic.Dictionary<string, short> NumericPrecision = new System.Collections.Generic.Dictionary<string, short>();
                        System.Collections.Generic.Dictionary<string, short> NumericScale = new System.Collections.Generic.Dictionary<string, short>();
                        System.Collections.Generic.Dictionary<string, string> Comments = new System.Collections.Generic.Dictionary<string, string>();

                        TableNameCns.TransHost = TransHost;
                        TableNameCns.TableName = tableName;
                        TableNameCns.TableSchema = tblSchema;
                        TableNameCns.TableSchema.TableName = tableName;
                        if (!string.IsNullOrEmpty(tblSchema.Rows[0]["TableComments"].ToString()))
                        {
                            TableNameCns.TableComments = tblSchema.Rows[0]["TableComments"].ToString();
                        }

                        for (int x = 0; x <= tblSchema.Rows.Count - 1; x++)
                        {
                            ColumnName = tblSchema.Rows[x]["ColumnName"].ToString().ToLower();

                            DataType.Add(ColumnName, tblSchema.Rows[x]["DataType"].ToString());
                            ColumnSize.Add(ColumnName, (Int32)tblSchema.Rows[x]["ColumnSize"]);

                            NumericPrecision.Add(ColumnName, (short)(!string.IsNullOrEmpty(tblSchema.Rows[x]["NumericPrecision"].ToString()) ? tblSchema.Rows[x]["NumericPrecision"] : (short)0));
                            NumericScale.Add(ColumnName, (short)(!string.IsNullOrEmpty(tblSchema.Rows[x]["NumericScale"].ToString()) ? tblSchema.Rows[x]["NumericScale"] : (short)0));
                            if (!string.IsNullOrEmpty(tblSchema.Rows[x]["Comments"].ToString()))
                            {
                                Comments.Add(ColumnName, tblSchema.Rows[x]["Comments"].ToString());
                            }
                        }

                        TableNameCns.DataType = DataType;
                        TableNameCns.ColumnSize = ColumnSize;
                        TableNameCns.NumericPrecision = NumericPrecision;
                        TableNameCns.NumericScale = NumericScale;
                        TableNameCns.Comments = Comments;

                        TableCns_[TrueDBHost].Table.Add(tableName, TableNameCns);

                        TableNameCns.Dispose();
                    }
                }

                if (TableCns_[TrueDBHost].Table.ContainsKey(tableName))
                {
                    //直接指向記憶體位置，而不使用 DataTable.Copy，因為 每 10000 筆資料慢了 6~7 秒
                    tblSchema = TableCns_[TrueDBHost].Table[tableName].TableSchema;
                }
            }

            return tblSchema;
        }

        private string ColumnsOverLong(DataRow myRow, string tableName, string DBLink, string TransHost, KTDBProvider sKTDB = null)
        {
            //作者：吳居易
            //日期：101-04-04
            //功能：判斷 DataRow 在連線主機資料表其結構資料值的大小是否太長(OverLong)
            //參數：myRow     : 要判斷的 DataRow
            //      tableName : 資料表名稱
            //      TransName : 連線交易主機
            //
            //回傳：當資料欄位有太長的資料，則回傳 索引欄位.欄位名稱
            //      當資料欄位正常，則回傳空字串
            //
            //速度：測試欄位 120 個(PRS006)，每 10000 筆的紀錄判斷，只增加 3 秒的時間

            //1060713 居易
            //Oracle 12g 自己會提示新增或修改的欄位哪一個太長
            //但是其他的型別的資料庫或舊版版本的 Oracle 並沒有提供此功能

            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB);

            switch (tmpCN.DBConfig)
            {
                case DBProviderType.Excel:
                case DBProviderType.Excel_xlsx:
                //Excel 的欄位大小可以無限延伸
                case DBProviderType.Oracle:
                    //Oracle 10以上的資料庫本身就有提示哪一個欄位的資料太長
                    //12c提示文字為中文
                    return "";
            }

            if (!string.IsNullOrEmpty(DBLink))
                DBLink = DBLink.ToUpper();

            TableSchemaCollection(tableName, DBLink, TransHost, sKTDB);

            string TrueDBHost = DBLink + TransHost;

            if (TableCns_[TrueDBHost].Table[tableName].TableSchema == null)
                return "";

            string columnName = "";
            string ColumnNameStr = "";
            if (TableCns_[TrueDBHost].Table.ContainsKey(tableName))
            {

                System.Collections.Generic.Dictionary<string, string> DataType = default(System.Collections.Generic.Dictionary<string, string>);
                System.Collections.Generic.Dictionary<string, Int32> ColumnSize = default(System.Collections.Generic.Dictionary<string, Int32>);
                System.Collections.Generic.Dictionary<string, short> NumericPrecision = default(System.Collections.Generic.Dictionary<string, short>);
                System.Collections.Generic.Dictionary<string, short> NumericScale = default(System.Collections.Generic.Dictionary<string, short>);
                System.Collections.Generic.Dictionary<string, string> Comments = default(System.Collections.Generic.Dictionary<string, string>);

                DataTable tblSchema = TableCns_[TrueDBHost].Table[tableName].TableSchema;

                DataType = TableCns_[TrueDBHost].Table[tableName].DataType;
                ColumnSize = TableCns_[TrueDBHost].Table[tableName].ColumnSize;
                NumericPrecision = TableCns_[TrueDBHost].Table[tableName].NumericPrecision;
                NumericScale = TableCns_[TrueDBHost].Table[tableName].NumericScale;
                Comments = TableCns_[TrueDBHost].Table[tableName].Comments;

                for (int x = 0; x <= myRow.Table.Columns.Count - 1; x++)
                {
                    columnName = myRow.Table.Columns[x].ColumnName.ToLower();
                    if (!string.IsNullOrEmpty(myRow[columnName].ToString()))
                    {
                        switch (DataType[columnName])
                        {
                            case "System.String":
                                if (DBStrLentgh(myRow[columnName].ToString(), tblSchema.Rows[x]["DataTypeName"].ToString(), tmpCN.DBConfig) > ColumnSize[columnName] & ColumnSize[columnName] > -1)
                                {
                                    if (!string.IsNullOrEmpty(ColumnNameStr))
                                        ColumnNameStr = ColumnNameStr + ",";
                                    ColumnNameStr = ColumnNameStr + (x + 1).ToString() + "." + columnName;
                                    if (Comments.ContainsKey(columnName))
                                    {
                                        ColumnNameStr = ColumnNameStr + "(" + Comments[columnName].Split(new string[] { KTConstant.vbCrLf }, StringSplitOptions.None)[0].Trim() + ")";
                                    }
                                }
                                break;

                            case "System.Decimal":
                            case "System.Short":
                            case "System.Integer":
                            case "System.Long":
                            case "System.Single":
                            case "System.Double":
                            case "System.Int16":
                            case "System.Int32":
                            case "System.Int64":
                                string[] Decimals = myRow[columnName].ToString().Split();
                                switch (Decimals.Length)
                                {
                                    case 1:
                                        if (Decimals[0].Length > NumericPrecision[columnName])
                                        {
                                            if (!string.IsNullOrEmpty(ColumnNameStr))
                                                ColumnNameStr = ColumnNameStr + ",";
                                            ColumnNameStr = ColumnNameStr + (x + 1).ToString() + "." + columnName;
                                            if (Comments.ContainsKey(columnName))
                                            {
                                                ColumnNameStr = ColumnNameStr + "(" + Comments[columnName].Split(new string[] { KTConstant.vbCrLf }, StringSplitOptions.None)[0].Trim() + ")";
                                            }
                                        }
                                        break;

                                    case 2:
                                        if (Decimals[0].Length > NumericPrecision[columnName])
                                        {
                                            if (!string.IsNullOrEmpty(ColumnNameStr))
                                                ColumnNameStr = ColumnNameStr + ",";
                                            ColumnNameStr = ColumnNameStr + (x + 1).ToString() + "." + columnName;
                                            if (Comments.ContainsKey(columnName))
                                            {
                                                ColumnNameStr = ColumnNameStr + "(" + Comments[columnName].Split(new string[] { KTConstant.vbCrLf }, StringSplitOptions.None)[0].Trim() + ")";
                                            }
                                            break; // TODO: might not be correct. Was : Exit Select
                                            //避免欄位抓到兩次
                                        }
                                        if (Decimals[1].Length > NumericScale[columnName])
                                        {
                                            if (!string.IsNullOrEmpty(ColumnNameStr))
                                                ColumnNameStr = ColumnNameStr + ",";
                                            ColumnNameStr = ColumnNameStr + (x + 1).ToString() + "." + columnName;
                                            if (Comments.ContainsKey(columnName))
                                            {
                                                ColumnNameStr = ColumnNameStr + "(" + Comments[columnName].Split(new string[] { KTConstant.vbCrLf }, StringSplitOptions.None)[0].Trim() + ")";
                                            }
                                        }
                                        break;
                                }
                                break;

                            case "System.TimeSpan":
                            case "System.DateTime":
                            case "MySql.Data.Types.MySqlDateTime":
                                //日期型態不會發生欄位太長
                                break;

                            case "System.Byte[]":
                                break;

                            default:
                                ErrorMessage_.Length = 0;
                                ErrorMessage_.Append("Function ColumnsOverLong" + KTConstant.vbCrLf);
                                ErrorMessage_.Append("尚未定義的資料型態 : " + DataType[columnName]);
                                throw new ApplicationException(ErrorMessage_.ToString());
                        }
                    }
                }
            }

            if (!string.IsNullOrEmpty(ColumnNameStr))
            {
                ColumnNameStr = "TableName " + tableName + " column's data is overlong, column Name the following are : " + KTConstant.vbCrLf + "資料表 " + tableName + " 欄位的資料太長，欄位名稱如下: " + KTConstant.vbCrLf + ColumnNameStr;
            }

            return ColumnNameStr;

        }

        public string ColumnsOverLong<T>(IEnumerable<T> myModel, string tableName, string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            //作者：吳居易
            //日期：106-08-30
            //功能：判斷 Model 在連線主機資料表其結構資料值的大小是否太長(OverLong)
            //參數： myModel  : 要判斷的 DapperRow
            //       tableName : 資料表名稱
            //       TransName : 連線交易主機
            //
            //回傳：當資料欄位有太長的資料，則回傳 索引欄位.欄位名稱
            //      當資料欄位正常，則回傳空字串
            //
            //速度：測試欄位 120 個(PRS006)，每 10000 筆的紀錄判斷，只增加 3 秒的時間

            //1060713 居易
            //Oracle 12g 自己會提示新增或修改的欄位哪一個太長
            //但是其他的型別的資料庫或舊版版本的 Oracle 並沒有提供此功能

            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB);

            switch (tmpCN.DBConfig)
            {
                case DBProviderType.Excel:
                case DBProviderType.Excel_xlsx:
                    //Excel 的欄位大小可以無限延伸 
                    return "";
            }

            if (!string.IsNullOrEmpty(DBLink))
                DBLink = DBLink.ToUpper();

            TableSchemaCollection(tableName, DBLink, TransHost, sKTDB);

            string TrueDBHost = DBLink + TransHost;

            if (TableCns_[TrueDBHost].Table[tableName].TableSchema == null)
                return "";

            string ColumnNameStr = "";
            string columnName = "";
            string CommentsTemp = "";
            object NGetValue = null;
            Int32 NowCount = 0;
            Int32 errCount = 0;
            bool rowError = false;

            if (TableCns_[TrueDBHost].Table.ContainsKey(tableName))
            {
                System.Collections.Generic.Dictionary<string, string> DataType = default(System.Collections.Generic.Dictionary<string, string>);
                System.Collections.Generic.Dictionary<string, Int32> ColumnSize = default(System.Collections.Generic.Dictionary<string, Int32>);
                System.Collections.Generic.Dictionary<string, short> NumericPrecision = default(System.Collections.Generic.Dictionary<string, short>);
                System.Collections.Generic.Dictionary<string, short> NumericScale = default(System.Collections.Generic.Dictionary<string, short>);
                System.Collections.Generic.Dictionary<string, string> Comments = default(System.Collections.Generic.Dictionary<string, string>);

                DataTable tblSchema = TableCns_[TrueDBHost].Table[tableName].TableSchema;

                DataType = TableCns_[TrueDBHost].Table[tableName].DataType;
                ColumnSize = TableCns_[TrueDBHost].Table[tableName].ColumnSize;
                NumericPrecision = TableCns_[TrueDBHost].Table[tableName].NumericPrecision;
                NumericScale = TableCns_[TrueDBHost].Table[tableName].NumericScale;
                Comments = TableCns_[TrueDBHost].Table[tableName].Comments;

                System.Reflection.PropertyInfo[] prop = null;

                foreach (object row in myModel)
                {
                    rowError = false;
                    NowCount = NowCount + 1;
                    prop = row.GetType().GetProperties();

                    for (int x = 0; x <= prop.Length - 1; x++)
                    {
                        if (prop[x].PropertyType.Namespace == "System")
                        {
                            NGetValue = prop[x].GetValue(row, null);
                            if (NGetValue != null)
                            {
                                columnName = prop[x].Name;
                                switch (DataType[columnName])
                                {
                                    case "System.String":
                                        if (DBStrLentgh(NGetValue.ToString(), tblSchema.Rows[x]["DataTypeName"].ToString(), tmpCN.DBConfig) > ColumnSize[columnName] & ColumnSize[columnName] > -1)
                                        {
                                            if ((!rowError) && (myModel.Count() > 1))
                                                ColumnNameStr = ColumnNameStr + "第" + KTString.AddRightSpace(NowCount, 4) + "筆 ";

                                            if (!string.IsNullOrEmpty(ColumnNameStr) && rowError)
                                                ColumnNameStr = ColumnNameStr + ",";

                                            ColumnNameStr = ColumnNameStr + (x + 1).ToString() + "." + columnName;

                                            if (Comments.ContainsKey(columnName))
                                            {
                                                CommentsTemp = Comments[columnName];
                                                CommentsTemp = KTString.Split(CommentsTemp, "\r")[0];
                                                CommentsTemp = KTString.Split(CommentsTemp, "\n")[0];
                                                CommentsTemp = CommentsTemp.Trim();
                                                ColumnNameStr = ColumnNameStr + "(" + CommentsTemp + ")";
                                            }

                                            rowError = true;
                                        }
                                        break;

                                    case "System.Decimal":
                                    case "System.Short":
                                    case "System.Integer":
                                    case "System.Long":
                                    case "System.Single":
                                    case "System.Double":
                                    case "System.Int16":
                                    case "System.Int32":
                                    case "System.Int64":
                                        string[] Decimals = NGetValue.ToString().Split();

                                        if (Decimals[0].Length > NumericPrecision[columnName])
                                        {
                                            if ((!rowError) && (myModel.Count() > 1))
                                                ColumnNameStr = ColumnNameStr + "第" + KTString.AddRightSpace(NowCount, 4) + "筆 ";

                                            if (!string.IsNullOrEmpty(ColumnNameStr) && rowError)
                                                ColumnNameStr = ColumnNameStr + ",";

                                            ColumnNameStr = ColumnNameStr + (x + 1).ToString() + "." + columnName;

                                            if (Comments.ContainsKey(columnName))
                                            {
                                                CommentsTemp = Comments[columnName];
                                                CommentsTemp = KTString.Split(CommentsTemp, "\r")[0];
                                                CommentsTemp = KTString.Split(CommentsTemp, "\n")[0];
                                                CommentsTemp = CommentsTemp.Trim();
                                                ColumnNameStr = ColumnNameStr + "(" + CommentsTemp + ")";
                                            }

                                            rowError = true;

                                            break;
                                        }

                                        if (Decimals.Length == 2)
                                            if (Decimals[1].Length > NumericScale[columnName])
                                            {
                                                if ((!rowError) && (myModel.Count() > 1))
                                                    ColumnNameStr = ColumnNameStr + "第" + KTString.AddRightSpace(NowCount, 4) + "筆 ";

                                                if (!string.IsNullOrEmpty(ColumnNameStr) && rowError)
                                                    ColumnNameStr = ColumnNameStr + ",";

                                                ColumnNameStr = ColumnNameStr + (x + 1).ToString() + "." + columnName;

                                                if (Comments.ContainsKey(columnName))
                                                {
                                                    CommentsTemp = Comments[columnName];
                                                    CommentsTemp = KTString.Split(CommentsTemp, "\r")[0];
                                                    CommentsTemp = KTString.Split(CommentsTemp, "\n")[0];
                                                    CommentsTemp = CommentsTemp.Trim();
                                                    ColumnNameStr = ColumnNameStr + "(" + CommentsTemp + ")";
                                                }

                                                rowError = true;
                                            }

                                        break;

                                    case "System.TimeSpan":
                                    case "System.DateTime":
                                    case "MySql.Data.Types.MySqlDateTime":
                                        //日期型態不會發生欄位太長
                                        break;

                                    case "System.Byte[]":
                                        //LONG RAW
                                        break;

                                    default:
                                        ErrorMessage_.Length = 0;
                                        ErrorMessage_.Append("Function ColumnsOverLong" + KTConstant.vbCrLf);
                                        ErrorMessage_.Append("尚未定義的資料型態 : " + DataType[columnName]);
                                        throw new ApplicationException(ErrorMessage_.ToString());
                                }
                            }
                        }
                    }

                    if (rowError)
                    {
                        errCount = errCount + 1;

                        if (errCount > 9)
                            break;

                        ColumnNameStr = ColumnNameStr + KTConstant.vbCrLf;
                    }
                }
            }

            if (!string.IsNullOrEmpty(ColumnNameStr))
            {
                //ColumnNameStr = "TableName " + tableName + " column's data is overlong, column Name the following are : " + KTConstant.vbCrLf + "資料表 " + tableName + " 欄位的資料太長，欄位名稱如下: " + KTConstant.vbCrLf + ColumnNameStr;
                ColumnNameStr = "資料表 " + tableName + " 欄位資料太長" + (errCount > 9 ? "(已" + errCount + "筆，後面暫省略)" : "") + "，欄位名稱如下: " + KTConstant.vbCrLf + ColumnNameStr;
            }

            return ColumnNameStr;

        }

        private int DBStrLentgh(string wkStr, string DataType, DBProviderType DBConfig)
        {

            int StrLentgh = 0;

            try
            {
                byte[] strBig5 = new byte[-1 + 1];

                switch (DBConfig)
                {
                    case DBProviderType.MySql:

                        //如果某一個欄位長度是 varchar(10)
                        //那麼英文字的儲存長度一定是 10，
                        //中文utf-8的占3個字節，大概可以儲存 3個中文字 !
                        //但是在 mysql 裡面 不管中文、英文都是存 10 個字
                        //mysql 長度太長，會自動將字串截斷，但是程式不會回應錯誤，
                        StrLentgh = wkStr.Length;
                        break;

                    case DBProviderType.Oracle:
                        StrLentgh = System.Text.Encoding.UTF8.GetBytes(wkStr).Length;
                        break;

                    case DBProviderType.SQLServer:
                        switch (DataType.ToLower())
                        {
                            case "nvarchar":
                            case "nchar":
                                //n系列開頭的字串，每個字節的中文與英文長度都相同
                                StrLentgh = wkStr.Length;
                                break;

                            case "varchar":
                            case "char":
                                StrLentgh = System.Text.Encoding.GetEncoding(950).GetBytes(wkStr).Length;
                                break;
                        }
                        break;

                    default:
                        StrLentgh = System.Text.Encoding.GetEncoding(950).GetBytes(wkStr).Length;
                        break;
                }

            }
            catch (Exception)
            {
                return 0;
            }

            return StrLentgh;

        }

        #endregion

        private DataTable GetTableSchema(string TableName, string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            //功能：取得 資料表的 屬性值與描述
            //日期：0990420
            //參數：TableName：資料表
            //      ConnHost ：連線交易的主機名稱
            //      DBlink   ：DataBaseLink
            //欄位：ColumnName, ColumnOrdinal, ColumnSize, NumericPrecision, NumericScale, DataType.ToString, Comments(自訂增加)
            //      欄位名稱   , 欄位次序       , 欄位大小   , 數字的整數長度     , 數字的小數長度 , 資料型別          , 備註說明(描述)
            //
            //範例：     oracle --> tbl = KTCC.getTableSchema("PRS013")
            //       sql Server --> tbl = KTCC.getTableSchema("News", "", WEBDBA)
            //
            //備註：此功能是使用 rdr 的 rdr.GetSchemaTable 取得 .NET程式預設的架構內容
            //      .NET 2.X 以上還有 New Connection.GetSchema("Tables")   '取得主機所有 Table
            //                        New Connection.GetSchema("Columns")  '取得主機所有 Columns
            //
            //0970117 居易 
            //DoSql 時傳入 DataReader 可以取得 Table Schema 字串長度, 內容的呈現方式為
            //rdrTable.GetSchemaTable --> DataTable 屬性 [Rows[0] 一定只有一筆]

            if (!string.IsNullOrEmpty(DBLink))
                DBLink = DBLink.ToUpper();

            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB);

            IDataReader rdr = null;
            System.Text.StringBuilder sqlstr = new System.Text.StringBuilder();

            switch (tmpCN.DBConfig)
            {
                case DBProviderType.Oracle:

                    if (!string.IsNullOrEmpty(DBLink))
                    {
                        if (DBLink != tmpCN.HostName)
                        {
                            //1050611 檢驗主機沒有 RPS3010 在判斷 DBLink 時發生異常
                            //        這裡是程式自動做處理的，
                            //        沒有被定義的主機就先不要進入查詢 PRS3010 
                            string LocalArea = tmpCN.HostName;
                            switch (tmpCN.HostName)
                            {
                                case "KTGH00":
                                case "HPK210":
                                case "TSHIS":
                                case "KTXS00":
                                case "KTGH03":
                                    LocalArea = "";
                                    //是當院區主機，DBlink 不要傳送
                                    break;

                                case "KTGH01":
                                    LocalArea = "KTGH00";
                                    break;

                                case "KTXS01":
                                    LocalArea = "KTXS00";
                                    break;

                                case "EMRDB":
                                    LocalArea = "KTGH00";
                                    break;

                                case "KTGHTA01":
                                    LocalArea = "HPK210";
                                    break;

                                case "TSEMR":
                                    LocalArea = "TSHIS";
                                    break;
                            }

                            string tmpDBLink = DBLink;
                            switch (DBLink)
                            {
                                case "KTGH00":
                                case "HPK210":
                                case "TSHIS":
                                case "KTXS00":
                                case "KTGH03":
                                    break;

                                case "KTGH01":
                                    tmpDBLink = "KTGH00";
                                    break;

                                case "KTXS01":
                                    tmpDBLink = "KTXS00";
                                    break;

                                case "EMRDB":
                                    tmpDBLink = "KTGH00";
                                    break;

                                case "KTGHTA01":
                                    tmpDBLink = "HPK210";
                                    break;

                                case "TSEMR":
                                    tmpDBLink = "TSHIS";
                                    break;
                            }

                            if (!string.IsNullOrEmpty(tmpDBLink))
                            {
                                //交易院區 LocalArea，要拿去找 getDBLinkState 狀態的位置。
                                getDBLinkState(tmpCN, TransHost, LocalArea);
                                if (!getDBLinkState(tmpDBLink))
                                {
                                    sqlstr.Length = 0;
                                    sqlstr.AppendLine("Select * From all_Tab_Columns ");
                                    sqlstr.AppendLine(" where Owner Not in ('SYS', 'SYSTEM') ");
                                    sqlstr.AppendLine("   And TABLE_NAME = '" + TableName + "'");
                                    DoSql(sqlstr.ToString(), out rdr, TransHost, tmpCN);
                                    if (!DoSqlOK) return new DataTable(TableName);

                                    //目前連線的主機院區也有相同 Table
                                    if (rdr.Read()) DBLink = "";
                                    rdr.Close();

                                    //如果目前院區也沒有相同的 Table
                                    if (!string.IsNullOrEmpty(DBLink)) return new DataTable(TableName);
                                }
                            }
                        }
                    }
                    break;
            }

            DataTable rtntbl = new DataTable(TableName);
            DataRow[] rtnrow = null;
            DataColumn rtncol = default(DataColumn);
            DataTable tbl = new DataTable(TableName);

            sqlstr.Length = 0;
            switch (tmpCN.DBConfig)
            {
                case DBProviderType.Oracle:
                    sqlstr.Append("Select * From " + DataBaseLink(DBLink, TableName, TransHost, sKTDB) + " Where rowNum = 1 ");
                    sqlstr.Append(" ----T1連線註記檢查_吳居易_KTConnectionControler_Function_GetTableSchema");
                    break;

                case DBProviderType.SQLServer:
                    sqlstr.Append("Select Top 1 * From " + (!string.IsNullOrEmpty(DBLink) ? DBLink + "." + TableName : TableName));
                    break;

                case DBProviderType.Access:
                case DBProviderType.Access_accdb:
                    sqlstr.Append("Select Top 1 * From " + TableName);
                    break;

                case DBProviderType.Excel:
                case DBProviderType.Excel_xlsx:
                    //TableName 必須包含 [$]，所以格式必須作判斷與調整
                    //目前還沒有人塞資料到 Excel，大多都使用 ComponentOne 的 c1.Excel 來轉檔

                    string tmpTableName = TableName;
                    if (tmpTableName.IndexOf("$") == -1)
                        tmpTableName = "[" + tmpTableName + "$] ";
                    if (tmpTableName.IndexOf("[") == -1)
                        tmpTableName = "[" + tmpTableName + "] ";

                    sqlstr.Append("Select Top 1 * From " + tmpTableName);
                    break;

                case DBProviderType.MySql:
                    sqlstr.Append("Select * From " + TableName + " limit 0, 1 ");
                    break;

                default:
                    //Postgre 語法 須研究
                    return rtntbl.Copy();
            }

            DoSql(sqlstr.ToString(), out rdr, TransHost, tmpCN);
            rtntbl = rdr.GetSchemaTable().Copy();
            rdr.Close();

            if (rtntbl.Rows.Count > 0)
            {
                sqlstr.Length = 0;

                switch (tmpCN.DBConfig)
                {
                    case DBProviderType.SQLServer:
                        //exec sp_addextendedproperty    N'MS_Description', N'表格說明', N'user', N'dbo', N'table', N'資料表名稱', default, default
                        //exec sp_updateextendedproperty N'MS_Description', N'表格說明', N'user', dbo, 'table', N'資料表名稱', default, default
                        //Select * From ::fn_listextendedproperty('MS_Description', 'user', 'dbo', 'table', '" & TableName "', default, default)  '表備註

                        //exec sp_addextendedproperty    N'MS_Description', N'欄位說明', N'user', N'dbo', N'table', N'資料表名稱', N'column', N'資料表欄位名稱'
                        //exec sp_updateextendedproperty N'MS_Description', N'欄位說明', N'user', dbo, 'table', N'資料表名稱', N'column', N'資料表欄位名稱'
                        //Select * From ::fn_listextendedproperty('MS_Description', 'user', 'dbo', 'table', '" & TableName "', 'column', default) '欄備註

                        sqlstr.AppendLine(" Select Sysobjects.name AS TableName, ");
                        //sqlstr.AppendLine("        (Select Value From Sysproperties Where sysproperties.id =  sysobjects.id And sysproperties.smallid =  0) As TableComments, ")
                        sqlstr.AppendLine("        '' As TableComments, ");
                        //sql server 2005 以後拿掉 Sysproperties table
                        sqlstr.AppendLine("        syscolumns.name AS ColumnName, ");
                        sqlstr.AppendLine("        syscolumns.colid As ColumnOrdinal, ");
                        sqlstr.AppendLine("        systypes.name AS DataType, ");
                        sqlstr.AppendLine("        syscolumns.prec AS ColumnSize, ");
                        //sqlstr.AppendLine("        (Select Value From Sysproperties Where sysproperties.id =  sysobjects.id And sysproperties.smallid =  syscolumns.colid) As Comments ")
                        sqlstr.AppendLine("        '' As Comments ");
                        //sql server 2005 以後拿掉 Sysproperties table
                        sqlstr.AppendLine("   From sysobjects, syscolumns, systypes ");
                        sqlstr.AppendLine("  Where sysobjects.name = '" + TableName + "' ");
                        sqlstr.AppendLine("    And sysobjects.id = syscolumns.id ");
                        sqlstr.AppendLine("    And syscolumns.xtype = systypes.xtype ");
                        sqlstr.AppendLine("    And sysobjects.xtype = 'U' ");
                        sqlstr.AppendLine("    And systypes.name <> 'sysname'");
                        sqlstr.AppendLine("  Order by syscolumns.colid ");
                        DoSql(sqlstr.ToString(), tbl, TransHost, tmpCN);
                        break;

                    case DBProviderType.Oracle:
                        //COMMENT ON TABLE 資料表名稱 IS '表格說明';
                        //COMMENT ON COLUMN 資料表名稱.資料表欄位 IS '欄位說明';

                        sqlstr.Length = 0;
                        sqlstr.AppendLine(" Select allTab.Table_Name As TableName, ");
                        sqlstr.AppendLine("        (Select Comments From " + DataBaseLink(DBLink, "User_Tab_Comments", TransHost, tmpCN) + " UserTab Where UserTab.table_name = allTab.Table_Name) as TableComments, ");
                        sqlstr.AppendLine("        allTab.Column_Name As ColumnName, ");
                        sqlstr.AppendLine("        allTab.Column_Id As ColumnOrdinal, ");
                        sqlstr.AppendLine("        allTab.Data_Type As DataType, ");
                        sqlstr.AppendLine("        Case when allTab.data_type = 'NUMBER' then allTab.data_precision when allTab.Char_USED = 'C' Then allTab.Char_Length else allTab.data_length end as ColumnSize, ");
                        sqlstr.AppendLine("        '' as Comments ");
                        sqlstr.AppendLine("   From " + DataBaseLink(DBLink, "all_Tab_Columns", TransHost, tmpCN) + " allTab ");
                        sqlstr.AppendLine("  Where allTab.Table_Name = '" + TableName + "' ");
                        sqlstr.AppendLine("    And allTab.Owner Not in ('SYS', 'SYSTEM') ");
                        sqlstr.AppendLine("  Order By Column_id ");
                        sqlstr.AppendLine(" ----T1連線註記檢查_吳居易_KTConnectionControler_Function_GetTableSchema");
                        DoSql(sqlstr.ToString(), tbl, TransHost, tmpCN);

                        DataTable tblCol = new DataTable();
                        DataRow[] rowCols = null;
                        sqlstr.Length = 0;
                        sqlstr.AppendLine(" Select Column_Name as ColumnName, Comments from " + DataBaseLink(DBLink, "dba_col_Comments", TransHost, tmpCN));
                        sqlstr.AppendLine("  Where Table_Name = '" + TableName.ToUpper() + "' ");
                        sqlstr.AppendLine("    And Owner Not in ('SYS', 'SYSTEM') ");
                        sqlstr.AppendLine(" ----T1連線註記檢查_吳居易_KTConnectionControler_Function_GetTableSchema");
                        DoSql(sqlstr.ToString(), tblCol, TransHost, tmpCN);
                        for (int x = 0; x <= tbl.Rows.Count - 1; x++)
                        {
                            rowCols = tblCol.Select("ColumnName = '" + tbl.Rows[x]["ColumnName"] + "' ");
                            if (rowCols.Length > 0)
                            {
                                tbl.Rows[x]["Comments"] = rowCols[0]["Comments"];
                            }
                        }
                        break;

                    case DBProviderType.MySql:

                        bool getInfoflag = false;
                        switch (tmpCN.ADOConfig)
                        {
                            case ADOProviderType.MysqlOdbc35:
                            case ADOProviderType.MysqlOdbc52:
                                if (((System.Data.Odbc.OdbcConnection)tmpCN.Connection).ServerVersion.Substring(0, 4) != "4")
                                {
                                    getInfoflag = true;
                                }
                                break;

                            case ADOProviderType.MySqlClient:
#if MySqlClient
                                if (((MySql.Data.MySqlClient.MySqlConnection)tmpCN.Connection).ServerVersion.Substring(0, 4) != "4")
                                {
                                    getInfoflag = true;
                                }
#endif
                                break;
                        }

                        if (getInfoflag)
                        {
                            //1061109 舊版的 MySql 資料庫不支援底下這段資料抓取
                            sqlstr.AppendLine(" SELECT Table_Name As TableName, '' As TableComments, ");
                            sqlstr.AppendLine("        Column_Name As ColumnName, ");
                            sqlstr.AppendLine("        Ordinal_Position As ColumnOrdinal, ");
                            sqlstr.AppendLine("        Data_Type As DataType, ");
                            sqlstr.AppendLine("        Case when CHARACTER_MAXIMUM_LENGTH Is Not Null then CHARACTER_MAXIMUM_LENGTH else NUMERIC_PRECISION end as ColumnSize, ");
                            sqlstr.AppendLine("        CHARACTER_MAXIMUM_LENGTH As ColumnSize,  ");
                            sqlstr.AppendLine("        NUMERIC_PRECISION As NumericPrecision, ");
                            sqlstr.AppendLine("        NUMERIC_SCALE As NumericScale, ");
                            sqlstr.AppendLine("        Column_Comment As Comments ");
                            sqlstr.AppendLine("   From INFORMATION_SCHEMA.COLUMNS ");
                            sqlstr.AppendLine("  Where Table_Name = '" + TableName + "' ");
                            sqlstr.AppendLine("  Order By Ordinal_Position ");
                            DoSql(sqlstr.ToString(), tbl, TransHost, tmpCN);
                        }
                        else
                        {
                            rtntbl.Columns["ColumnSize"].ReadOnly = false;
                            for (int x = 0; x <= rtntbl.Rows.Count - 1; x++)
                                rtntbl.Rows[x]["ColumnSize"] = 0;
                        }
                        break;
                }


                rtntbl.TableName = TableName;

                rtncol = new DataColumn("TableComments");
                rtncol.DataType = Type.GetType("System.String");
                rtntbl.Columns.Add(rtncol);

                rtncol = new DataColumn("Comments");
                rtncol.DataType = Type.GetType("System.String");
                rtntbl.Columns.Add(rtncol);

                if (!rtntbl.Columns.Contains("DataTypeName"))
                {
                    rtncol = new DataColumn("DataTypeName");
                    rtncol.DataType = Type.GetType("System.String");
                    rtntbl.Columns.Add(rtncol);
                }

                //iDataReader 的 GetSchemaTable 抓不到 Mysql 資料結構
                //必須轉換 DataTable Column 讀寫屬性，才能告知數據長度
                rtntbl.Columns["ColumnSize"].ReadOnly = false;
                rtntbl.Columns["ColumnSize"].DefaultValue = 0;
                rtntbl.Columns["NumericPrecision"].ReadOnly = false;
                rtntbl.Columns["NumericPrecision"].DefaultValue = 0;
                rtntbl.Columns["NumericScale"].ReadOnly = false;
                rtntbl.Columns["NumericScale"].DefaultValue = 0;
                rtntbl.Columns["DataTypeName"].ReadOnly = false;
                rtntbl.Columns["DataTypeName"].DefaultValue = "";
                rtntbl.Columns["BaseTableName"].ReadOnly = false;
                rtntbl.Columns["BaseTableName"].DefaultValue = "";

                for (int x = 0; x <= tbl.Rows.Count - 1; x++)
                {
                    rtnrow = rtntbl.Select("ColumnName = '" + tbl.Rows[x]["ColumnName"].ToString() + "'");
                    if (rtnrow.Length == 1)
                    {
                        rtnrow[0]["TableComments"] = tbl.Rows[x]["TableComments"].ToString();
                        rtnrow[0]["Comments"] = tbl.Rows[x]["Comments"].ToString();
                        rtnrow[0]["DataTypeName"] = tbl.Rows[x]["DataType"].ToString();
                        rtnrow[0]["BaseTableName"] = tbl.Rows[x]["TableName"].ToString();

#if OracleODAC
                        //1090121 執行電子病歷簽章時，抓了很久的 Bug
                        //OracleODAC 使 Schema 的 NumericPrecision 資料數字成為 null
                        if (Convert.IsDBNull(rtnrow[0]["NumericPrecision"])) rtnrow[0]["NumericPrecision"] = 0;
                        if (Convert.IsDBNull(rtnrow[0]["NumericScale"])) rtnrow[0]["NumericScale"] = 0;
#endif

                        switch (tmpCN.DBConfig)
                        {
                            case DBProviderType.Oracle:
                                if (!Convert.IsDBNull(tbl.Rows[x]["ColumnSize"]))
                                {
                                    switch (rtnrow[0]["DataTypeName"].ToString())
                                    {
                                        case "CLOB":
                                            rtnrow[0]["ColumnSize"] = -1;
                                            break;

                                        default:
                                            rtnrow[0]["ColumnSize"] = tbl.Rows[x]["ColumnSize"];
                                            break;
                                    }
                                }
                                break;

                            case DBProviderType.MySql:
                                //iDataReader 的 GetSchemaTable 抓不到 Mysql 資料結構
                                //尚未測試 MySqlClient
                                if (!Convert.IsDBNull(tbl.Rows[x]["ColumnSize"]))
                                {
                                    rtnrow[0]["ColumnSize"] = tbl.Rows[x]["ColumnSize"];
                                }
                                if (!Convert.IsDBNull(tbl.Rows[x]["NumericPrecision"]))
                                {
                                    rtnrow[0]["NumericPrecision"] = tbl.Rows[x]["NumericPrecision"];
                                }
                                if (!Convert.IsDBNull(tbl.Rows[x]["NumericScale"]))
                                {
                                    rtnrow[0]["NumericScale"] = tbl.Rows[x]["NumericScale"].ToString();
                                }
                                break;
                        }
                    }
                }
            }

            return rtntbl.Copy();
        }

        public DataTable CreateDataTable(string tableName, string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            //建立一個資料庫的資料結構 給  DataTable
            if (string.IsNullOrEmpty(tableName))
                throw new ApplicationException("請指定要建立的 TableName 名稱");

            TransHost = 主機代碼(TransHost);

            DataTable tbl = null;

            DataTable tblSchema = GetTableSchema(tableName, DBLink, TransHost, sKTDB);
            if (tblSchema.Rows.Count > 0)
            {
                tbl = new DataTable();
                tbl.TableName = tableName;
            }

            DataColumn col = null;
            string ColType = "";

            for (int x = 0; x <= tblSchema.Rows.Count - 1; x++)
            {
                col = new DataColumn(tblSchema.Rows[x][0].ToString());

                ColType = tblSchema.Rows[x]["DataType"].ToString();

                switch (ColType)
                {
                    case "MySql.Data.Types.MySqlDateTime":
                        //'col.DataType = Type.GetType(ColType) 建立 TableSchema 時，System.Data 不認識 MySqlDateTime
                        col.DataType = Type.GetType("System.DateTime");
                        break;

                    default:
                        col.DataType = Type.GetType(ColType);
                        break;
                }
                tbl.Columns.Add(col);
            }

            return tbl;
        }

        public string CreateTableModels(string tableName, bool CommentsDesc = false, string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            //1060912 居易
            //指定交易主機的 Table 自動產生 該 Table 的 Models
            //產生的路徑在執行程式的 Bin 目錄下
            //Web 專案則會產生在 C:\Program Files\IIS Express
            //詳細路徑 請以接收 回傳的檔案路徑，來尋找 Table Model 檔案

            if (string.IsNullOrEmpty(tableName))
                throw new ApplicationException("請指定要建立的 TableName 名稱");

            TransHost = 主機代碼(TransHost);

            KTDBProvider tmpCN = 取得CN連線(TransHost, sKTDB);

            DataTable tblSchema = GetTableSchema(tableName, DBLink, TransHost, sKTDB);

            if (tblSchema.Rows[0]["BaseTableName"].ToString() != "")
                tableName = tblSchema.Rows[0]["BaseTableName"].ToString();

            //產生在自己的應用程式 bin 目錄底下
            string outPutfile = "mdl-" + tableName.ToLower() + ".cs";

            System.Text.StringBuilder wkStr = new System.Text.StringBuilder();

            wkStr.Length = 0;
            wkStr.AppendLine("using System;");
            wkStr.AppendLine("using System.Collections.Generic;");
            wkStr.AppendLine("using System.Linq;");
            wkStr.AppendLine("using System.Text;");
            wkStr.AppendLine("using System.Threading.Tasks;");
            wkStr.AppendLine("");
            //wkStr.AppendLine("namespace " + tableName + ".Models");
            //wkStr.AppendLine("{");
            wkStr.AppendLine("public partial class mdl_" + tableName.ToUpper() + "_");
            wkStr.AppendLine("{");

            string ColumnName = "";
            string ColType = "";
            string Comments = "";

            string getandset = " { get; set; }";

            for (int x = 0; x <= tblSchema.Rows.Count - 1; x++)
            {
                ColumnName = tblSchema.Rows[x][0].ToString().ToLower();

                Comments = "";
                if (CommentsDesc)
                {
                    Comments = tblSchema.Rows[x]["Comments"].ToString();

                    if (Comments != "")
                    {
                        Comments = KTString.Split(Comments, "\r")[0];
                        Comments = KTString.Split(Comments, "\n")[0];
                        wkStr.AppendLine("    /// <summary>" + Comments);
                        //wkStr.AppendLine("    ///" + Comments);
                        wkStr.AppendLine("    /// </summary>");
                    }
                }

                //1070207 居易
                //自動產生序號的欄位，必須在欄位前面自行加入 ? 令其改變為允許 null
                //寫入資料庫時，就會自動產生序號，資料庫有預設值的 Models 都應該允許 null。
                //SqlServer 會因為 沒有 null 導致預設為 0，會產生 IDENTITY_INSERT 錯誤
                //記錄當下無法得知 SqlServer 2000如何抓取，也無 Sql Server 以上版本測試
                //也未測試 MySql 與 Oracle 如何抓取，故暫時由人工處理。

                ColType = tblSchema.Rows[x]["DataType"].ToString();
                switch (ColType)
                {
                    case "System.String":
                        wkStr.AppendLine("    public string " + ColumnName + getandset);
                        break;

                    case "System.Decimal":
                        wkStr.AppendLine("    public decimal " + ColumnName + getandset);
                        break;

                    case "System.Int16":
                    case "System.Short":
                        wkStr.AppendLine("    public Int16 " + ColumnName + getandset);
                        break;

                    case "System.Int32":
                    case "System.Integer":
                        wkStr.AppendLine("    public Int32 " + ColumnName + getandset);
                        break;

                    case "System.Int64":
                    case "System.Long":
                        wkStr.AppendLine("    public Int64 " + ColumnName + getandset);
                        break;

                    case "System.Single":
                        wkStr.AppendLine("    public Single " + ColumnName + getandset);
                        break;

                    case "System.Double":
                        wkStr.AppendLine("    public double " + ColumnName + getandset);
                        break;

                    case "System.TimeSpan":
                        wkStr.AppendLine("    public TimeSpan? " + ColumnName + getandset);
                        break;

                    case "System.DateTime":
                        wkStr.AppendLine("    public DateTime? " + ColumnName + getandset);
                        break;

                    case "MySql.Data.Types.MySqlDateTime":
                        wkStr.AppendLine("    public DateTime? " + ColumnName + getandset);
                        break;

                    case "System.Byte[]":
                        wkStr.AppendLine("    public byte[] " + ColumnName + getandset);
                        break;

                    default:
                        throw new ApplicationException("Table名稱：" + tblSchema + "欄位名稱：" + ColumnName + "尚未定義的資料庫類型：" + ColType);
                }
            }

            //Clone 可以複製此 Model 物件當下相同的資料。
            wkStr.AppendLine("");
            wkStr.AppendLine("    public mdl_" + tableName.ToUpper() + "_ Clone()");
            wkStr.AppendLine("    {");
            wkStr.AppendLine("        return (mdl_" + tableName.ToUpper() + "_)this.MemberwiseClone();");
            wkStr.AppendLine("    }");

            wkStr.AppendLine("}");
            //wkStr.AppendLine("}");

            System.IO.File.WriteAllText(outPutfile, wkStr.ToString(), System.Text.Encoding.UTF8);
            return (new System.IO.FileInfo(outPutfile)).FullName;
        }


        public string CreateDataTableModels(DataTable tbl, string tableName = "", bool CommentsDesc = false)
        {
            //1061227 居易
            //Select 與 Join 多個 Table 的指定欄位，自動產生 Models
            //產生的路徑在執行程式的 Bin 目錄下
            //Web 專案則會產生在 C:\Program Files\IIS Express
            //詳細路徑 請以接收 回傳的檔案路徑，來尋找 Table Model 檔案

            if (tableName == "") tableName = tbl.TableName;

            //產生在自己的應用程式 bin 目錄底下
            string outPutfile = "mdl-" + tableName.ToLower() + ".cs";

            System.Text.StringBuilder wkStr = new System.Text.StringBuilder();

            wkStr.Length = 0;
            wkStr.AppendLine("using System;");
            wkStr.AppendLine("using System.Collections.Generic;");
            wkStr.AppendLine("using System.Linq;");
            wkStr.AppendLine("using System.Text;");
            wkStr.AppendLine("using System.Threading.Tasks;");
            wkStr.AppendLine("");
            //wkStr.AppendLine("namespace " + tableName + ".Models");
            //wkStr.AppendLine("{");
            wkStr.AppendLine("public partial class mdl_" + tableName.ToUpper() + "_");
            wkStr.AppendLine("{");

            string ColumnName = "";
            string ColType = "";
            string Comments = "";

            string getandset = " { get; set; }";

            for (int x = 0; x <= tbl.Columns.Count - 1; x++)
            {
                ColumnName = tbl.Columns[x].ColumnName.ToLower();

                Comments = "";
                if (CommentsDesc)
                {
                    Comments = tbl.Columns[x].Caption;

                    if (Comments != "")
                    {
                        Comments = KTString.Split(Comments, "\r")[0];
                        Comments = KTString.Split(Comments, "\n")[0];
                        wkStr.AppendLine("    /// <summary>" + Comments);
                        //wkStr.AppendLine("    ///" + Comments);
                        wkStr.AppendLine("    /// </summary>");
                    }
                }

                ColType = tbl.Columns[x].DataType.ToString();
                switch (ColType)
                {
                    case "System.String":
                        wkStr.AppendLine("    public string " + ColumnName + getandset);
                        break;

                    case "System.Decimal":
                        wkStr.AppendLine("    public decimal " + ColumnName + getandset);
                        break;

                    case "System.Int16":
                    case "System.Short":
                        wkStr.AppendLine("    public Int16 " + ColumnName + getandset);
                        break;

                    case "System.Int32":
                    case "System.Integer":
                        wkStr.AppendLine("    public Int32 " + ColumnName + getandset);
                        break;

                    case "System.Int64":
                    case "System.Long":
                        wkStr.AppendLine("    public Int64 " + ColumnName + getandset);
                        break;

                    case "System.Single":
                        wkStr.AppendLine("    public Single " + ColumnName + getandset);
                        break;

                    case "System.Double":
                        wkStr.AppendLine("    public double " + ColumnName + getandset);
                        break;

                    case "System.TimeSpan":
                        wkStr.AppendLine("    public TimeSpan? " + ColumnName + getandset);
                        break;

                    case "System.DateTime":
                        wkStr.AppendLine("    public DateTime? " + ColumnName + getandset);
                        break;

                    case "MySql.Data.Types.MySqlDateTime":
                        wkStr.AppendLine("    public DateTime? " + ColumnName + getandset);
                        break;

                    case "System.Byte[]":
                        wkStr.AppendLine("    public byte[] " + ColumnName + getandset);
                        break;

                    default:
                        throw new ApplicationException("自訂Table名稱：" + tableName + "欄位名稱：" + ColumnName + "尚未定義的資料庫類型：" + ColType);
                }
            }

            //Clone 可以複製此 Model 物件當下相同的資料。
            wkStr.AppendLine("");
            wkStr.AppendLine("    public mdl_" + tableName.ToUpper() + "_ Clone()");
            wkStr.AppendLine("    {");
            wkStr.AppendLine("        return (mdl_" + tableName.ToUpper() + "_)this.MemberwiseClone();");
            wkStr.AppendLine("    }");

            wkStr.AppendLine("}");
            //wkStr.AppendLine("}");

            System.IO.File.WriteAllText(outPutfile, wkStr.ToString(), System.Text.Encoding.UTF8);
            return (new System.IO.FileInfo(outPutfile)).FullName;
        }

        //###################################################################
        //判斷新增主機的使用狀態 
        private class PRS003_Models
        {
            public String COSTDPT { get; set; }
        }

        public String LoginOPID { get; set; }

        private string UseNewAreaDate = "";
        private string UseNewAreaCOSTDPT = "";
        private void CheckNewArea(string wkDate = "", string COSTDPT = "", string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            // 向上院區主機是否可使用的判別 
            // 1090410 居易
            // 程式說明
            // 先執行
            // KTCC.getDBLinkState() 
            // 取得三個院區的使用狀況，很多人的城市都已經有這一段，再判別是否可使用 DBLink
            // 
            // 接下來判斷 向上院區是否可使用
            // KTCC.CheckNewArea()
            // wkDate ：  日期，不傳入日期，預設為今天 KTDateTime.Today()
            // COSTDPT：  成本中心沒有指定，會使用 GetUserID 得到的員工編號，取得資料庫 PRS003 的 COSTDPT
            // TransHost：交易主機如果不是 CNHOME，請特別傳入 KTGH01 或 KTGHTA01 
            // DBLink：   如果交易主機是 KTGH01 或 KTGHTA01 那麼 DBLink 就要使用 KTGH00
            // 除非主機也有相同的 PRS003 資料也是正確的(1090410 檢查 KTGH01 的 PRS003 資料是舊的)

            // 再透過 KTCC.getDBLinkState
            // if KTCC.getDBLinkState("KTGH00") Then
            // if KTCC.getDBLinkState("HPK210") Then
            // if KTCC.getDBLinkState("TSHIS") Then
            // 知道三個院區目前的狀況是 True 就可以執行 DataBaseLink
            // 
            // 
            // 或是使用 KTCC.UseNewAreaFlag
            // If KTCC.UseNewAreaFlag("KTXS00") Then
            // '向上院區可使用
            // End If
            // 或
            // If KTCC.UseNewAreaFlag("KTXS00") = False Then
            // '向上院區不可使用
            // End If
            // 
            // 但是 KTCC.UseNewAreaFlag("KTXS00") 與 KTCC.getDBLinkState("KTXS00") 是有差別的
            // KTCC.UseNewAreaFlag("KTXS00") 是判斷到底 KTXS00 能不能用
            // KTCC.getDBLinkState("KTXS00") 是判斷資料庫是不是掛了，但也包含 KTXS00 能不能使用。
            // 
            // 程式範例
            // 
            // 抓取全部院區的狀態
            // 
            // KTCC.LoginOPID = OPID (Web程式必須明確指定登入的員工編號)
            // KTCC.getDBLinkState()
            // 
            // Dim DBLink As String = ""
            // For x As Integer = 1 To 5
            // 
            // DBLink = getNumHostCode(x) '當數字為 4，Return 為空白，不執行。
            // 
            // 狀況一：判斷到底 KTXS00 能不能用
            // If KTCC.UseNewAreaFlag(DBLink) And DBLink <> "" Then
            // 
            // End If
            // 或
            // 狀況二：判斷資料庫是不是掛了，但也包含 KTXS00 能不能使用。
            // If KTCC.getDBLinkState(DBLink) And DBLink <> "" Then
            // 
            // End If
            // Next

            // 已經可以使用就永遠可以使用
            if (UseNewAreaFlag_) return;

            // 執行的今天日期，
            // 1.日期清空
            // 2.定義的日期之後，全部的人都可以使用
            if (UseNewAreaDate == "")
            {
                UseNewAreaFlag_ = true;
                return;
            }
            else
            {
                if (wkDate == "")
                    wkDate = KTDateTime.Today();

                DateTime wkDateTime = Convert.ToDateTime(KTDateTime.CToWDate(wkDate));
                DateTime NewAreaDate = Convert.ToDateTime(KTDateTime.CToWDate(UseNewAreaDate));

                if (wkDateTime >= NewAreaDate)
                {
                    UseNewAreaFlag_ = true;
                    return;
                }
            }

            // 這個日期之前，只有這些成本中心的人可以使用
            if (!UseNewAreaFlag_)
            {
                if (COSTDPT == "")
                {
                    string OPID = "";

                    if (!string.IsNullOrEmpty(LoginOPID))
                        OPID = LoginOPID.ToString().ToUpper();

                    if (OPID == "")
                        if (ExeOrWeb == "exe")
                            OPID = KTString.GetUserID();

                    if (OPID != "" & AlreadyConnectOK(TransHost))
                    {
                        System.Text.StringBuilder sqlstr = new System.Text.StringBuilder();

                        System.Data.DataTable tblPRS003 = new System.Data.DataTable("PRS003");

                        PRS003_Models PRS003 = new PRS003_Models();
                        List<PRS003_Models> dy_PRS003s = new List<PRS003_Models>();

                        sqlstr.Length = 0;
                        sqlstr.AppendLine("Select * From " + DataBaseLink(DBLink, "PRS003", TransHost, sKTDB));
                        sqlstr.AppendLine(" Where OPID = '" + OPID + "' ");
                        sqlstr.AppendLine("  And COSTDPT Is Not Null ");
#if !noDapper
                        dy_PRS003s = DoSql<PRS003_Models>(sqlstr.ToString(), TransHost: TransHost, sKTDB: sKTDB).ToList();
#else
                        DoSql(sqlstr.ToString(), tblPRS003, TransHost: TransHost, sKTDB: sKTDB);
                        dy_PRS003s = DataTableToList<PRS003_Models>(tblPRS003).ToList();
#endif

                        if (dy_PRS003s.Count() > 0)
                        {
                            PRS003 = dy_PRS003s[0];
                            KTString.ReplaceModelsNull(PRS003);
                            COSTDPT = PRS003.COSTDPT;
                        }
                    }
                }

                if (COSTDPT != "")
                {
                    if (UseNewAreaCOSTDPT.IndexOf(COSTDPT) > -1)
                        UseNewAreaFlag_ = true;
                }
            }
        }

        private bool UseNewAreaFlag_ = false;
        public bool UseNewAreaFlag(string DBLink)
        {
            if (DBLink == "")
                return false;

            // 1090417 
            // 這是新建院區時的處理方式，
            // 未來新院區已經上線，程式也不用改掉

            // 還有繼續在拓展院區時，將 PRS3010.Type = 131 院區代碼改掉就好
            switch (DBLink)
            {
                case "5":
                case "KTXS00":
                case "KTXS01":
                case "向上":
                case "向上院區":
                case "向上檢驗":
                    {
                        break;
                    }

                default:
                    {
                        return true;
                    }
            }

            if (UseNewAreaFlag_)
                return true;

            return false;
        }

        //###################################################################
        //判斷院區主機的連線狀態 
        private string KTXS00DBLink = "";
        private string KTGH00DBLink = "";
        private string HPK210DBLink = "";
        private string TSHISDBLink = "";
        private string OLTPSRVDBLink = "";
        private string EMRDBDBLink = "";
        private string TSEMRDBLink = "";
        private string CMEDDBLink = "";

        private class PRS3010_DBStatus
        {
            public String Code { get; set; }
            public String Description { get; set; }
            public String Description2 { get; set; }
            public String Para1 { get; set; }
        }
        public void getDBLinkState(KTDBProvider sKTDB = null, string TransHost = "", string DBLink = "")
        {
            //功能：取得三個院區的連線狀態(病歷資料使用) (Overloads)
            //日期：0970327 

            //居易
            //修改：0990320 

            //只要有做到其他資料庫 1.沙鹿 2.大甲 3.通霄 O(歐).EIP 的資料存取判斷
            //都要先呼叫此 Funcion 一次

            System.Text.StringBuilder sqlstr = new System.Text.StringBuilder();

            System.Data.DataTable tblPRS3010 = new System.Data.DataTable();

            PRS3010_DBStatus PRS3010 = new PRS3010_DBStatus();
            List<PRS3010_DBStatus> dy_PRS3010 = new List<PRS3010_DBStatus>();

            sqlstr.Length = 0;
            sqlstr.AppendLine("Select * From " + DataBaseLink(DBLink, "PRS3010", TransHost, sKTDB));
            sqlstr.AppendLine(" Where Type = '13' ");
            sqlstr.AppendLine(" Order by Code ");
#if !noDapper
            dy_PRS3010 = DoSql<PRS3010_DBStatus>(sqlstr.ToString(), TransHost: TransHost, sKTDB: sKTDB).ToList();
#else
            DoSql(sqlstr.ToString(), tblPRS3010, TransHost: TransHost, sKTDB: sKTDB);
            dy_PRS3010 = DataTableToList<PRS3010_DBStatus>(tblPRS3010).ToList();
#endif
            string DBStatus = "";
            if (dy_PRS3010.Count() == 0)
            {
                //假設 註記全部不見了 仍然要當作正常連線的院區
                KTGH00DBLink = "Y";
                HPK210DBLink = "Y";
                TSHISDBLink = "Y";
                KTXS00DBLink = "Y";
                OLTPSRVDBLink = "Y";
                EMRDBDBLink = "Y";
                TSEMRDBLink = "Y";
                CMEDDBLink = "Y";
            }
            else
            {
                for (int x = 0; x <= dy_PRS3010.Count() - 1; x++)
                {
                    PRS3010 = dy_PRS3010[x];

                    if (!string.IsNullOrEmpty(PRS3010.Description))
                    {
                        DBStatus = PRS3010.Description.ToUpper().Substring(0, 1);
                    }

                    switch (PRS3010.Code)
                    {
                        case "1":
                        case "KTGH00":
                            KTGH00DBLink = DBStatus;
                            break;

                        case "2":
                        case "HPK210":
                            HPK210DBLink = DBStatus;
                            break;

                        case "3":
                        case "TSHIS":
                            TSHISDBLink = DBStatus;
                            break;

                        case "5":
                        case "KTXS00":
                            KTXS00DBLink = DBStatus;

                            if (KTXS00DBLink == "Y")
                            {
                                PRS3010.Para1 = PRS3010.Para1 ?? "";
                                UseNewAreaDate = PRS3010.Para1.ToString().ToUpper();

                                PRS3010.Description2 = PRS3010.Description2 ?? "";
                                UseNewAreaCOSTDPT = PRS3010.Description2.ToString().ToUpper();

                                CheckNewArea(DBLink: DBLink, TransHost: TransHost, sKTDB: sKTDB);

                                if (!UseNewAreaFlag_) KTXS00DBLink = "N";
                            }

                            break;

                        case "O":
                        case "OLTPSRV":
                            OLTPSRVDBLink = DBStatus;
                            break;

                        case "EMRDB":
                            EMRDBDBLink = DBStatus;
                            break;

                        case "TSEMR":
                            TSEMRDBLink = DBStatus;
                            break;

                        case "CMED":
                            CMEDDBLink = DBStatus;
                            break;
                    }
                }
            }
        }

        public bool getDBLinkState(string Area)
        {
            //0970327 判斷指定的院區在資料庫是否連線
            Area = Area.Trim().ToUpper();

            if (string.IsNullOrEmpty(Area))
                return true;

            if (Area == OracleHostDSN_)
                //當地院區..不用再判斷
                return true;

            switch (Area.Trim().ToUpper())
            {
                case "0":
                case "CNHOME":
                case "1":
                case "KTGH00":
                    if (KTGH00DBLink == "N") return false;
                    break;

                case "2":
                case "HPK210":
                    if (HPK210DBLink == "N") return false;
                    break;

                case "3":
                case "TSHIS":
                    if (TSHISDBLink == "N") return false;
                    break;

                case "5":
                case "KTXS00":
                    if (KTXS00DBLink == "N") return false;
                    break;

                case "O":
                case "OLTPSRV":
                    if (OLTPSRVDBLink == "N") return false;
                    break;

                case "EMRDB":
                    if (EMRDBDBLink == "N") return false;
                    break;

                case "TSEMR":
                    if (TSEMRDBLink == "N") return false;
                    break;

                case "CMED":
                    if (CMEDDBLink == "N") return false;
                    break;
            }
            return true;
        }

        public static string getNumHostCode(string NumCode = "")
        {
            //功能：取得由數字傳入以取得 當時的主機代碼
            //日期：0970125
            //備註：0.當院區 KTCC.OracleHostDSN 
            //     1.沙鹿 KTGH00 
            //     2.大甲 HPK210
            //     3.通霄 TSHIS
            switch (NumCode)
            {
                case "":
                case "0":
                    return OracleHostDSN;

                case "1":
                case "KTGH00":
                case "KTGH03":
                    //0981020 居易
                    //如果在測試院區.全部都當測試院區, 不指定 KTGH00
                    if (OracleHostDSN == "KTGH03")
                        return "KTGH03";
                    return "KTGH00";

                case "2":
                case "HPK210":
                    return "HPK210";

                case "3":
                case "TSHIS":
                    return "TSHIS";

                case "4":
                    return "";

                case "5":
                case "KTXS00":
                    return "KTXS00";

                default:
                    throw new System.Exception("不要丟尚未定義的院區的資料進來");
                //MessageBox.Show("不要丟尚未定義的院區的資料進來");
                //return NumCode;
            }
        }

        public static string getOtherHost(string Host = "")
        {
            //功能：取得另外一台對應的遠端主機，要給 DBLink 使用的主機名稱
            //日期：0950724 
            //備註：目前只有 KTGH00 與 HPK210, TSHIS
            //     通霄院區如果要抓掛院區的資料，會回傳空白表示只更新當地院區
            //     若要還要判斷 KTGH01 或 KTGHTA01 環境需要重新整理 

            if (!string.IsNullOrEmpty(Host))
            {
                Host = Host.ToUpper().Trim();
                Host = Host.Replace("@", "");
            }
            else
                Host = OracleHostDSN;

            switch (Host)
            {
                case "KTGH00":
                case "KTGH03":
                    return "HPK210";

                case "HPK210":
                    return "KTGH00";

                case "TSHIS":
                    //通霄院區不同醫事機構代碼
                    //只要發現回傳是空白，不做跨院區更新
                    return "";
            }
            return "";
        }

        public static string[] getOtherHosts(string Host = "")
        {
            //功能：取得另外其他對應的資料庫主機，要給 DBLink 使用的主機名稱
            //日期：1090407
            //備註：沙鹿、大甲、向上 相同醫事機構代碼
            //     相同醫事機構需要資料同步時，會回應還需要一起抓取或更新的主機。

            if (!string.IsNullOrEmpty(Host))
            {
                Host = Host.ToUpper().Trim();
                Host = Host.Replace("@", "");
            }
            else
                Host = OracleHostDSN;

            string[] Hosts = new string[0];

            switch (Host)
            {
                case "1":
                case "KTGH00":
                case "KTGH03":
                    //Hosts = KTString.Split("HPK210,KTXS00", ",");
                    Hosts = KTString.Split("HPK210", ",");
                    break;

                case "2":
                case "HPK210":
                    //Hosts = KTString.Split("KTGH00,KTXS00", ",");
                    Hosts = KTString.Split("KTGH00", ",");
                    break;

                case "3":
                case "TSHIS":
                    //通霄院區不同醫事機構代碼
                    //只要發現回傳是空白，不做跨院區更新
                    break;

                case "5":
                case "KTXS00":
                    Hosts = KTString.Split("KTGH00,HPK210", ",");
                    break;
            }

            return Hosts;
        }

        public static string[] getAllAreaHosts()
        {
            // 功能：取得全部主機代碼(依據 主要院區排序)
            // 日期：1090424 居易

            //return KTString.Split("5,1,2,3", ",");
            return KTString.Split("1,2,3", ",");
        }

        public static string[] getAllCodeHosts(string NumCode = "C")
        {
            // 功能：取得全部主機(依據 主要院區排序)
            // 日期：1090424 居易

            string[] Hosts = new string[0];

            switch (NumCode)
            {
                case "C":
                    //Hosts = KTString.Split("KTXS00,KTGH00,HPK210,TSHIS", ",");
                    Hosts = KTString.Split("KTGH00,HPK210,TSHIS", ",");
                    break;

                case "N":
                    //Hosts = KTString.Split("5,1,2,3", ",");
                    Hosts = KTString.Split("1,2,3", ",");
                    break;
            }
            return Hosts;
        }

        public static string[] getAllLabHosts()
        {
            // 功能：取得全部檢驗主機(依據 主要院區排序)
            // 日期：1090424 居易

            //return KTString.Split("KTXS01,KTGH01,KTGHTA01,TSHIS", ",");
            return KTString.Split("KTGH01,KTGHTA01,TSHIS", ",");
        }

        public static string[] getTheSameHosts(string Host = "", string NumCode = "C")
        {
            //功能：取得相同一世紀購代碼的全部主機，包含 Host 本身 (既然要 Union All 就應該全部都執行)
            //日期：1090407 居易
            //備註：沙鹿、大甲、向上 相同醫事機構代碼
            //     相同醫事機構需要資料同步時，會回應還需要一起抓取或更新的主機。

            if (!string.IsNullOrEmpty(Host))
            {
                Host = Host.ToUpper().Trim();
                Host = Host.Replace("@", "");
            }
            else
                Host = OracleHostDSN;

            string[] Hosts = new string[0];

            switch (Host)
            {
                case "1":
                case "KTGH00":
                case "沙鹿":
                case "2":
                case "HPK210":
                case "大甲":
                case "5":
                case "KTXS00":
                case "向上":
                    if (OracleHostDSN == "KTGH03")
                        Hosts = KTString.Split("KTGH03", ",");
                    else
                        switch (NumCode)
                        {
                            case "C":
                                //Hosts = KTString.Split("KTXS00,KTGH00,HPK210", ",");
                                Hosts = KTString.Split("KTGH00,HPK210", ",");
                                break;
                            case "N":
                                //Hosts = KTString.Split("5,1,2", ",");
                                Hosts = KTString.Split("1,2", ",");
                                break;
                        }
                    break;

                case "3":
                case "TSHIS":
                case "通霄":
                    Hosts = KTString.Split("TSHIS", ",");
                    break;
            }

            return Hosts;
        }

        public static string[] getOtherLabHosts(string Host = "")
        {
            //功能：取得相同醫事機構對應的Lab主機，要給 DBLink 使用的主機名稱
            //日期：1090409 居易
            //備註：通霄院區如果要抓掛院區的資料，會回傳空白表示只更新當地院區
            //     若要還要判斷 KTGH01 或 KTGHTA01 環境需要重新整理 

            if (!string.IsNullOrEmpty(Host))
            {
                Host = Host.ToUpper().Trim();
                Host = Host.Replace("@", "");
            }
            else
                Host = OracleHostDSN;

            string[] Hosts = new string[0];

            switch (Host)
            {
                case "1":
                case "KTGH01":
                case "沙鹿":
                    //Hosts = KTString.Split("KTXS01,KTGHTA01", ",");
                    Hosts = KTString.Split("KTGHTA01", ",");
                    break;

                case "2":
                case "HPK210":
                case "大甲":
                    //Hosts = KTString.Split("KTXS01,KTGH01", ",");
                    Hosts = KTString.Split("KTGH01", ",");
                    break;

                case "5":
                case "KTXS00":
                case "向上":
                    Hosts = KTString.Split("KTGH01,KTGHTA01", ",");
                    break;

                case "3":
                case "TSHIS":
                case "通霄":
                    //只要發現回傳是空白，不做跨院區更新
                    break;
            }

            return Hosts;
        }

        public static string[] getTheSameLabHosts(string Host = "")
        {
            //功能：取得相同一世紀購代碼的全部主機，包含 Host 本身 (既然要 Union All 就應該全部都執行)
            //日期：1090407 居易
            //備註：沙鹿、大甲、向上 相同醫事機構代碼
            //     相同醫事機構需要資料同步時，會回應還需要一起抓取或更新的主機。

            if (!string.IsNullOrEmpty(Host))
            {
                Host = Host.ToUpper().Trim();
                Host = Host.Replace("@", "");
            }
            else
                Host = OracleHostDSN;

            string[] Hosts = new string[0];

            switch (Host)
            {
                case "1":
                case "KTGH01":
                case "沙鹿":
                case "2":
                case "KTGHTA01":
                case "大甲":
                case "5":
                case "KTXS01":
                case "向上":
                    //Hosts = KTString.Split("KTXS01,KTGH01,KTGHTA01", ",");
                    Hosts = KTString.Split("KTGH01,KTGHTA01", ",");
                    break;

                case "3":
                case "TSHIS":
                case "通霄":
                    Hosts = KTString.Split("TSHIS", ",");
                    break;
            }

            return Hosts;
        }

        public static string getLabHost(string Host = "")
        {
            //功能：取得目前院區的檢驗主機
            //日期：1090407 居易

            if (!string.IsNullOrEmpty(Host))
            {
                Host = Host.ToUpper().Trim();
                Host = Host.Replace("@", "");
            }
            else
                Host = OracleHostDSN;

            switch (Host)
            {
                case "KTGH03":
                    break;

                case "1":
                case "KTGH01":
                case "沙鹿":
                    if (OracleHostDSN == "KTGH03")
                        Host = "KTGH03";
                    else
                        Host = "KTGH01";
                    break;

                case "2":
                case "HPK210":
                case "大甲":
                    Host = "KTGHTA01";
                    break;

                case "5":
                case "KTXS00":
                case "向上":
                    Host = "KTXS01";
                    break;

                case "3":
                case "TSHIS":
                case "通霄":
                    Host = "TSHIS";
                    break;
            }

            return Host;
        }

        private class SaTaT1Ok_Models
        {
            public String Code { get; set; }
        }
        public bool SaTaT1Ok(string DBLink = "", string TransHost = "")
        {
            //功能：判斷沙鹿大甲的T1狀況是否正常
            //回傳：正常 回傳 True,不正常回傳 False
            //說明：本副程式不是真正去檢查T1那條線是否Working,而是去讀T1狀態記錄檔 PUB002.DTATYP = "30"的資料,根據該資料去
            //      判斷T1是否正常
            //      當發現 T1 斷線時，若發現無法即時回復則將 PUB002.DTATYP= "30" 之CODE 設為 0,待 T1正常後再將該筆資料設為 1

            System.Text.StringBuilder sqlstr = new System.Text.StringBuilder();
            System.Data.DataTable tblPUB002 = new System.Data.DataTable();
            List<SaTaT1Ok_Models> dy_PUB002s = new List<SaTaT1Ok_Models>();

            string CODE = "";

            sqlstr.Length = 0;
            sqlstr.AppendLine("select CODE From " + DataBaseLink(DBLink, "PUB002", TransHost) + " Where DTATYP = '30' ");

#if !noDapper
            dy_PUB002s = DoSql<SaTaT1Ok_Models>(sqlstr.ToString(), TransHost: TransHost).ToList();
#else
            DoSql(sqlstr.ToString(), tblPUB002, TransHost: TransHost);
            dy_PUB002s = DataTableToList<SaTaT1Ok_Models>(tblPUB002).ToList();
#endif

            if (dy_PUB002s.Count > 0)
                CODE = dy_PUB002s[0].Code;

            bool returnT1OK = false;

            switch (CODE)
            {
                case "":
                    throw new System.Exception("KTConnectionControler.SaTaT1Ok() 無法判斷沙鹿大甲的T1是否正常工作!!");

                case "0":
                    returnT1OK = false;
                    break;

                case "1":
                    returnT1OK = true;
                    break;
            }

            return returnT1OK;
        }

        private class WhereAmI_Models
        {
            public String WhereAmI { get; set; }
        }
        private string TmpWhereAmI = "";
        public string WhereAmI()
        {
            //功能:回傳預設連線院區的代碼 1.沙鹿  2.大甲  3.通霄

            if (TmpWhereAmI != "") return TmpWhereAmI;

            System.Text.StringBuilder sqlstr = new System.Text.StringBuilder();
            System.Data.DataTable tblWhereAmI = new System.Data.DataTable();
            List<WhereAmI_Models> dy_WhereAmIs = new List<WhereAmI_Models>();

            sqlstr.Length = 0;
            sqlstr.AppendLine("Select WhereAmI as WhereAmI From dual");

#if !noDapper
            dy_WhereAmIs = DoSql<WhereAmI_Models>(sqlstr.ToString()).ToList();
#else
            DoSql(sqlstr.ToString(), tblWhereAmI);
            dy_WhereAmIs = DataTableToList<WhereAmI_Models>(tblWhereAmI).ToList();
#endif

            if (dy_WhereAmIs.Count > 0)
                TmpWhereAmI = dy_WhereAmIs[0].WhereAmI;

            if (string.IsNullOrEmpty(TmpWhereAmI))
                throw new System.Exception("KTConnectionControler.WhereAmI() 無法判斷院區");

            return TmpWhereAmI;
        }

        public HospitalData getHosptialData(string DBLink = "", string TransHost = "", KTDBProvider sKTDB = null)
        {
            System.Text.StringBuilder sqlstr = new System.Text.StringBuilder();

            DataTable tblPRS001 = new DataTable("PRS001");
            List<HospitalData> dy_HospitalDatas = new List<HospitalData>();

            sqlstr.Length = 0;
            sqlstr.AppendLine("Select * From " + DataBaseLink(DBLink, "PRS001", TransHost, sKTDB));
            sqlstr.AppendLine(" Where DTFLG = '0' ");

#if !noDapper
            dy_HospitalDatas = DoSql<HospitalData>(sqlstr.ToString(), TransHost: TransHost, sKTDB: sKTDB).ToList();
#else
            DoSql(sqlstr.ToString(),tblPRS001, TransHost, sKTDB);
            dy_HospitalDatas = DataTableToList<HospitalData>(tblPRS001).ToList();
#endif

            if (!DoSqlOK)
                return null;

            HospitalData dy_HospitalData = new HospitalData();

            if (dy_HospitalDatas.Count > 0)
                dy_HospitalData = dy_HospitalDatas[0];

            KTString.ReplaceModelsNull(dy_HospitalData);

            using (HospitalData h = new HospitalData())
            {
                h.HSPID = dy_HospitalData.HSPID;
                h.FNAME = dy_HospitalData.FNAME;
                h.ADRS = dy_HospitalData.ADRS;
                h.HSPTEL = dy_HospitalData.HSPTEL;
                h.HSPFAX = dy_HospitalData.HSPFAX;
                h.CLAS = dy_HospitalData.CLAS;

                if (h.CLAS == "B")
                    // 暫時還沒有人使用此 Class
                    // 故意將大甲院區改為 新的醫事機構代碼
                    // 要使用的人就會發現問題，請有問題的再提出異議
                    h.HSPID = "0936050029";

                return h;
            }
        }

        public class HospitalData : IDisposable
        {
            // 1090424 居易
            // 
            // Dim h As KTDB.KTConnectionControler.HospitalData = KTCC.getHosptialData()
            // h.getHosptialData()
            // h.FNAME 
            // h.ADRS 
            // h.HSPTEL

            // 或

            // Using h As KTDB.KTConnectionControler.HospitalData = KTCC.getHosptialData()
            //     h.getHosptialData()
            //     h.FNAME 
            //     h.ADRS 
            //     h.HSPTEL
            // End Using

            public String AREA { get; set; }
            public String AREA_CODE { get; set; }
            public String AREA_CNAME { get; set; }

            public String HSPID { get; set; }
            public String FNAME { get; set; }
            public String ADRS { get; set; }
            public String ADRS_ENAME { get; set; }

            public String CLAS { get; set; }
            public String HSPTEL { get; set; }
            public String HSPFAX { get; set; }

            private bool disposedValue = false;        // 偵測多餘的呼叫

            // IDisposable
            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposedValue)
                {
                    if (disposing)
                    {
                    }
                }
                this.disposedValue = true;
            }

            // 由 Visual Basic 新增此程式碼以正確實作可處置的模式。
            public void Dispose()
            {
                // 請勿變更此程式碼。在以上的 Dispose 置入清除程式碼 (ByVal 視為布林值處置)。
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }

        #region " IDisposable Support "
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                CloseAll(true);
        }

        public void Dispose()
        {
            // 請勿變更此程式碼。在以上的 Dispose 置入清除程式碼。
            if (this.disposed)
                return;

            disposed = true;

            Dispose(disposed);
            GC.SuppressFinalize(this);
        }
        #endregion
    }

    ////1050122 居易 改版
    ////        因應 VB6 將大量使用 Dll，會導致 Oracle 瞬間產生多條連線數，直到 1~2 分鐘後才會釋放連線
    ////        故設計出讓 VB6 透過 Class Connect_To_Dll 產生一條預設連線，再由預設連線丟給其他的 Dll
    ////        這個條件 直接讓 Dll 承接主程式的 Connect
    ////                 1.避免連線數過多
    ////                 2.避免讓每個 Dll 於 Connect 時停頓。
    ////
    ////測試上已知 Bug 
    ////若 VB6 透過 A_Dll 產生一條連線，而將要使用的功能剛好也在 A_Dll 裡面
    ////因為是同一個 Dll，A_Dll 的連線早已經存在，不會再進入是否為 Dll 連線條件判斷，
    ////結論就是，會被誤判為非 Dll 條件，
    ////將導致離 1.Close Connect 時，連線真的被中斷。
    ////         2.若呼叫的是一個 Dll 表單，可能會讓整隻外部呼叫的程式(EXE檔)被關閉。
    //public class cls_Connect_To_Dll : IDisposable
    //{

    //        //dll表單，呼叫前的連線必須告訴 Function isDllConnect = True
    //    private bool isDllConnect_ = false;
    //    public bool isDllConnect {
    ////千萬不能開啟下方程式碼的這個條件，
    ////會導致被 dll 另外呼叫的表單誤判該表單為 dll 連線，進而 Cns.Remove 目前連線。
    ////If xTrans = "" Then xTrans = "CNHOME"
    ////If xTrans <> "" Then xTrans = xTrans.ToUpper
    ////If KTCC.Cns.Contains(xTrans) Then
    ////    isDllConnect_ = KTCC.Cns(xTrans).isDllConnect
    ////End If
    //        get { return isDllConnect_; }
    //        set { isDllConnect_ = value; }
    //    }

    //    private DBProviderType DBConfig_ = DBProviderType.Oracle;
    //    public DBProviderType DBConfig {
    //        get {
    //            if (string.IsNullOrEmpty(xTrans))
    //                xTrans = "CNHOME";
    //            if (!string.IsNullOrEmpty(xTrans))
    //                xTrans = xTrans.ToUpper;
    //            if (KTCC.Cns.Contains(xTrans)) {
    //                DBConfig_ = KTCC.Cns(xTrans).DBConfig;
    //                //MsgBox("xTrans : " & xTrans & vbCrLf & "DBConfig_ : " & DBConfig_)
    //            }
    //            return DBConfig_;
    //        }
    //        set { DBConfig_ = value; }
    //    }

    //    private ADOProviderType ADOConfig_ = ADOProviderType.Oledb;
    //    public ADOProviderType ADOConfig {
    //        get {
    //            if (string.IsNullOrEmpty(xTrans))
    //                xTrans = "CNHOME";
    //            if (!string.IsNullOrEmpty(xTrans))
    //                xTrans = xTrans.ToUpper;
    //            if (KTCC.Cns.Contains(xTrans)) {
    //                ADOConfig_ = KTCC.Cns(xTrans).ADOConfig;
    //                //MsgBox("xTrans : " & xTrans & vbCrLf & "ADOConfig_ : " & ADOConfig_)
    //            }
    //            return ADOConfig_;
    //        }
    //        set { ADOConfig_ = value; }
    //    }

    //    private IDbConnection Connection_ = null;
    //    public IDbConnection Connection {
    //        get {
    //            if (string.IsNullOrEmpty(xTrans))
    //                xTrans = "CNHOME";
    //            if (!string.IsNullOrEmpty(xTrans))
    //                xTrans = xTrans.ToUpper;
    //            if (KTCC.Cns.Contains(xTrans)) {
    //                Connection_ = KTCC.Cns(xTrans).Connection;
    //                //MsgBox("xTrans : " & xTrans & vbCrLf & "Connection_ : " & Connection_.ConnectionString)
    //            }
    //            return Connection_;
    //        }
    //        set { Connection_ = value; }
    //    }

    //    private string HostName_ = "";
    //    public string HostName {
    //        get {
    //            if (string.IsNullOrEmpty(xTrans))
    //                xTrans = "CNHOME";
    //            if (!string.IsNullOrEmpty(xTrans))
    //                xTrans = xTrans.ToUpper;
    //            if (KTCC.Cns.Contains(xTrans)) {
    //                HostName_ = KTCC.Cns(xTrans).HostName;
    //                //MsgBox("xTrans : " & xTrans & vbCrLf & "HostName_ : " & HostName_)
    //            }
    //            return HostName_;
    //        }
    //        set { HostName_ = value; }
    //    }

    //    private string CnKey_ = "";
    //    public string CnKey {
    //        get {
    //            if (string.IsNullOrEmpty(xTrans))
    //                xTrans = "CNHOME";
    //            if (!string.IsNullOrEmpty(xTrans))
    //                xTrans = xTrans.ToUpper;
    //            if (KTCC.Cns.Contains(xTrans)) {
    //                CnKey_ = KTCC.Cns(xTrans).CnKey;
    //                //MsgBox("xTrans : " & xTrans & vbCrLf & "CnKey_ : " & CnKey_)
    //            }
    //            return CnKey_;
    //        }
    //        set { CnKey_ = value; }
    //    }

    //    //無法直接使用 KTDBProvider，dll 不知道怎麼回事，無法傳入 Class
    //    //Public DllKTDB As New KTDBProvider
    //    public bool Connect()
    //    {

    //        if (isDllConnect_) {
    //            KTCC.ConnectDll(DBConfig_, ADOConfig_, Connection_, HostName_, CnKey_);
    //        }

    //        return isDllConnect_;

    //    }


    //    public void Close(string xTrans = "")
    //    {
    //        if (string.IsNullOrEmpty(xTrans))
    //            xTrans = "CNHOME";
    //        if (!string.IsNullOrEmpty(xTrans))
    //            xTrans = xTrans.ToUpper;
    //        if (KTCC.Cns.Contains(xTrans)) {
    //            if (KTCC.Cns(xTrans).isDllConnect) {
    //                KTCC.CloseDBLinkAll();
    //                KTCC.CloseAll();
    //            }
    //        } else {
    //            if (isDllConnect_) {
    //                KTCC.CloseDBLinkAll();
    //                KTCC.CloseAll();
    //            }
    //        }

    //    }

    //    #region " IDisposable Support "
    //    // 由 Visual Basic 新增此程式碼以正確實作可處置的模式。
    //    public void Dispose()
    //    {
    //        Close();
    //        GC.SuppressFinalize(this);
    //    }
    //    #endregion

    //}

    public class KTConnections : DictionaryBase, IDisposable
    {
        public KTDBProvider this[String key]
        {
            get
            {
                return ((KTDBProvider)Dictionary[key]);
            }
            set
            {
                Dictionary[key] = value;
            }
        }

        public ICollection Keys
        {
            get
            {
                return (Dictionary.Keys);
            }
        }

        public ICollection Values
        {
            get
            {
                return (Dictionary.Values);
            }
        }

        public void Add(String key, KTDBProvider value)
        {
            Dictionary.Add(key, value);
        }

        public bool Contains(String key)
        {
            return (Dictionary.Contains(key));
        }

        public void Remove(String key)
        {
            Dictionary.Remove(key);
        }

        #region " IDisposable Support "
        // 由 Visual Basic 新增此程式碼以正確實作可處置的模式。

        private bool disposed = false;
        public void Dispose()
        {
            if (this.disposed)
                return;

            disposed = true;
            GC.SuppressFinalize(this);
        }
        #endregion
    }

    public class KTDBCommand : IDisposable
    {
        private IDbCommand Command_;
        private OdbcCommand OdbcCommand_;
        private OleDbCommand OleDBCommand_;
        private SqlCommand SqlCommand_;
#if OracleODAC
        private Oracle.ManagedDataAccess.Client.OracleCommand OracleCommand_;
#elif OracleClient
	    private System.Data.OracleClient.OracleCommand OracleCommand_;
#endif
#if MySqlClient
        private MySqlCommand MySqlCommand_;
#endif

        //private IDbDataParameter DataParameter;
        private OdbcParameter OdbcParameter_;
        private OleDbParameter OleDBParameter_;
        private SqlParameter SqlParameter_;
#if OracleODAC
        private Oracle.ManagedDataAccess.Client.OracleParameter OracleParameter_;
#elif OracleClient
        private System.Data.OracleClient.OracleParameter OracleParameter_;
#endif
#if MySqlClient
        private MySqlParameter MySqlParameter_;
#endif

        private System.Collections.Specialized.StringCollection CLOBColumn_ = null;
        public string CLOBColumn
        {
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    CLOBColumn_ = new System.Collections.Specialized.StringCollection();
                    CLOBColumn_.AddRange(value.Replace(" ", "").Split(','));
                }
            }
        }

        private string CommandText_ = "";

        //預設 System.Data.CommandType.Text
        private CommandType CommandType_ = System.Data.CommandType.Text;

        private System.Collections.Specialized.ListDictionary myParameters = new System.Collections.Specialized.ListDictionary();
        private System.Collections.Specialized.ListDictionary mySize = new System.Collections.Specialized.ListDictionary();

        private System.Collections.Specialized.ListDictionary myDirection = new System.Collections.Specialized.ListDictionary();

        private DBProviderType initDBConfig;
        private ADOProviderType initADOConfig;
        public bool CreateCommand(DBProviderType DBConfig, ADOProviderType ADOConfig)
        {
            initDBConfig = DBConfig;
            initADOConfig = ADOConfig;

            switch (initADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:
                    if (OdbcCommand_ == null)
                    {
                        OdbcCommand_ = new OdbcCommand();
                        Command_ = OdbcCommand_;
                    }
                    break;

                case ADOProviderType.Oledb:
                    if (OleDBCommand_ == null)
                    {
                        OleDBCommand_ = new OleDbCommand();
                        Command_ = OleDBCommand_;
                    }
                    break;

                case ADOProviderType.SQLClient:
                    if (SqlCommand_ == null)
                    {
                        SqlCommand_ = new SqlCommand();
                        Command_ = SqlCommand_;
                    }
                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:
                    if (OracleCommand_ == null)
                    {
                        OracleCommand_ = new Oracle.ManagedDataAccess.Client.OracleCommand();
                        Command_ = OracleCommand_;
                    }
                    break;
#elif OracleClient
                case ADOProviderType.OracleClient:
				    if (OracleCommand_ == null) {
                        OracleCommand_ = new System.Data.OracleClient.OracleCommand();
					    Command_ = OracleCommand_;
				    }
                    break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:
                    if (MySqlCommand_ == null)
                    {
                        MySqlCommand_ = new MySqlCommand();
                        Command_ = MySqlCommand_;
                    }
                    break;
#endif
            }

            if (!InitCommand())
                return false;

            return true;
        }

        public IDbCommand Command
        {
            get { return Command_; }
        }

        private bool InitCommand()
        {
            if (string.IsNullOrEmpty(CommandText_))
                throw new ApplicationException("SQL 語法錯誤：沒有輸入 CommandText 的 SQL 語法");


            string tmpSQL = CommandText_;

            while (tmpSQL.IndexOf("  ") > -1)
                tmpSQL = tmpSQL.Replace("  ", " ");

            if (myParameters.Count > 0)
            {

                foreach (DictionaryEntry myDE in myParameters)
                    Add_To_Parameters_((string)myDE.Key, myDE.Value);

                if (tmpSQL.IndexOf("= ?") > -1 | tmpSQL.IndexOf("=?") > -1)
                {
                    bool insertFlag = false;
                    bool replaceSql = false;

                    switch (initADOConfig)
                    {
                        case ADOProviderType.OracleClient:
                        case ADOProviderType.SQLClient:
                        case ADOProviderType.MySqlClient:
                            replaceSql = true;
                            break;
                    }

                    if (replaceSql)
                    {
                        if (tmpSQL.ToUpper().IndexOf("Insert ".ToUpper()) > -1)
                            insertFlag = true;

                        foreach (DictionaryEntry myDE in myParameters)
                        {
                            if (!insertFlag)
                            {
                                int sidx = tmpSQL.ToUpper().IndexOf((string)myDE.Key.ToString().ToUpper());
                                int eidx = -1;
                                if (sidx == -1)
                                {

                                    break;
                                }

                                eidx = tmpSQL.IndexOf("?", sidx);
                                switch (initDBConfig)
                                {
                                    case DBProviderType.Oracle:
                                        tmpSQL = tmpSQL.Substring(0, eidx) + ":" + (string)myDE.Key.ToString().ToUpper() + tmpSQL.Substring(eidx + 1);
                                        break;

                                    case DBProviderType.SQLServer:
                                    case DBProviderType.MySql:
                                        tmpSQL = tmpSQL.Substring(0, eidx) + "@" + (string)myDE.Key.ToString().ToUpper() + tmpSQL.Substring(eidx + 1);
                                        break;
                                }
                            }
                            else
                            {
                                int sidx = tmpSQL.IndexOf("?");

                                switch (initDBConfig)
                                {
                                    case DBProviderType.Oracle:
                                        tmpSQL = tmpSQL.Substring(0, sidx) + ":" + (string)myDE.Key.ToString().ToUpper() + tmpSQL.Substring(sidx + 1);
                                        break;

                                    case DBProviderType.SQLServer:
                                    case DBProviderType.MySql:
                                        tmpSQL = tmpSQL.Substring(0, sidx) + "@" + (string)myDE.Key.ToString().ToUpper() + tmpSQL.Substring(sidx + 1);
                                        break;

                                }
                            }
                        }

                        CommandText_ = tmpSQL + " -- InitCommand 自動 調整 具名參數(命名參數) Sql 語法";
                    }
                }
            }

            Command_.CommandText = CommandText_;
            Command_.CommandType = CommandType_;

            return true;
        }

        private void Add_To_Parameters_(string Name, object DataValue)
        {
            //加入 SQLParameters(以下執行結果均為相同)
            //1 使用 SQLCommand.Parameters.Add 方法 
            //  cmd.Parameters.Add("@myregion", SqlDbType.NVarChar); 
            //  cmd.Parameters["@myregion"].Value = textBox1.Text;

            //2 使用 SQLCommand.Parameters.Add 方法加入 SQLParameters 類別 
            //  cmd.Parameters.Add(new SqlParameter("@myregion", textBox1.Text));
            //
            //3 使用 SQLCommand.Parameters.AddWithValue 方法 (IDbCommand 沒有 AddWithValue 方法，必須回到原始的 Data Property)
            //  cmd.Parameters.AddWithValue("@myregion", textBox1.Text);

            switch (initADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:

                    if (OdbcCommand_.Parameters.Contains(Name))
                        OdbcCommand_.Parameters[Name].Value = DataValue;
                    else
                    {
                        OdbcParameter_ = new OdbcParameter(Name, DataValue);
                        if (myDirection.Contains(Name))
                            OdbcParameter_.Direction = (ParameterDirection)myDirection[Name];
                        if (mySize.Contains(Name))
                            OdbcParameter_.Size = (int)mySize[Name];
                        OdbcCommand_.Parameters.Add(OdbcParameter_);
                    }

                    break;

                case ADOProviderType.Oledb:

                    if (OleDBCommand_.Parameters.Contains(Name))
                        OleDBCommand_.Parameters[Name].Value = DataValue;
                    else
                    {
                        OleDBParameter_ = new OleDbParameter(Name, DataValue);
                        if (myDirection.Contains(Name))
                            OleDBParameter_.Direction = (ParameterDirection)myDirection[Name];
                        if (mySize.Contains(Name))
                            OleDBParameter_.Size = (int)mySize[Name];
                        OleDBCommand_.Parameters.Add(OleDBParameter_);
                    }

                    break;

                case ADOProviderType.SQLClient:

                    if (Name.Substring(0, 1) != "@")
                        Name = "@" + Name;

                    if (SqlCommand_.Parameters.Contains(Name))
                        SqlCommand_.Parameters[Name].Value = DataValue;
                    else
                    {
                        SqlParameter_ = new SqlParameter(Name, DataValue);
                        if (myDirection.Contains(Name))
                            SqlParameter_.Direction = (ParameterDirection)myDirection[Name];
                        if (mySize.Contains(Name))
                            SqlParameter_.Size = (int)mySize[Name];
                        SqlCommand_.Parameters.Add(SqlParameter_);
                    }

                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:

                    if (OracleCommand_.Parameters.Contains(Name))
                        OracleCommand_.Parameters[Name].Value = DataValue;
                    else
                    {
                        OracleParameter_ = new Oracle.ManagedDataAccess.Client.OracleParameter(Name, DataValue);
                        if (myDirection.Contains(Name))
                            OracleParameter_.Direction = (ParameterDirection)myDirection[Name];
                        if (mySize.Contains(Name))
                            OracleParameter_.Size = (int)mySize[Name];
                        OracleCommand_.Parameters.Add(OracleParameter_);
                    }

                    break;
#elif OracleClient
                case ADOProviderType.OracleClient:

                    if (OracleCommand_.Parameters.Contains(Name))
                        OracleCommand_.Parameters[Name].Value = DataValue;
                    else
                    {
                        OracleParameter_ = new System.Data.OracleClient.OracleParameter(Name, DataValue);
				        if (myDirection.Contains(Name))
					        OracleParameter_.Direction = (ParameterDirection) myDirection[Name];
				        if (mySize.Contains(Name))
					        OracleParameter_.Size = (int) mySize[Name];
				        OracleCommand_.Parameters.Add(OracleParameter_);
                    }

                    break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:

                    if (MySqlCommand_.Parameters.Contains(Name))
                        MySqlCommand_.Parameters[Name].Value = DataValue;
                    else
                    {
                        MySqlParameter_ = new MySqlParameter(Name, DataValue);
                        if (myDirection.Contains(Name))
                            MySqlParameter_.Direction = (ParameterDirection)myDirection[Name];
                        if (mySize.Contains(Name))
                            MySqlParameter_.Size = (int)mySize[Name];
                        MySqlCommand_.Parameters.Add(MySqlParameter_);
                    }
                    break;
#endif

            }

            //If myDirection.Contains(Name) Then DataParameter.Direction = myDirection[Name]
            //Command_.Parameters.Add(DataParameter)

        }

        public CommandType CommandType
        {
            get { return CommandType_; }
            set { CommandType_ = value; }
        }

        public string CommandText
        {
            get { return CommandText_; }
            set { CommandText_ = value; }
        }

        //1061227 居易
        //經常會忘記 CommandText 屬性，故新增 sqlstr 
        public string sqlstr
        {
            get { return CommandText_; }
            set { CommandText_ = value; }
        }

        public void Parameters(string Name, object DataValue)
        {
            if (!myParameters.Contains(Name))
                myParameters.Add(Name, DataValue);
        }

        public object Parameters(string Name)
        {

            object value = Convert.DBNull;

            switch (initADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:
                    value = OdbcCommand_.Parameters[Name].Value;
                    break;

                case ADOProviderType.Oledb:
                    value = OleDBCommand_.Parameters[Name].Value;
                    break;

                case ADOProviderType.SQLClient:
                    if (Name.Substring(0, 1) != "@")
                        Name = "@" + Name;
                    value = SqlCommand_.Parameters[Name].Value;
                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:
                    value = OracleCommand_.Parameters[Name].Value;
                    break;
#elif OracleClient
                case ADOProviderType.OracleClient:
				    value = OracleCommand_.Parameters[Name].Value;
                    break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:
                    value = MySqlCommand_.Parameters[Name].Value;
                    break;
#endif
            }

            return value;

            //Return CType(Command_.Parameters[Name], System.Data.IDataParameter).Value
        }

        public void Direction(string Name, System.Data.ParameterDirection Direction)
        {
            if (myDirection.Contains(Name))
                myDirection.Remove(Name);
            if (!myDirection.Contains(Name))
                myDirection.Add(Name, Direction);

            switch (initADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:
                    if ((OdbcCommand_ != null))
                    {
                        if (OdbcCommand_.Parameters.Contains(Name))
                        {
                            OdbcCommand_.Parameters[Name].Direction = Direction;
                        }
                    }
                    break;

                case ADOProviderType.Oledb:
                    if ((OleDBCommand_ != null))
                    {
                        if (OleDBCommand_.Parameters.Contains(Name) & (myDirection[Name] != null))
                        {
                            OleDBCommand_.Parameters[Name].Direction = Direction;
                        }
                    }
                    break;

                case ADOProviderType.SQLClient:
                    if ((SqlCommand_ != null))
                    {
                        if (Name.Substring(0, 1) != "@") Name = "@" + Name;
                        if (SqlCommand_.Parameters.Contains(Name))
                        {
                            SqlCommand_.Parameters[Name].Direction = Direction;
                        }
                    }
                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:
                    if ((OracleCommand_ != null))
                    {
                        if (OracleCommand_.Parameters.Contains(Name))
                        {
                            OracleCommand_.Parameters[Name].Direction = Direction;
                        }
                    }
                    break;
#elif OracleClient
                case ADOProviderType.OracleClient:
				    if ((OracleCommand_ != null)) {
					    if (OracleCommand_.Parameters.Contains(Name)) {
						    OracleCommand_.Parameters[Name].Direction = Direction;
					    }
				    }
                    break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:
                    if ((MySqlCommand_ != null))
                    {
                        if (MySqlCommand_.Parameters.Contains(Name) & (myDirection[Name] != null))
                        {
                            MySqlCommand_.Parameters[Name].Direction = Direction;
                        }
                    }
                    break;
#endif
            }
        }


        public void Direction(string Name, int size = -1, System.Data.ParameterDirection Direction = ParameterDirection.Input)
        {
            //1060613 Direction 的使用方式
            //1.必須先使用 ParametersSet(沒定義資料型別)
            //2.或 ParametersCreate (有定義資料型別) 建立紀錄之後，之後重新修正 Direction 參數
            //不然沒事情是可以不需要使用此 Function 

            if (myDirection.Contains(Name))
                myDirection.Remove(Name);
            if (size != -1)
                if (!mySize.Contains(Name))
                    mySize.Add(Name, size);
            if (!myDirection.Contains(Name))
                myDirection.Add(Name, Direction);

            switch (initADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:
                    if ((OdbcCommand_ != null))
                    {
                        if (OdbcCommand_.Parameters.Contains(Name))
                        {
                            OdbcCommand_.Parameters[Name].Direction = Direction;
                            if (mySize.Contains(Name))
                                OdbcCommand_.Parameters[Name].Size = (int)mySize[Name];
                        }
                    }
                    break;

                case ADOProviderType.Oledb:
                    if ((OleDBCommand_ != null))
                    {
                        if (OleDBCommand_.Parameters.Contains(Name) & (myDirection[Name] != null))
                        {
                            OleDBCommand_.Parameters[Name].Direction = Direction;
                            if (mySize.Contains(Name))
                                OleDBCommand_.Parameters[Name].Size = (int)mySize[Name];
                        }
                    }

                    break;

                case ADOProviderType.SQLClient:
                    if ((SqlCommand_ != null))
                    {
                        if (Name.Substring(0, 1) != "@") Name = "@" + Name;
                        if (SqlCommand_.Parameters.Contains(Name))
                        {
                            SqlCommand_.Parameters[Name].Direction = Direction;
                            if (mySize.Contains(Name))
                                SqlCommand_.Parameters[Name].Size = (int)mySize[Name];
                        }
                    }
                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:
                    if ((OracleCommand_ != null))
                    {
                        if (OracleCommand_.Parameters.Contains(Name))
                        {
                            OracleCommand_.Parameters[Name].Direction = Direction;
                            if (mySize.Contains(Name))
                                OracleCommand_.Parameters[Name].Size = (int)mySize[Name];
                        }
                    }
                    break;
#elif OracleClient
                case ADOProviderType.OracleClient:
				    if ((OracleCommand_ != null)) {
					    if (OracleCommand_.Parameters.Contains(Name)) {
						    OracleCommand_.Parameters[Name].Direction = Direction;
						    if (mySize.Contains(Name))
							    OracleCommand_.Parameters[Name].Size = (int) mySize[Name];
					    }
				    }
                    break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:
                    if ((MySqlCommand_ != null))
                    {
                        if (MySqlCommand_.Parameters.Contains(Name) & (myDirection[Name] != null))
                        {
                            MySqlCommand_.Parameters[Name].Direction = Direction;
                            if (mySize.Contains(Name))
                                MySqlCommand_.Parameters[Name].Size = (int)mySize[Name];
                        }
                    }
                    break;
#endif
            }
        }

        public void ParametersClear()
        {
            myParameters.Clear();
            mySize.Clear();
            myDirection.Clear();

            switch (initADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:
                    if ((OdbcCommand_ != null))
                        OdbcCommand_.Parameters.Clear();
                    break;

                case ADOProviderType.Oledb:
                    if ((OleDBCommand_ != null))
                        OleDBCommand_.Parameters.Clear();

                    break;

                case ADOProviderType.SQLClient:
                    if ((SqlCommand_ != null))
                        SqlCommand_.Parameters.Clear();
                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:
                    if ((SqlCommand_ != null))
                        OracleCommand_.Parameters.Clear();
                    break;
#elif OracleClient
                case ADOProviderType.OracleClient:
				    if ((SqlCommand_ != null))
					    OracleCommand_.Parameters.Clear();
                    break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:
                    if ((MySqlCommand_ != null))
                        MySqlCommand_.Parameters.Clear();
                    break;
#endif
            }
        }

        public void DisposeCommand()
        {
            ParametersClear();
            Command_.Dispose();

            if ((OdbcCommand_ != null)) OdbcCommand_.Dispose();
            if ((OleDBCommand_ != null)) OleDBCommand_.Dispose();
            if ((SqlCommand_ != null)) SqlCommand_.Dispose();
#if OracleODAC
            if ((OracleCommand_ != null)) OracleCommand_.Dispose();
#elif OracleClient
		    if ((OracleCommand_ != null)) OracleCommand_.Dispose();
#endif
#if MySqlClient
            if ((MySqlCommand_ != null)) MySqlCommand_.Dispose();
#endif
        }

        public KTDBProvider KTDBProvider
        {
            set
            {
                initADOConfig = value.ADOConfig;
                initDBConfig = value.DBConfig;
                Connection = value.Connection;
                Command_.Transaction = value.Transaction;
            }
        }

        private IDbConnection Connection
        {

            set
            {
                //                switch (value.GetType().ToString())
                //                {
                //				    case "System.Data.Odbc.OdbcConnection":
                //					    initADOConfig = ADOProviderType.Odbc;
                //					    break;

                //				    case "System.Data.OleDb.OleDbConnection":
                //					    initADOConfig = ADOProviderType.Oledb;
                //					    break;

                //				    case "System.Data.SqlClient.SqlConnection":
                //					    initADOConfig = ADOProviderType.SQLClient;
                //					    break;

                //#if OracleODAC
                //                    case "Oracle.ManagedDataAccess.Client.OracleConnection":
                //                        initDBConfig = DBProviderType.Oracle;
                //                        initADOConfig = ADOProviderType.OracleClient;
                //                        break;
                //#elif OracleClient
                //				    case "System.Data.OracleClient.OracleConnection":
                //                        initDBConfig = DBProviderType.Oracle;
                //					    initADOConfig = ADOProviderType.OracleClient;
                //                        break;
                //#endif

                //#if MySqlClient
                //                    case "MySql.Data.MySqlClient.MySqlConnection":
                //					    initDBConfig = DBProviderType.MySql;
                //					    initADOConfig = ADOProviderType.MySqlClient;
                //                        break;
                //#endif
                //			    }

                switch (initADOConfig)
                {
                    case ADOProviderType.Odbc:
                    case ADOProviderType.MysqlOdbc35:
                    case ADOProviderType.MysqlOdbc52:
                        OdbcCommand_ = new OdbcCommand();
                        OdbcCommand_.Connection = (System.Data.Odbc.OdbcConnection)value;
                        Command_ = OdbcCommand_;
                        break;

                    case ADOProviderType.Oledb:
                        OleDBCommand_ = new OleDbCommand();
                        OleDBCommand_.Connection = (System.Data.OleDb.OleDbConnection)value;
                        Command_ = OleDBCommand_;
                        break;

                    case ADOProviderType.SQLClient:
                        SqlCommand_ = new SqlCommand();
                        SqlCommand_.Connection = (System.Data.SqlClient.SqlConnection)value;
                        Command_ = SqlCommand_;
                        break;

#if OracleODAC
                    case ADOProviderType.OracleClient:
                        OracleCommand_ = new Oracle.ManagedDataAccess.Client.OracleCommand();
                        OracleCommand_.Connection = (Oracle.ManagedDataAccess.Client.OracleConnection)value;
                        Command_ = OracleCommand_;
                        break;
#elif OracleClient
                    case ADOProviderType.OracleClient:
                        OracleCommand_ = new System.Data.OracleClient.OracleCommand();
					    OracleCommand_.Connection = (System.Data.OracleClient.OracleConnection) value;
					    Command_ = OracleCommand_;
                        break;
#endif

#if MySqlClient
                    case ADOProviderType.MySqlClient:
                        MySqlCommand_ = new MySqlCommand();
                        MySqlCommand_.Connection = (MySql.Data.MySqlClient.MySqlConnection)value;
                        Command_ = MySqlCommand_;
                        break;
#endif
                }
            }
        }



        //Parameters Direction 觀念
        //stored procedure：沒有回傳值，需要回傳值必須使用 Output 來抓回傳值

        //function：有回傳值，可以使用 select from dual 來抓回傳值
        //          若有 Output 則無法使用 select from dual。
        //          dual 是 oracle 的預設 table，存在的目的是為了能夠直接顯示某些簡單計算的答案

        //package：類似 Java 中的 package 或是 .NET 中的 namespace 一樣，
        //         可以用來區隔相同名稱的 stored procedure 或是 function。
        public void ParametersCreate(string Name, OdbcType dataType, int size = -1)
        {
            if (size == -1)
            {
                OdbcCommand_.Parameters.Add(new OdbcParameter(Name, dataType));
            }
            else
            {
                OdbcCommand_.Parameters.Add(new OdbcParameter(Name, dataType, size));
            }
        }

        public void ParametersCreate(string Name, OleDbType dataType, int size = -1)
        {
            if (size == -1)
            {
                OleDBCommand_.Parameters.Add(new OleDbParameter(Name, dataType));
            }
            else
            {
                OleDBCommand_.Parameters.Add(new OleDbParameter(Name, dataType, size));
            }
        }

        public void ParametersCreate(string Name, SqlDbType dataType, int size = -1)
        {
            if (Name.Substring(0, 1) != "@")
                Name = "@" + Name;
            if (size == -1)
            {
                SqlCommand_.Parameters.Add(new SqlParameter(Name, dataType));
            }
            else
            {
                SqlCommand_.Parameters.Add(new SqlParameter(Name, dataType, size));
            }
        }

#if OracleODAC
        public void ParametersCreate(string Name, Oracle.ManagedDataAccess.Client.OracleDbType dataType, int size = -1)
        {
            if (size == -1)
            {
                OracleCommand_.Parameters.Add(new Oracle.ManagedDataAccess.Client.OracleParameter(Name, dataType));
            }
            else
            {
                OracleCommand_.Parameters.Add(new Oracle.ManagedDataAccess.Client.OracleParameter(Name, dataType, size));
            }
        }
#elif OracleClient
	    public void ParametersCreate(string Name, System.Data.OracleClient.OracleType dataType, int size = -1)
	    {
		    if (size == -1) {
                OracleCommand_.Parameters.Add(new System.Data.OracleClient.OracleParameter(Name, dataType));
		    } else {
                OracleCommand_.Parameters.Add(new System.Data.OracleClient.OracleParameter(Name, dataType, size));
		    }
	    }
#endif

#if MySqlClient
        public void ParametersCreate(string Name, MySqlDbType dataType, int size = -1)
        {
            if (size == -1)
            {
                MySqlCommand_.Parameters.Add(new MySqlParameter(Name, dataType));
            }
            else
            {
                MySqlCommand_.Parameters.Add(new MySqlParameter(Name, dataType, size));
            }
        }
#endif

        public void ParametersCreate(string Name, System.TypeCode dataType, int size = -1)
        {
            switch (initADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:
                    if (size == -1)
                        OdbcCommand_.Parameters.Add(new OdbcParameter(Name, GetOdbcType((int)dataType)));
                    else
                        OdbcCommand_.Parameters.Add(new OdbcParameter(Name, GetOdbcType((int)dataType), size));
                    break;

                case ADOProviderType.Oledb:
                    if (size == -1)
                        OleDBCommand_.Parameters.Add(new OleDbParameter(Name, GetOleDbType((int)dataType)));
                    else
                        OleDBCommand_.Parameters.Add(new OleDbParameter(Name, GetOleDbType((int)dataType), size));
                    break;

                case ADOProviderType.SQLClient:
                    if (Name.Substring(0, 1) != "@") Name = "@" + Name;
                    if (size == -1)
                        SqlCommand_.Parameters.Add(new SqlParameter(Name, GetSqlDbType((int)dataType)));
                    else
                        SqlCommand_.Parameters.Add(new SqlParameter(Name, GetSqlDbType((int)dataType), size));
                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:
                    if (size == -1)
                        OracleCommand_.Parameters.Add(new Oracle.ManagedDataAccess.Client.OracleParameter(Name, GetOracleType((int)dataType)));
                    else
                        OracleCommand_.Parameters.Add(new Oracle.ManagedDataAccess.Client.OracleParameter(Name, GetOracleType((int)dataType), size));
                    break;
#elif OracleClient
                case ADOProviderType.OracleClient:
                    if (size == -1) 
                        OracleCommand_.Parameters.Add(new System.Data.OracleClient.OracleParameter(Name, GetOracleType((int) dataType)));
                    else 
                        OracleCommand_.Parameters.Add(new System.Data.OracleClient.OracleParameter(Name, GetOracleType((int) dataType), size));
                    break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:
                    if (size == -1)
                        MySqlCommand_.Parameters.Add(new MySqlParameter(Name, GetMySqlType((int)dataType)));
                    else
                        MySqlCommand_.Parameters.Add(new MySqlParameter(Name, GetMySqlType((int)dataType), size));
                    break;
#endif
            }
        }

        public void ParametersCreate(string Name, OdbcType dataType, int size, System.Data.ParameterDirection Direction)
        {
            OdbcCommand_.Parameters.Add(new OdbcParameter(Name, dataType, size));
            OdbcCommand_.Parameters[Name].Direction = Direction;
        }

        public void ParametersCreate(string Name, OleDbType dataType, int size, System.Data.ParameterDirection Direction)
        {
            OleDBCommand_.Parameters.Add(new OleDbParameter(Name, dataType, size));
            OleDBCommand_.Parameters[Name].Direction = Direction;
        }

        public void ParametersCreate(string Name, SqlDbType dataType, int size, System.Data.ParameterDirection Direction)
        {
            if (Name.Substring(0, 1) != "@") Name = "@" + Name;
            SqlCommand_.Parameters.Add(new SqlParameter(Name, dataType, size));
            SqlCommand_.Parameters[Name].Direction = Direction;
        }

#if OracleODAC
        public void ParametersCreate(string Name, Oracle.ManagedDataAccess.Client.OracleDbType dataType, int size, System.Data.ParameterDirection Direction)
        {
            OracleCommand_.Parameters.Add(new Oracle.ManagedDataAccess.Client.OracleParameter(Name, dataType, size));
            OracleCommand_.Parameters[Name].Direction = Direction;
        }
#elif OracleClient
        public void ParametersCreate(string Name, System.Data.OracleClient.OracleType dataType, int size, System.Data.ParameterDirection Direction)
        {
            OracleCommand_.Parameters.Add(new System.Data.OracleClient.OracleParameter(Name, dataType, size));
            OracleCommand_.Parameters[Name].Direction = Direction;
        }
#endif

#if MySqlClient
        public void ParametersCreate(string Name, MySqlDbType dataType, int size, System.Data.ParameterDirection Direction)
        {
            MySqlCommand_.Parameters.Add(new MySqlParameter(Name, dataType, size));
            MySqlCommand_.Parameters[Name].Direction = Direction;
        }
#endif

        public void ParametersCreate(string Name, System.TypeCode dataType, int size, System.Data.ParameterDirection Direction)
        {
            switch (initADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:
                    OdbcCommand_.Parameters.Add(new OdbcParameter(Name, GetOdbcType((int)dataType), size));
                    OdbcCommand_.Parameters[Name].Direction = Direction;
                    break;

                case ADOProviderType.Oledb:
                    OleDBCommand_.Parameters.Add(new OleDbParameter(Name, GetOleDbType((int)dataType), size));
                    OleDBCommand_.Parameters[Name].Direction = Direction;
                    break;

                case ADOProviderType.SQLClient:
                    if (Name.Substring(0, 1) != "@") Name = "@" + Name;
                    SqlCommand_.Parameters.Add(new SqlParameter(Name, GetSqlDbType((int)dataType), size));
                    SqlCommand_.Parameters[Name].Direction = Direction;
                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:
                    OracleCommand_.Parameters.Add(new Oracle.ManagedDataAccess.Client.OracleParameter(Name, GetOracleType((int)dataType), size));
                    OracleCommand_.Parameters[Name].Direction = Direction;
                    break;
#elif OracleClient
                case ADOProviderType.OracleClient:
                    OracleCommand_.Parameters.Add(new System.Data.OracleClient.OracleParameter(Name, GetOracleType((int) dataType), size));
                    OracleCommand_.Parameters[Name].Direction = Direction;
                    break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:
                    MySqlCommand_.Parameters.Add(new MySqlParameter(Name, GetMySqlType((int)dataType), size));
                    MySqlCommand_.Parameters[Name].Direction = Direction;
                    break;
#endif
            }
        }

        public void ParametersSet(String Name, object value)
        {
            switch (initADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:
                    if (!OdbcCommand_.Parameters.Contains(Name))
                    {
                        throw new ApplicationException("沒有 Parameters參數：" + Name + Environment.NewLine + Environment.NewLine + "請先執行：ParamtersCreate");
                    }

                    if ((value != null))
                        OdbcCommand_.Parameters[Name].Value = value;
                    if (myDirection.Contains(Name))
                        OdbcCommand_.Parameters[Name].Direction = (ParameterDirection)myDirection[Name];
                    break;

                case ADOProviderType.Oledb:
                    if (!OleDBCommand_.Parameters.Contains(Name))
                    {
                        throw new ApplicationException("沒有 Parameters參數：" + Name + Environment.NewLine + Environment.NewLine + "請先執行：ParamtersCreate");
                    }

                    if ((value != null))
                        OleDBCommand_.Parameters[Name].Value = value;
                    if (myDirection.Contains(Name))
                        OleDBCommand_.Parameters[Name].Direction = (ParameterDirection)myDirection[Name];
                    break;

                case ADOProviderType.SQLClient:
                    if (Name.Substring(0, 1) != "@")
                        Name = "@" + Name;
                    if (!SqlCommand_.Parameters.Contains(Name))
                    {
                        throw new ApplicationException("沒有 Parameters參數：" + Name + Environment.NewLine + Environment.NewLine + "請先執行：ParamtersCreate");
                    }

                    if ((value != null))
                        SqlCommand_.Parameters[Name].Value = value;
                    if (myDirection.Contains(Name))
                        SqlCommand_.Parameters[Name].Direction = (ParameterDirection)myDirection[Name];
                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:
                    if (!OracleCommand_.Parameters.Contains(Name))
                    {
                        throw new ApplicationException("沒有 Parameters參數：" + Name + Environment.NewLine + Environment.NewLine + "請先執行：ParamtersCreate");
                    }

                    if ((value != null))
                        OracleCommand_.Parameters[Name].Value = value;
                    if (myDirection.Contains(Name))
                        OracleCommand_.Parameters[Name].Direction = (ParameterDirection)myDirection[Name];
                    break;
#elif OracleClient
                    case ADOProviderType.OracleClient:
					    if (!OracleCommand_.Parameters.Contains(Name)) {
						    throw new ApplicationException("沒有 Parameters參數：" + Name + Environment.NewLine + Environment.NewLine + "請先執行：ParamtersCreate");
					    }

					    if ((value != null))
						    OracleCommand_.Parameters[Name].Value = value;
					    if (myDirection.Contains(Name))
						    OracleCommand_.Parameters[Name].Direction = (ParameterDirection) myDirection[Name];
                        break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:
                    if (!MySqlCommand_.Parameters.Contains(Name))
                    {
                        throw new ApplicationException("沒有 Parameters參數：" + Name + Environment.NewLine + Environment.NewLine + "請先執行：ParamtersCreate");
                    }

                    if ((value != null))
                        MySqlCommand_.Parameters[Name].Value = value;
                    if (myDirection.Contains(Name))
                        MySqlCommand_.Parameters[Name].Direction = (ParameterDirection)myDirection[Name];
                    break;
#endif
            }
        }

        //sql Server 屬性可以將整個 DataTable 利用 Structured 參數方式整個寫入 DataBase
        //Oracle 開發的 ODP.NET 或許也可以，但是沒有測試。
        //Dim SqlCommand As New SqlCommand
        //SqlCommand.Connection = Command.Connection
        //SqlCommand.CommandType = Data.CommandType.StoredProcedure
        //SqlCommand.CommandText = "TempTable"
        //SqlCommand.Parameters.Add("@TempTable", SqlDbType.Structured).Value = DataTable_
        private string TableName_ = "";
        public string TableName
        {
            set { TableName_ = value; }
        }

        private DataTable DataTable_;
        public DataTable DataTable
        {
            set
            {
                DataTable_ = value;
                if (string.IsNullOrEmpty(TableName_) & DataTable_.TableName != "Table")
                    TableName_ = DataTable_.TableName;
            }
        }

        private DataRow DataRow_;
        public DataRow DataRow
        {
            set
            {
                DataRow_ = value;
                if (string.IsNullOrEmpty(TableName_) & DataRow_.Table.TableName != "Table")
                    TableName_ = DataRow_.Table.TableName;
            }
        }

        private int CommitCount_ = 1000;
        public int CommitCount
        {
            get
            {
                int functionReturnValue = 0;
                return functionReturnValue;
            }
            set { CommitCount_ = value; }
        }


        public void DataTableInsert()
        {
            if (string.IsNullOrEmpty(TableName_))
            {
                throw new ApplicationException("沒有資料表名稱(TableName)");
            }

            if (Command_.Connection == null)
            {
                throw new ApplicationException("沒有傳入連線");
            }

            switch (initADOConfig)
            {
                case ADOProviderType.Odbc:
                case ADOProviderType.MysqlOdbc35:
                case ADOProviderType.MysqlOdbc52:
                    OdbcCommand_.CommandType = System.Data.CommandType.Text;
                    OdbcCommand_.CommandText = CreateInsertSql();
                    break;

                case ADOProviderType.Oledb:
                    OleDBCommand_.CommandType = System.Data.CommandType.Text;
                    OleDBCommand_.CommandText = CreateInsertSql();
                    break;

                case ADOProviderType.SQLClient:
                    SqlCommand_.CommandType = System.Data.CommandType.Text;
                    SqlCommand_.CommandText = CreateInsertSql();
                    break;

#if OracleODAC
                case ADOProviderType.OracleClient:
                    OracleCommand_.CommandType = System.Data.CommandType.Text;
                    OracleCommand_.CommandText = CreateInsertSql();
                    break;
#elif OracleClient
                case ADOProviderType.OracleClient:
				    OracleCommand_.CommandType = System.Data.CommandType.Text;
				    OracleCommand_.CommandText = CreateInsertSql();
                    break;
#endif

#if MySqlClient
                case ADOProviderType.MySqlClient:
                    MySqlCommand_.CommandType = System.Data.CommandType.Text;
                    MySqlCommand_.CommandText = CreateInsertSql();
                    break;
#endif
            }

            string ColumnName = "";
            for (int x = 0; x <= DataTable_.Columns.Count - 1; x++)
            {
                //dColumn = DataTable_.Columns[x]

                ColumnName = DataTable_.Columns[x].ColumnName;

                switch (initADOConfig)
                {
                    case ADOProviderType.Odbc:
                    case ADOProviderType.MysqlOdbc35:
                    case ADOProviderType.MysqlOdbc52:
                        OdbcParameter Odbcp = new OdbcParameter();
                        Odbcp.ParameterName = ColumnName;
                        Odbcp.SourceColumn = ColumnName;
                        //Odbcp.OdbcType = GetOdbcType(DataTable_.Columns[x].DataType)
                        OdbcCommand_.Parameters.Add(Odbcp);
                        break;

                    case ADOProviderType.Oledb:
                        OleDbParameter OleDBp = new OleDbParameter();
                        OleDBp.ParameterName = ColumnName;
                        OleDBp.SourceColumn = ColumnName;
                        //OleDBp.OleDbType = GetOleDbType(DataTable_.Columns[x].DataType)
                        OleDBCommand_.Parameters.Add(OleDBp);
                        break;

                    case ADOProviderType.SQLClient:
                        SqlParameter SQLClientp = new SqlParameter();
                        SQLClientp.ParameterName = "@" + ColumnName;
                        SQLClientp.SourceColumn = ColumnName;
                        //SQLClientp.SqlDbType = GetSqlDbType(DataTable_.Columns[x].DataType)
                        SqlCommand_.Parameters.Add(SQLClientp);
                        break;

#if OracleODAC
                    case ADOProviderType.OracleClient:
                        //Oracle Client 應該也可以整個 Structured 傳入 DB Server
                        Oracle.ManagedDataAccess.Client.OracleParameter OracleClientp = new Oracle.ManagedDataAccess.Client.OracleParameter();
                        OracleClientp.ParameterName = ColumnName;
                        OracleClientp.SourceColumn = ColumnName;

                        switch (DataTable_.Columns[x].DataType.ToString())
                        {
                            case "System.DateTime":
                                //1080625
                                //資料產生 ORA-01861，以 Parameter 方式寫入 Date 型態的
                                //必須指定 Oracle Date的資料型態，才不會有異常發生
                                OracleClientp.OracleDbType = OracleDbType.Date;
                                break;
                        }

                        if (!Convert.IsDBNull(CLOBColumn_))
                        {

                        }
                        if ((CLOBColumn_ != null))
                        {
                            //1060609 居易 Oracle CLOB 仍然有錯誤
                            //雖然CLOB可以超過 4000個字，但是太長時仍然必須指定長度
                            //否則將出現 ORA-01461: 只有在將值插入資料類型為 LONG 的資料欄時, 才可以連結一個 LONG 值
                            //其他資料庫再找時間處理
                            if (CLOBColumn_.Contains(ColumnName))
                            {
                                OracleClientp.Size = 32765;
                                //整批寫入時，不知道全部 DataTable 長度，就給最大長度。
                            }
                        }
                        OracleCommand_.Parameters.Add(OracleClientp);
                        break;

#elif OracleClient
                    case ADOProviderType.OracleClient:
					    //Oracle Client 應該也可以整個 Structured 傳入 DB Server
                        System.Data.OracleClient.OracleParameter OracleClientp = new System.Data.OracleClient.OracleParameter();
					    OracleClientp.ParameterName = ColumnName;
					    OracleClientp.SourceColumn = ColumnName;

                        switch (DataTable_.Columns[x].DataType.ToString()) {
                            case "System.DateTime":
                                //1080625
                                //資料產生 ORA-01861，以 Parameter 方式寫入 Date 型態的
                                //必須指定 Oracle Date的資料型態，才不會有異常發生
                                OracleClientp.OracleType = OracleType.DateTime;
                                break;
                        }

					    if ((CLOBColumn_ != null)) {
						    //1060609 居易 Oracle CLOB 仍然有錯誤
						    //雖然CLOB可以超過 4000個字，但是太長時仍然必須指定長度
						    //否則將出現 ORA-01461: 只有在將值插入資料類型為 LONG 的資料欄時, 才可以連結一個 LONG 值
						    //其他資料庫再找時間處理
						    if (CLOBColumn_.Contains(ColumnName)) {
							    OracleClientp.Size = 32765;
							    //整批寫入時，不知道全部 DataTable 長度，就給最大長度。
						    }
					    }
					    OracleCommand_.Parameters.Add(OracleClientp);
                        break;
#endif

#if MySqlClient
                    case ADOProviderType.MySqlClient:
                        MySqlParameter MySqlClient = new MySqlParameter();
                        MySqlClient.ParameterName = ColumnName;
                        MySqlClient.SourceColumn = ColumnName;
                        //OleDBp.OleDbType = GetMySqlType(DataTable_.Columns[x].DataType)
                        MySqlCommand_.Parameters.Add(MySqlClient);
                        break;
#endif
                }
            }

            DataRow DataRow_ = default(DataRow);
            int Commitidx = 0;
            if (CommitCount_ > 0)
            {
                switch (initADOConfig)
                {
                    case ADOProviderType.Odbc:
                    case ADOProviderType.MysqlOdbc35:
                    case ADOProviderType.MysqlOdbc52:
                        OdbcCommand_.Transaction = OdbcCommand_.Connection.BeginTransaction();
                        break;

                    case ADOProviderType.Oledb:
                        OleDBCommand_.Transaction = OleDBCommand_.Connection.BeginTransaction();
                        break;

                    case ADOProviderType.SQLClient:
                        SqlCommand_.Transaction = SqlCommand_.Connection.BeginTransaction();
                        break;

#if OracleODAC
                    case ADOProviderType.OracleClient:
                        OracleCommand_.Transaction = OracleCommand_.Connection.BeginTransaction();
                        break;
#elif OracleClient
                    case ADOProviderType.OracleClient:
					    OracleCommand_.Transaction = OracleCommand_.Connection.BeginTransaction();
                        break;
#endif

#if MySqlClient
                    case ADOProviderType.MySqlClient:
                        MySqlCommand_.Transaction = MySqlCommand_.Connection.BeginTransaction();
                        break;
#endif
                }
            }

            for (int x = 0; x <= DataTable_.Rows.Count - 1; x++)
            {
                DataRow_ = DataTable_.Rows[x];

                for (int y = 0; y <= DataTable_.Columns.Count - 1; y++)
                {
                    switch (initADOConfig)
                    {
                        case ADOProviderType.Odbc:
                        case ADOProviderType.MysqlOdbc35:
                        case ADOProviderType.MysqlOdbc52:
                            OdbcCommand_.Parameters[DataTable_.Columns[y].ColumnName].Value = DataRow_[DataTable_.Columns[y].ColumnName];
                            break;

                        case ADOProviderType.Oledb:
                            OleDBCommand_.Parameters[DataTable_.Columns[y].ColumnName].Value = DataRow_[DataTable_.Columns[y].ColumnName];
                            break;

                        case ADOProviderType.SQLClient:
                            SqlCommand_.Parameters["@" + DataTable_.Columns[y].ColumnName].Value = DataRow_[DataTable_.Columns[y].ColumnName];
                            break;

#if OracleODAC
                        case ADOProviderType.OracleClient:
                            OracleCommand_.Parameters[DataTable_.Columns[y].ColumnName].Value = DataRow_[DataTable_.Columns[y].ColumnName];
                            break;
#elif OracleClient
                        case ADOProviderType.OracleClient:
						    OracleCommand_.Parameters[DataTable_.Columns[y].ColumnName].Value = DataRow_[DataTable_.Columns[y].ColumnName];
                            break;
#endif

#if MySqlClient
                        case ADOProviderType.MySqlClient:
                            MySqlCommand_.Parameters[DataTable_.Columns[y].ColumnName].Value = DataRow_[DataTable_.Columns[y].ColumnName];
                            break;
#endif
                    }

                }

                Command_.ExecuteScalar();

                if (CommitCount_ > 0)
                {
                    Commitidx = Commitidx + 1;
                    if (Commitidx == CommitCount_)
                    {
                        System.Threading.Thread.Sleep(1);
                        Commitidx = 0;
                        Commitidx = Commitidx + 1;

                        switch (initADOConfig)
                        {
                            case ADOProviderType.Odbc:
                            case ADOProviderType.MysqlOdbc35:
                            case ADOProviderType.MysqlOdbc52:
                                OdbcCommand_.Transaction.Commit();
                                OdbcCommand_.Transaction = OdbcCommand_.Connection.BeginTransaction();
                                break;

                            case ADOProviderType.Oledb:
                                OleDBCommand_.Transaction.Commit();
                                OleDBCommand_.Transaction = OleDBCommand_.Connection.BeginTransaction();
                                break;

                            case ADOProviderType.SQLClient:
                                SqlCommand_.Transaction.Commit();
                                SqlCommand_.Transaction = SqlCommand_.Connection.BeginTransaction();
                                break;

#if OracleODAC
                            case ADOProviderType.OracleClient:
                                OracleCommand_.Transaction.Commit();
                                OracleCommand_.Transaction = OracleCommand_.Connection.BeginTransaction();
                                break;
#elif OracleClient 
                            case ADOProviderType.OracleClient:
							    OracleCommand_.Transaction.Commit();
							    OracleCommand_.Transaction = OracleCommand_.Connection.BeginTransaction();
                                break;
#endif

#if MySqlClient
                            case ADOProviderType.MySqlClient:
                                MySqlCommand_.Transaction.Commit();
                                MySqlCommand_.Transaction = MySqlCommand_.Connection.BeginTransaction();
                                break;
#endif
                        }
                    }
                }
            }

            if (CommitCount_ > 0)
            {
                switch (initADOConfig)
                {
                    case ADOProviderType.Odbc:
                    case ADOProviderType.MysqlOdbc35:
                    case ADOProviderType.MysqlOdbc52:
                        if ((OdbcCommand_.Transaction != null))
                            OdbcCommand_.Transaction.Commit();
                        break;

                    case ADOProviderType.Oledb:
                        if ((OleDBCommand_.Transaction != null))
                            OleDBCommand_.Transaction.Commit();
                        break;

                    case ADOProviderType.SQLClient:
                        if ((SqlCommand_.Transaction != null))
                            SqlCommand_.Transaction.Commit();
                        break;

#if OracleODAC
                    case ADOProviderType.OracleClient:
                        if ((OracleCommand_.Transaction != null))
                            OracleCommand_.Transaction.Commit();
                        break;
#elif OracleClient
                    case ADOProviderType.OracleClient:
					    if ((OracleCommand_.Transaction != null))
						    OracleCommand_.Transaction.Commit();
                        break;
#endif

#if MySqlClient
                    case ADOProviderType.MySqlClient:
                        if ((MySqlCommand_.Transaction != null))
                            MySqlCommand_.Transaction.Commit();
                        break;
#endif
                }
            }

        }

        private string SpecialColumnName(string ColumnName, DBProviderType DBConfig = DBProviderType.Oracle)
        {
            string colStr = "";

            switch (DBConfig)
            {
                case DBProviderType.Oracle:
                    colStr = colStr + ColumnName;
                    break;

                case DBProviderType.Access:
                case DBProviderType.Access_accdb:
                    //Access 可以讓所有的欄位都當作特殊欄位
                    colStr = colStr + "[" + ColumnName + "]";
                    break;

                case DBProviderType.SQLServer:
                    if (ColumnName.IndexOf("/") > -1 | ColumnName.IndexOf(" ") > -1)
                        colStr = colStr + "[" + ColumnName + "]";
                    else
                        colStr = colStr + ColumnName;
                    break;

                case DBProviderType.MySql:
                    colStr = colStr + "`" + ColumnName + "`";
                    break;

                default:
                    //預設使用中括弧標示出來，其他資料庫有不同之處，再額外處理
                    if (ColumnName.IndexOf("/") > -1 | ColumnName.IndexOf(" ") > -1)
                        colStr = colStr + "[" + ColumnName + "]";
                    else
                        colStr = colStr + ColumnName;
                    break;
            }

            return colStr;
        }

        private string CreateInsertSql()
        {

            System.Text.StringBuilder wkstr = new System.Text.StringBuilder();
            System.Text.StringBuilder ColumnStr = new System.Text.StringBuilder();
            System.Text.StringBuilder QMarkStr = new System.Text.StringBuilder();
            string ColumnName = "";

            for (int x = 0; x <= DataTable_.Columns.Count - 1; x++)
            {
                ColumnName = DataTable_.Columns[x].ColumnName;

                if (ColumnStr.Length > 0)
                    ColumnStr.Append(", ");

                ColumnStr.Append(SpecialColumnName(ColumnName, initDBConfig));

                if (QMarkStr.Length > 0)
                    QMarkStr.Append(", ");

                switch (initADOConfig)
                {
                    case ADOProviderType.Odbc:
                    case ADOProviderType.MysqlOdbc35:
                    case ADOProviderType.MysqlOdbc52:
                        QMarkStr.Append("?");
                        break;

                    case ADOProviderType.Oledb:
                        QMarkStr.Append("?");
                        break;

                    case ADOProviderType.SQLClient:
                    case ADOProviderType.MySqlClient:
                        QMarkStr.Append("@" + ColumnName);
                        break;

                    case ADOProviderType.OracleClient:
                        QMarkStr.Append(":" + ColumnName);
                        break;
                }

                if ((x + 1) % 5 == 0)
                {
                    ColumnStr.Append(Environment.NewLine);
                    QMarkStr.Append(Environment.NewLine);
                }
            }

            wkstr.Append("Insert Into " + TableName_ + Environment.NewLine);
            wkstr.Append("(" + ColumnStr.ToString() + ") Values (" + Environment.NewLine + QMarkStr.ToString() + ")");

            return wkstr.ToString();
        }

        public void InsertParameters()
        {
            if (string.IsNullOrEmpty(TableName_))
            {
                throw new ApplicationException("沒有資料表名稱");
            }

            if (DataRow_ == null)
            {
                throw new ApplicationException("沒有資料內容");
            }

            if (Command_.Connection == null)
            {
                throw new ApplicationException("沒有傳入連線");
            }

            Command_.CommandType = System.Data.CommandType.Text;
            Command_.CommandText = CommandText_;

            //            switch (initADOConfig) {
            //                case ADOProviderType.Odbc:
            //                case ADOProviderType.MysqlOdbc35:
            //                case ADOProviderType.MysqlOdbc52:
            //                    OdbcCommand_.CommandType = System.Data.CommandType.Text;
            //                    OdbcCommand_.CommandText = CommandText_;
            //                    break;

            //                case ADOProviderType.Oledb:
            //                    OleDBCommand_.CommandType = System.Data.CommandType.Text;
            //                    OleDBCommand_.CommandText = CommandText_;
            //                    break;

            //                case ADOProviderType.SQLClient:
            //                    SqlCommand_.CommandType = System.Data.CommandType.Text;
            //                    SqlCommand_.CommandText = CommandText_;
            //                    break;

            //#if OracleODAC
            //                case ADOProviderType.OracleClient:
            //                    OracleCommand_.CommandType = System.Data.CommandType.Text;
            //                    OracleCommand_.CommandText = CommandText_;
            //                    break;
            //#elif OracleClient 
            //                case ADOProviderType.OracleClient:
            //                    OracleCommand_.CommandType = System.Data.CommandType.Text;
            //                    OracleCommand_.CommandText = CommandText_;
            //                    break;
            //#endif

            //#if MySqlClient
            //                case ADOProviderType.MySqlClient:
            //                    MySqlCommand_.CommandType = System.Data.CommandType.Text;
            //                    MySqlCommand_.CommandText = CommandText_;
            //                    break;
            //#endif
            //            }

            object ValuesStr = null;
            DataRow R = DataRow_;

            foreach (System.Data.DataColumn C in R.Table.Columns)
            {

                if (!Convert.IsDBNull(R[C]))
                {
                    switch (initADOConfig)
                    {
                        case ADOProviderType.Odbc:
                        case ADOProviderType.MysqlOdbc35:
                        case ADOProviderType.MysqlOdbc52:
                            OdbcParameter Odbcp = new OdbcParameter();
                            Odbcp.ParameterName = C.ColumnName;
                            Odbcp.SourceColumn = C.ColumnName;
                            Command_.Parameters.Add(Odbcp);
                            break;

                        case ADOProviderType.Oledb:
                            OleDbParameter OleDBp = new OleDbParameter();
                            OleDBp.ParameterName = C.ColumnName;
                            OleDBp.SourceColumn = C.ColumnName;
                            Command_.Parameters.Add(OleDBp);
                            break;

                        case ADOProviderType.SQLClient:
                            //可以整個 Structured 傳入 sql Server
                            SqlParameter SQLClientp = new SqlParameter();
                            SQLClientp.ParameterName = "@" + C.ColumnName;
                            SQLClientp.SourceColumn = C.ColumnName;
                            Command_.Parameters.Add(SQLClientp);
                            break;

#if OracleODAC
                        case ADOProviderType.OracleClient:
                            //Oracle Client 應該也可以整個 Structured 傳入 DB Server
                            Oracle.ManagedDataAccess.Client.OracleParameter OracleClientp = new Oracle.ManagedDataAccess.Client.OracleParameter();
                            OracleClientp.ParameterName = C.ColumnName;
                            OracleClientp.SourceColumn = C.ColumnName;

                            switch (C.DataType.ToString())
                            {
                                case "System.DateTime":
                                    //1080625
                                    //資料產生 ORA-01861，以 Parameter 方式寫入 Date 型態的
                                    //必須指定 Oracle Date的資料型態，才不會有異常發生
                                    OracleClientp.OracleDbType = OracleDbType.Date;
                                    break;
                            }

                            if ((CLOBColumn_ != null))
                            {
                                //1060609 居易 Oracle CLOB 仍然有錯誤
                                //雖然CLOB可以超過 4000個字，但是太長時仍然必須指定長度
                                //否則將出現 ORA-01461: 只有在將值插入資料類型為 LONG 的資料欄時, 才可以連結一個 LONG 值
                                //其他資料庫再找時間處理
                                if (CLOBColumn_.Contains(C.ColumnName))
                                {
                                    OracleClientp.Size = 32765;
                                }
                            }

                            Command_.Parameters.Add(OracleClientp);
                            break;
#elif OracleClient
                        case ADOProviderType.OracleClient:
						    //Oracle Client 應該也可以整個 Structured 傳入 DB Server
                            System.Data.OracleClient.OracleParameter OracleClientp = new System.Data.OracleClient.OracleParameter();
						    OracleClientp.ParameterName = C.ColumnName;
						    OracleClientp.SourceColumn = C.ColumnName;

                            switch (C.DataType.ToString())
                            {
                                case "System.DateTime":
                                    //1080625
                                    //資料產生 ORA-01861，以 Parameter 方式寫入 Date 型態的
                                    //必須指定 Oracle Date的資料型態，才不會有異常發生
                                    OracleClientp.OracleType = OracleType.DateTime;
                                    break;
                            }

                            if ((CLOBColumn_ != null)) {
							    //1060609 居易 Oracle CLOB 仍然有錯誤
							    //雖然CLOB可以超過 4000個字，但是太長時仍然必須指定長度
							    //否則將出現 ORA-01461: 只有在將值插入資料類型為 LONG 的資料欄時, 才可以連結一個 LONG 值
							    //其他資料庫再找時間處理
							    if (CLOBColumn_.Contains(C.ColumnName)) {
								    OracleClientp.Size = 32765;
							    }
						    }

						    Command_.Parameters.Add(OracleClientp);
                            break;
#endif

#if MySqlClient
                        case ADOProviderType.MySqlClient:
                            MySqlParameter MySqlClient = new MySqlParameter();
                            MySqlClient.ParameterName = C.ColumnName;
                            MySqlClient.SourceColumn = C.ColumnName;
                            Command_.Parameters.Add(MySqlClient);
                            break;
#endif
                    }

                    switch (C.DataType.ToString())
                    {
                        case "System.String":
                            ValuesStr = R[C];
                            break;

                        case "System.Decimal":
                        case "System.Short":
                        case "System.Integer":
                        case "System.Long":
                        case "System.Single":
                        case "System.Double":
                        case "System.Int16":
                        case "System.Int32":
                        case "System.Int64":
                            //數字型態更新為 Null 時，SQL 語法會發生失敗，必須產生 0 值
                            //利用 Val() 排除 0 的紀錄
                            ValuesStr = R[C];
                            break;

                        case "System.DateTime":
                        case "MySql.Data.Types.MySqlDateTime":
                            switch (initDBConfig)
                            {
                                case DBProviderType.Access:
                                case DBProviderType.Access_accdb:
                                    ValuesStr = " #'" + Convert.ToDateTime(R[C]).ToString("yyyy/MM/dd HH:mm:ss") + "'# ";
                                    break;

                                default:
                                    ValuesStr = Convert.ToDateTime(R[C]).ToString("yyyy/MM/dd HH:mm:ss");
                                    break;
                            }
                            break;

                        case "System.TimeSpan":
                            ValuesStr = R[C].ToString();
                            break;

                        case "System.Byte[]":
                            ValuesStr = R[C];
                            break;

                    }

                    //C# 無法使用 IDbCommand.Parameters[ColumnName].Value = Value;
                    //   必須回到相對應的 ADOConfig
                    //Command_.Parameters[C.ColumnName] = ValuesStr;

                    switch (initADOConfig)
                    {
                        case ADOProviderType.Odbc:
                        case ADOProviderType.MysqlOdbc35:
                        case ADOProviderType.MysqlOdbc52:
                            OdbcCommand_.Parameters[C.ColumnName].Value = ValuesStr;
                            break;

                        case ADOProviderType.Oledb:
                            OleDBCommand_.Parameters[C.ColumnName].Value = ValuesStr;
                            break;

                        case ADOProviderType.SQLClient:
                            SqlCommand_.Parameters["@" + C.ColumnName].Value = ValuesStr;
                            break;

#if OracleODAC
                        case ADOProviderType.OracleClient:
                            OracleCommand_.Parameters[C.ColumnName].Value = ValuesStr;
                            break;
#elif OracleClient
                        case ADOProviderType.OracleClient:
                            OracleCommand_.Parameters[C.ColumnName].Value = ValuesStr;
                            break;
#endif

#if MySqlClient
                        case ADOProviderType.MySqlClient:
                            MySqlCommand_.Parameters[C.ColumnName].Value = ValuesStr;
                            break;
#endif
                    }
                }
            }
        }


        public void UpdateParameters()
        {
            if (string.IsNullOrEmpty(TableName_))
                throw new ApplicationException("沒有資料表名稱");

            if (DataRow_ == null)
                throw new ApplicationException("沒有資料內容");

            if (Command_.Connection == null)
                throw new ApplicationException("沒有傳入連線");

            Command_.CommandType = System.Data.CommandType.Text;
            Command_.CommandText = CommandText_;

            //            switch (initADOConfig) {
            //                case ADOProviderType.Odbc:
            //                case ADOProviderType.MysqlOdbc35:
            //                case ADOProviderType.MysqlOdbc52:
            //                    OdbcCommand_.CommandType = System.Data.CommandType.Text;
            //                    OdbcCommand_.CommandText = CommandText_;
            //                    break;

            //                case ADOProviderType.Oledb:
            //                    OleDBCommand_.CommandType = System.Data.CommandType.Text;
            //                    OleDBCommand_.CommandText = CommandText_;
            //                    break;

            //                case ADOProviderType.SQLClient:
            //                    SqlCommand_.CommandType = System.Data.CommandType.Text;
            //                    SqlCommand_.CommandText = CommandText_;
            //                    break;

            //#if OracleODAC
            //                case ADOProviderType.OracleClient:
            //                    OracleCommand_.CommandType = System.Data.CommandType.Text;
            //                    OracleCommand_.CommandText = CommandText_;
            //                    break;
            //#elif OracleClient
            //                case ADOProviderType.OracleClient:
            //                    OracleCommand_.CommandType = System.Data.CommandType.Text;
            //                    OracleCommand_.CommandText = CommandText_;
            //                    break;
            //#endif

            //#if MySqlClient
            //                case ADOProviderType.MySqlClient:
            //                    MySqlCommand_.CommandType = System.Data.CommandType.Text;
            //                    MySqlCommand_.CommandText = CommandText_;
            //                    break;
            //#endif
            //            }

            object ValuesStr = null;
            DataRow R = DataRow_;

            foreach (System.Data.DataColumn C in R.Table.Columns)
            {

                //if (R[C] + "" != R[C, DataRowVersion.Original] + "") {
                if (!R[C].Equals(R[C, DataRowVersion.Original]))
                {
                    switch (initADOConfig)
                    {
                        case ADOProviderType.Odbc:
                        case ADOProviderType.MysqlOdbc35:
                        case ADOProviderType.MysqlOdbc52:
                            OdbcParameter Odbcp = new OdbcParameter();
                            Odbcp.ParameterName = C.ColumnName;
                            Odbcp.SourceColumn = C.ColumnName;
                            Command_.Parameters.Add(Odbcp);
                            break;

                        case ADOProviderType.Oledb:
                            OleDbParameter OleDBp = new OleDbParameter();
                            OleDBp.ParameterName = C.ColumnName;
                            OleDBp.SourceColumn = C.ColumnName;
                            Command_.Parameters.Add(OleDBp);
                            break;

                        case ADOProviderType.SQLClient:
                            //可以整個 Structured 傳入 sql Server
                            SqlParameter SQLClientp = new SqlParameter();
                            SQLClientp.ParameterName = "@" + C.ColumnName;
                            SQLClientp.SourceColumn = C.ColumnName;
                            Command_.Parameters.Add(SQLClientp);
                            break;

#if OracleODAC
                        case ADOProviderType.OracleClient:
                            //Oracle Client 應該也可以整個 Structured 傳入 DB Server
                            Oracle.ManagedDataAccess.Client.OracleParameter OracleClientp = new Oracle.ManagedDataAccess.Client.OracleParameter();
                            OracleClientp.ParameterName = C.ColumnName;
                            OracleClientp.SourceColumn = C.ColumnName;

                            switch (C.DataType.ToString())
                            {
                                case "System.DateTime":
                                    //1080625
                                    //資料產生 ORA-01861，以 Parameter 方式寫入 Date 型態的
                                    //必須指定 Oracle Date的資料型態，才不會有異常發生
                                    OracleClientp.OracleDbType = OracleDbType.Date;
                                    break;
                            }

                            if ((CLOBColumn_ != null))
                            {
                                //1060609 居易 Oracle CLOB 仍然有錯誤
                                //雖然CLOB可以超過 4000個字，但是太長時仍然必須指定長度
                                //否則將出現 ORA-01461: 只有在將值插入資料類型為 LONG 的資料欄時, 才可以連結一個 LONG 值
                                //其他資料庫再找時間處理
                                if (CLOBColumn_.Contains(C.ColumnName))
                                {
                                    OracleClientp.Size = 32765;
                                }
                            }

                            Command_.Parameters.Add(OracleClientp);
                            break;
#elif OracleClient
                        case ADOProviderType.OracleClient:
						    //Oracle Client 應該也可以整個 Structured 傳入 DB Server
                            System.Data.OracleClient.OracleParameter OracleClientp = new System.Data.OracleClient.OracleParameter();
						    OracleClientp.ParameterName = C.ColumnName;
						    OracleClientp.SourceColumn = C.ColumnName;

                            switch (C.DataType.ToString())
                            {
                                case "System.DateTime":
                                    //1080625
                                    //資料產生 ORA-01861，以 Parameter 方式寫入 Date 型態的
                                    //必須指定 Oracle Date的資料型態，才不會有異常發生
                                    OracleClientp.OracleType = OracleType.DateTime;
                                    break;
                            }

                            if ((CLOBColumn_ != null)) {
							    //1060609 居易 Oracle CLOB 仍然有錯誤
							    //雖然CLOB可以超過 4000個字，但是太長時仍然必須指定長度
							    //否則將出現 ORA-01461: 只有在將值插入資料類型為 LONG 的資料欄時, 才可以連結一個 LONG 值
							    //其他資料庫再找時間處理
							    if (CLOBColumn_.Contains(C.ColumnName)) {
								    OracleClientp.Size = 32765;
							    }
						    }

						    Command_.Parameters.Add(OracleClientp);
                            break;
#endif

#if MySqlClient
                        case ADOProviderType.MySqlClient:
                            MySqlParameter MySqlClient = new MySqlParameter();
                            MySqlClient.ParameterName = C.ColumnName;
                            MySqlClient.SourceColumn = C.ColumnName;
                            Command_.Parameters.Add(MySqlClient);
                            break;
#endif
                    }

                    switch (C.DataType.ToString())
                    {
                        case "System.String":
                            ValuesStr = R[C];
                            break;

                        case "System.Decimal":
                        case "System.Short":
                        case "System.Integer":
                        case "System.Long":
                        case "System.Single":
                        case "System.Double":
                        case "System.Int16":
                        case "System.Int32":
                        case "System.Int64":
                            //數字型態更新為 Null 時，SQL 語法會發生失敗，必須產生 0 值
                            //利用 Val() 排除 0 的紀錄
                            //if (Conversion.Val(R[C].ToString) != Conversion.Val(R[C, DataRowVersion.Original].ToString())) {
                            //    ValuesStr = R[C].ToString();
                            //    if (string.IsNullOrEmpty(ValuesStr))
                            //        ValuesStr = "0";
                            //}
                            double sOut;
                            double oOut;
                            if ((Double.TryParse(R[C].ToString(), out sOut)) && (Double.TryParse(R[C, DataRowVersion.Original].ToString(), out oOut)))
                            {
                                ValuesStr = R[C].ToString();
                                if (string.IsNullOrEmpty(ValuesStr.ToString()))
                                    ValuesStr = "0";
                            }
                            break;

                        case "System.DateTime":
                        case "MySql.Data.Types.MySqlDateTime":
                            switch (initDBConfig)
                            {
                                case DBProviderType.Access:
                                case DBProviderType.Access_accdb:
                                    ValuesStr = " #'" + Convert.ToDateTime(R[C]).ToString("yyyy/MM/dd HH:mm:ss") + "'# ";
                                    break;

                                default:
                                    ValuesStr = Convert.ToDateTime(R[C]).ToString("yyyy/MM/dd HH:mm:ss");
                                    break;
                            }
                            break;

                        case "System.TimeSpan":
                            ValuesStr = R[C];
                            break;

                        case "System.Byte[]":
                            ValuesStr = R[C];
                            break;
                    }

                    //C# 無法使用 IDbCommand.Parameters[ColumnName].Value = Value;
                    //   必須回到相對應的 ADOConfig
                    //Command_.Parameters[C.ColumnName] = ValuesStr;


                    switch (initADOConfig)
                    {
                        case ADOProviderType.Odbc:
                        case ADOProviderType.MysqlOdbc35:
                        case ADOProviderType.MysqlOdbc52:
                            OdbcCommand_.Parameters[C.ColumnName].Value = ValuesStr;
                            break;

                        case ADOProviderType.Oledb:
                            OleDBCommand_.Parameters[C.ColumnName].Value = ValuesStr;
                            break;

                        case ADOProviderType.SQLClient:
                            SqlCommand_.Parameters["@" + C.ColumnName].Value = ValuesStr;
                            break;

#if OracleODAC
                        case ADOProviderType.OracleClient:
                            OracleCommand_.Parameters[C.ColumnName].Value = ValuesStr;
                            break;
#elif OracleClient
                        case ADOProviderType.OracleClient:
                            OracleCommand_.Parameters[C.ColumnName].Value = ValuesStr;
                            break;
#endif

#if MySqlClient
                        case ADOProviderType.MySqlClient:
                            MySqlCommand_.Parameters[C.ColumnName].Value = ValuesStr;
                            break;
#endif
                    }
                }
            }
        }

#if OracleODAC
        private Oracle.ManagedDataAccess.Client.OracleDbType GetOracleType(int value)
        {
            //If value Is GetType(Guid) Then
            //    Return OracleType.VarChar
            //End If

            //If value Is GetType(TimeSpan) Then
            //    Return OracleType.IntervalDayToSecond
            //End If

            switch ((TypeCode)value)
            {
                case TypeCode.Boolean:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Boolean;

                case TypeCode.Byte:
                    if (value.GetType().IsArray)
                    {
                        return Oracle.ManagedDataAccess.Client.OracleDbType.Blob;
                    }
                    else
                    {
                        return Oracle.ManagedDataAccess.Client.OracleDbType.Clob;
                    }

                case TypeCode.Char:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Char;

                case TypeCode.DateTime:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Date;

                case TypeCode.DBNull:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Char;

                case TypeCode.Empty:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Raw;

                case TypeCode.Decimal:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Decimal;

                case TypeCode.Double:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Double;

                case TypeCode.Int16:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Int16;

                case TypeCode.Int32:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Int32;

                case TypeCode.Int64:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Int32;

                case TypeCode.SByte:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Byte;

                case TypeCode.String:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Varchar2;

                case TypeCode.Single:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Single;

                case TypeCode.UInt64:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Int64;

                case TypeCode.UInt32:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Int32;

                case TypeCode.UInt16:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Int32;

                case TypeCode.Object:
                    return Oracle.ManagedDataAccess.Client.OracleDbType.Char;
            }

            return Oracle.ManagedDataAccess.Client.OracleDbType.Varchar2;
        }

#elif OracleClient
	    private System.Data.OracleClient.OracleType GetOracleType(int value)
	    {
		    //https://msdn.microsoft.com/zh-tw/library/system.data.oracleclient.oracletype(v=vs.110).aspx
		    //If value Is GetType(Guid) Then
		    //    Return OracleType.VarChar
		    //End If

		    //If value Is GetType(TimeSpan) Then
		    //    Return OracleType.IntervalDayToSecond
		    //End If

            switch ((TypeCode) value ) {
			    //Type.GetTypeCode(value)
			    case TypeCode.Boolean:
				    //找不到 Boolean
				    return System.Data.OracleClient.OracleType.SByte;

			    case TypeCode.Byte:
				    if (value.GetType().IsArray) {
                        return System.Data.OracleClient.OracleType.Blob;
				    } else {
                        return System.Data.OracleClient.OracleType.Clob;
					    //不確定
				    }

			    case TypeCode.Char:
                    return System.Data.OracleClient.OracleType.Char;

			    case TypeCode.DateTime:
                    return System.Data.OracleClient.OracleType.DateTime;

			    case TypeCode.DBNull:
                    return System.Data.OracleClient.OracleType.Char;

			    case TypeCode.Empty:
                    return System.Data.OracleClient.OracleType.Raw;

			    case TypeCode.Decimal:
                    return System.Data.OracleClient.OracleType.Number;

			    case TypeCode.Double:
                    return System.Data.OracleClient.OracleType.Double;

			    case TypeCode.Int16:
                    return System.Data.OracleClient.OracleType.Int16;

			    case TypeCode.Int32:
                    return System.Data.OracleClient.OracleType.Int32;

			    case TypeCode.Int64:
                    return System.Data.OracleClient.OracleType.Int32;

			    case TypeCode.SByte:
                    return System.Data.OracleClient.OracleType.SByte;

			    case TypeCode.String:
                    return System.Data.OracleClient.OracleType.VarChar;

			    case TypeCode.Single:
                    return System.Data.OracleClient.OracleType.Float;

			    case TypeCode.UInt64:
                    return System.Data.OracleClient.OracleType.UInt16;

			    case TypeCode.UInt32:
                    return System.Data.OracleClient.OracleType.UInt32;

			    case TypeCode.UInt16:
                    return System.Data.OracleClient.OracleType.UInt32;

			    case TypeCode.Object:
                    return System.Data.OracleClient.OracleType.Char;
		    }
            return System.Data.OracleClient.OracleType.Char;
	    }
#endif

#if MySqlClient
        private MySqlDbType GetMySqlType(int value)
        {
            //https://msdn.microsoft.com/zh-tw/library/system.data.oracleclient.oracletype(v=vs.110).aspx
            //If value Is GetType(Guid) Then
            //    Return OracleType.VarChar
            //End If

            //If value Is GetType(TimeSpan) Then
            //    Return OracleType.IntervalDayToSecond
            //End If

            switch ((TypeCode)value)
            {
                //Type.GetTypeCode(value)
                case TypeCode.Boolean:
                    //找不到 Boolean
                    return MySqlDbType.Byte;

                case TypeCode.Byte:
                    return MySqlDbType.Blob;
                //Return MySqlDbType.TinyBlob
                //Return MySqlDbType.MediumBlob
                //Return MySqlDbType.LongBlob

                case TypeCode.Char:
                    return MySqlDbType.VarChar;

                case TypeCode.DateTime:
                    return MySqlDbType.DateTime;

                case TypeCode.DBNull:
                    return MySqlDbType.VarChar;

                case TypeCode.Empty:
                    return MySqlDbType.VarChar;

                case TypeCode.Decimal:
                    return MySqlDbType.Decimal;

                case TypeCode.Double:
                    return MySqlDbType.Double;

                case TypeCode.Int16:
                    return MySqlDbType.Int16;

                case TypeCode.Int32:
                    return MySqlDbType.Int32;

                case TypeCode.Int64:
                    return MySqlDbType.Int64;

                case TypeCode.SByte:
                    return MySqlDbType.Byte;

                case TypeCode.String:
                    return MySqlDbType.VarString;

                case TypeCode.Single:
                    return MySqlDbType.Float;

                case TypeCode.UInt64:
                    return MySqlDbType.UInt64;

                case TypeCode.UInt32:
                    return MySqlDbType.UInt32;

                case TypeCode.UInt16:
                    return MySqlDbType.UInt16;

                case TypeCode.Object:
                    return MySqlDbType.VarChar;
            }

            return MySqlDbType.VarChar;

        }

#endif

        private SqlDbType GetSqlDbType(int value)
        {
            //https://msdn.microsoft.com/zh-tw/library/system.data.sqldbtype(v=vs.110).aspx
            //If value Is GetType(Guid) Then
            //    Return SqlDbType.VarChar
            //End If

            //If value Is GetType(TimeSpan) Then
            //    Return SqlDbType.TinyInt
            //End If

            switch ((TypeCode)value)
            {
                //Type.GetTypeCode(value)
                case TypeCode.Boolean:
                    return SqlDbType.Bit;

                case TypeCode.Byte:
                    if (value.GetType().IsArray)
                    {
                        return SqlDbType.Binary;
                    }
                    else
                    {
                        return SqlDbType.Image;
                    }

                case TypeCode.Char:
                    return SqlDbType.Char;

                case TypeCode.DateTime:
                    return SqlDbType.DateTime;

                case TypeCode.DBNull:
                    return SqlDbType.Variant;

                case TypeCode.Empty:
                    return SqlDbType.Variant;

                case TypeCode.Decimal:
                    return SqlDbType.Decimal;

                case TypeCode.Double:
                    return SqlDbType.Float;

                case TypeCode.Int16:
                    return SqlDbType.SmallInt;

                case TypeCode.Int32:
                    return SqlDbType.Int;

                case TypeCode.Int64:
                    return SqlDbType.BigInt;

                case TypeCode.SByte:
                    return SqlDbType.TinyInt;

                case TypeCode.String:
                    return SqlDbType.VarChar;

                case TypeCode.Single:
                    return SqlDbType.Real;

                case TypeCode.UInt64:
                    return SqlDbType.BigInt;

                case TypeCode.UInt32:
                    return SqlDbType.Int;

                case TypeCode.UInt16:
                    return SqlDbType.SmallInt;

                case TypeCode.Object:
                    return SqlDbType.Variant;

            }

            return SqlDbType.Variant;

        }

        private OleDbType GetOleDbType(int value)
        {
            //https://msdn.microsoft.com/zh-tw/library/system.data.oledb.oledbtype(v=vs.110).aspx

            //If value Is GetType(Guid) Then
            //    Return OleDbType.Guid
            //End If

            //If value Is GetType(TimeSpan) Then
            //    Return OleDbType.DBTime
            //End If

            switch ((TypeCode)value)
            {
                //Type.GetTypeCode(value)
                case TypeCode.Boolean:
                    return OleDbType.Boolean;

                case TypeCode.Byte:
                    if (value.GetType().IsArray)
                    {
                        return OleDbType.Binary;
                    }
                    else
                    {
                        return OleDbType.UnsignedTinyInt;
                    }

                case TypeCode.Char:
                    return OleDbType.Char;

                case TypeCode.DateTime:
                    return OleDbType.DBDate;

                case TypeCode.DBNull:
                    return OleDbType.Empty;

                case TypeCode.Empty:
                    return OleDbType.Empty;

                case TypeCode.Decimal:
                    return OleDbType.Decimal;

                case TypeCode.Double:
                    return OleDbType.Double;

                case TypeCode.Int16:
                    return OleDbType.SmallInt;

                case TypeCode.Int32:
                    return OleDbType.Integer;

                case TypeCode.Int64:
                    return OleDbType.BigInt;

                case TypeCode.SByte:
                    return OleDbType.TinyInt;

                case TypeCode.String:
                    return OleDbType.VarChar;

                case TypeCode.Single:
                    return OleDbType.Single;

                case TypeCode.UInt64:
                    return OleDbType.UnsignedBigInt;

                case TypeCode.UInt32:
                    return OleDbType.UnsignedInt;

                case TypeCode.UInt16:
                    return OleDbType.UnsignedSmallInt;

                case TypeCode.Object:
                    return OleDbType.IUnknown;
            }

            return OleDbType.IUnknown;

        }

        private OdbcType GetOdbcType(int value)
        {

            //If value Is GetType(Guid) Then
            //    Return OdbcType.UniqueIdentifier
            //End If

            //If value Is GetType(TimeSpan) Then
            //    Return OdbcType.Timestamp
            //End If

            switch ((TypeCode)value)
            {
                //Type.GetTypeCode(value)
                case TypeCode.Boolean:
                    return OdbcType.Bit;

                case TypeCode.Byte:
                    if (value.GetType().IsArray)
                    {
                        return OdbcType.VarBinary;
                    }
                    else
                    {
                        return OdbcType.Binary;
                    }

                case TypeCode.Char:
                    return OdbcType.Char;

                case TypeCode.DateTime:
                    return OdbcType.Date;

                case TypeCode.DBNull:
                    return OdbcType.NVarChar;

                case TypeCode.Empty:
                    return OdbcType.NVarChar;

                case TypeCode.Decimal:
                    return OdbcType.Decimal;
                case TypeCode.Double:
                    return OdbcType.Double;

                case TypeCode.Int16:
                    return OdbcType.SmallInt;

                case TypeCode.Int32:
                    return OdbcType.Int;

                case TypeCode.Int64:
                    return OdbcType.BigInt;

                case TypeCode.SByte:
                    return OdbcType.TinyInt;

                case TypeCode.String:
                    return OdbcType.VarChar;

                case TypeCode.Single:
                    return OdbcType.Real;

                case TypeCode.UInt64:
                    return OdbcType.BigInt;

                case TypeCode.UInt32:
                    return OdbcType.Int;

                case TypeCode.UInt16:
                    return OdbcType.SmallInt;

                case TypeCode.Object:
                    return OdbcType.VarChar;
            }

            return OdbcType.VarChar;

        }


        #region " IDisposable Support "
        // 由 Visual Basic 新增此程式碼以正確實作可處置的模式。

        private bool disposed = false;
        public void Dispose()
        {
            if (this.disposed)
                return;

            disposed = true;
            GC.SuppressFinalize(this);
        }
        #endregion
    }

    public class KTDBProvider : IDisposable
    {

        #region " 資料庫連線字串"
        //Driver  Ole DB 
        private const string ConnectionOptional = "?DATASOURCE?;" + "?DATABASE?;" + "?USERID?;" + "?PASSWORD?;" + "?PORT?;";

        #region " Oracle ConnectionString  - OK"

        //private string OracleOdbcConnectionString_ = "Driver={Microsoft ODBC for Oracle};" + ConnectionOptional;
        ////MSDAORA 提供者並未登錄於本機電腦上 
        ////懷疑因程式預設以x64模式執行，試圖讀取x64版本OLE DB Driver時失敗(沒安裝或該Driver根本沒出x64版)
        ////網上建議的解決方法是重 Build Project，將 Target 由 Any CPU 改為 x86。
        ////[專案]-->[屬性]-->[編譯]-->[進階編譯選項]-->[目標CPU] 將 Any CPU 改為 x86

        //private string OracleOleDBConnectionString_ = "Provider=MSDAORA;" + ConnectionOptional;
        ////OracleClient 元件不需要指定 Provider 因為元件自己已經表明他是 Oracle 的連線元件

        //0950519
        //9i 要使用 不同的 Provider，在 VB6居然可以使用 MSDAORA.1
        //目前若要 8i 與 9i 共存.. 9i 只能使用 OleDB Connection
        private string OracleOleDBConnectionString_ = "Provider=OraOLEDB.Oracle.1;" + ConnectionOptional;

#if OracleODAC || OracleClient
        private string OracleConnectionString_ = "" + ConnectionOptional;
#endif

        //0950520  
        //9i Odbc 這裡是使用Local的 DriverName 每一台電腦有可能都不一樣 
        //此處需要統一才能使用 ODBC Driver
        private string Oracle9iODBConnectionString_ = "Driver={Oracle ??DEFAULT_HOME9i};" + ConnectionOptional;
        #endregion

        #region " SQLServer ConnectionString - OK "

        private string SQLOdbcConnectionString_ = "Driver={SQL Server};" + ConnectionOptional;

        private string SQLOledbConnectionString_ = "Provider=SQLOLEDB;" + ConnectionOptional;

        private string SQLClientConnectionString_ = "Data Source=?DATASOURCE?;" + ConnectionOptional;
        #endregion

        #region " Access ConnectionString - OK"
        //OleDB 
        private string ACSOdbcConnectionString_ = "Driver={Microsoft Access Driver (*.mdb)};" + ConnectionOptional;

        //0940728 -- G.E
        //Jet.Oledb.3.51 會忽略部分關鍵字 Ex:year, Month, day ...等等
        //private string ACSOledb3ConnectionString_ = "Provider=Microsoft.Jet.Oledb.3.51;" + ConnectionOptional;

        //Jet.Oledb.4.0 以上會檢查欄位的關鍵字 Ex:year, Month, day ...等等 
        //當 Sql 語法有上述或更多關鍵字時，將導致語法錯誤，必須在關鍵字加上 中括號 [year], 排除錯誤
        private string ACSOledbConnectionString_ = "Provider=Microsoft.Jet.Oledb.4.0;" + ConnectionOptional;

        private string ACSOledb12ConnectionString_ = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Persist Security Info=False;" + ConnectionOptional;
        #endregion

        #region " Excel ConnectionString - OK"
        private string ExcelOdbcConnectionString_ = "Provider=MSDASQL.1;" + "Driver={Microsoft Excel Driver (*.xls)};" + "Persist Security Info=False;" + ConnectionOptional;

        private string ExcelOledb4ConnectionString_ = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Extended Properties=Excel 8.0;" + "Persist Security Info=False;" + ConnectionOptional;

        private string ExcelOledb12ConnectionString_ = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Extended Properties=Excel 12.0 Xml;" + "Persist Security Info=False;" + ConnectionOptional;
        #endregion

        #region " Mysql ConnectionString - OK"

        //因為 4.1 版的資料庫..由於編碼的問題..有可能會有亂碼
        //所以連線之後需要設定該 資料庫的存取內碼 
        //KTCC.DoSql("SET CHARACTER SET big5", , MySQL)
        private string MysqlOdbc35ConnectionString_ = "Provider=MSDASQL.1;" + "DESC=;Option=;STMT=;" + "DRIVER={MySQL ODBC 3.51 Driver};" + ConnectionOptional;

        //105/11/15 居易
        //MySQL ODBC 5.3 Unicode Driver 測試後有很大的問題，暫時不使用
        //Dosql 一次就斷線一次，下次 Dosql 時 Command.Connection.Open 會立刻被變成 Close 狀態。
        //雖然 MySQL ODBC 5.3 ANSI Driver 也可以使用，但是字碼可能不夠多，不考慮使用。

        //新版的資料庫增加許多資料型態，必須使用 ODBC 5.x 版本，測試後目前可用的穩定版本為 5.2
        private string MysqlOdbc52ConnectionString_ = "Provider=MSDASQL.1;" + "DESC=;Option=;STMT=;" + "DRIVER={MySQL ODBC 5.2 Unicode Driver};" + ConnectionOptional;

        //各個 VB.NET 版本要安裝不同的 MySql.Data.MySqlClient 版本，才能正常使用。
        //使用 MySql.Data.MySqlClient 的方式，必須引用參考，這個設計方式與 OracleClient 相同。
        //此功能只能作用在 .Net 4.0 以上

        //Convert Zero Datetime=True
        //當沒有設置此屬性時，如果 Mysql 數據庫中的 DateTime 列為 null 的時候，
        //.net 在換時會拋出異常訊息: Unable to convert MySQL date/time value to System.DateTime at MySql.Data.Types.MySqlDateTime.GetDateTime()
        //這是因為 .net 的默認最小日期和 Mysql 的不匹配，導致轉換出錯，解決方式就是在連接字串中設定 Convert Zero Datetime=True
        //;Convert Zero Datetime=True;Allow Zero Datetime=True
        private string MysqlClientConnectionString_ = ";persistsecurityinfo=True;Convert Zero Datetime=True;Allow Zero Datetime=True;" + ConnectionOptional;

        #endregion

        #region " PostgreSQL ConnectionString - Test "
        //Driver={PostgreSQL Unicode};Server=192.168.1.17;Port=5435;Database=hiep;Uid=hiep; Pwd=hieph2dba;
        private string PostgreSQLODBCConnectionString_ = "Driver={PostgreSQL Unicode};" + ConnectionOptional;
        #endregion

        #endregion

        #region " IDbConnection"
        //Public Connection As New OdbcConnection
        //private OdbcConnection _OdbcConnection = new OdbcConnection();
        private OdbcConnection _OdbcConnection = null;

        //private OleDbConnection _OledbConnection = new OleDbConnection();
        private OleDbConnection _OledbConnection = null;

        //private SqlConnection _SqlConnection = new SqlConnection();
        private SqlConnection _SqlConnection = null;

#if OracleODAC
        //private Oracle.ManagedDataAccess.Client.OracleConnection _OraConnection = new Oracle.ManagedDataAccess.Client.OracleConnection();
        private Oracle.ManagedDataAccess.Client.OracleConnection _OraConnection = null;
#elif OracleClient
        //private System.Data.OracleClient.OracleConnection _OraConnection = new System.Data.OracleClient.OracleConnection();
        private System.Data.OracleClient.OracleConnection _OraConnection = null;
#endif

#if MySqlClient
        //private MySqlConnection _MySqlConnection = new MySqlConnection();
        private MySqlConnection _MySqlConnection = null;
#endif

        #endregion

        #region " IDbDataAdapter "

        //private OdbcDataAdapter _OdbcDataAdapter = new OdbcDataAdapter(new System.Data.Odbc.OdbcCommand());
        private OdbcDataAdapter _OdbcDataAdapter = null;

        //private OleDbDataAdapter _OleDbDataAdapter = new OleDbDataAdapter(new System.Data.OleDb.OleDbCommand());
        private OleDbDataAdapter _OleDbDataAdapter = null;

        //private SqlDataAdapter _SqlDataAdapter = new SqlDataAdapter(new System.Data.SqlClient.SqlCommand());
        private SqlDataAdapter _SqlDataAdapter = null;

#if OracleODAC
        //private Oracle.ManagedDataAccess.Client.OracleDataAdapter _OraDataAdapter = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(new Oracle.ManagedDataAccess.Client.OracleCommand());
        private Oracle.ManagedDataAccess.Client.OracleDataAdapter _OraDataAdapter = null;
#elif OracleClient
        private System.Data.OracleClient.OracleDataAdapter _OraDataAdapter = null;
#endif

#if MySqlClient
        private MySqlDataAdapter _MySqlDataAdapter = null;
#endif

        #endregion

        #region " IDbCommand "
        //Public Command As New OdbcCommand
        //private OdbcCommand _OdbcCommand = new OdbcCommand();
        private OdbcCommand _OdbcCommand = null;

        //private OleDbCommand _OledbCommand = new OleDbCommand();
        private OleDbCommand _OleDbCommand = null;

        //private SqlCommand _SqlCommand = new SqlCommand();
        private SqlCommand _SqlCommand = null;

#if OracleODAC
        private Oracle.ManagedDataAccess.Client.OracleCommand _OraCommand = new Oracle.ManagedDataAccess.Client.OracleCommand();
#elif OracleClient
        private System.Data.OracleClient.OracleCommand _OraCommand = new System.Data.OracleClient.OracleCommand();
#endif

#if MySqlClient
        private MySqlCommand _MySqlCommand = new MySqlCommand();
#endif

        #endregion

        #region " IDataReader "
        //Public DataReader As OdbcDataReader
        private OdbcDataReader _OdbcDataReader;
        private OleDbDataReader _OleDbDataReader;
        private SqlDataReader _SqlDataReader;

#if OracleODAC
        private Oracle.ManagedDataAccess.Client.OracleDataReader _OraDataReader;
#elif OracleClient
        private System.Data.OracleClient.OracleDataReader _OraDataReader;
#endif

#if MySqlClient
        private MySqlDataReader _MySqlDataReader;
#endif

        #endregion

        #region " IDbTransaction "
        //Public Transaction As OdbcTransaction
        private OdbcTransaction _OdbcTransaction;
        private OleDbTransaction _OleDbTransaction;
        private SqlTransaction _SqlTransaction;

#if OracleODAC
        private Oracle.ManagedDataAccess.Client.OracleTransaction _OraTransaction;
#elif OracleClient
        private System.Data.OracleClient.OracleTransaction _OraTransaction;
#endif

#if MySqlClient
        private MySqlTransaction _MySqlTransaction;
#endif

        #endregion

        #region " 主機名稱, 資料庫, 帳號, 密碼"

        public string ConnectionString(string sDataSource, string sDataBase = "", string sUserID = "", string sPassword = "", string sPort = "")
        {

            string ConnectionString_ = "";

            switch (KTDBType.DBConfig)
            {
                case DBProviderType.Oracle:

                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.Oledb:
                            ConnectionString_ = OracleOleDBConnectionString_;
                            break;

                        case ADOProviderType.OracleClient:
#if OracleODAC
                            ConnectionString_ = OracleConnectionString_;
#elif OracleClient
                            ConnectionString_ = OracleConnectionString_ + "unicode=true;";
#endif
                            break;

                        default:
                            //這一段不會再有功能了
                            ConnectionString_ = Oracle9iODBConnectionString_;
                            break;
                    }
                    break;

                case DBProviderType.SQLServer:
                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.Odbc:
                            ConnectionString_ = SQLOdbcConnectionString_;
                            break;

                        case ADOProviderType.Oledb:
                            ConnectionString_ = SQLOledbConnectionString_;
                            break;

                        case ADOProviderType.SQLClient:
                            ConnectionString_ = SQLClientConnectionString_;
                            break;
                    }
                    break;

                case DBProviderType.Access:
                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.Odbc:
                            ConnectionString_ = ACSOdbcConnectionString_;
                            break;

                        default:
                            ConnectionString_ = ACSOledbConnectionString_;
                            break;
                    }
                    break;

                case DBProviderType.Access_accdb:
                    ConnectionString_ = ACSOledb12ConnectionString_;
                    break;

                case DBProviderType.Excel:
                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.Odbc:
                            ConnectionString_ = ExcelOdbcConnectionString_;
                            break;

                        default:
                            ConnectionString_ = ExcelOledb4ConnectionString_;
                            break;
                    }
                    break;

                case DBProviderType.Excel_xlsx:
                    ConnectionString_ = ExcelOledb12ConnectionString_;
                    break;

                case DBProviderType.MySql:
                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.MysqlOdbc35:
                            ConnectionString_ = MysqlOdbc35ConnectionString_;
                            break;

                        case ADOProviderType.MysqlOdbc52:
                            ConnectionString_ = MysqlOdbc52ConnectionString_;
                            break;

                        case ADOProviderType.MySqlClient:
                            ConnectionString_ = MysqlClientConnectionString_;
                            break;
                    }
                    break;

                case DBProviderType.Postgre:
                    ConnectionString_ = PostgreSQLODBCConnectionString_;
                    break;
            }

            ConnectionString_ = ConnectionString_.Replace("?DATASOURCE?", ReplaceDataSource(sDataSource, sPort));
            ConnectionString_ = ConnectionString_.Replace("?DATABASE?", ReplaceDataBase(sDataBase));
            ConnectionString_ = ConnectionString_.Replace("?USERID?", ReplaceUserID(sUserID, sDataSource));
            ConnectionString_ = ConnectionString_.Replace("?PASSWORD?", ReplacePassword(sPassword, sDataSource));
            ConnectionString_ = ConnectionString_.Replace("?PORT?", ReplacePort(sPort));
            return ConnectionString_;

        }

        private string ReplaceDataSource(string DataSource, string Port = "")
        {
            string ReplaceDataSource_ = "";

            //if (string.IsNullOrEmpty(DataSource)) {
            //    MsgBox("請務必輸入連線主機名稱", "DataSource Or DataServer");
            //    return functionReturnValue;
            //}
            switch (KTDBType.ADOConfig)
            {
                case ADOProviderType.Oledb:
                case ADOProviderType.OracleClient:
                case ADOProviderType.MySqlClient:
                    switch (KTDBType.DBConfig)
                    {
                        case DBProviderType.MySql:
                            ReplaceDataSource_ = "Server=" + DataSource;
                            break;

                        default:
                            ReplaceDataSource_ = "Data Source=" + DataSource;
                            break;
                    }
                    break;

                case ADOProviderType.SQLClient:
                    ReplaceDataSource_ = "Data Source=" + DataSource;

                    //1030918 
                    //SQL Server 2005、2008 的 Port 參數必須跟 Data Source 的參數放在一起，
                    //SQL Server 2000、2005、2008 都適用
                    //仍然要傳入 Port，避免 SqlServer Port 有修改
                    if (string.IsNullOrEmpty(Port))
                        Port = "1433";

                    ReplaceDataSource_ = ReplaceDataSource_ + "," + Port;
                    break;

                default:
                    //ODBC
                    switch (KTDBType.DBConfig)
                    {
                        case DBProviderType.Access:
                        case DBProviderType.Excel:
                        case DBProviderType.Oracle:
                            //0950520 --> 9i 的 Odbc 主機名稱要使用 DBQ 
                            ReplaceDataSource_ = "DBQ=" + DataSource;
                            break;

                        default:
                            //Sql Server ODBC 不確定連接字串
                            ReplaceDataSource_ = "Server=" + DataSource;
                            break;
                    }
                    break;
            }

            return ReplaceDataSource_;

        }

        private string ReplaceDataBase(string DataBase = "")
        {
            string ReplaceDataBase_ = "";

            //目前程式控制的只有 SQLServer Mysql 有多個 DataBase 
            switch (KTDBType.DBConfig)
            {
                case DBProviderType.SQLServer:
                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.Odbc:
                            ReplaceDataBase_ = "DataBase=" + DataBase;
                            break;

                        case ADOProviderType.Oledb:
                            ReplaceDataBase_ = "Initial Catalog=" + DataBase;
                            break;

                        case ADOProviderType.SQLClient:
                            ReplaceDataBase_ = "Initial Catalog=" + DataBase;
                            break;
                    }
                    break;

                case DBProviderType.MySql:
                case DBProviderType.Postgre:
                    ReplaceDataBase_ = "DataBase=" + DataBase;
                    break;
            }

            return ReplaceDataBase_;
        }

        private string ReplaceUserID(string UserID = "", string DataSource = "")
        {
            string ReplaceUserID_ = "";

            switch (KTDBType.DBConfig)
            {
                case DBProviderType.Oracle:

                    if (string.IsNullOrEmpty(UserID))
                    {
                        switch (DataSource)
                        {
                            case "OLTPSRV":
                                UserID = "HRP";
                                break;

                            case "OLAPSRV":
                                UserID = "IPDSTAGE";
                                break;

                            default:
                                UserID = "AP06BZ";
                                break;
                        }
                    }

                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.Oledb:
                        case ADOProviderType.OracleClient:
                            //OleDB 與 Oracle
                            ReplaceUserID_ = "USER ID=" + UserID;
                            break;

                        default:
                            //Odbc
                            ReplaceUserID_ = "UID=" + UserID;
                            break;
                    }
                    break;

                case DBProviderType.SQLServer:
                    if (string.IsNullOrEmpty(UserID))
                        UserID = "sa";

                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.Odbc:
                            ReplaceUserID_ = "UID=" + UserID;
                            break;

                        case ADOProviderType.Oledb:
                            ReplaceUserID_ = "User ID=" + UserID;
                            break;

                        case ADOProviderType.SQLClient:
                            ReplaceUserID_ = "User ID=" + UserID;
                            break;
                    }
                    break;

                case DBProviderType.Access:
                    //0951007 , Access 只會設定密碼, 不會被設定 UserID 
                    //If UserID <> "" Then ReplaceUserID_ = "User ID=" & UserID
                    break;

                case DBProviderType.MySql:
                    if (string.IsNullOrEmpty(UserID))
                        UserID = "root";

                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.MysqlOdbc35:
                        case ADOProviderType.MysqlOdbc52:
                            ReplaceUserID_ = "UID=" + UserID;
                            break;

                        case ADOProviderType.MySqlClient:
                            ReplaceUserID_ = "UserID=" + UserID;
                            break;
                    }

                    break;

                case DBProviderType.Postgre:
                    if (string.IsNullOrEmpty(UserID))
                        UserID = "postgre";

                    ReplaceUserID_ = "UID=" + UserID;
                    break;
            }

            return ReplaceUserID_;
        }

        private string ReplacePassword(string Password = "", string DataSource = "")
        {
            string ReplacePassword_ = "";

            switch (KTDBType.DBConfig)
            {
                case DBProviderType.Oracle:

                    if (string.IsNullOrEmpty(Password))
                    {
                        switch (DataSource)
                        {
                            case "OLTPSRV":
                                Password = "HRP";
                                break;

                            case "OLAPSRV":
                                Password = "IPDSTAGE";
                                break;

                            default:
                                Password = "AP06BZ";
                                break;
                        }
                    }

                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.Oledb:
                        case ADOProviderType.OracleClient:
                            //OleDB 與 Oracle
                            ReplacePassword_ = "PASSWORD=" + Password;
                            break;

                        default:
                            //ODBC
                            ReplacePassword_ = "PWD=" + Password;
                            break;
                    }

                    break;

                case DBProviderType.SQLServer:
                    if (string.IsNullOrEmpty(Password))
                        Password = "";

                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.Odbc:
                            ReplacePassword_ = "PWD=" + Password;
                            break;

                        case ADOProviderType.Oledb:
                            ReplacePassword_ = "PASSWORD=" + Password;
                            break;

                        case ADOProviderType.SQLClient:
                            ReplacePassword_ = "PASSWORD=" + Password;
                            break;
                    }
                    break;

                case DBProviderType.Access:
                case DBProviderType.Access_accdb:
                    //0951007 , Access 只會設定密碼, 不會被設定 UserID 
                    if (!string.IsNullOrEmpty(Password))
                        ReplacePassword_ = "Jet OLEDB:Database PASSWORD=" + Password;
                    break;

                case DBProviderType.MySql:
                    ReplacePassword_ = "PASSWORD=" + Password;
                    break;

                case DBProviderType.Postgre:
                    ReplacePassword_ = "Pwd=" + Password;
                    break;

            }

            return ReplacePassword_;
        }

        private string ReplacePort(string Port = "")
        {
            string ReplacePort_ = "";

            switch (KTDBType.DBConfig)
            {
                case DBProviderType.SQLServer:
                    break;

                //SqlServer 2000 應該也要加 Port 才對，但預設為 1433 可以不用輸入
                //1030918
                //SqlServer 2005、2008 的 Port 參數卻不能放在這個地方
                //必須跟 Data Source 的參數放在一起，SQL Server 2000、2005、2008 都適用

                case DBProviderType.MySql:
                    if (string.IsNullOrEmpty(Port))
                        Port = "3306";

                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.MysqlOdbc35:
                        case ADOProviderType.MysqlOdbc52:
                            ReplacePort_ = "PORT=" + Port;
                            break;
                    }
                    break;

                case DBProviderType.Postgre:
                    if (string.IsNullOrEmpty(Port))
                        Port = "5432";

                    switch (KTDBType.ADOConfig)
                    {
                        case ADOProviderType.Odbc:
                            ReplacePort_ = "PORT=" + Port;
                            break;
                    }
                    break;

            }
            return ReplacePort_;
        }

        #endregion

        #region " 連線資料庫的其他參數紀錄"

        //記錄最後近一次 Dosql 的 SqlStr
        public string SqlStr { get; set; }

        public bool DoSqlOK { get; set; }

        public string ErrorCode { get; set; }

        public string ErrorMessage { get; set; }

        //本次交易連線的 Key
        public string CnKey { get; set; }

        //本次連線的主機名稱
        public string HostName { get; set; }

        public string DataSource { get; set; }
        public string DataBase { get; set; }

        //交易註記
        public bool SetTransaction { get; set; }

        //執行檔改變編譯為 dll檔案，連線時以呼叫的主程式連線(共用同一個 Session)，必須紀錄是否為 dll 的連線
        public bool isDllConnect { get; set; }

        //最後執行 Sql 的時間
        private DateTime _LastDoSqlTime = DateTime.Now;
        public DateTime LastDoSqlTime
        {
            get { return _LastDoSqlTime; }

            set { _LastDoSqlTime = value; }
        }
        #endregion

        #region " 連線資料庫的環境型別"

        public DBProviderType DBConfig { get; set; }

        public ADOProviderType ADOConfig { get; set; }

        private KTDBType KTDBType { get; set; }
        public KTDBProvider(DBProviderType DBProvider = DBProviderType.Oracle, ADOProviderType ADOProvider = ADOProviderType.Oledb)
        {
            KTDBType = new KTDBType(DBProvider, ADOProvider);

            DBConfig = KTDBType.DBConfig;
            ADOConfig = KTDBType.ADOConfig;
        }

        public IDbConnection Connection
        {
            get
            {
                switch (KTDBType.DBConfig)
                {
                    case DBProviderType.Oracle:

                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Oledb:
                                if (_OledbConnection == null) _OledbConnection = new OleDbConnection();
                                return _OledbConnection;

#if OracleODAC
                            case ADOProviderType.OracleClient:
                                if (_OraConnection == null) _OraConnection = new Oracle.ManagedDataAccess.Client.OracleConnection();
                                return _OraConnection;
#elif OracleClient
                            case ADOProviderType.OracleClient:
                                if (_OraConnection == null) _OraConnection = new System.Data.OracleClient.OracleConnection();
                                return _OraConnection;
#endif

                            default:
                                if (_OdbcConnection == null) _OdbcConnection = new OdbcConnection();
                                return _OdbcConnection;
                        }

                    case DBProviderType.SQLServer:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Oledb:
                                if (_OledbConnection == null) _OledbConnection = new OleDbConnection();
                                return _OledbConnection;

                            case ADOProviderType.SQLClient:
                                if (_SqlConnection == null) _SqlConnection = new SqlConnection();
                                return _SqlConnection;

                            case ADOProviderType.Odbc:
                                if (_OdbcConnection == null) _OdbcConnection = new OdbcConnection();
                                return _OdbcConnection;
                        }
                        break;

                    case DBProviderType.Access:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                if (_OdbcConnection == null) _OdbcConnection = new OdbcConnection();
                                return _OdbcConnection;

                            default:
                                if (_OledbConnection == null) _OledbConnection = new OleDbConnection();
                                return _OledbConnection;
                        }

                    case DBProviderType.Access_accdb:
                        if (_OledbConnection == null) _OledbConnection = new OleDbConnection();
                        return _OledbConnection;

                    case DBProviderType.Excel:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                if (_OdbcConnection == null) _OdbcConnection = new OdbcConnection();
                                return _OdbcConnection;

                            default:
                                if (_OledbConnection == null) _OledbConnection = new OleDbConnection();
                                return _OledbConnection;
                        }

                    case DBProviderType.Excel_xlsx:
                        if (_OledbConnection == null) _OledbConnection = new OleDbConnection();
                        return _OledbConnection;

                    case DBProviderType.MySql:
                        switch (KTDBType.ADOConfig)
                        {
#if MySqlClient
                            case ADOProviderType.MySqlClient:
                                if (_MySqlConnection == null) _MySqlConnection = new MySql.Data.MySqlClient.MySqlConnection();
                                return _MySqlConnection;
#endif
                            default:
                                if (_OdbcConnection == null) _OdbcConnection = new OdbcConnection();
                                return _OdbcConnection;
                        }

                    case DBProviderType.Postgre:
                        if (_OdbcConnection == null) _OdbcConnection = new OdbcConnection();
                        return _OdbcConnection;
                }
                return null;
            }

            set
            {
                switch (KTDBType.DBConfig)
                {
                    case DBProviderType.Oracle:

                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Oledb:
                                _OledbConnection = (System.Data.OleDb.OleDbConnection)value;
                                break;

#if OracleODAC
                            case ADOProviderType.OracleClient:
                                _OraConnection = (Oracle.ManagedDataAccess.Client.OracleConnection)value;
                                break;
#elif OracleClient
                            case ADOProviderType.OracleClient:
                                _OraConnection = (System.Data.OracleClient.OracleConnection)value;
                                break;
#endif
                            default:
                                _OdbcConnection = (System.Data.Odbc.OdbcConnection)value;
                                break;
                        }
                        break;

                    case DBProviderType.SQLServer:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                _OdbcConnection = (System.Data.Odbc.OdbcConnection)value;
                                break;

                            case ADOProviderType.Oledb:
                                _OledbConnection = (System.Data.OleDb.OleDbConnection)value;
                                break;

                            case ADOProviderType.SQLClient:
                                _SqlConnection = (System.Data.SqlClient.SqlConnection)value;
                                break;
                        }
                        break;

                    case DBProviderType.Access:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                _OdbcConnection = (System.Data.Odbc.OdbcConnection)value;
                                break;

                            default:
                                _OledbConnection = (System.Data.OleDb.OleDbConnection)value;
                                break;
                        }
                        break;

                    case DBProviderType.Access_accdb:
                        _OledbConnection = (System.Data.OleDb.OleDbConnection)value;
                        break;

                    case DBProviderType.Excel:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                _OdbcConnection = (System.Data.Odbc.OdbcConnection)value;
                                break;

                            default:
                                _OledbConnection = (System.Data.OleDb.OleDbConnection)value;
                                break;
                        }
                        break;

                    case DBProviderType.Excel_xlsx:
                        _OledbConnection = (System.Data.OleDb.OleDbConnection)value;
                        break;

                    case DBProviderType.MySql:
                        switch (KTDBType.ADOConfig)
                        {
#if MySqlClient
                            case ADOProviderType.MySqlClient:
                                _MySqlConnection = (MySql.Data.MySqlClient.MySqlConnection)value;
                                break;
#endif
                            default:
                                _OdbcConnection = (System.Data.Odbc.OdbcConnection)value;
                                break;
                        }
                        break;

                    case DBProviderType.Postgre:
                        _OdbcConnection = (System.Data.Odbc.OdbcConnection)value;
                        break;
                }

            }
        }

        public IDbDataAdapter DataAdapter
        {
            get
            {
                switch (KTDBType.DBConfig)
                {
                    case DBProviderType.Oracle:

                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Oledb:
                                if (_OleDbDataAdapter == null) _OleDbDataAdapter = new OleDbDataAdapter(new System.Data.OleDb.OleDbCommand());
                                return _OleDbDataAdapter;

#if OracleODAC
                            case ADOProviderType.OracleClient:
                                if (_OraDataAdapter == null) _OraDataAdapter = new Oracle.ManagedDataAccess.Client.OracleDataAdapter(new Oracle.ManagedDataAccess.Client.OracleCommand());
                                return _OraDataAdapter;
#elif OracleClient
                            case ADOProviderType.OracleClient:
                                if (_OraDataAdapter == null) _OraDataAdapter = new System.Data.OracleClient.OracleDataAdapter(new System.Data.OracleClient.OracleCommand());
                                return _OraDataAdapter;
#endif
                            default:
                                if (_OdbcDataAdapter == null) _OdbcDataAdapter = new OdbcDataAdapter(new System.Data.Odbc.OdbcCommand());
                                return _OdbcDataAdapter;
                        }

                    case DBProviderType.SQLServer:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                if (_OdbcDataAdapter == null) _OdbcDataAdapter = new OdbcDataAdapter(new System.Data.Odbc.OdbcCommand());
                                return _OdbcDataAdapter;

                            case ADOProviderType.Oledb:
                                if (_OleDbDataAdapter == null) _OleDbDataAdapter = new OleDbDataAdapter(new System.Data.OleDb.OleDbCommand());
                                return _OleDbDataAdapter;

                            case ADOProviderType.SQLClient:
                                if (_SqlDataAdapter == null) _SqlDataAdapter = new SqlDataAdapter(new System.Data.SqlClient.SqlCommand());
                                return _SqlDataAdapter;
                        }
                        break;

                    case DBProviderType.Access:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                if (_OdbcDataAdapter == null) _OdbcDataAdapter = new OdbcDataAdapter(new System.Data.Odbc.OdbcCommand());
                                return _OdbcDataAdapter;

                            default:
                                if (_OleDbDataAdapter == null) _OleDbDataAdapter = new OleDbDataAdapter(new System.Data.OleDb.OleDbCommand());
                                return _OleDbDataAdapter;
                        }

                    case DBProviderType.Access_accdb:
                        if (_OleDbDataAdapter == null) _OleDbDataAdapter = new OleDbDataAdapter(new System.Data.OleDb.OleDbCommand());
                        return _OleDbDataAdapter;

                    case DBProviderType.Excel:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                if (_OdbcDataAdapter == null) _OdbcDataAdapter = new OdbcDataAdapter(new System.Data.Odbc.OdbcCommand());
                                return _OdbcDataAdapter;

                            default:
                                if (_OleDbDataAdapter == null) _OleDbDataAdapter = new OleDbDataAdapter(new System.Data.OleDb.OleDbCommand());
                                return _OleDbDataAdapter;
                        }

                    case DBProviderType.Excel_xlsx:
                        if (_OleDbDataAdapter == null) _OleDbDataAdapter = new OleDbDataAdapter(new System.Data.OleDb.OleDbCommand());
                        return _OleDbDataAdapter;

                    case DBProviderType.MySql:
                        switch (KTDBType.ADOConfig)
                        {
#if MySqlClient
                            case ADOProviderType.MySqlClient:
                                if (_MySqlDataAdapter == null) _MySqlDataAdapter = new MySqlDataAdapter(new MySql.Data.MySqlClient.MySqlCommand());
                                return _MySqlDataAdapter;
#endif
                            default:
                                if (_OdbcDataAdapter == null) _OdbcDataAdapter = new OdbcDataAdapter(new System.Data.Odbc.OdbcCommand());
                                return _OdbcDataAdapter;
                        }

                    case DBProviderType.Postgre:
                        if (_OdbcDataAdapter == null) _OdbcDataAdapter = new OdbcDataAdapter(new System.Data.Odbc.OdbcCommand());
                        return _OdbcDataAdapter;
                }

                return null;
            }
        }

        public IDbCommand Command
        {
            get
            {
                switch (KTDBType.DBConfig)
                {
                    case DBProviderType.Oracle:

                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Oledb:
                                if (_OleDbCommand == null) _OleDbCommand = new OleDbCommand();
                                return _OleDbCommand;

#if OracleODAC
                            case ADOProviderType.OracleClient:
                                if (_OraCommand == null) _OraCommand = new Oracle.ManagedDataAccess.Client.OracleCommand();
                                return _OraCommand;
#elif OracleClient
                            case ADOProviderType.OracleClient:
                                if (_OraCommand == null) _OraCommand = new System.Data.OracleClient.OracleCommand();
                                return _OraCommand;
#endif

                            default:
                                if (_OdbcCommand == null) _OdbcCommand = new OdbcCommand();
                                return _OdbcCommand;
                        }

                    case DBProviderType.SQLServer:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                if (_OdbcCommand == null) _OdbcCommand = new OdbcCommand();
                                return _OdbcCommand;

                            case ADOProviderType.Oledb:
                                if (_OleDbCommand == null) _OleDbCommand = new OleDbCommand();
                                return _OleDbCommand;

                            case ADOProviderType.SQLClient:
                                if (_SqlCommand == null) _SqlCommand = new SqlCommand();
                                return _SqlCommand;
                        }
                        break;

                    case DBProviderType.Access:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                if (_OdbcCommand == null) _OdbcCommand = new OdbcCommand();
                                return _OdbcCommand;

                            default:
                                if (_OleDbCommand == null) _OleDbCommand = new OleDbCommand();
                                return _OleDbCommand;
                        }

                    case DBProviderType.Access_accdb:
                        if (_OleDbCommand == null) _OleDbCommand = new OleDbCommand();
                        return _OleDbCommand;

                    case DBProviderType.Excel:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                if (_OdbcCommand == null) _OdbcCommand = new OdbcCommand();
                                return _OdbcCommand;

                            case ADOProviderType.Oledb:
                                if (_OleDbCommand == null) _OleDbCommand = new OleDbCommand();
                                return _OleDbCommand;
                        }
                        break;

                    case DBProviderType.Excel_xlsx:
                        if (_OleDbCommand == null) _OleDbCommand = new OleDbCommand();
                        return _OleDbCommand;

                    case DBProviderType.MySql:
                        switch (KTDBType.ADOConfig)
                        {
#if MySqlClient
                            case ADOProviderType.MySqlClient:
                                if (_MySqlCommand == null) _MySqlCommand = new MySql.Data.MySqlClient.MySqlCommand();
                                return _MySqlCommand;
#endif
                            default:
                                if (_OdbcCommand == null) _OdbcCommand = new OdbcCommand();
                                return _OdbcCommand;
                        }

                    case DBProviderType.Postgre:
                        if (_OdbcCommand == null) _OdbcCommand = new OdbcCommand();
                        return _OdbcCommand;
                }
                return null;
            }
        }

        public IDataReader DataReader
        {
            get
            {
                switch (KTDBType.DBConfig)
                {
                    case DBProviderType.Oracle:

                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Oledb:
                                return _OleDbDataReader;

#if OracleODAC
                            case ADOProviderType.OracleClient:
                                return _OraDataReader;
#elif OracleClient
                            case ADOProviderType.OracleClient:
                                return _OraDataReader;
#endif
                            default:
                                return _OdbcDataReader;
                        }

                    case DBProviderType.SQLServer:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                return _OdbcDataReader;

                            case ADOProviderType.Oledb:
                                return _OleDbDataReader;

                            case ADOProviderType.SQLClient:
                                return _SqlDataReader;
                        }
                        break;

                    case DBProviderType.Access:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                return _OdbcDataReader;

                            default:
                                return _OleDbDataReader;
                        }

                    case DBProviderType.Excel:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                return _OdbcDataReader;

                            default:
                                return _OleDbDataReader;
                        }

                    case DBProviderType.MySql:
                        switch (KTDBType.ADOConfig)
                        {
#if MySqlClient
                            case ADOProviderType.MySqlClient:
                                return _MySqlDataReader;
#endif
                            default:
                                return _OdbcDataReader;
                        }

                    case DBProviderType.Postgre:
                        return _OdbcDataReader;
                }

                return null;
            }

            set
            {
                switch (KTDBType.DBConfig)
                {
                    case DBProviderType.Oracle:

                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Oledb:
                                _OleDbDataReader = (System.Data.OleDb.OleDbDataReader)value;
                                break;

#if OracleODAC
                            case ADOProviderType.OracleClient:
                                _OraDataReader = (Oracle.ManagedDataAccess.Client.OracleDataReader)value;
                                break;
#elif OracleClient
                            case ADOProviderType.OracleClient:
							    _OraDataReader = (System.Data.OracleClient.OracleDataReader)value;
                                break;
#endif

                            default:
                                _OdbcDataReader = (System.Data.Odbc.OdbcDataReader)value;
                                break;
                        }
                        break;

                    case DBProviderType.SQLServer:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                _OdbcDataReader = (System.Data.Odbc.OdbcDataReader)value;
                                break;

                            case ADOProviderType.Oledb:
                                _OleDbDataReader = (System.Data.OleDb.OleDbDataReader)value;
                                break;

                            case ADOProviderType.SQLClient:
                                _SqlDataReader = (System.Data.SqlClient.SqlDataReader)value;
                                break;
                        }
                        break;

                    case DBProviderType.Access:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                _OdbcDataReader = (System.Data.Odbc.OdbcDataReader)value;
                                break;

                            default:
                                _OleDbDataReader = (System.Data.OleDb.OleDbDataReader)value;
                                break;
                        }
                        break;

                    case DBProviderType.Excel:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                _OdbcDataReader = (System.Data.Odbc.OdbcDataReader)value;
                                break;

                            default:
                                _OleDbDataReader = (System.Data.OleDb.OleDbDataReader)value;
                                break;
                        }
                        break;

                    case DBProviderType.MySql:
                        switch (KTDBType.ADOConfig)
                        {
#if MySqlClient
                            case ADOProviderType.MySqlClient:
                                _MySqlDataReader = (MySql.Data.MySqlClient.MySqlDataReader)value;
                                break;
#endif
                            default:
                                _OdbcDataReader = (System.Data.Odbc.OdbcDataReader)value;
                                break;

                        }
                        break;

                    case DBProviderType.Postgre:
                        _OdbcDataReader = (System.Data.Odbc.OdbcDataReader)value;
                        break;
                }

            }
        }

        public IDbTransaction Transaction
        {
            get
            {
                switch (KTDBType.DBConfig)
                {
                    case DBProviderType.Oracle:

                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Oledb:
                                return _OleDbTransaction;

#if OracleODAC
                            case ADOProviderType.OracleClient:
                                return _OraTransaction;
#elif OracleClient
                            case ADOProviderType.OracleClient:
                                return _OraTransaction;
#endif

                            default:
                                return _OdbcTransaction;
                        }

                    case DBProviderType.SQLServer:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                return _OdbcTransaction;

                            case ADOProviderType.Oledb:
                                return _OleDbTransaction;

                            case ADOProviderType.SQLClient:
                                return _SqlTransaction;
                        }
                        break;

                    case DBProviderType.Access:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                return _OdbcTransaction;

                            default:
                                return _OleDbTransaction;
                        }

                    case DBProviderType.Access_accdb:
                        return _OleDbTransaction;

                    case DBProviderType.Excel:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                return _OdbcTransaction;

                            default:
                                return _OleDbTransaction;
                        }

                    case DBProviderType.Excel_xlsx:
                        return _OleDbTransaction;

                    case DBProviderType.MySql:
                        switch (KTDBType.ADOConfig)
                        {
#if MySqlClient
                            case ADOProviderType.MySqlClient:
                                return _MySqlTransaction;
#endif
                            default:
                                return _OdbcTransaction;

                        }

                    case DBProviderType.Postgre:
                        return _OdbcTransaction;
                }

                return null;
            }

            set
            {
                switch (KTDBType.DBConfig)
                {
                    case DBProviderType.Oracle:

                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Oledb:
                                _OleDbTransaction = (System.Data.OleDb.OleDbTransaction)value;
                                break;

#if OracleODAC
                            case ADOProviderType.OracleClient:
                                _OraTransaction = (Oracle.ManagedDataAccess.Client.OracleTransaction)value;
                                break;
#elif OracleClient
                            case ADOProviderType.OracleClient:
                                _OraTransaction = (System.Data.OracleClient.OracleTransaction)value;
                                break;
#endif

                            default:
                                _OdbcTransaction = (System.Data.Odbc.OdbcTransaction)value;
                                break;
                        }
                        break;

                    case DBProviderType.SQLServer:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                _OdbcTransaction = (System.Data.Odbc.OdbcTransaction)value;
                                break;

                            case ADOProviderType.Oledb:
                                _OleDbTransaction = (System.Data.OleDb.OleDbTransaction)value;
                                break;

                            case ADOProviderType.SQLClient:
                                _SqlTransaction = (System.Data.SqlClient.SqlTransaction)value;
                                break;
                        }
                        break;

                    case DBProviderType.Access:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                _OdbcTransaction = (System.Data.Odbc.OdbcTransaction)value;
                                break;

                            default:
                                _OleDbTransaction = (System.Data.OleDb.OleDbTransaction)value;
                                break;
                        }
                        break;

                    case DBProviderType.Access_accdb:
                        _OleDbTransaction = (System.Data.OleDb.OleDbTransaction)value;
                        break;

                    case DBProviderType.Excel:
                        switch (KTDBType.ADOConfig)
                        {
                            case ADOProviderType.Odbc:
                                _OdbcTransaction = (System.Data.Odbc.OdbcTransaction)value;
                                break;

                            default:
                                _OleDbTransaction = (System.Data.OleDb.OleDbTransaction)value;
                                break;
                        }
                        break;

                    case DBProviderType.Excel_xlsx:
                        _OleDbTransaction = (System.Data.OleDb.OleDbTransaction)value;
                        break;

                    case DBProviderType.MySql:
                        switch (KTDBType.ADOConfig)
                        {
#if MySqlClient
                            case ADOProviderType.MySqlClient:
                                _MySqlTransaction = (MySql.Data.MySqlClient.MySqlTransaction)value;
                                break;
#endif
                            default:
                                _OdbcTransaction = (System.Data.Odbc.OdbcTransaction)value;
                                break;
                        }
                        break;

                    case DBProviderType.Postgre:
                        _OdbcTransaction = (System.Data.Odbc.OdbcTransaction)value;
                        break;
                }
            }
        }

        #endregion

        private bool disposed = false;
        public void Dispose()
        {
            if (this.disposed)
                return;

            disposed = true;
            GC.SuppressFinalize(this);
        }
    }

    namespace nsTableProvider
    {
        public class TableProvider : IDisposable
        {

            public TableProvider()
            {
                Table = new System.Collections.Generic.Dictionary<string, TableProvider>();
                DataType = new System.Collections.Generic.Dictionary<string, string>();
                ColumnSize = new System.Collections.Generic.Dictionary<string, Int32>();
                NumericPrecision = new System.Collections.Generic.Dictionary<string, short>();
                NumericScale = new System.Collections.Generic.Dictionary<string, short>();
                Comments = new System.Collections.Generic.Dictionary<string, string>();
            }

            //記錄當時 連線交易主機(TransHost) 的資料表名稱(TableName) 的 Schema 屬性
            public string TransHost { get; set; }

            public string TableName { get; set; }

            public string TableComments { get; set; }

            public DataTable TableSchema { get; set; }

            public System.Collections.Generic.Dictionary<string, TableProvider> Table { get; set; }
            public System.Collections.Generic.Dictionary<string, string> DataType { get; set; }
            public System.Collections.Generic.Dictionary<string, Int32> ColumnSize { get; set; }
            public System.Collections.Generic.Dictionary<string, short> NumericPrecision { get; set; }
            public System.Collections.Generic.Dictionary<string, short> NumericScale { get; set; }
            public System.Collections.Generic.Dictionary<string, string> Comments { get; set; }

            #region " IDisposable Support "
            private bool disposed = false;
            public void Dispose()
            {
                if (this.disposed)
                    return;

                disposed = true;
                GC.SuppressFinalize(this);
            }
            #endregion
        }
    }

    public class KTDBType
    {
        public DBProviderType DBConfig { get; set; }
        public ADOProviderType ADOConfig { get; set; }
        public KTDBType(DBProviderType DBProvider, ADOProviderType ADOProvider)
        {
            //設定此 Connection 與 Command 使用什麼類別的 DataBase 以及 ADO
            //會使用此方式定義 目的是強制定義 1.一定能夠使用 2.開發環境統一 3.或較為快速

            //此處的 Select Case 項目與其他的地方不同，主要是以這裡的定義為主
            //KTDBProvider 與 KTDBCommand 的定義只是往後能夠拓展程式連線使用
            switch (DBProvider)
            {
                case DBProviderType.Oracle:
                    //0950519 由於 9i Odbc 名稱命名問題，可能會連不上去
                    //        預設強制指定連線環境只能使用 OleDB
                    //        但是 Provider 與 8i 不一樣
                    switch (ADOProvider)
                    {
                        case ADOProviderType.OracleClient:
                            ADOProvider = ADOProviderType.OracleClient;
                            break;

                        default:
                            ADOProvider = ADOProviderType.Oledb;
                            break;
                    }
                    break;

                case DBProviderType.SQLServer:
                    switch (ADOProvider)
                    {
                        case ADOProviderType.SQLClient:
                            //特別指定為 SQLClient 才給 SQLClient
                            ADOProvider = ADOProviderType.SQLClient;
                            break;

                        default:
                            //否則全部都是 OleDB - 開發維護方便
                            ADOProvider = ADOProviderType.Oledb;
                            break;
                    }
                    break;

                case DBProviderType.Access:
                    //強制給 OleDB - Oledb 速度較快
                    ADOProvider = ADOProviderType.Oledb;
                    break;

                case DBProviderType.Excel:
                    //強制給 OleDB - Odbc 必須使用更新查詢 ??
                    ADOProvider = ADOProviderType.Oledb;
                    break;

                case DBProviderType.MySql:
                    //版本有三種 MysqlOdbc35、MysqlOdbc52、MySqlClient
                    //由使用者自行去決定使用版本
                    break;

                case DBProviderType.Postgre:
                    //強制給 Odbc - Postgre 如果要使用 .Net 的模組, 需要所有的人都有安裝才能使用
                    ADOProvider = ADOProviderType.Odbc;
                    break;
            }

            DBConfig = DBProvider;
            ADOConfig = ADOProvider;
        }
    }

    public enum DBProviderType
    {
        Oracle = 1,         //Oracle
        SQLServer = 2,      //SQLServer 
        MySql = 3,          //MySql
        Access = 4,	        //Access  mdb
        Access_accdb = 5,   //Access  accdb
        Excel = 6,	        //Excel   xls
        Excel_xlsx = 7,     //Excel   xlst
        Postgre = 8         //Postgre SQL
    }

    public enum ADOProviderType
    {
        Odbc = 0,           //使用 ODBC   資料提供者
        Oledb = 1,          //使用 OLE DB 資料提供者
        SQLClient = 2,      //使用 SQLClient 資料提供者
        OracleClient = 3,   //使用 預設 OldDb Oracle.ManagedDataAccess.dll 或 System.Data.OracleClient.dll 資料提供者
        MysqlOdbc52 = 4,    //MysqlODBC 5.2     
        MySqlClient = 5,    //Mysql.Data MySqlClient
        MysqlOdbc35 = 6     //預計廢除 MysqlODBC 3.5.1 在 C# 上面無法使用 應該是 .net framework 版本問題
    }
}
