﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

//namespace KTDB
//{

class KTConstant
{
    public const string vbCrLf = "\r\n";
    public const string NewLine = "\r\n";
}

class KTString
{
    //字串處理
    //           Commands                取得 CommandLine 參數值
    //           GetUserID               取得 CommandLine 的使用者代碼
    //           GetUserName             取得 CommandLine 的使用者姓名
    //           GetUserDept             取得 CommandLine 的使用者部門
    //           Space                   列出空白字長度
    //           Split                   以字串為分隔字串，回傳陣列
    //           IsNumeric               判斷傳入的整數、浮點數是否為數字
    //           LenB                    傳回以位元組數為單位的字串長度。不同於 String.Lenght
    //           MidB                    擷取字串 中文字以 2 個 BYTE，英文字以 1 個 Byte 的方式切字串長度
    //           FLS                     擷取字串 回傳固定長度字串，所有的字串文字皆視為 1 Byte 
    //           AddRightSpace           擷取字串 或 擷取後的字串在右邊補空白
    //           AddLeftSpace            擷取字串 或 擷取後的字串在左邊補空白
    //           Make_Array              丟字串，回傳陣列，同split功能
    //           CnvNull                 將 DBNull 值轉為換為空字串
    //           Copy                    將資料複製到剪貼簿
    //           Paste                   將目前剪貼簿的資料 [貼上]
    //           WriteToXML              將DataTable資料存入XML
    //           ReadFromXML             將XML資料讀到DataTable
    //           Contrary_SlantLine      解決 Mysql 跳脫字元 / \
    //           ReplaceDBNull           將整個 DataTable 所有 Row 的 Columns 值，排除有 DBNull 的值
    //           ReplaceDBNull           將 DataTable 的某個 Row 裡面的值，排除 DBNull 的值
    //           Multi_String            將字串拆解成固定長度的Array(這樣才不會切到英文或中文的一半)
    //           Multi_String_Enter      和 Multi_String一樣，但加上換行符號
    //           Multi_String_AddSpace   延伸 Multi_String_ 與 Multi_String_Enter，處理斷行的前面要補空白
    //           Row_Change              將某Table裡面的兩個Row換位置
    //           Display_Prg             檢查程式版本，呼叫自動更新程式
    //           errException            取得例外的錯誤處理程序內容
    //           Clear_Zero              將 DataTable 指定欄位資料為 0 的設定為 ""
    //           Clear_Dot               將 DataTable 指定欄位資料在小數點以下為 0 的移除
    //           CHANGE_ID               將病歷號or身分證字號轉碼加密
    //           RECOVER_ID              將病歷號or身分證字號轉碼回復為原來值
    //           字串編碼轉換Big5        將傳入之字串編碼方式轉成Big5，以防止USER打入UniCode的編碼
    //           StringToByteArray       將傳入之字串轉成ByteArray的方式
    //           ByteArrayToBase64       將傳入之ByteArray轉成成Base64編碼的方式
    //           Base64ToByteArray       將傳入之Base64編碼字串轉成ByteArray
    //           ByteArrayToString       將傳入之ByteArray轉成String
    //           VerifySignature         電子病歷驗章
    //           開啟程式操作手冊        傳入員工編號、程式編號，開立EIP網頁式的操作手冊
    //           Get_Words長度           傳入一串中英文字，回傳共多少Words，這常常是院長在限制字數用的
    //           ReplaceXMLSpecialChar   轉換XML特殊符號
    //           置換單引號              將單引號 ' 改成 ''

    public static string Commands()
    {
        //功能：取得 CommandLine 參數值
        //作者：吳居易
        //日期：094/07/13
        //說明：如果執行程式不傳入任何參數值，將回傳 [空字串]
        //參數：無
        //傳回：Commands 字串
        //版本：
        //範例：06bz.exe C173  吳居易  1222
        //備註：模擬與 Vb6 相同的 Command 參數值
        //     名稱不使用 Command，因為 Command 已經加入於 MTS 組件
        //     故使用 Commands 名稱來模擬 VB6 的 Command

        string CommandStr = System.Environment.CommandLine;
        int CutStart = CommandStr.IndexOf(" ");
        if (CutStart > -1)
        {
            //居易 1030626 
            //Win 7 使用 silent.Bat 呼叫程式，傳入 CommandLine 時，會產生兩個空白
            //但是一般程式呼叫或開發程式啟動，卻是正常的一個空白
            //所以再判斷一次原本的空白字元，後面是否還有重複出現一次
            //如果有出現空白擷取二個空白以後的 CommandLine
            CommandStr = CommandStr.Substring(CutStart + 1);
            if (!string.IsNullOrEmpty(CommandStr))
                if (CommandStr.Substring(0, 1) == " ")
                    CommandStr = CommandStr.Substring(1);
            return CommandStr;
        }

        return "";

    }

    public static string GetUserID()
    {
        if (string.IsNullOrEmpty(Commands()))
            return "";
        return MidB(Commands(), 1, 6).Trim();
    }

    public static string GetUserName()
    {
        if (string.IsNullOrEmpty(Commands()))
            return "";
        return MidB(Commands(), 7, 20).Trim();
    }

    public static string GetUserDept()
    {
        if (string.IsNullOrEmpty(Commands()))
            return "";
        return MidB(Commands(), 27, 4).Trim();
    }

    public static string GetINIFileNme()
    {
        if (string.IsNullOrEmpty(Commands()))
            return "";

        string Command = Commands();
        if (Command.IndexOf(":\\") > -1 & LenB(Commands()) > 30)
            return MidB(Command, 30, 100);

        return "";
    }

    public static string Space(int n)
    {
        return new String(' ', n);
    }

    public static string[] Split(string Context, string separator, StringSplitOptions sso = StringSplitOptions.None)
    {
        return Context.Split(new string[] { separator }, sso);
    }

    public static bool IsNumeric(string strNum)
    {
        //必須含浮點數判斷
        System.Text.RegularExpressions.Regex NumberPattern = new System.Text.RegularExpressions.Regex("[^0-9.]");

        return !NumberPattern.IsMatch(strNum);
    }

    public static int LenB(string Expression)
    {
        //功能：傳回以位元組數為單位的字串長度。不同於 String.Lenght
        //說明：本函數適用於多位元組(Multi-Bytes)字集的字串。
        //參數：Expression --> 來自於 String 運算式的字元。 
        //傳回：字串的位元組個數長度
        //備註：
        //範例：
        //作者：楊啟言
        //日期：094/05/04
        //版本：
        //將所傳入的字串(也就是字元的陣列)，以 Big5 編碼(codepage 950)寫入位元組陣列

        try
        {
            byte[] strBig5 = System.Text.Encoding.GetEncoding(950).GetBytes(Expression);
            return strBig5.Length;
        }
        catch (Exception)
        {
            return 0;
        }
    }

    public static int LenB(string wkStr, KTDB.DBProviderType DBConfig, string DataType = "")
    {

        int StrLentgh = 0;

        try
        {
            byte[] strBig5 = new byte[0];

            switch (DBConfig)
            {
                case KTDB.DBProviderType.MySql:
                    //如果某一個欄位長度是 varchar(10)
                    //那麼英文字的儲存長度一定是 10，
                    //中文utf-8的占3個字節，大概可以儲存 3個中文字 !
                    //但是在 mysql 裡面 不管中文、英文都是存 10 個字
                    //mysql 長度太長，會自動將字串截斷，但是程式不會回應錯誤，

                    StrLentgh = wkStr.Length;
                    break;

                case KTDB.DBProviderType.Oracle:
                    StrLentgh = System.Text.Encoding.UTF8.GetBytes(wkStr).Length;
                    break;

                case KTDB.DBProviderType.SQLServer:

                    switch (DataType.ToLower())
                    {
                        case "":
                            throw new System.Exception("SQLServer 必須傳入資料庫的文字型態 nvarchar, nchar, varchar, char");

                        case "nvarchar":
                        case "nchar":
                            //n系列開頭的字串，每個字節的中文與英文長度都相同
                            StrLentgh = wkStr.Length;
                            break;

                        case "varchar":
                        case "char":
                            StrLentgh = System.Text.Encoding.GetEncoding(950).GetBytes(wkStr).Length;
                            break;
                    }
                    break;

                default:
                    StrLentgh = System.Text.Encoding.GetEncoding(950).GetBytes(wkStr).Length;
                    break;
            }
        }
        catch (Exception)
        {
            return 0;
        }
        return StrLentgh;
    }

    public static string MidB(string Expression, int Start, int Length)
    {
        //功能：擷取字串 中文字以 2 個 BYTE，英文字以 1 個 Byte 的方式切字串長度
        //說明：本函數適用於多位元組(Multi-Bytes)字集的字串。
        //參數：Expression --> 欲切的字串
        //     Start      --> 切字串的起始位置，Start 開始位置必須大於 0
        //     Length     --> 切字串的長度，已經強制設定不能比 LenB(Expression) 長
        //傳回：MidB 擷取之後的字串
        //作者：楊啟言
        //範例：Ex：MidB("誰說我是帥哥阿", 3, 10) --> "說我是帥哥"
        //日期：094/05/04
        //版本：
        //備註：當擷取中文字時，如果從 2 Byte 只擷取其中 1 Byte 有可能會變亂碼
        //     0980130 居易 : 當遇到中文字的亂碼時，補一個空白回傳給 Function

        int i = 0;
        int j = 0;

        //將所傳入的字串(也就是字元的陣列)，以 Big5 編碼(codepage 950)寫入位元組陣列
        byte[] strBig5 = System.Text.Encoding.GetEncoding(950).GetBytes(Expression);

        //避免 Expression 字串不夠長，導致擷取時發生陣列索引超出範圍的錯誤 - 居易
        if (Length >= strBig5.Length)
        {
            if (Start == 1)
                Length = strBig5.Length;
            //當 Start 開始位置不是 1，其長度需要再扣除 (Start - 1)
            if (Start != 1)
                Length = strBig5.Length - (Start - 1);
            if (Length <= 0)
                return "";
        }

        //處理取得字串中的部份文字(依據Multi-Byte字集)
        byte[] strTmp = new byte[Length];

        int c = 0;
        for (i = Start - 1; i <= Start + Length - 2; i++)
        {
            if (strBig5[i] > 128 | c == 1)
            {
                //一個全行字型文字 是由兩個 大於 128 byte 字元結合在一起的
                // "是" 的第二個 byte 小於 128，意思是有部分的全型文字第二碼會小於 128
                // 當第一個字碼大於 128 時，第二個字碼一定也是中文字型
                c = c + 1;
            }
            else
            {
                // <= 128 的字元
                c = 3;
            }

            switch (c)
            {
                case 2:
                    strTmp[j - 1] = strBig5[i - 1];
                    strTmp[j] = strBig5[i];
                    c = 0;
                    break;

                case 3:
                    strTmp[j] = strBig5[i];
                    c = 0;
                    break;
            }

            j = j + 1;
        }

        //擷取到最後 c 是 1
        if (c == 1)
            strTmp[j - 1] = System.Text.Encoding.GetEncoding(950).GetBytes(" ")[0];

        //將 Big5 編碼的位元組陣列轉成字串
        return System.Text.Encoding.GetEncoding(950).GetString(strTmp);
    }


    public string MidDBLength(string wkStr, int Start, int Length, KTDB.DBProviderType DBConfig = KTDB.DBProviderType.Access, string DataType = "")
    {

        // 功能：擷取(資料庫編碼)字串，中文字在不同資料庫的不同編碼長度，切字串長度
        // 參數：wkStr   --> 欲切的字串
        //       Start   --> 切字串的起始位置，Start 開始位置必須大於 0
        //       Length  --> 切字串的長度，已經強制設定不能比 LenB(Expression) 長
        // 傳回：MidDBLength 擷取之後的字串
        // 作者：吳居易
        // 範例：Ex：MidDBLength("誰說我是帥哥阿", 3, 10) --> "說我是帥哥"
        //           MidDBLength("誰說我是帥哥阿", 4, 10) --> "說我是帥哥"
        // 日期：109/01/31
        // 版本：
        // 備註：1.擷取中文字時，不是從第一個字擷取，而是從第二或第三個指定位置擷取，
        // 該中文字會被轉為亂碼，改為空白。
        // 2.擷取中文字時，當最後一碼遇到中文字的亂碼時，改為空白。

        string GetStringType = "";

        switch (DBConfig)
        {
            case KTDB.DBProviderType.MySql:
                {
                    // 如果某一個欄位長度是 varchar(10)
                    // 那麼英文字的儲存長度一定是 10，
                    // 中文utf-8的占3個字節，大概可以儲存 3個中文字 !
                    // 但是在 mysql 裡面 不管中文、英文都是存 10 個字
                    // mysql 長度太長，會自動將字串截斷，但是程式不會回應錯誤，
                    GetStringType = "1";
                    break;
                }
            case KTDB.DBProviderType.Oracle:
                {
                    GetStringType = "3";
                    break;
                }

            case KTDB.DBProviderType.SQLServer:
                {
                    switch (DataType.ToLower())
                    {
                        case "":
                            {
                                throw new System.Exception("SQLServer 必須傳入資料庫的文字型態 nvarchar, nchar, varchar, char");
                            }

                        case "nvarchar":
                        case "nchar":
                            {
                                // n系列開頭的字串，每個字節的中文與英文長度都相同
                                GetStringType = "1";
                                break;
                            }

                        case "varchar":
                        case "char":
                            {
                                GetStringType = "2";
                                break;
                            }
                    }
                    break;
                }

            default:
                {
                    GetStringType = "2";
                    break;
                }
        }

        byte[] strBig5 = new byte[0];
        int StrLentgh = 0;
        switch (GetStringType)
        {
            case "1":
                {
                    strBig5 = new byte[wkStr.Length - 1 + 1];
                    StrLentgh = strBig5.Length;
                    break;
                }

            case "2":
                {
                    // 將所傳入的字串(也就是字元的陣列)，以 Big5 編碼(codepage 950)寫入位元組陣列
                    strBig5 = System.Text.Encoding.GetEncoding(950).GetBytes(wkStr);
                    StrLentgh = strBig5.Length;
                    break;
                }

            case "3":
                {
                    // 將所傳入的字串(也就是字元的陣列)，以 UTF8 編碼寫入位元組陣列
                    strBig5 = System.Text.Encoding.UTF8.GetBytes(wkStr);
                    StrLentgh = strBig5.Length;
                    break;
                }
        }

        // 避免 Expression 字串不夠長，導致擷取時發生陣列索引超出範圍的錯誤 - 居易
        if (Length >= StrLentgh)
        {
            if (Start == 1)
                Length = strBig5.Length;
            // 當 Start 開始位置不是 1，其長度需要再扣除 (Start - 1)
            if (Start != 1)
                Length = strBig5.Length - (Start - 1);
            if (Length <= 0)
                return "";
        }

        string newStr = "";

        switch (GetStringType)
        {
            case "1":
                {
                    newStr = wkStr.Substring(0, Length);
                    break;
                }

            case "2":
                {
                    newStr = MidB(wkStr, Start, Length);
                    break;
                }

            case "3":
                {
                    // 一個中文字 3 個 byte
                    byte[] strTmp = new byte[Length - 1 + 1];
                    int y = 0;
                    for (int x = Start; x <= (Length + Start) - 1; x++)
                    {
                        //strTmp[y] = strBig5.GetValue(x);
                        y = y + 1;
                    }

                    // 當地一個字或最後一個字是亂碼時，在透過 MidB 轉換一次會變成問號
                    string tmpStr = System.Text.Encoding.UTF8.GetString(strTmp);
                    newStr = MidB(tmpStr, 1, Length);

                    // 判斷 tmpStr 最初的文字是否　與重新轉換後的文字相同
                    // 不相同全部改為 空白
                    strTmp = System.Text.Encoding.GetEncoding(950).GetBytes(tmpStr);
                    if (System.Text.Encoding.GetEncoding(950).GetString(System.Text.Encoding.GetEncoding(950).GetBytes(newStr.Substring(0, 1))) == tmpStr.Substring(0, 1))
                    {
                    }
                    else
                        strTmp[0] = System.Text.Encoding.GetEncoding(950).GetBytes(" ")[0];
                    tmpStr = System.Text.Encoding.GetEncoding(950).GetString(strTmp);

                    strTmp = System.Text.Encoding.GetEncoding(950).GetBytes(tmpStr);
                    if (System.Text.Encoding.GetEncoding(950).GetString(System.Text.Encoding.GetEncoding(950).GetBytes(newStr.Substring(newStr.Length - 1, 1))) == tmpStr.Substring(tmpStr.Length - 1, 1))
                    {
                    }
                    else
                        strTmp[strTmp.Length - 1] = System.Text.Encoding.GetEncoding(950).GetBytes(" ")[0];
                    newStr = System.Text.Encoding.GetEncoding(950).GetString(strTmp);
                    break;
                }
        }

        return newStr;
    }


    public static string CnvNull(object wk_data)
    {
        //功能：將 DBNull 值轉為換為空字串
        //備註：.NET 不再適用 Null 常數值，也就是說 Coding 的時候沒有 Null 了
        //      此處的 Null 是資料庫搜尋出來的 Null 欄位
        return (Convert.IsDBNull(wk_data) ? "" : wk_data.ToString().Trim());
    }

    public static string CnvNullStr(object WkStr)
    {
        return (Convert.IsDBNull(WkStr) ? "" : (string)WkStr);
    }

    public static object CnvNullNum(object wkNum)
    {
        return (Convert.IsDBNull(wkNum) ? 0 : wkNum);
    }

    //public static string Fls(object oIn, int nLen)
    //{
    //    //功能：擷取字串 回傳固定長度字串，所有的字串文字皆視為 1 Byte 
    //    //改 G. E.
    //    //出現兩個錯誤情形
    //    //1.
    //    //不再做 OverLoads
    //    //直接由 Object 讓程式去轉型，避免 由 DataBase 抓下來的資料欄位會有 Null 情形
    //    //而導致錯誤訊息 [假如沒有縮小轉換，就無法呼叫可存取的多載 'KTString.Fls]
    //    //2.
    //    //將 Byval sIn As String 改為 Object ，Object 為最原始的資料型態，
    //    //存放的資料型態，程式會自動判斷，避免 由 Database 抓下來的資料欄位會有 DBNull 情形
    //    //
    //    //當 DataBase 欄位為 System.DBNull 的話，輸出的 Object 會自動轉型成為空字串
    //    //
    //    //3.字串後面補空白，若不考慮 2 Bytes的中文碼，亦可使用 oIn.PadRight(nLen)
    //    //
    //    return (oIn + KTString.Space(nLen)).Substring(0, nLen);
    //}

    public static string AddRightSpace(object Expression, int fixLenght)
    {
        //功能：擷取字串 或 擷取後的字串在右邊補空白
        //作者：吳居易
        //日期：094/08/08
        //說明：擷取字串 或 擷取後的字串在右邊補空白
        //參數：fixString --> 欲修改的字串，Object 型態當他是數字時會自動轉型
        //     fixLenght --> 欲擷取的字串大小
        //傳回：AddRightSpace 修改過後的字串
        //版本：
        //範例：Ex：AddRightSpace("我是帥哥", 10)  --> "我是帥哥  "
        //     Ex：AddRightSpace("我是帥哥嗎", 8) --> "我是帥哥"
        //備註：此功能與 Fls 不同的地方在於，會將 中文字視為 1 個 byte

        int LenB_Lenght = 0;
        string LenB_String = "";

        try
        {
            LenB_String = Convert.ToString(Expression);
            LenB_Lenght = LenB(LenB_String);
        }
        catch (Exception ex)
        {
            throw new System.Exception(ex.Message);
        }

        if (fixLenght >= LenB_Lenght)
        {
            return LenB_String + KTString.Space(fixLenght - LenB_Lenght);
        }
        else
        {
            return MidB(LenB_String, 1, fixLenght);
        }
    }


    public static void AppendFile(string FileNme, string FileStr)
    {
        //FileNme 是檔案的名稱 要先加 @
        //FileStr 是要Append的string 

        // This text is always added, making the file longer over time
        // if it is not deleted.
        using (StreamWriter sw = File.AppendText(FileNme))
        {
            sw.WriteLine(FileStr);
        }
    }

    public static string AddLeftSpace(string Expression, int fixLenght)
    {
        //功能：擷取字串 或 擷取後的字串在左邊補空白
        //作者：吳居易
        //日期：094/08/08
        //說明：擷取字串 或 擷取後的字串在左邊補空白
        //參數：fixString --> 欲修改的字串，Object 型態當他是數字時會自動轉型
        //     fixLenght --> 欲擷取的字串大小
        //傳回：AddLeftSpace 修改過後的字串
        //版本：
        //範例：Ex：AddLeftSpace("我是帥哥", 10)  --> "  我是帥哥"
        //     Ex：AddLeftSpace("我是帥哥嗎", 8) --> "我是帥哥"
        //備註：此功能與 Fls 不同的地方在於，會將 中文字視為 1 個 byte

        int LenB_Lenght = LenB(Expression);

        if (fixLenght >= LenB_Lenght)
        {
            return KTString.Space(fixLenght - LenB_Lenght) + Expression;
        }
        else
        {
            return MidB(Expression, 1, fixLenght);
        }

    }

    public static string[] Make_Array(string strWord, string strControlChar)
    {
        //丟字串，回傳陣列，同split功能
        return KTString.Split(strWord, strControlChar);
    }

    //將DataTable資料存到XML
    public static void WriteToXML(string Target_XMLFile, DataTable Source_Tbl, bool ShowErrMsg = true)
    {
        //功能：傳入路徑，原始資料Table，就存入指定路徑XML
        //作者：林珀辰
        //日期：094/08/31
        //說明：傳入路徑，原始資料Table，就存入指定路徑XML
        //參數：Target_XMLFile -> 欲存檔之路徑及檔名(C:\AP06BZ\LOG\XXX.XML)
        //     SourceTable    -> 欲儲存之Table
        //範例：Ex：AddRightSpace("C:\AP06BZ\LOG\1.XML",Tbl1) -> 將tbl1內容存到C:\AP06BZ\LOG\1.XML

        try
        {
            DataSet TempDs = new DataSet();
            DataTable TempTbl = new DataTable();
            TempTbl = Source_Tbl.Copy();
            TempDs.Tables.Add(TempTbl);
            TempDs.WriteXml(Target_XMLFile);
            TempTbl.Dispose();
            TempDs.Dispose();
        }
        catch (Exception ex)
        {
            if (ShowErrMsg == true)
                throw new System.Exception(ex.Message);
            //Interaction.MsgBox(ex.Message);
        }
    }

    public static DataTable ReadFromXML(string Target_XMLFile, bool ShowErrMsg = true)
    {
        //將XML存到DataTAble
        //功能：將指定之Target_XMLFile內容轉成DataTable
        //作者：林珀辰
        //日期：094/08/31
        //說明：將指定之Target_XMLFile內容轉成DataTable
        //參數：Target_XMLFile -> 欲使用之XML
        //回傳：回傳DataTable
        //範例：Ex：AddRightSpace("C:\AP06BZ\LOG\1.XML") -> 將C:\AP06BZ\LOG\1.XML 內容存到 DataTable

        //1050311 居易
        //        當 xmlFile 的資料內容是空白， DataSet 的 Tables.Count = 0
        //        Return New Datatable

        DataTable temptbl = new DataTable();
        try
        {
            if (System.IO.File.Exists(Target_XMLFile))
            {
                DataSet TempDs = new DataSet();
                TempDs.ReadXml(Target_XMLFile, XmlReadMode.Auto);
                //讀XML檔案            
                if (TempDs.Tables.Count == 0)
                {
                    TempDs.Dispose();
                }
                else
                {
                    temptbl = TempDs.Tables[0].Copy();
                    TempDs.Dispose();
                }
            }
        }
        catch (Exception ex)
        {
            if (ShowErrMsg == true)
                throw new System.Exception(ex.Message);
            //Interaction.MsgBox(ex.Message);
        }

        return temptbl;
    }


    //public static string StringToCode128(string chaine)
    //{
    //    string functionReturnValue = null;
    //    //V 2.0.0
    //    //Parametres : une chaine
    //    //Parameters : a string
    //    //Retour : * une chaine qui, affichee avec la police CODE128.TTF, donne le code barre
    //    //         * une chaine vide si parametre fourni incorrect
    //    //Return : * a string which give the bar code when it is dispayed with CODE128.TTF font
    //    //         * an empty string if the supplied parameter is no good
    //    dynamic i = null;
    //    dynamic checksum = null;
    //    dynamic mini = null;
    //    dynamic dummy = null;
    //    bool tableB = false;
    //    functionReturnValue = "";
    //    if (chaine.Length > 0) {
    //        //Verifier si caracteres valides
    //        //Check for valid characters
    //        for (i = 1; i <= chaine.Length; i++) {
    //            switch (Strings.AscW(Strings.Mid(chaine, i, 1))) {
    //                case 32: // TODO: to 126
    //                case 203:
    //                    break;
    //                default:
    //                    i = 0;
    //                    break; // TODO: might not be correct. Was : Exit For

    //                    break;
    //            }
    //        }
    //        //Calculer la chaine de code en optimisant l'usage des tables B et C
    //        //Calculation of the code string with optimized use of tables B and C
    //        functionReturnValue = "";
    //        tableB = true;
    //        if (i > 0) {
    //            i = 1;
    //            //i% devient l'index sur la chaine / i% become the string index
    //            while (i <= Strings.Len(chaine)) {
    //                if (tableB) {
    //                    //Voir si interessant de passer en table C / See if interesting to switch to table C
    //                    //Oui pour 4 chiffres au debut ou a la fin, sinon pour 6 chiffres / yes for 4 digits at start or end, else if 6 digits
    //                    mini = (i == 1 | i + 3 == Strings.Len(chaine) ? 4 : 6);
    //                    //GoTo testnum
    //                    TranBarCodeNum(ref mini, ref chaine, i);
    //                    //Choix table C / Choice of table C
    //                    if (mini < 0) {
    //                        //Debuter sur table C / Starting with table C
    //                        if (i == 1) {
    //                            functionReturnValue = Strings.ChrW(210);
    //                        //Commuter sur table C / Switch to table C
    //                        } else {
    //                            functionReturnValue = functionReturnValue + Strings.ChrW(204);
    //                        }
    //                        tableB = false;
    //                    } else {
    //                        if (i == 1)
    //                            functionReturnValue = Strings.ChrW(209);
    //                        //Debuter sur table B / Starting with table B
    //                    }
    //                }
    //                if (!tableB) {
    //                    //On est sur la table C, essayer de traiter 2 chiffres / We are on table C, try to process 2 digits
    //                    mini = 2;
    //                    //GoTo testnum
    //                    TranBarCodeNum(ref mini, ref chaine, i);
    //                    //OK pour 2 chiffres, les traiter / OK for 2 digits, process it
    //                    if (mini < 0) {
    //                        dummy = Conversion.Val(Strings.Mid(chaine, i, 2));
    //                        dummy = (dummy < 95 ? dummy + 32 : dummy + 105);
    //                        functionReturnValue = functionReturnValue + Strings.ChrW(dummy);
    //                        i = i + 2;
    //                    //On n'a pas 2 chiffres, repasser en table B / We haven't 2 digits, switch to table B
    //                    } else {
    //                        functionReturnValue = functionReturnValue + Strings.ChrW(205);
    //                        tableB = true;
    //                    }
    //                }
    //                if (tableB) {
    //                    //Traiter 1 caractere en table B / Process 1 digit with table B
    //                    functionReturnValue = functionReturnValue + Strings.Mid(chaine, i, 1);
    //                    i = i + 1;
    //                }
    //            }
    //            //Calcul de la cle de controle / Calculation of the checksum
    //            for (i = 1; i <= Strings.Len(StringToCode128()); i++) {
    //                dummy = Strings.AscW(Strings.Mid(StringToCode128(), i, 1));
    //                dummy = (dummy < 127 ? dummy - 32 : dummy - 105);
    //                if (i == 1)
    //                    checksum = dummy;
    //                checksum = (checksum + (i - 1) * dummy) % 103;
    //            }
    //            //Calcul du code ASCII de la cle / Calculation of the checksum ASCII code
    //            checksum = (checksum < 95 ? checksum + 32 : checksum + 105);
    //            //Ajout de la cle et du STOP / Add the checksum and the STOP
    //            functionReturnValue = functionReturnValue + Strings.ChrW(checksum) + Strings.ChrW(211);
    //        }
    //    }
    //    return functionReturnValue;
    //    return functionReturnValue;
    //    //testnum:
    //    //        'si les mini% caracteres a partir de i% sont numeriques, alors mini%=0
    //    //        'if the mini% characters from i% are numeric, then mini%=0
    //    //        mini% = mini% - 1
    //    //        If i% + mini% <= Len(chaine$) Then
    //    //            Do While mini% >= 0
    //    //                If AscW(Mid$(chaine$, i% + mini%, 1)) < 48 Or AscW(Mid$(chaine$, i% + mini%, 1)) > 57 Then Exit Do
    //    //                mini% = mini% - 1
    //    //            Loop
    //    //        End If
    //}


    //public static void TranBarCodeNum(ref int mini, ref string chaine, int i)
    //{
    //    mini = mini - 1;
    //    if (i + mini <= chaine.Length) {
    //        while (mini >= 0) {
    //            if (Strings.AscW(Strings.Mid(chaine, i + mini, 1)) < 48 | Strings.AscW(Strings.Mid(chaine, i + mini, 1)) > 57)
    //                break; // TODO: might not be correct. Was : Exit Do
    //            mini = mini - 1;
    //        }
    //    }
    //}

    public static string Contrary_SlantLine(string ComStr)
    {
        //功能：解決 Mysql 跳脫字元
        //參數：輸入 : ComStr$ --> 只需將寫入 Mysql 的字串丟進來讓程式做整理
        //     輸出 : Contrary_SlantLine = Str_Full$
        //說明：什麼是跳脫字元
        //     中文字係由兩個字元所組成，第一個字元大於 ASCII 127，第二個字元則不限。
        //     在許多程式語言之中，ASCII 92（\）被當作是跳脫（escape）字元
        //     在程式中需要輸出特定字元時，先加上 \，才能被系統所辨識出來。
        //     故寫入的中文字部份會產生錯誤 Ex:許功蓋俞餐（\） 柦（/）
        //     因為這些中文字的第二個字元就是（\）（/）
        //作者：吳居易
        //日期：0940923
        //備註：此 函式只能解決 \ / 的跳脫字元
        //     尚未發現其他錯誤的字元，有發現只要加入 IF 判斷即可
        //     以 \ 字元來說詞是真正的跳脫字元，而柦的跳脫字元應該也是 \
        //     但是卻要使用 / 來忽略這個關鍵字元

        Int32 str_len = default(Int32);
        //傳入整個字串的長度
        string Str_One = null;
        //一個字一個字抓
        string Str_Last = null;
        //中文字的第二個字元
        string Str_Full = null;
        //處理後的回傳字串
        Int32 x = default(Int32);

        str_len = ComStr.Length;
        Str_Full = "";
        for (x = 0; x <= str_len - 1; x++)
        {
            Str_One = ComStr.Substring(x, 1);
            Str_Last = MidB(Str_One, 2, 1);
            if (Str_Last == "\\")
            {
                Str_One = Str_One + "\\";
            }
            if (Str_Last == "/")
            {
                //解決 另一個字元 "柦" --> "/"
                Str_One = Str_One + "/";
            }
            Str_Full = Str_Full + Str_One;
        }

        return Str_Full;
    }

    public static void ReplaceDBNull(DataTable dTable)
    {
        //功能：將整個 DataTable 所有 Row 的 Columns 值，排除有 DBNull 的值
        //作者：吳居易
        //日期：0941115
        //參數：dTable --> 整個 DataTable
        //說明：Columns.DataType 是字串，若為 DBNull 則改為空字串 ""
        //     Columns.DataType 是數字，若為 DBNull 則改為零 0
        //範例：ReplaceDBNull(DataTable)
        //備註：請參考 ReplaceDBNull by DataRow
        if (dTable == null)
            return;

        if (dTable.Rows.Count == 0)
            return;

        foreach (DataRow row in dTable.Rows)
        {
            ReplaceDBNull(row);
        }
    }

    public static void ReplaceDBNull(DataRow dRows)
    {
        //功能：將 DataTable 的某個 Row 裡面的值，排除有 DBNull 的值
        //作者：吳居易
        //日期：0941005
        //參數：dRows --> 某個 DataTable 的 Row
        //說明：dRows 某個 Columns.DataType 是字串，若為 DBNull 則改為空字串 ""
        //     dRows 某個 Columns.DataType 是數字，若為 DBNull 則改為零 0
        //範例：1.ReplaceDBNull(DataTable.Rows(0))
        //     2.ReplaceDBNull(Row)
        //備註：請參考 ReplaceDBNull by DataTable

        if (dRows == null) return;

        foreach (DataColumn col in dRows.Table.Columns)
        {
            try
            {
                switch (col.DataType.ToString())
                {
                    case "System.String":

                        if (!col.ReadOnly)
                            dRows[col] = (Convert.IsDBNull(dRows[col]) ? "" : dRows[col]);
                        break;

                    case "System.DateTime":
                    case "System.TimeSpan":
                        break;

                    //尚無法得知 DateTime 為 Null 的時候如何處理
                    //dRows(col) = IIf(Convert.IsDBNull(dRows(col)), New System.DateTime, dRows(col))
                    case "System.Decimal":
                    case "System.Short":
                    case "System.Integer":
                    case "System.Long":
                    case "System.Single":
                    case "System.Double":
                    case "System.Int16":
                    case "System.Int32":
                    case "System.Int64":
                    case "System.Char":
                        if (!col.ReadOnly)
                            dRows[col] = (Convert.IsDBNull(dRows[col]) ? 0 : dRows[col]);
                        break;
                }

            }
            catch (Exception ex)
            {
                string errMsg = "";
                if ((col != null))
                {
                    errMsg = "例外的錯誤:" + col.ColumnName + "欄位須針對該資料型態內容重新處理";
                    throw new System.Exception(ex.Message + KTConstant.vbCrLf + KTConstant.vbCrLf + errMsg);
                }
            }
        }
    }

    public static void ReplaceModelsNull(dynamic Listmodels)
    {
        //功能：將 Model 排除有 null 的值
        //作者：吳居易
        //日期：1070102
        //參數：Listmodels 兩種情況 --> 多筆的 List Models
        //                       --> 單筆的 Model
        //說明：某個欄位是字串，若為 null 則改為空字串 ""
        //範例：KTString.ReplaceModelsNull(model);
        //備註：dynamic 無法使用多載，識別傳入的參數使用不同的 Function
        //     只能取得屬性的Name 是否為 List 集合，做後續的 Replace 處理

        if (Listmodels.GetType().Name.Substring(0, 4) == "List")
            foreach (dynamic model in Listmodels)
                ReplaceModelNull(model);
        else
            ReplaceModelNull(Listmodels);
    }

    private static void ReplaceModelNull(dynamic model)
    {
        //功能：將 Model 排除有 null 的值
        //作者：吳居易
        //日期：1070102
        //參數：models --> 單一筆的 Row
        //說明：某個欄位是字串，若為 null 則改為空字串 ""
        //範例：利用 KTString.ReplaceModelsNull [多筆Models] 去來呼叫此 ReplaceModelNull [單一Model] ;
        //備註：僅強行別可以轉換，匿名型別都是 object 無法轉換。
        //     除非匿名型別都允許視為 string, 或傳入指定的欄位表示為字串。 
        System.Reflection.PropertyInfo[] props = model.GetType().GetProperties();

        foreach (System.Reflection.PropertyInfo prop in props)
        {
            if (prop.PropertyType.FullName == "System.String")
                if (prop.GetValue(model, null) == null)
                    prop.SetValue(model, "", null);
        }
    }

    public static object getdynamicData(dynamic model, string columnname)
    {
        //功能：getdynamicData      取得 Dapper dynamic 某個欄位值(很難定義名稱)
        //日期：108/02/12
        //參數：Expresssion         List<dynamic> 的其中一個陣列(Row)
        //範例：dy = (List<dynamic>)KTCC.DoSql<dynamic>(sqlstr.ToString(), param).ToList();
        //    (string) KTString.getdynamicData(dy[0], ColumnName);
        return (model as IDictionary<string, object>)[columnname];
    }

    public static string[] Multi_String(string Expression, int Len)
    {
        //功能：Multi_String        將字串拆解成固定長度的Array(這樣才不會切到英文或中文的一半)
        //日期：0941102
        //參數：Expresssion  欲切割的String，Len 切割多長
        //回傳：ReturnValue() as Array
        //說明：用在字串一行要折解成多行時使用，英文會依照空白字元拆解，中文會依照字數拆解
        //範例：Multi_String("TestTextTest Test Test",18)
        //     ->TestTextTest Test 
        //     ->Test
        //備註：更詳細範例請上Visual Source Safe->測試->切字串(有直接針對該Function產出的Array做拆解)
        string[] ReturnValue = new string[0];
        string[] Temp_Array = new string[0];
        string TempString = "";

        string SubStringChar = "";

        //"\r\n"
        for (int i = 0; i <= Expression.Length - 1; i++)
        {
            //英文
            SubStringChar = Expression.Substring(i, 1);

            if (LenB(SubStringChar) == 1)
            {
                if (SubStringChar == " ")
                    TempString = TempString + " <SPC>";

                else if (SubStringChar == "\r")
                    TempString = TempString + "<SPC>";

                else if (SubStringChar == "\n")
                    TempString = TempString + "<ENT>";

                else
                    TempString = TempString + SubStringChar;
            }
            else//中文
                TempString = TempString + SubStringChar + "<SPC>";
        }

        //TempString = TempString & "<SPC><END>"
        Temp_Array = KTString.Split(TempString, "<SPC>");

        //開始組成字串陣列
        for (int i = 0; i <= Temp_Array.Length - 1; i++)
        {
            int Temp_Array_LenB = LenB(Temp_Array[i]);

            if (ReturnValue.Length == 0)
            {
                //連單行都超過寬的話，則再自行單行拆解多行
                if (Temp_Array_LenB > Len)
                {
                    int L = 0;
                    for (L = 0; L <= 100; L++)
                    {
                        if (Temp_Array_LenB > L)
                        {
                            if (!string.IsNullOrEmpty(Temp_Array[i].Substring(L * Len, Len)))
                            {
                                if (ReturnValue.Length == 0)
                                {
                                    Array.Resize(ref ReturnValue, 1);
                                    ReturnValue[0] = Temp_Array[i].Substring(L * Len, Len);
                                }
                                else
                                {
                                    Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                                    ReturnValue[ReturnValue.Length - 1] = Temp_Array[i].Substring(L * Len, Len);
                                }
                            }
                        }
                    }
                }
                else
                {
                    Array.Resize(ref ReturnValue, 1);
                    ReturnValue[0] = Temp_Array[i];
                }
            }
            else
            {
                //超過每行的寬
                if (Temp_Array_LenB + LenB(ReturnValue[ReturnValue.Length - 1]) > Len)
                {
                    //count += 1

                    //連單行都超過寬的話，則再自行單行拆解多行
                    if (Temp_Array_LenB > Len)
                    {
                        int L = 0;
                        for (L = 0; L <= 100; L++)
                        {
                            if (!string.IsNullOrEmpty(Temp_Array[i].Substring(L * Len, Len)))
                            {
                                Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                                ReturnValue[ReturnValue.Length - 1] = Temp_Array[i].Substring(L * Len, Len);
                            }
                        }
                    }
                    else
                    {
                        Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                        ReturnValue[ReturnValue.Length - 1] = Temp_Array[i];
                    }

                    //ReDim Preserve ReturnValue(count)
                    //ReturnValue(count) = Temp_Array[i]
                    //未超過每行的寬
                }
                else
                {
                    ReturnValue[ReturnValue.Length - 1] = ReturnValue[ReturnValue.Length - 1] + Temp_Array[i];
                }
            }
        }

        Temp_Array = null;
        return ReturnValue;
    }

    public static string[] Multi_String_Enter(string Expression, int Len)
    {
        //和Multi_String一樣，但加上換行符號
        //功能：Multi_String        將字串拆解成固定長度的Array(這樣才不會切到英文或中文的一半)
        //日期：0941102
        //參數：Expresssion  欲切割的String，Len 切割多長
        //回傳：ReturnValue() as Array
        //說明：用在字串一行要折解成多行時使用，英文會依照空白字元拆解，中文會依照字數拆解
        //範例：Multi_String("TestTextTest Test Test", 18)
        //     ->TestTextTest Test 
        //     ->Test
        //備註：更詳細範例請上Visual Source Safe->測試->切字串(有直接針對該Function產出的Array做拆解)
        string[] ReturnValue = new string[0];
        string[] Temp_Array = new string[0];
        string TempString = "";

        string SubStringChar = "";

        //"\r\n"
        for (int i = 0; i <= Expression.Length - 1; i++)
        {
            //英文
            SubStringChar = Expression.Substring(i, 1);

            if (LenB(SubStringChar) == 1)
            {
                if (SubStringChar == " ")
                    TempString = TempString + " <SPC>";

                else if (SubStringChar == "\r")
                    TempString = TempString + "<SPC>";

                else if (SubStringChar == "\n")
                    TempString = TempString + "<ENT>";

                else
                    TempString = TempString + SubStringChar;
            }
            else//中文
                TempString = TempString + SubStringChar + "<SPC>";
        }

        string TempSubString = "";

        Temp_Array = KTString.Split(TempString, "<SPC>");

        string[] Temp_Array_Ent = null;

        //開始組成字串陣列
        for (int i = 0; i <= Temp_Array.Length - 1; i++)
        {
            int Temp_Array_LenB = LenB(Temp_Array[i]);

            //此行中有段行符號
            if (Temp_Array[i].IndexOf("<ENT>") > -1)
            {
                Temp_Array_Ent = KTString.Split(Temp_Array[i], "<ENT>");
                for (int j = 0; j <= Temp_Array_Ent.Length - 1; j++)
                {
                    int Temp_Array_Ent_LenB = LenB(Temp_Array_Ent[j]);

                    if (j == 0)
                    {
                        //0970306 解決:第一行是 Enter 空白的時候會發生錯誤
                        if (ReturnValue.Length == 0)
                        {

                            //連單行都超過寬的話，則再自行單行拆解多行
                            if (Temp_Array_Ent_LenB > Len)
                            {
                                for (int L = 0; L <= 100; L++)
                                {
                                    TempSubString = Temp_Array_Ent[j].Substring(L * Len, Len);
                                    if (!string.IsNullOrEmpty(TempSubString))
                                    {
                                        if (ReturnValue.Length == 0)
                                        {
                                            Array.Resize(ref ReturnValue, 1);
                                            ReturnValue[0] = TempSubString;
                                        }
                                        else
                                        {
                                            Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                                            ReturnValue[ReturnValue.Length - 1] = TempSubString;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Array.Resize(ref ReturnValue, 1);
                                ReturnValue[ReturnValue.Length - 1] = Temp_Array_Ent[j];
                            }
                        }
                        //超過每行的寬
                        else if (Temp_Array_Ent_LenB + LenB(ReturnValue[ReturnValue.Length - 1]) > Len)
                        {
                            //連單行都超過寬的話，則再自行單行拆解多行
                            if (Temp_Array_Ent_LenB > Len)
                            {
                                for (int L = 0; L <= 100; L++)
                                {
                                    TempSubString = Temp_Array_Ent[j].Substring(L * Len, Len);
                                    if (!string.IsNullOrEmpty(TempSubString))
                                    {
                                        Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                                        ReturnValue[ReturnValue.Length - 1] = TempSubString;
                                    }
                                }
                            }
                            else
                            {
                                Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                                ReturnValue[ReturnValue.Length - 1] = Temp_Array_Ent[j];
                            }
                            //未超過每行的寬
                        }
                        else
                        {
                            ReturnValue[ReturnValue.Length - 1] = ReturnValue[ReturnValue.Length - 1] + Temp_Array_Ent[j];
                        }
                    }
                    else
                    {

                        if (ReturnValue.Length == 0)
                        {
                            //連單行都超過寬的話，則再自行單行拆解多行
                            if (Temp_Array_Ent_LenB > Len)
                            {
                                for (int L = 0; L <= 100; L++)
                                {
                                    TempSubString = Temp_Array_Ent[j].Substring(L * Len, Len);
                                    if (!string.IsNullOrEmpty(TempSubString))
                                    {
                                        if (ReturnValue.Length == 0)
                                        {
                                            Array.Resize(ref ReturnValue, 1);
                                            ReturnValue[0] = TempSubString;
                                        }
                                        else
                                        {
                                            Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                                            ReturnValue[ReturnValue.Length - 1] = TempSubString;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Array.Resize(ref ReturnValue, 1);
                                ReturnValue[ReturnValue.Length - 1] = Temp_Array_Ent[j];
                            }

                        }
                        else
                        {
                            //連單行都超過寬的話，則再自行單行拆解多行
                            if (Temp_Array_Ent_LenB > Len)
                            {
                                for (int L = 0; L <= 100; L++)
                                {
                                    TempSubString = Temp_Array_Ent[j].Substring(L * Len, Len);
                                    if (!string.IsNullOrEmpty(TempSubString))
                                    {
                                        Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                                        ReturnValue[ReturnValue.Length - 1] = TempSubString;
                                    }
                                }
                            }
                            else
                            {
                                Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                                ReturnValue[ReturnValue.Length - 1] = Temp_Array_Ent[j];
                            }
                        }
                    }
                }
            }
            else
            {
                if (ReturnValue.Length == 0)
                {
                    //連單行都超過寬的話，則再自行單行拆解多行
                    if (Temp_Array_LenB > Len)
                    {
                        for (int L = 0; L <= 100; L++)
                        {
                            TempSubString = Temp_Array[i].Substring(L * Len, Len);
                            if (!string.IsNullOrEmpty(TempSubString))
                            {
                                if (ReturnValue.Length == 0)
                                {
                                    Array.Resize(ref ReturnValue, 1);
                                    ReturnValue[0] = TempSubString;
                                }
                                else
                                {
                                    Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                                    ReturnValue[ReturnValue.Length - 1] = TempSubString;
                                }
                            }
                        }
                    }
                    else
                    {
                        Array.Resize(ref ReturnValue, 1);
                        ReturnValue[ReturnValue.Length - 1] = Temp_Array[i];
                    }

                }
                //超過每行的寬
                else if (Temp_Array_LenB + LenB(ReturnValue[ReturnValue.Length - 1]) > Len)
                {
                    //連單行都超過寬的話，則再自行單行拆解多行
                    if (Temp_Array_LenB > Len)
                    {
                        for (int L = 0; L <= 100; L++)
                        {
                            TempSubString = Temp_Array[i].Substring(L * Len, Len);
                            if (!string.IsNullOrEmpty(TempSubString))
                            {
                                Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                                ReturnValue[ReturnValue.Length - 1] = TempSubString;
                            }
                        }
                    }
                    else
                    {
                        Array.Resize(ref ReturnValue, ReturnValue.Length + 1);
                        ReturnValue[ReturnValue.Length - 1] = Temp_Array[i];
                    }
                }
                else
                {
                    //未超過每行的寬
                    ReturnValue[ReturnValue.Length - 1] = ReturnValue[ReturnValue.Length - 1] + Temp_Array[i];
                }
            }
        }

        Temp_Array = null;
        return ReturnValue;
    }

    public static int Multi_String_AddSpace(ref string linestr, int cutNum, int sSpace, int showLine, string MultiMode = "")
    {
        //功能：處理斷行的前面要補空白
        //參數：
        //lineStr  ：要處理斷行的字串 --> 給 Multi_String or Multi_String_Enter
        //cutNum   ：切多少字體 --> 給 Multi_String or Multi_String_Enter
        //sSpace   ：第一行有可能是標題 + 說明
        //            所以這裡是第二行的起始位置前面加空白與第一行的起始位置對齊
        //showLine ：當 Array 有多行的時候要顯示幾行就可以了
        //            當 showLine 是 9999 的話時候表示切好的斷行全部顯示出來
        //MultiMode：空白 --> 一般的斷行
        //            "Enter" --> 包含醫師的斷行
        //回傳參數   ：處理斷行 --> 回傳有幾行
        // Ex：例如
        //     我要從這裡開始印，對齊 [例如] 所以前面補空白
        //備註：如果有需要，也許 Multi_String_Enter 也要處理一次

        int xx = 0;
        int line = 0;
        string[] arraystr = new string[0];

        switch (MultiMode.ToUpper())
        {
            case "":
                arraystr = Multi_String(linestr, cutNum);
                break;

            case "ENTER":
                arraystr = Multi_String_Enter(linestr, cutNum);
                break;
        }

        linestr = "";

        if (showLine == 9999)
            showLine = arraystr.Length;
        if (showLine >= arraystr.Length)
            showLine = arraystr.Length;

        for (xx = 0; xx <= showLine - 1; xx++)
        {
            if (xx != 0)
                linestr = linestr + KTConstant.vbCrLf + (sSpace != 0 ? KTString.Space(sSpace) : "");
            linestr = linestr + arraystr[xx];
            line = line + 1;
        }

        return line;

    }

    public static void Row_Change(ref DataTable Target_Tbl, int FromRow, int ToRow)
    {
        //功能：Row_Change   將某Table的兩Row調換位置
        //日期：0950109
        //參數：Target_Tbl-某Table，FromRow-欲被移動的Row，ToRow欲移動至那個Row
        //回傳：Target_Tbl完成移動結果
        //範例：Target_Tbl的Row資料內容為 0123456789，Row_Change(Target_Tbl, 8, 3)
        //     ->0128456739
        try
        {
            if (FromRow > ToRow)
            {
                int j = 0;
                j = FromRow;
                FromRow = ToRow;
                ToRow = j;
            }

            DataTable tmp = new DataTable();
            tmp = Target_Tbl.Clone();
            int i = 0;

            object[] o = null;
            for (i = 0; i <= Target_Tbl.Rows.Count - 1; i++)
            {
                if (i == FromRow)
                {
                    o = Target_Tbl.Rows[ToRow].ItemArray;
                    tmp.LoadDataRow(o, true);
                }
                else if (i == ToRow)
                {
                    o = Target_Tbl.Rows[FromRow].ItemArray;
                    tmp.LoadDataRow(o, true);
                }
                else
                {
                    o = Target_Tbl.Rows[i].ItemArray;
                    tmp.LoadDataRow(o, true);
                }
            }

            Target_Tbl.Rows.Clear();
            Target_Tbl = tmp.Copy();
            tmp.Rows.Clear();
            tmp.Dispose();
            tmp = null;
        }
        catch (Exception)
        {
        }
    }

    public static string Get_Application_Data(檔案內容 Type, string Target_File, bool TOCDATE = true)
    {
        //取得某檔案的檔案各種屬性
        //TOCDATE 是否轉成民國年
        if (System.IO.File.Exists(Target_File) == false)
        {
            return "";
        }

        string dt = "";

        switch (Type)
        {
            case 檔案內容.建立日期:
                DateTime GetCreateDateTime = System.IO.File.GetCreationTime(Target_File);
                if (TOCDATE == true)
                {
                    dt = GetCreateDateTime.Year - 1911 + "/" + GetCreateDateTime.Month + "/" + GetCreateDateTime.Day + " " + GetCreateDateTime.ToLongTimeString();
                }
                else
                {
                    dt = GetCreateDateTime.Year + "/" + GetCreateDateTime.Month + "/" + GetCreateDateTime.Day + " " + GetCreateDateTime.ToLongTimeString();
                }
                break;

            case 檔案內容.修改日期:
                DateTime LastWriteDateTime = System.IO.File.GetLastWriteTime(Target_File);
                if (TOCDATE == true)
                {
                    dt = LastWriteDateTime.Year - 1911 + "/" + LastWriteDateTime.Month + "/" + LastWriteDateTime.Day + " " + LastWriteDateTime.ToLongTimeString();
                }
                else
                {
                    dt = LastWriteDateTime.Year + "/" + LastWriteDateTime.Month + "/" + LastWriteDateTime.Day + " " + LastWriteDateTime.ToLongTimeString();
                }
                break;

            case 檔案內容.存取日期:
                DateTime LastAccessDateTime = System.IO.File.GetLastAccessTime(Target_File);
                if (TOCDATE == true)
                {
                    dt = LastAccessDateTime.Year - 1911 + "/" + LastAccessDateTime.Month + "/" + LastAccessDateTime.Day + " " + LastAccessDateTime.ToLongTimeString();
                }
                else
                {
                    dt = LastAccessDateTime.Year + "/" + LastAccessDateTime.Month + "/" + LastAccessDateTime.Day + " " + LastAccessDateTime.ToLongTimeString();
                }
                break;

            case 檔案內容.檔案屬性:
                dt = System.IO.File.GetAttributes(Target_File).ToString();
                break;
        }

        return dt;
    }

    public enum 檔案內容
    {
        建立日期 = 0,
        修改日期 = 1,
        存取日期 = 2,
        檔案屬性 = 3
    }

    public static string errException(Exception ex)
    {
        //功能：取得例外的錯誤處理程序內容
        //作者：吳居易
        //日期：0950204
        //參數：ex          --> 某個 處理程序上的 Try Catch Error 事件
        //     showMessage --> 是否直接顯示錯誤訊息
        //回傳：ex 處理後的 String 資料值
        //說明：第一行所出現的錯誤行數，是 Try Catch 接收到執行錯誤的地方
        //     越往下是代表執行錯誤之處的原始呼叫來源 Function
        //範例：1.Messagebox.Show(errException(ex)) --> 由外部呼叫程式 Show Message
        //     2.Call errException(ex, True)       --> 由 Function Show Message

        string errMessage = "";
        string[] errArray = null;
        string[] nowLine = null;
        string[] DotItem = null;
        string errLine = "";
        int errx = 0;

        errArray = KTString.Split(ex.StackTrace, " at ");
        if (errArray.Length == 1)
            errArray = KTString.Split(ex.StackTrace, " 於 ");

        //最後面的錯誤是一開始的呼叫位置
        errMessage = "程式片段：" + KTConstant.vbCrLf;
        for (errx = 1; errx <= errArray.Length - 1; errx++)
        {
            nowLine = KTString.Split(errArray[errx], " in ");
            if (nowLine.Length > 1)
            {
                DotItem = nowLine[0].Split('.');
                if (string.IsNullOrEmpty(errLine))
                    errLine = DotItem[DotItem.Length - 1] + " --> 第 " + KTString.Split(errArray[errx], "line")[1].Trim() + " 行" + KTConstant.vbCrLf;
                errMessage = errMessage + (errx).ToString().Trim() + "." + DotItem[DotItem.Length - 1] + KTString.Space(1) + nowLine[1].Trim() + KTConstant.vbCrLf;
            }
            else
            {
                errMessage = errMessage + (errx).ToString().Trim() + "." + nowLine[0].Trim() + KTConstant.vbCrLf;
            }

        }

        errMessage = errMessage + KTConstant.vbCrLf;
        errMessage = errMessage + errLine;
        errMessage = errMessage + ex.Message + KTConstant.vbCrLf + KTConstant.vbCrLf;

        return errMessage;
    }

    public static string ExcludeDotNumIsZero(float Num, string sType)
    {
        //功能：排除 小數點以下 數字是 0 的
        //備註：珀辰 ORDP1000.exe 門診共用模組, 有類似程式(但是忘了是哪一個)
        //     a.Oracle 裡面有小數點, 如果都呈現出來不好看, 
        //       所以需要排除小數點以後都是後是 0 的數字
        //     b.TrueDBGrid , 也不能呈現太多的 0, 會太複雜
        //
        //     1.小數點以下是 0 , 回傳整數.
        //     2.小數點以下是 0 , 回傳整數, 若整數是 0 回傳空字串
        //     else. 直接回傳整個 Num

        string[] NumAry = Num.ToString().Trim().Split(Convert.ToChar("."));

        //小數點不是 0 
        if (NumAry.Length > 1)
            if (Convert.ToInt32(NumAry[1]) > 0)
                return Num.ToString();

        switch (sType)
        {
            case "1":
                //小數點是 0 
                return NumAry[0];

            case "2":
                //小數點是 0 , 整數是 0 的回傳 空字串
                if (NumAry[0] == "0")
                    return "";

                return NumAry[0];

            default:
                //數字包含小數點完全呈現出
                return Num.ToString();
        }
    }

    public static void 條碼標籤_病歷基本資料列印(string wk_crtno, string wk_NAME, string wk_BRDTE, string wk_PTSEX, string wk_VSDPT, int wk_Page)
    {
        //功能：條碼標籤病歷基本資料列印
        //範例：Call 條碼標籤_病歷基本資料列印(病歷號, 患者姓名, 生日, 性別, 科別, 列印張數)
        //     Call 條碼標籤_病歷基本資料列印("0000000004", "測試患者", "097/01/01", "男", "急診小兒科", 1)

        string WkName = "";
        string WkBRDate = "";
        string Wkvsdpt = "";
        string WkPage = null;
        string OutPutStr = "";
        string WkCrtno = "";
        string WKSEX = "";
        string WkBarCode = "";

        string BarcodeTxt = "";
        string FileName = "";
        string Arguments = "";

        OutPutStr = "";
        WkBarCode = AddRightSpace(wk_crtno, 10);
        //條碼
        WkName = AddRightSpace(wk_NAME, 15);
        //姓名
        WkBRDate = AddRightSpace(wk_BRDTE, 10);
        //生日
        WKSEX = AddRightSpace(wk_PTSEX, 4);
        //性別
        WkCrtno = AddRightSpace(wk_crtno, 10);
        //病歷號
        Wkvsdpt = AddRightSpace(wk_VSDPT, 10);
        //科別
        WkPage = wk_Page.ToString().PadLeft(2, '0');
        //張數

        OutPutStr = WkBarCode + WkName + WkBRDate + WKSEX + WkCrtno + Wkvsdpt + WkPage;

        BarcodeTxt = "C:\\ZEBRA\\急診標籤\\barcode.TXT";
        FileName = "C:\\zebra\\急診標籤\\tranE.exe";
        Arguments = "C:\\zebra\\急診標籤\\zebra.ini " + BarcodeTxt;

        try
        {
            try
            {
                System.IO.File.Delete(BarcodeTxt);
            }
            catch (Exception)
            {
            }
            System.IO.File.WriteAllText(BarcodeTxt, OutPutStr, System.Text.Encoding.Default);
        }
        catch (Exception)
        {
        }

        System.Diagnostics.Process p = new System.Diagnostics.Process();
        p.StartInfo.UseShellExecute = false;
        p.StartInfo.CreateNoWindow = true;
        p.StartInfo.FileName = FileName;
        p.StartInfo.Arguments = Arguments;
        p.Start();

        //Interaction.Shell(WkPath, AppWinStyle.MinimizedNoFocus);

    }

    #region " Clear_0. "

    public static void Clear_Zero(ref DataTable Target_Tbl, string Column)
    {
        //將 DataTable 指定欄位資料為 0 的設定為 ""
        foreach (DataRow row in Target_Tbl.Rows)
        {
            try
            {
                if (row.IsNull(Column) == true)
                    row[Column] = "";
                else
                    row[Column] = (row[Column].ToString() == "0" ? "" : row[Column]);
            }
            catch (Exception)
            {
            }
        }
    }

    public static void Clear_Dot(ref DataTable Target_Tbl, string Column)
    {
        //將 DataTable 指定欄位資料在小數點以下為 0 的移除
        //把x.00清空為x
        //1.50->1.5
        //1.00->1
        //1.51->1.51
        foreach (DataRow row in Target_Tbl.Rows)
        {
            try
            {
                if (row.IsNull(Column) == true)
                    row[Column] = "";
                else
                    if (row[Column].ToString() == "0")
                        row[Column] = "";
                    else
                        row[Column] = KTMath.ROUND45(Convert.ToSingle(row[Column]), 2);
            }
            catch (Exception)
            {
            }
        }
    }

    #endregion

    public static string Change_ID(string ID)
    {
        //功能: 將病歷號or身分證字號轉碼加密
        string CharStr = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z";
        string Char1 = null;
        int PosOfChar1 = 0;
        int I = 0;
        string CHANGE = "";
        byte[] textbyte = null;
        int ID_LENGTH = 0;

        ID = ID.Trim();
        Char1 = ID.Substring(0, 1);
        PosOfChar1 = CharStr.IndexOf(Char1);

        switch (PosOfChar1)
        {
            case 0:
                //MsgBox("病")
                textbyte = System.Text.Encoding.ASCII.GetBytes(Convert.ToInt64(ID).ToString());
                ID_LENGTH = ExcludeDotNumIsZero(Convert.ToInt64(ID), "1").Length;
                break;

            default:
                //MsgBox("身")
                textbyte = System.Text.Encoding.ASCII.GetBytes(ID);
                ID_LENGTH = ID.Length;
                break;
        }

        for (I = 0; I <= (ID_LENGTH - 1); I++)
        {
            switch (textbyte[I])
            {
                case 48:
                    //0'
                    CHANGE = CHANGE + "H";
                    break;

                case 49:
                    //1'
                    CHANGE = CHANGE + "X";
                    break;

                case 50:
                    //2'
                    CHANGE = CHANGE + "B";
                    break;

                case 51:
                    //3'
                    CHANGE = CHANGE + "C";
                    break;

                case 52:
                    //4'
                    CHANGE = CHANGE + "A";
                    break;

                case 53:
                    //5'
                    CHANGE = CHANGE + "E";
                    break;

                case 54:
                    //6'
                    CHANGE = CHANGE + "W";
                    break;

                case 55:
                    //7'
                    CHANGE = CHANGE + "Y";
                    break;

                case 56:
                    //8'
                    CHANGE = CHANGE + "Z";
                    break;

                case 57:
                    //9'
                    CHANGE = CHANGE + "G";
                    break;

                default:
                    CHANGE = CHANGE + (Char)(textbyte[I]);
                    break;
            }
        }
        return CHANGE;
    }

    public static string Recover_ID(string ID)
    {
        //功能: 將病歷號or身分證字號轉碼回復為原來值
        string RECOVER = "";
        byte[] textbyte = null;
        int I = 0;

        textbyte = System.Text.Encoding.ASCII.GetBytes(ID);

        switch (ID.Length)
        {
            case 10:
                for (I = 0; I <= (ID.Length - 1); I++)
                {
                    if (I == 0)
                    {
                        RECOVER = RECOVER + (Char)textbyte[I];
                    }
                    else
                    {
                        switch (textbyte[I])
                        {
                            case 72:
                                //H'
                                RECOVER = RECOVER + "0";
                                break;

                            case 88:
                                //X'
                                RECOVER = RECOVER + "1";
                                break;

                            case 66:
                                //B'
                                RECOVER = RECOVER + "2";
                                break;

                            case 67:
                                //C'
                                RECOVER = RECOVER + "3";
                                break;

                            case 65:
                                //A'
                                RECOVER = RECOVER + "4";
                                break;

                            case 69:
                                //E'
                                RECOVER = RECOVER + "5";
                                break;

                            case 87:
                                //W'
                                RECOVER = RECOVER + "6";
                                break;

                            case 89:
                                //Y'
                                RECOVER = RECOVER + "7";
                                break;

                            case 90:
                                //Z'
                                RECOVER = RECOVER + "8";
                                break;

                            case 71:
                                //G'
                                RECOVER = RECOVER + "9";
                                break;
                        }
                    }
                }
                break;

            default:
                for (I = 0; I <= (ID.Length - 1); I++)
                {
                    switch (textbyte[I])
                    {
                        case 72:
                            //H'
                            RECOVER = RECOVER + "0";
                            break;

                        case 88:
                            //X'
                            RECOVER = RECOVER + "1";
                            break;

                        case 66:
                            //B'
                            RECOVER = RECOVER + "2";
                            break;

                        case 67:
                            //C'
                            RECOVER = RECOVER + "3";
                            break;

                        case 65:
                            //A'
                            RECOVER = RECOVER + "4";
                            break;

                        case 69:
                            //E'
                            RECOVER = RECOVER + "5";
                            break;

                        case 87:
                            //W'
                            RECOVER = RECOVER + "6";
                            break;

                        case 89:
                            //Y'
                            RECOVER = RECOVER + "7";
                            break;

                        case 90:
                            //Z'
                            RECOVER = RECOVER + "8";
                            break;

                        case 71:
                            //G'
                            RECOVER = RECOVER + "9";
                            break;
                    }
                }

                while (RECOVER.Length < 10)
                {
                    RECOVER = "0" + RECOVER;
                }
                break;
        }
        return RECOVER;
    }

    public static string 字串編碼轉換Big5(string SourceTxt)
    {
        string functionReturnValue = null;
        //SourceTxt 欲轉換之字串
        try
        {
            byte[] strBytes = null;
            // 宣告位元陣列 
            strBytes = System.Text.Encoding.Default.GetBytes(SourceTxt);
            // 將其轉成位元陣列
            functionReturnValue = System.Text.Encoding.GetEncoding(950).GetString(strBytes);
        }
        catch (Exception)
        {
            functionReturnValue = SourceTxt;
        }
        return functionReturnValue;
    }

    public static byte[] StringToByteArray(string InputStr)
    {
        //功能：將傳入之字串轉成ByteArray的方式
        //作者：林珀辰
        //日期：0991217
        //參數：InputStr --> 傳入之「字串」
        //回傳：經處理後之ByteAraay()
        //範例：StringToByteArray("ABC")  -->   (0): 65,(1): 66,(2): 67
        //ERROR: Not supported in C#: ReDimStatement

        System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
        return encoding.GetBytes(InputStr);
    }

    public static string ByteArrayToBase64(byte[] InputByte)
    {
        //功能：將傳入之ByteArray轉成成Base64編碼的方式
        //作者：林珀辰
        //日期：0991217
        //參數：InputByte --> 傳入之「字串Byte」
        //回傳：經處理後之Base64編碼字串
        //範例：ByteArrayToBase64(xxx())  -->  "QUJD"
        return Convert.ToBase64String(InputByte);
    }

    public static byte[] Base64ToByteArray(string InputBase64Str)
    {
        //功能：將傳入之Base64編碼字串轉成ByteArray
        //作者：林珀辰
        //日期：0991217
        //參數：InputBase64Str --> 傳入之「Base64字串」
        //回傳：經處理後之ByteArray
        //範例：Base64ToByteArray("QUJD")  -->    (0): 65,(1): 66,(2): 67
        return Convert.FromBase64String(InputBase64Str);
    }

    public static string ByteArrayToString(byte[] InputByte)
    {
        //功能：將傳入之ByteArray轉成String
        //作者：林珀辰
        //日期：0991217
        //參數：InputByte --> 傳入之「字串Byte」
        //回傳：經處理後之String
        //範例：ByteArrayToString(xxx())  -->  "ABC"
        System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
        return encoding.GetString(InputByte);
    }

    public static string StringToBase64(string InputStr)
    {
        return Convert.ToBase64String(new System.Text.UTF8Encoding().GetBytes(InputStr));
    }

    public static string Base64ToString(string url)
    {
        return new System.Text.ASCIIEncoding().GetString(Convert.FromBase64String(url));
    }

    //// 將憑證檔載入工作區。再做[資料驗章]與[RSA公鑰加密]之前須先做此動作。
    //// ERROR: Not supported in C#: DeclareDeclaration
    //public const int HCA_F_LoadCert = 10208;
    //    // 對資料驗章。
    //public const int HCA_F_VerifySignMessage = 10203;

    //public bool VerifySignature(byte[] 簽章本文, byte[] 簽章值, byte[] 憑證值, string SHA1值, string 簽章方法)
    //{
    //    //功能：將傳入之「簽章本文」「簽章值」「憑證值」，做電子病歷驗章
    //    //作者：林珀辰
    //    //日期：100/01/05
    //    //參數：
    //    //回傳：True 驗章成功，False 驗章失敗
    //    //範例：

    //    //簽章方法  1.PM0版本  2.W3C版本

    //    bool ReturnBoolean = false;
    //    switch (簽章方法) {
    //        case "1":
    //            //1.PM0版本
    //            int nErrCode = 0;
    //            nErrCode = HCA_GNFuncCall(HCA_F_LoadCert, 憑證值, ref 憑證值(0), 憑證值.Length, 0, 0, 0, 0);
    //            if (!(nErrCode == 0)) {
    //                ReturnBoolean = false;
    //            }

    //            nErrCode = HCA_GNFuncCall(HCA_F_VerifySignMessage, 簽章本文, ref 簽章值(0), 簽章本文.Length, 簽章值.Length, 0, 0, 0);
    //            if ((nErrCode == 0)) {
    //                ReturnBoolean = true;
    //            } else {
    //                ReturnBoolean = false;
    //            }
    //            break;
    //        case "2":
    //            // 2.W3C版本
    //            //使用微軟內建的取Sha1值
    //            byte[] Sha1Restlt = null;
    //            SHA1CryptoServiceProvider Sha1 = new SHA1CryptoServiceProvider();
    //            Sha1Restlt = Sha1.ComputeHash(簽章本文);

    //            if (SHA1值 == Convert.ToBase64String(Sha1Restlt)) {
    //                ReturnBoolean = true;
    //            } else {
    //                ReturnBoolean = false;
    //            }
    //            break;
    //    }

    //    return ReturnBoolean;

    //}

    //public const int HCA_F_TSVerify = 10210;

    //public const int HCA_F_GetTSInfo = 10213;
    //public bool Verify時戳(byte[] 時戳值, byte[] 簽章值)
    //{
    //    //取得時戳內的時間
    //    byte[] Value = new byte[1025];
    //    long ValueLen = 0;
    //    int nErrCode = 0;

    //    ValueLen = 1024;
    //    nErrCode = HCA_GNFuncCall(HCA_F_GetTSInfo, 時戳值, ref Value(0), 時戳值.Length, ValueLen, 3, 0, 0);

    //    if ((nErrCode >= 0 & nErrCode < 1024)) {
    //    } else {
    //        return false;
    //    }

    //    //驗時戳與簽章值是否正確
    //    nErrCode = HCA_GNFuncCall(HCA_F_TSVerify, 簽章值, ref 時戳值(0), 簽章值.Length, 時戳值.Length, 0, 0, 0);
    //    if ((nErrCode == 0)) {
    //        return true;
    //    } else {
    //        return false;
    //    }

    //    return true;
    //}

    ////開啟程式操作手冊        傳入員工編號、程式編號，開立EIP網頁式的操作手冊
    //public void 開啟程式操作手冊(string OPID, string PGMID, string gType = "1")
    //{
    //    switch (gType) {
    //        case "1":
    //            //個人 + 程式
    //            Process.Start("http://apclu1.ktgh.com.tw:7777/SUP/common/User/index_one.jsp?lgid=" + OPID + "&PGNAME=" + PGMID);
    //            break;
    //        case "2":
    //            //個人
    //            Process.Start("http://apclu1.ktgh.com.tw:7777/SUP/common/User/index.jsp?lgid=" + OPID);
    //            break;
    //        case "3":
    //            //單一程式
    //            Process.Start("http://apclu1.ktgh.com.tw:7777/SUP/common/User/index_pg.jsp?PGNAME=" + PGMID);
    //            break;
    //    }

    //}

    public static string 置換單引號(string InpS)
    {
        InpS = InpS + "";
        if (InpS.IndexOf("''") > -1)
            return InpS;
        return InpS.Replace("'", "''");
    }

    public static int Get_Words長度(string Expression)
    {
        //功能：將傳入之Expression回傳字串長度
        //作者：林珀辰
        //日期：1000329
        //參數：Expression --> 傳入之中英文字
        //回傳：經處理後之長度
        //範例：Get_Words長度("ABC") = 1    英文以Space算一字
        //範例：Get_Words長度("AB C") = 2   英文以Space算一字
        //範例：Get_Words長度("A B C") = 3  英文以Space算一字
        //範例：Get_Words長度("ABC 範例") = 3  中文一字算一字，英文以Space算一字
        string[] ReturnValue = new string[0];
        string[] Temp_Array = new string[0];
        string TempString = "";
        int i = 0;
        for (i = 0; i <= Expression.Length - 1; i++)
        {
            //英文
            if (LenB(Expression.Substring(i, 1)) == 1)
            {
                if (Expression.Substring(i, 1) == " ")
                {
                    TempString = TempString + "<SPC>";
                }
                else if (Expression.Substring(i, 1) == "\r")
                {
                    TempString = TempString + "<SPC>";
                }
                else if (Expression.Substring(i, 1) == "\n")
                {
                    TempString = TempString + "<SPC>";
                }
                else
                {
                    TempString = TempString + Expression.Substring(i, 1);
                }
                //中文
            }
            else
            {
                TempString = TempString + Expression.Substring(i, 1) + "<SPC>";
            }
        }

        Temp_Array = KTString.Split(TempString, "<SPC>");

        return Temp_Array.Length;
    }

    public static string ReplaceXMLSpecialChar(string Instr)
    {
        string tmpStr = Instr;

        tmpStr = tmpStr.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&apos;", "'").Replace("&quot;", "\"");
        tmpStr = tmpStr.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("'", "&apos;").Replace("\"", "&quot;");
        //'發生錯誤	2	運算式沒有產生值。	D:\VB2008\INO\INOR0003\2008\INOR0003\INOR0003\KTDB\VBNetRtn.vb	1746	50	INOR0003
        //tmpStr = Replace(Replace(Replace(Replace(Replace(tmpStr, "&amp;", "&"), "&lt;", "<"), "&gt;", ">"), "&apos;", "'"), "&quot;", """")
        //tmpStr = Replace(Replace(Replace(Replace(Replace(tmpStr, "&", "&amp;"), "<", "&lt;"), ">", "&gt;"), "'", "&apos;"), """", "&quot;")
        return tmpStr;
    }

    public static bool 產生台新刷卡機檔案(int Money)
    {
        //  欄位名稱   長度 位置 說明
        //1 Trans_Type 2    1    交易別
        //2 Reserve    2    3    保留
        //3 Reserve    6    5    保留
        //4 Reserve    19   11   保留
        //5 Reserve    4    30   保留
        //6            12   34   交易金額  Trans_Amount:包含兩位小數，但不包含小數點
        //
        //總長度 144

        try
        {
            string 傳入刷卡機字串 = "";
            if (Money > 0)
            {
                傳入刷卡機字串 = "11";
                //刷卡，正金額
                傳入刷卡機字串 = 傳入刷卡機字串 + Money.ToString().PadLeft(41, '0') + "00";
            }
            else
            {
                傳入刷卡機字串 = "02";
                //刷退，負金額
                傳入刷卡機字串 = 傳入刷卡機字串 + (Money * -1).ToString().PadLeft(41, '0') + "00";
            }
            傳入刷卡機字串 = 傳入刷卡機字串 + AddRightSpace(" ", 99);
            //
            傳入刷卡機字串 = 傳入刷卡機字串.Replace(" ", "0");

            string ECR_IN = "C:\\ecr_TS\\in.dat";
            if (System.IO.File.Exists(ECR_IN) == true)
            {
                System.IO.File.Delete(ECR_IN);
            }

            string ECR_OUT = "C:\\ecr_TS\\OUT.dat";
            if (System.IO.File.Exists(ECR_OUT) == true)
            {
                System.IO.File.Delete(ECR_OUT);
            }

            System.IO.FileStream Target_File = new System.IO.FileStream(ECR_IN, System.IO.FileMode.CreateNew);
            System.IO.StreamWriter Target_Writer_File = new System.IO.StreamWriter(Target_File, System.Text.Encoding.Default);
            Target_Writer_File.Write(傳入刷卡機字串);
            Target_Writer_File.Close();
            Target_File.Close();
        }
        catch (Exception ex)
        {
            throw new System.Exception(ex.Message);
        }

        return true;
    }

    public static bool 取得台新刷卡機回傳結果(ref string 卡號, ref string 簽單序號)
    {
        string ECR_OUT = "C:\\ecr_TS\\OUT.DAT";
        if (System.IO.File.Exists(ECR_OUT) == false)
        {
            return false;
        }

        System.IO.FileStream FS = new System.IO.FileStream(ECR_OUT, System.IO.FileMode.Open);
        System.IO.StreamReader SR = new System.IO.StreamReader(FS, System.Text.Encoding.Default);
        string[] allLine = null;
        allLine = SR.ReadToEnd().Split(Convert.ToChar(KTConstant.vbCrLf));
        SR.Close();
        FS.Close();

        卡號 = allLine[0].Substring(10, 19).Trim();
        簽單序號 = allLine[0].Substring(57, 9).Trim();

        if (string.IsNullOrEmpty(卡號) | string.IsNullOrEmpty(簽單序號))
        {
            return false;
        }

        return true;
    }


    // ERROR: Not supported in C#: DeclareDeclaration
    public static bool 產生合庫刷卡機檔案(int Money)
    {
        //  欄位名稱   長度 位置 說明
        //1 Trans_Type 2    1    交易別
        //2 Reserve    2    3    保留
        //3 Reserve    6    5    保留
        //4 Reserve    19   11   保留
        //5 Reserve    4    30   保留
        //6            12   34   交易金額  Trans_Amount:包含兩位小數，但不包含小數點
        //
        //總長度 144

        try
        {
            string 傳入刷卡機字串 = "";
            if (Money > 0)
            {
                傳入刷卡機字串 = "01".ToString().PadRight(33, ' ');
                //刷卡，正金額
                傳入刷卡機字串 = 傳入刷卡機字串 + Money.ToString().PadLeft(10, '0') + "00";
            }
            else
            {
                傳入刷卡機字串 = "02".ToString().PadRight(33, ' ');
                //刷退，負金額
                傳入刷卡機字串 = 傳入刷卡機字串 + (Money * -1).ToString().PadLeft(10, '0') + "00";
            }

            string ECR_IN = "C:\\ecr\\in.dat";
            if (System.IO.File.Exists(ECR_IN) == true)
            {
                System.IO.File.Delete(ECR_IN);
            }

            string ECR_OUT = "C:\\ecr\\OUT.dat";
            if (System.IO.File.Exists(ECR_OUT) == true)
            {
                System.IO.File.Delete(ECR_OUT);
            }

            System.IO.FileStream Target_File = new System.IO.FileStream(ECR_IN, System.IO.FileMode.CreateNew);
            System.IO.StreamWriter Target_Writer_File = new System.IO.StreamWriter(Target_File, System.Text.Encoding.Default);
            Target_Writer_File.Write(傳入刷卡機字串);
            Target_Writer_File.Close();
            Target_File.Close();
        }
        catch (Exception ex)
        {
            throw new System.Exception(ex.Message);
        }

        return true;
    }

    public static bool 取得合庫刷卡機回傳結果(ref string 卡號, ref string 簽單序號)
    {
        string ECR_OUT = "C:\\ecr\\OUT.DAT";
        if (System.IO.File.Exists(ECR_OUT) == false)
        {
            return false;
        }

        System.IO.FileStream FS = new System.IO.FileStream(ECR_OUT, System.IO.FileMode.Open);
        System.IO.StreamReader SR = new System.IO.StreamReader(FS, System.Text.Encoding.Default);
        string[] allLine = null;
        allLine = SR.ReadToEnd().Split(Convert.ToChar(KTConstant.vbCrLf));
        SR.Close();
        FS.Close();

        卡號 = allLine[0].Substring(10, 19).Trim();
        簽單序號 = allLine[0].Substring(57, 9).Trim();

        if (string.IsNullOrEmpty(卡號) | string.IsNullOrEmpty(簽單序號))
        {
            return false;
        }

        return true;
    }

    public static string AddZero(string StrTemp, int ILong)
    {
        //StrTemp 輸入字串
        //ILong   輸出長度
        //EX. AddZero(3,2) >> output: 03

        StrTemp = StrTemp.Trim();
        StrTemp = StrTemp.PadLeft(ILong, '0');

        //for (int j = 0; j <= ILong - StrTemp.Length; j++) {
        //    StrTemp = "0" + StrTemp;
        //}

        return StrTemp;
    }

    public static string errException(Exception ex, bool showMessage = false)
    {
        //功能：取得例外的錯誤處理程序內容
        //作者：吳居易
        //日期：0950204
        //參數：ex          --> 某個 處理程序上的 Try Catch Error 事件
        //     showMessage --> 是否直接顯示錯誤訊息
        //回傳：ex 處理後的 String 資料值
        //說明：第一行所出現的錯誤行數，是 Try Catch 接收到執行錯誤的地方
        //     越往下是代表執行錯誤之處的原始呼叫來源 Function
        //範例：1.Messagebox.Show(errException(ex)) --> 由外部呼叫程式 Show Message
        //     2.Call errException(ex, True)       --> 由 Function Show Message

        string errMessage = "";
        string[] errArray = null;
        string[] nowLine = null;
        string[] DotItem = null;
        string errLine = "";
        Int16 errx = 0;

        errArray = KTString.Split(ex.StackTrace, " at ");
        if (errArray.Length == 1)
            errArray = KTString.Split(ex.StackTrace, " 於 ");

        //最後面的錯誤是一開始的呼叫位置
        errMessage = "程式片段：" + KTConstant.vbCrLf;
        for (errx = 1; errx <= errArray.Length - 1; errx++)
        {
            nowLine = errArray[errx].Split(new string[] { " in " }, StringSplitOptions.None);
            if (nowLine.Length > 1)
            {
                DotItem = nowLine[0].Split('.');
                if (string.IsNullOrEmpty(errLine))
                    errLine = DotItem[DotItem.Length - 1] + " --> 第 " + errArray[errx].Split(new string[] { "line" }, StringSplitOptions.None)[1].Trim() + " 行" + KTConstant.vbCrLf;
                errMessage = errMessage + (errx).ToString().Trim() + "." + DotItem[DotItem.Length - 1] + KTString.Space(1) + nowLine[1].Trim() + KTConstant.vbCrLf;
            }
            else
            {
                errMessage = errMessage + (errx).ToString().Trim() + "." + nowLine[0].Trim() + KTConstant.vbCrLf;
            }

        }
        errMessage = errMessage + KTConstant.vbCrLf;

        errMessage = errMessage + errLine;
        errMessage = errMessage + ex.Message + KTConstant.vbCrLf + KTConstant.vbCrLf;
        //if (showMessage)
        //    MessageBox.Show(errMessage, ex.Source);
        return errMessage;
    }
}

class KTVerify
{
    //資料驗証
    //           IdNoOk               檢查身分証號(含居留証號)是否正確
    //           手動輸入病歷號碼檢查 檢查病歷號是否正確
    //           CreditCardOK         檢查輸入的信用卡號是否正確
    //           Replace_PTNAME       將患者姓名中間的字「圈」起來
    public static bool IDNoOk(string sIDNo)
    {
        //功能 : 檢查國民身分證號，外僑及大陸人士在台居留証，旅行證統一編號是否正確
        //參數 : sIDNO 身分証號
        //Author:L.C.C 94/03/25

        //1010224: 居易，修改成 .Net 寫法
        //         身分證號: A~Z 跑 10000 次回圈，以前的寫法需要 10 秒，目前此方式只要 5 秒
        //         居留證號: A~Z 與 A~Z 組合，跑 100 次回圈，以前的寫法需要 27 秒，目前此方式只要 14 秒

        try
        {
            sIDNo = sIDNo.ToUpper().Trim();

            //排除不小心輸入空白身分証
            if (!string.IsNullOrEmpty(sIDNo))
                sIDNo = sIDNo.Replace(" ", "");

            //不足十碼視為錯誤
            if (sIDNo.Length != 10)
                return false;

            //3 To 10 不可以是文字
            if (!KTString.IsNumeric(sIDNo.Substring(2)))
                return false;

            string[] EngCharAry = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z".Split(',');
            string[] EngNumAry = "10,11,12,13,14,15,16,17,34,18,19,20,21,22,35,23,24,25,26,27,28,29,32,30,31,33".Split(',');
            int[] chkNumAry = { 1, 8, 7, 6, 5, 4, 3, 2, 1 };

            //Char1 身分証號第一碼
            //NumberOfChar1 身分証號第一碼對應的數字
            string Char1 = null;
            string NumberOfChar1 = null;

            //Char2 身分証號第二碼
            //NumberOfChar2 身分証號第二碼對應的數字
            string Char2 = null;
            string NumberOfChar2 = null;

            int PosOfChar1 = 0;
            int PosOfChar2 = 0;

            Char1 = sIDNo.Substring(0, 1);
            PosOfChar1 = Array.IndexOf(EngCharAry, Char1);

            //第一碼不是英文字母
            if (PosOfChar1 == -1)
                return false;

            int[] Digt = new int[9];
            NumberOfChar1 = EngNumAry[PosOfChar1];
            //取得第一碼對應的數字
            //身分証第一碼對應數字的個位數乘以 9 再加十位數所得之合除10取餘數
            Digt[0] = (Convert.ToInt16(NumberOfChar1.Substring(1, 1)) * 9 + Convert.ToInt16(NumberOfChar1.Substring(0, 1))) % 10;

            Char2 = sIDNo.Substring(1, 1);
            PosOfChar2 = Array.IndexOf(EngCharAry, Char2);
            if (PosOfChar2 == -1)
            {
                Digt[1] = Convert.ToInt16(Char2);
                //第二碼是數字　
            }
            else
            {
                //外僑及大陸人士在台居留証，旅行證統一編號第二碼為字母
                //找出其對應的數字並取個位數
                NumberOfChar2 = EngNumAry[PosOfChar2];
                //取得第二碼對應的數字
                Digt[1] = Convert.ToInt16(NumberOfChar2.Substring(1, 1));
                //取第二碼對應數字的個位數
            }

            int Sum = 0;
            for (int x = 2; x <= Digt.Length - 1; x++)
            {
                Digt[x] = Convert.ToInt16(sIDNo.Substring(x, 1));
                //將身分証的3-9碼搬到DIGT()
            }

            //將DIGT() 0 TO 8 碼和ChkNum　對應的碼相乘，digt(0) 乘chkNum的第一碼
            for (int x = 0; x <= Digt.Length - 1; x++)
            {
                Sum = Sum + Digt[x] * chkNumAry[x];
            }

            int ChkSum = (10 - (Sum % 10)) % 10;
            //取餘數的補數
            if (ChkSum == Convert.ToInt32(sIDNo.Substring(sIDNo.Length - 1, 1)))
            {
                return true;
                //餘數的補數和身分証第十碼相等則為正確
            }
            else
            {
                return false;
                //餘數的補數和身分証第十碼不相等則不正確
            }

        }
        catch (Exception ex)
        {
            throw new System.Exception("檢查身分証是否正確時發生錯誤" + KTConstant.vbCrLf + ex.Message);
        }

    }

    public static string 手動輸入病歷號碼檢查(string crtno)
    {
        crtno = crtno.Trim();
        if (string.IsNullOrEmpty(crtno))
            return "";

        ulong i = default(ulong);
        bool result = ulong.TryParse(crtno, out i);
        if (result)
        {
            if (System.Text.UTF8Encoding.UTF8.GetBytes(crtno.ToCharArray()).Length > 10)
                throw new System.Exception("病歷號碼檢查" + KTConstant.vbCrLf + KTConstant.vbCrLf + crtno + " : 你輸入的病歷號碼大於 10 個字--無法判斷!!" + KTConstant.vbCrLf + KTConstant.vbCrLf + "請重新輸入");
        }
        else
        {
            throw new System.Exception("病歷號碼檢查" + KTConstant.vbCrLf + KTConstant.vbCrLf + crtno + " : 病歷號碼格式錯誤--只能是數字!!!" + KTConstant.vbCrLf + KTConstant.vbCrLf + "請重新輸入");
        }

        return crtno.PadLeft(10, '0');
    }

    public static bool CreditCardOK(string CreditCardNumber)
    {
        //功能：驗證輸入的信用卡號是否正確
        //參數：CreditCardNumber，請輸入16碼數字信用卡號
        //規則：【由右至左（共 1~ 16 ），奇數位乘上 1 ，偶數位乘上 2 ，共得出 16 個[新數字]，
        //     再將每個[新數字]的十位數加上個位數，再產生[新數字]；
        //     共16個[新數字]把所有16個[新數字]合計加總，能整除10者，為正確卡號。】。
        //輸出：True OR False
        //範例：MSGBOX( CreditCardOK("4563****0493****") ) (中間的星號*是因為不想讓你們看到全部的卡號，測試時，請用自已的信用卡來測)

        //長度小於16碼，則不是信用卡號
        if (CreditCardNumber.Length < 16)
            return false;

        //非數字類，則不是信用卡號
        if (KTString.IsNumeric(CreditCardNumber) == false)
        {
            return false;
        }

        int[] Int = new int[16];

        int num = 0;
        int sun = 0;
        for (int x = 0; x <= 15; x++)
        {
            num = Convert.ToInt32(CreditCardNumber.Substring(x, 1));
            //偶數乘1，奇數乘2 
            if ((x + 1) % 2 != 0)
            {
                Int[x] = num * 2;
            }
            else
            {
                Int[x] = num;
            }
        }

        for (int x = 0; x <= 15; x++)
        {
            if (Int[x] > 9)
            {
                Int[x] = (Int[x] % 10) + 1;
            }
            sun += Int[x];
        }

        if ((sun % 10 != 0))
            return false;

        return true;
    }

    public static void CHK_HIGH_FORMAT(string VALUE_S, string UNIT)
    {
        //功能：檢查身高值是否在合理範圍內,若資料允許空值,請在有數值時再透過此函式進行檢查
        //參數：VALUE_S-輸入值,UNIT-單位值

        VALUE_S = VALUE_S.Trim();
        if (string.IsNullOrEmpty(VALUE_S))
            return;

        if (KTString.IsNumeric(VALUE_S))
            throw new System.Exception("身高值非數字，請重新確認!!");

        if (VALUE_S.Split('.').Length > 1)
            if (string.IsNullOrEmpty(VALUE_S.Split('.')[1]))
                throw new System.Exception("身高數據 小數點後未填值，請重新確認!!");

        double InVal = Convert.ToDouble(VALUE_S);
        if (InVal < 0)
            throw new System.Exception("此身高值不可為負數，請重新確認!!");

        switch (UNIT.ToUpper())
        {
            case "CM":
            case "公分":
                if (InVal < 20 | InVal > 300)
                    throw new System.Exception("身高值未在合理範圍內，請重新確認!!");
                break;

            case "M":
            case "公尺":
                if (InVal < 0.2 | InVal > 3)
                    throw new System.Exception("身高值未在合理範圍內，請重新確認!!");
                break;

            default:
                throw new System.Exception("身高輸入的單位有問題，請重新確認!!");
        }
    }

    public static void CHK_WEIGHT_FORMAT(string VALUE_S, string UNIT, string STNID = "")
    {
        //功能：檢查體重值是否在合理範圍內,若資料允許空值,請在有數值時再透過此函式進行檢查
        //參數：VALUE_S-輸入值,UNIT-單位值

        VALUE_S = VALUE_S.Trim();
        if (string.IsNullOrEmpty(VALUE_S))
            return;

        if (KTString.IsNumeric(VALUE_S))
            throw new System.Exception("體重值非數字，請重新確認!!");

        if (VALUE_S.Split('.').Length > 1)
            if (string.IsNullOrEmpty(VALUE_S.Split('.')[1]))
                throw new System.Exception("體重數據 小數點後未填值，請重新確認!!");

        double InVal = Convert.ToDouble(VALUE_S);
        if (InVal < 0)
            throw new System.Exception("此體重值不可為負數，請重新確認!!");

        switch (UNIT.ToUpper())
        {
            case "G":
            case "克":
                if (STNID == "002D")
                    if (InVal < 200 | InVal > 300000)       //PS. 105/11/11產房反應會到200G, 下限改為200G
                        throw new System.Exception("體重值未在合理範圍內，請重新確認!!");
                    else
                        if (InVal < 500 | InVal > 300000)   //PS. 產房規定500以上有VS需KEY病歷 105/11/11產房反應會到200G, 下限改為200G
                            throw new System.Exception("體重值未在合理範圍內，請重新確認!!");
                break;

            case "KG":
            case "公斤":
                if (InVal < 1 | InVal > 300)
                    throw new System.Exception("體重值未在合理範圍內，請重新確認!!");
                break;

            default:
                throw new System.Exception("體重輸入的單位有問題，請重新確認!!");
        }
    }

    public static bool BMI_HWB(float H, float W, ref float b, ref string BMISay)
    {
        //H     ：身高公分
        //W     ：體重公斤
        //B     ：計算出的 BMI 分數
        //BMISay：理想體重範圍說明
        //Return：True 計算成功 False 計算失敗

        b = 0;
        BMISay = "";
        if (W == 0 | H == 0)
            return false;

        float height = 0;
        int Length = 0;
        if (H.ToString().Split('.')[0].Length > 1)
        {
            height = H / 100;
        }

        b = W / (height * height);
        Length = b.ToString().Length;
        if (Length > 4)
            Length = 4;

        b = Convert.ToSingle(b.ToString().Substring(0, Length));

        if (b != 0)
        {
            if (b < 18.5)
                BMISay = "體重過輕 BMI < 18.5";
            else if (b >= 18.5 & b <= 23.9)
                BMISay = "正常範圍 18.5 ≦ BMI ＜24";
            else if (b >= 24 & b <= 26.9)
                BMISay = "稍　　重 24 ≦ BMI ＜27";
            else if (b >= 27 & b <= 29.9)
                BMISay = "輕度肥胖 27 ≦ BMI ＜30";
            else if (b >= 30 & b <= 34.9)
                BMISay = "中度肥胖 30 ≦ BMI ＜35";
            else
                BMISay = "重度肥胖 BMI ≧35";
        }
        return true;
    }

    public static string Replace_PTNAME(string PTNAME)
    {
        System.Text.RegularExpressions.Regex CharPattern =
    new System.Text.RegularExpressions.Regex("^[a-zA-Z.]+");

        string tmpPTName = "";

        //英文姓名
        if (CharPattern.IsMatch(PTNAME.Substring(0, 1)))
            tmpPTName = PTNAME.Split(' ')[0];
        else
            switch (PTNAME.Length)
            {
                case 1:
                    //1090714 因遇到一個字的患者，請示珀辰，一律直接顯示
                    tmpPTName = PTNAME;
                    break;
                case 4:
                    tmpPTName = PTNAME.Substring(0, 2) + "○" + PTNAME.Substring(3, 1);
                    break;

                default:
                    tmpPTName = PTNAME.Substring(0, 1) + "○" + PTNAME.Substring(2);
                    break;
            }

        return tmpPTName;
    }

    public static string Replace_IDNO(string IDNO_STR, int sPoint, int StrLen = 3)
    {
        if (string.IsNullOrEmpty(IDNO_STR))
        {
            return "";
        }

        string tmpIDNO = "";
        string tmpMARK = "";
        //Create "＊"_Mark String
        for (int x = 0; x <= StrLen - 1; x++)
            tmpMARK += "*";

        tmpIDNO = IDNO_STR.Substring(0, sPoint) + tmpMARK + IDNO_STR.Substring((sPoint + StrLen));

        return tmpIDNO;
    }

    public static string Replace_PTTEL(string PTTEL)
    {
        PTTEL = PTTEL.Trim();

        int telLength = PTTEL.Length;
        if (telLength == 0)
            return "";

        if (telLength > 3)
            return PTTEL.Substring(0, telLength - 3) + "＊＊＊";
        else if (telLength <= 3)
            return PTTEL.Substring(0, telLength - 1) + "＊";

        return "";
    }
}

class KTMath
{
    //數學處理   
    //           ROUND45             四捨五入
    //           RmDot_GetIntBas     小數點拾去無條件進位 
    //           小數點無條件捨去    　　小數點無條件捨去

    //數字處理   
    //           CMONEY              丟數字，回傳中文金額(Input:12345  Ouput:壹萬貳仟參佰肆拾伍元 整)
    //           CAge                都入日期取得年齡

    public static double ROUND45(double dNum, int iDigit)
    {
        //功能：四捨五入
        //輸入：dNum    要四捨五入的數字
        //     iDigit  要取到小數第幾位，0 -> 取到整數，小數以下一位四捨五入
        //                           1 -> 取到小數一位，小數第二位四拾五入
        //回傳:四拾五入後的數值

        double dNum1 = 0;
        double dNum2 = 0;
        double M1 = 0;
        M1 = Math.Pow(10, iDigit);
        dNum1 = (int)(dNum * M1 + 0.5);
        dNum2 = dNum1 / M1;
        return dNum2;
    }

    public static double RmDot_GetIntBas(double InNumber)
    {
        //功能:將小數點拾去無條件進位
        //輸入:InNumber    要無條件進位的數字
        //回傳:無條件進位後的數值

        bool NegFlg = false;

        if (InNumber == 0)
            return 0;

        if (InNumber < 0)
            NegFlg = true;

        if (NegFlg)
            InNumber = 0 - InNumber;

        if (InNumber < 1)
            InNumber = 1;

        //int number = 0;
        //bool result = Int32.TryParse(InNumber.ToString(), out number);

        double number = Math.Floor(InNumber);

        if (InNumber != number)
            InNumber = number + 1;

        if (NegFlg)
            InNumber = 0 - InNumber;

        return InNumber;
    }

    public static double 小數點無條件捨去(double InNumber, int iDigit)
    {
        //功能：將小數點無條件捨去
        //輸入：InNumber     要無條件捨去的數字
        //      iDigit      第幾位後無條件捨去，2 -> 取到小數第2位，小數第三位捨去
        //回傳：無條件捨去後的數值

        for (int i = 1; i <= iDigit; i++)
        {
            InNumber = InNumber * 10;
        }

        InNumber = Math.Floor(InNumber);

        for (int i = 1; i <= iDigit; i++)
        {
            InNumber = InNumber / 10;
        }

        return InNumber;

    }

    public static string CMONEY(long input_money)
    {
        //作者 居易
        //6543 2109 8765 4321
        //  兆   億   萬   元
        //10000001 --> 壹千萬零壹元整                 這才是正確的格式
        //             壹仟零佰零拾萬零仟零佰零拾壹   這不是標準的格式

        if (input_money.ToString().Length > 16)
            throw new System.Exception("金額的費用太長，須修改程式才能解決");

        if (input_money == 0)
            return "零元 整";

        //有增加位數再說
        int[] thousand = new int[4];

        string tmp_Money = input_money.ToString();
        string Tmp_Str = "";
        bool haveMoeny = false;     //檢查前面是否有數字

        double int_Money = 0;

        for (int x = 0; x <= thousand.Length - 1; x++)
        {
            thousand[x] = 0;
            if (tmp_Money.Length > 4)
            {
                thousand[x] = Convert.ToInt16(tmp_Money.Substring(tmp_Money.Length - 4, 4));
                tmp_Money = tmp_Money.Substring(0, tmp_Money.Length - 4);
            }
            else
            {
                if (tmp_Money != "")
                {
                    thousand[x] = Convert.ToInt16(tmp_Money);
                    tmp_Money = "";
                    break;
                }
            }
        }

        for (int x = thousand.Length - 1; x >= 0; x += -1)
        {
            if (thousand[x] != 0)
            {
                int_Money = thousand[x] % 10000;
                //int_Money = Conversion.Fix(thousand[x] / 1000);
                int_Money = 小數點無條件捨去(int_Money / 1000, 0);

                if (int_Money != 0)
                {
                    //如果 Tmp_Str <> "" 且前面的金額階級都是 0
                    if (Tmp_Str.IndexOf(MoenyLevel(x)) == -1)
                    {
                        if (haveMoeny == false & !string.IsNullOrEmpty(Tmp_Str))
                            Tmp_Str = Tmp_Str + "零";
                    }
                    Tmp_Str = Tmp_Str + match_number((int)int_Money) + "仟";
                    haveMoeny = true;
                }
                else
                {
                    haveMoeny = false;
                }

                int_Money = thousand[x] % 1000;
                //int_Money = Conversion.Fix(int_Money / 100);
                int_Money = 小數點無條件捨去(int_Money / 100, 0);
                if (int_Money != 0)
                {
                    if (haveMoeny == false & !string.IsNullOrEmpty(Tmp_Str))
                        Tmp_Str = Tmp_Str + "零";
                    Tmp_Str = Tmp_Str + match_number((int)int_Money) + "佰";
                    haveMoeny = true;
                }
                else
                {
                    haveMoeny = false;
                }

                int_Money = thousand[x] % 100;
                //int_Money = Conversion.Fix(int_Money / 10);
                int_Money = 小數點無條件捨去(int_Money / 10, 0);
                if (int_Money != 0)
                {
                    if (haveMoeny == false & !string.IsNullOrEmpty(Tmp_Str))
                        Tmp_Str = Tmp_Str + "零";
                    Tmp_Str = Tmp_Str + match_number((int)int_Money) + "拾";
                    haveMoeny = true;
                }
                else
                {
                    haveMoeny = false;
                }

                int_Money = thousand[x] % 10;
                //int_Money = Conversion.Fix(int_Money);
                int_Money = 小數點無條件捨去(int_Money, 0);
                if (int_Money != 0)
                {
                    if (haveMoeny == false & !string.IsNullOrEmpty(Tmp_Str))
                        Tmp_Str = Tmp_Str + "零";
                    Tmp_Str = Tmp_Str + match_number((int)int_Money);
                    haveMoeny = true;
                }
                else
                {
                    haveMoeny = false;
                }

                //如果最後一個中文字不是上次金額階級，就需要加上這次的金額階級
                if (Tmp_Str.Substring(Tmp_Str.Length - 1) != MoenyLevel(x))
                {
                    Tmp_Str = Tmp_Str + MoenyLevel(x);
                }
            }
        }
        return Tmp_Str + "元 整";

    }

    private static string MoenyLevel(int level)
    {
        //把這裡當作是一個 金額的階級
        switch (level)
        {
            case 0:
                //千
                return "";

            case 1:
                //千萬
                return "萬";

            case 2:
                //千億
                return "億";

            case 3:
                //千兆
                return "兆";
        }
        return "";
    }

    public static string match_number(int input_code)
    {
        switch (input_code)
        {
            case 0:
                return "零";

            case 1:
                return "壹";

            case 2:
                return "貳";

            case 3:
                return "參";

            case 4:
                return "肆";

            case 5:
                return "伍";

            case 6:
                return "陸";

            case 7:
                return "柒";

            case 8:
                return "捌";

            case 9:
                return "玖";
        }
        return "";
    }

    public static int CAge(string Brdte, string Mode = "Y", string wkDate = "", bool hundred = false)
    {
        //參數：Brdte    --> 生日
        //     Mode     --> 計算模式
        //                  Y 表示計算到年
        //                  M 計算到月
        //                  D 計算到日
        //     wkDate   --> 計算的日期
        //     hundred  --> 對年減年 大於100歲的時候, 以日計算
        //回傳：CAge 年齡的數字
        //備註：如果 wkDate = "" 以生日與今天的日期比對
        //     若 wkDate <> "" 以生日與 wkDate 的日期比對

        if (Convert.IsDBNull(Brdte) | string.IsNullOrEmpty(Brdte))
            return 0;

        if (!KTDateTime.IsCDate(Brdte))
            throw new System.Exception("生日的日期格式不正確，" + Brdte + " 錯誤，無法執行 CAge");

        if (string.IsNullOrEmpty(wkDate))
            wkDate = KTDateTime.Get_Now_Day("C");

        wkDate = wkDate.Replace("/", "");
        Brdte = Brdte.Replace("/", "");

        //使用 西元年來相減 是最正確的
        System.DateTime sDate = KTDateTime.CToWDate(Brdte);
        System.DateTime eDate = KTDateTime.CToWDate(wkDate);

        int RealAge = Math.Abs(sDate.Year - eDate.Year);

        //0960420 居易
        //當生日比 001 年還要小的時候. 是直接跳到 -01 年, 所以生日沒有 00 年
        //
        //因此當生日為負的時候，若要計算到年，需要再額外減 1，因為沒有 00 年
        //
        //若是精準的計算到 月 日，無論生日是否為負數, 
        //需判斷今天的日期與生日的日期相減是否小於 0
        //*** 小於 0 表示未滿的那一歲，需要將那一歲扣除 *** 

        //0960420 居易
        //惠姿 - 需求單
        //當年滿一百歲時，要以實際天數來計算是否真的滿一百歲
        if (hundred)
            if (RealAge == 100)
                Mode = "D";

        switch (Mode.ToUpper())
        {
            case "Y":
                break;

            case "M":
                if (Convert.ToInt32(wkDate.Substring(3, 2)) - Convert.ToInt32(Brdte.Substring(3, 2)) < 0)
                    RealAge = RealAge - 1;
                break;

            case "D":
                if (Convert.ToInt32(wkDate.Substring(3, 4)) - Convert.ToInt32(Brdte.Substring(3, 4)) < 0)
                    RealAge = RealAge - 1;
                break;
        }

        return RealAge;
    }

}

class KTEnvs
{
    //電腦主機環境 KTEnvironment
    //           GetLocationInChinese    回傳院區中文名稱　Ex.沙鹿院區,大甲院區
    //           getOtherHost            回傳對應的他院區主機 KTGH00 --> HPK210 Or HPK210 --> KTGH00
    //           Get_OS_Version          取得目前執行此程式的作業系統版本
    //           Get_HostsFile           取得 Hosts 檔案路徑
    //           Get_HostsName           取得 當地電腦 Hosts 所定義的主機名稱
    //           Get_IPAddress           取得指定 HostName(LocalHost) 的 IPAddress 
    //           GetLocal_ComputerName   取得本機的電腦名稱 
    //           PrevInstance            檢查程式是否重複執行，若有重複第二次執行的程式將自動結束
    //           SingleInstance          檢查程式是否重複執行，若有重複第二次執行的程式將自動結束
    //           KillProcess             以執行檔名稱-Kill 執行中的應用程式
    //           SaTaT1Ok                判斷沙鹿大甲的T1狀況是否正常
    //           HISProgramer            取得 HIS主機 上的程式負責人姓名
    //           Set_Form_Caption        設定每隻程式的表頭都一樣
    //           getNumHostCode          取得由數字傳入以取得 當時的主機代碼
    //           getAppProgramPath       取得程式本身執行的所在路徑
    //           getAppProgramName       取得程式本身的程式代碼
    //           讀取院區電話號碼           讀取院區電話號碼
    //           產生SYS021檔案LOG        產生SYS021的執行記錄
    //           Get_Stkcod              取得電腦庫別

    #region " 傳送錯誤訊息至指定IP位置"
    public static string ListenerAddress = "";
    public static void Error_Log(string Error_String)
    {
        if ((Error_String == "") | (ListenerAddress == ""))
        { return; }

        try
        {
            Send_Msg(Error_String);
        }
        catch (Exception)
        {
        }
    }

    private static void Send_Msg(string Error_String)
    {
        ThreadClient tThreadClient = new ThreadClient();
        tThreadClient.Error_String = Error_String;
        tThreadClient.ListenerAddress = ListenerAddress;
        System.Threading.Thread ActiveThreadStart = new System.Threading.Thread(new System.Threading.ThreadStart(tThreadClient.StartClient));
        ActiveThreadStart.Start();
    }

    private class ThreadClient
    {
        public string Error_String = "";
        public string ListenerAddress = "";
        public void StartClient()
        {
            try
            {
                System.Threading.Thread NewThread = default(System.Threading.Thread);
                System.Net.Sockets.TcpClient Client = default(System.Net.Sockets.TcpClient);
                byte[] Buffer = null;
                string Temp = null;

                NewThread = System.Threading.Thread.CurrentThread;
                Client = new System.Net.Sockets.TcpClient();
                try
                {
                    Client.Connect(ListenerAddress, 9105);
                }
                catch
                {
                    goto Close;
                }

                Temp = Environment.MachineName + " " + KTEnvs.Get_IPAddress() + ":" + System.DateTime.Now + " " + Error_String;
                Buffer = System.Text.Encoding.Default.GetBytes(Temp.ToCharArray());
                Client.GetStream().Write(Buffer, 0, Buffer.Length);
            Close:
                Client.Close();
            }
            catch (Exception)
            {
            }
        }
    }
    #endregion

    public static string GetLocationInChinese(string hospital, string Lang = "big5")
    {
        //功能：不透過資料庫，直接由 Oracle.ini 回傳目前預設連線院區的中文名稱

        if (string.IsNullOrEmpty(hospital))
            hospital = KTDB.KTConnectionControler.OracleHostDSN;

        //如果 參數是空的 抓預設連線院區
        if (hospital.IndexOf("目前") != -1)
            hospital = KTDB.KTConnectionControler.OracleHostDSN;

        Lang = Lang.ToLower();
        if (hospital == "1" | hospital == "KTGH00" | hospital == "SAKTHIS" | hospital.IndexOf("沙鹿") != -1)
            if (Lang == "en")
                hospital = "Sha-Lu Branch";
            else
                hospital = "沙鹿院區";
        else if (hospital == "2" | hospital == "HPK210" | hospital == "NTSERTA" | hospital.IndexOf("大甲") != -1)
            if (Lang == "en")
                hospital = "Da-Jia Branch";
            else
                hospital = "大甲院區";
        else if (hospital == "3" | hospital == "TSHIS" | hospital.IndexOf("通霄") != -1)
            if (Lang == "en")
                hospital = "Tong-Siao Branch";
            else
                hospital = "通霄院區";
        else if (hospital == "5" | hospital == "KTXS00" | hospital.IndexOf("向上") != -1)
            if (Lang == "en")
                hospital = "Xiang-Shang Branch";
            else
                hospital = "向上院區";
        else if (hospital == "O" | hospital == "OLTPSRV" | hospital.IndexOf("EIP") != -1)
            if (Lang == "en")
                hospital = "EIP Server";
            else
                hospital = "EIP 主機";
        else if (hospital == "KTGH03" | hospital.IndexOf("測試") != -1)
            if (Lang == "en")
                hospital = "TEST Branch";
            else
                hospital = "測試院區";

        //如果是連線到備份環境  SAKTHIS 或 ntserta，將無法知道是哪個院區
        return hospital;

    }

    public static string getNumHostCode(string Host = "")
    {
        return KTDB.KTConnectionControler.getNumHostCode(Host);
    }

    public static string getOtherHost(string Host = "")
    {
        return KTDB.KTConnectionControler.getOtherHost(Host);
    }

    public static string[] getOtherHosts(string Host = "")
    {
        return KTDB.KTConnectionControler.getOtherHosts(Host);
    }

    public static string[] getTheSameHosts(string Host = "")
    {
        return KTDB.KTConnectionControler.getTheSameHosts(Host);
    }

    public static string[] getOtherLabHosts(string Host = "")
    {
        return KTDB.KTConnectionControler.getOtherLabHosts(Host);
    }

    public static string[] getTheSameLabHosts(string Host = "")
    {
        return KTDB.KTConnectionControler.getTheSameLabHosts(Host);
    }

    public static string getBackUpHost(string DBLink)
    {
        //功能：取得目前院區的備份主機
        //日期：1040325
        //備註：備份主機尚未定義代碼為何 !
        DBLink = DBLink.ToUpper();
        switch (DBLink)
        {
            case "1":
            case "KTGH00":
            case "SAKTHIS":
                DBLink = "SAKTHIS";
                break;

            case "2":
            case "HPK210":
            case "NTSERTA":
                DBLink = "NTSERTA";
                break;

            case "3":
            case "TSHIS":
                DBLink = "TSHIS";
                break;

            default:
                throw new System.Exception(DBLink + " 不是定義的備份主機位置");
            //Interaction.MsgBox(DBLink + " 不是定義的備份主機位置");
            //break;
        }

        return DBLink;

    }

    public static string Get_OS_Version()
    {
        //功能：取得目前執行此程式的作業系統版本
        //作者：吳居易
        //日期：094/08/04
        //說明：
        //參數：無
        //傳回：get_OS_Version 字串
        //版本：網路版
        //範例：MessageBox.Show(get_OS_Version)

        string OS = "";
        System.OperatingSystem osInfo = default(System.OperatingSystem);
        osInfo = System.Environment.OSVersion;

        var _with1 = osInfo;
        switch (_with1.Platform)
        {
            case PlatformID.Win32Windows:
                switch ((_with1.Version.Minor))
                {
                    case 0:
                        OS = "Win95";
                        break;

                    case 10:
                        if (_with1.Version.Revision.ToString() == "2222A")
                        {
                            //Win98 Second Edition
                            OS = "Win98 SE";
                        }
                        else
                        {
                            OS = "Win98";
                        }
                        break;

                    case 90:
                        OS = "WinMe";
                        break;
                }
                break;

            case PlatformID.Win32NT:
                switch ((_with1.Version.Major))
                {
                    case 3:
                        OS = "WinNT351";
                        break;

                    case 4:
                        OS = "WinNT4";
                        break;

                    case 5:
                        if (_with1.Version.Minor == 0)
                        {
                            OS = "Win2K";
                        }
                        else
                        {
                            OS = "WinXP";
                        }
                        break;

                    case 6:
                        OS = "Win7";
                        break;
                }
                break;

            default:
                OS = "Failed";
                break;
        }
        return OS;
    }

    //public static string Get_HostsFile()
    //{
    //    //功能：取得 Hosts 檔案路徑
    //    //作者：吳居易
    //    //日期：094/08/04
    //    //說明：
    //    //參數：無
    //    //傳回：Get_HostsFile 字串
    //    //版本：
    //    //範例：MessageBox.Show(Get_HostsFile)
    //    //備註：參考 get_OS_Version

    //    string sVersion = Get_OS_Version();
    //    if (sVersion == "Win95" | sVersion == "Win98" | sVersion == "Win98 SE")
    //            sVersion = "C:\\Windows\\Hosts";
    //    else if (sVersion == "Win2K" | sVersion == "WinXP")
    //        if (!string.IsNullOrEmpty(System.IO.FileSystem.Dir("c:\\winnt\\system32\\drivers\\etc\\hosts", System.IO.FileAttribute.Directory))) {
    //            sVersion = "c:\\winnt\\system32\\drivers\\etc\\hosts";
    //        } else {
    //            sVersion = "c:\\Windows\\system32\\drivers\\etc\\hosts";
    //        }
    //    return sVersion;
    //}

    //public static string[] Get_HostsName()
    //{
    //    //功能：取得 當地電腦 Hosts 所定義的主機名稱
    //    //作者：吳居易
    //    //日期：094/08/04
    //    //說明：
    //    //參數：
    //    //傳回：一組 Array 字串 
    //    //版本：
    //    //範例：Dim myHosts() as String = Get_HostsName
    //    //備註：參考 Get_HostsFile

    //    System.IO.FileStream FS = default(System.IO.FileStream);
    //    System.IO.StreamReader SR = default(System.IO.StreamReader);
    //    bool ReadContinue = true;
    //    string ReadLine = null;
    //    int ReadLine_i = 0;
    //    string[] HostsAry = new string[0];
    //    int HostsAry_i = 0;

    //    FS = new System.IO.FileStream(Get_HostsFile(), System.IO.FileMode.Open);
    //    SR = new System.IO.StreamReader(FS);
    //    while (ReadContinue) {
    //        ReadLine = SR.ReadLine;
    //        if (ReadLine == null) {
    //            ReadContinue = false;
    //        } else {
    //            ReadContinue = true;
    //            if (ReadLine.IndexOf("#") == -1) {
    //                //前面數字，後面英文
    //                ReadLine = ReadLine.Trim;
    //                ReadLine_i = Strings.InStrRev(ReadLine, Strings.Chr(9));
    //                if (ReadLine_i == 0)
    //                    ReadLine_i = Strings.InStrRev(ReadLine, " ");
    //                if (ReadLine_i != 0) {
    //                    HostsAry_i = HostsAry.Length;
    //                    Array.Resize(ref HostsAry, HostsAry_i + 1);
    //                    HostsAry(HostsAry_i) = ReadLine.Substring(ReadLine_i, ReadLine.Length - ReadLine_i);
    //                }
    //            }
    //        }
    //    }
    //    SR.Close();
    //    FS.Close();
    //    return HostsAry;
    //}

    //public static bool 讀取Icd10基準日(string ICD10類別, ref string Icd10_Date, CnKey = "", KTDBProvider sKTDB = null)
    //{
    //    bool functionReturnValue = false;

    //    //CODE:
    //    //ICD10_INP   住院ICD-10開始實施日
    //    //ICD_INPINS  住院申報ICD-10開始實施日
    //    //ICD_OPD     門診前線ICD-10開始實施日
    //    //ICD_OPDINS  門診申報ICD-10開始實施日
    //    //ICD_OPR     手術ICD-10開始實施日

    //    DataTable DtaTbl = new DataTable();
    //    dynamic Sql_Str = null;

    //    functionReturnValue = false;

    //    Sql_Str = " SELECT CNAME FROM PUB002";
    //    Sql_Str = Sql_Str + " WHERE DTATYP='0A' ";
    //    Sql_Str = Sql_Str + " AND CODE='" + ICD10類別 + "' ";
    //    KTCC.DoSql(Sql_Str, DtaTbl, CnKey, sKTDB);
    //    if (KTCC.DoSqlOK == false) {
    //        Interaction.MsgBox("讀取ICD10實施日期有問題");
    //        return false;
    //    }

    //    if (KTCC.Rowindicator == 0)
    //        return false;

    //    Icd10_Date = DtaTbl.Rows(0).Item("CNAME");

    //    return true;
    //    return functionReturnValue;
    //    //讀取Icd10基準日 = True
    //}

    public static string Get_Proc_IPAddress()
    {
        //功能：取得本機的 IPAddress 
        //作者：吳居易
        //日期：094/08/23
        //說明：一定會回傳一個 Ip Address，除非安裝數據機或沒有裝網路卡
        //     此錯誤尚無法得之其錯誤訊息
        //傳回：IP Address(正常) 或 空白(失敗)
        //參考：cls_WakeOnLan.vb
        try
        {
            string dirResults = "";
            System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            psi.FileName = "ipconfig";
            psi.RedirectStandardInput = false;
            psi.RedirectStandardOutput = true;
            psi.UseShellExecute = false;
            proc = System.Diagnostics.Process.Start(psi);

            int x = -1;

            string[] IPAddress = new string[0];

            while (!(x > -1))
            {

                if (!string.IsNullOrEmpty(dirResults))
                {
                    x = dirResults.Trim().ToLower().IndexOf("ip address", 0);
                    if (x > -1)
                    {
                        dirResults = dirResults.Replace(" ", "");
                        IPAddress = dirResults.Split(':');
                        if (IPAddress.Length > 1)
                        {
                            if (IPAddress[1] == "0.0.0.0") { }
                            else if (IPAddress[1].Substring(0, 6) == "0.1.0.") { }
                            else break;  //Exit while
                        }
                    }
                }
                dirResults = proc.StandardOutput.ReadLine().ToString();
            }
            proc.WaitForExit();

            proc.Dispose();
            switch (IPAddress.Length)
            {
                case 0:
                case 1:
                    return "";

                default:
                    return IPAddress[1].Trim();
            }

        }
        catch (Exception ex)
        {
            throw new System.Exception(ex.Message + ex.StackTrace);
            //Interaction.MsgBox(err.Message + err.StackTrace);
        }
    }

    public static string Get_Stkcod()
    {
        //功能：取得電腦的庫別
        //作者：陳鎮揮
        //日期：109/10/02
        //      有 STNID 回傳 STNID 
        //      有 STKCOD 回傳 STKCOD
        string STKCOD_="";
        string STNID_="";
        string ini_06bz = "C:\\ap06bz\\exe\\06bz.ini";

        if (!System.IO.File.Exists(ini_06bz))    return "";

        string[] allLine = System.IO.File.ReadAllLines(ini_06bz);

        for (int x = 0; x <= allLine.Length - 1; x++)
        {
            if (!string.IsNullOrEmpty(allLine[x].ToString()))
            {
                if (allLine[x].ToUpper().IndexOf("STKCOD") > -1)
                    STKCOD_ = allLine[x].Split(Convert.ToChar('='))[1].ToUpper();                    
                else if (allLine[x].ToUpper().IndexOf("STNID") > -1)
                    STNID_ = allLine[x].Split(Convert.ToChar('='))[1].ToUpper();

            }
        }

        //以前的護理站樓層都是 STKCOD 
        //是之後才有增加一個 STNID
        if (string.IsNullOrEmpty(STNID_))            return STKCOD_;

        return STNID_;


    }

    public static string Get_IPAddress(string HostName = "")
    {
        //功能：取得指定 HostName(LocalHost) 的 IPAddress
        //作者：吳居易
        //日期：095/01/17
        //說明：以 HostName 來回傳一個 Ip Address，除非 HostName 隨便亂指定
        //傳回：IP Address(正常) 或 空白(失敗)
        //備註：失敗的話會 Load 很久喔
        //     若為 InterNet 的某個網址 Ex: www.ktgh.com.tw
        //     務必設置 Dns Server 的 IP 否則會發生錯誤

        int i = 0;
        string ip = "";
        try
        {
            if (string.IsNullOrEmpty(HostName))
                HostName = System.Net.Dns.GetHostName();

            System.Net.IPHostEntry ipE = default(System.Net.IPHostEntry);
            ipE = System.Net.Dns.GetHostEntry(HostName);

            for (i = 0; i <= ipE.AddressList.Length - 1; i++)
            {
                if (ipE.AddressList[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    ip = ipE.AddressList[i].ToString();
                    if (ip == "0.0.0.0") { }
                    else if (ip.Substring(0, 6) == "0.1.0.") { }
                    else break;
                }
            }

            return ip;
        }
        catch (Exception)
        {
            //失敗時, 使用 ipconfig 來處理
            return Get_Proc_IPAddress();
        }
    }

#if !noDapper
    //1081019 居易
    //        故意在這裡特別使用 noDapper 條件式編譯符號的原因
    //        不想用 Dapper 的，一定不是寫網頁
    //        就算是寫網頁，既然會使用 Dapper 為何不能再多引用一個 System.Web 呢 ? 
    public static string getClinetIPAddress(System.Web.HttpRequestBase request)
    {
        //功能：取得本機的 IPAddress 
        //作者：吳居易
        //日期：107/03/16
        //範例：KTEnvs.getClinetIPAddress(HttpContext.Request);
        //     傳送 page 或 Controler 當下的 HttpContext.Request 給此 Function
        try
        {
            string szRemoteAddr = request.ServerVariables["REMOTE_ADDR"];
            string szXForwardedFor = request.ServerVariables["X_FORWARDED_FOR"];
            string szIP = "";

            if (szXForwardedFor == null)
            {
                szIP = szRemoteAddr;
            }
            else
            {
                szIP = szXForwardedFor;

                if (szIP.IndexOf(",") > 0)
                {
                    string[] arIPs = szIP.Split(',');

                    foreach (string item in arIPs)
                    {
                        //if (!isPrivateIP(item))
                        //{
                        return item;
                        //}
                    }
                }
            }
            return szIP;
            //return request.ServerVariables["REMOTE_ADDR"];
        }
        catch (Exception ex)
        {
            throw new System.Exception(ex.Message + ex.StackTrace);
            //Interaction.MsgBox(err.Message + err.StackTrace);
        }
    }
#endif

    public static string GetLocal_ComputerName()
    {
        //功能：取得本機的 電腦編號
        //傳回：V1PC063 , V2PC154        
        return KTString.AddRightSpace(Environment.MachineName, 20).Trim();
    }

    //public static void SingleInstance(bool showMessage = true, string programName = "", bool programExit = true)
    //{
    //    //功能：檢查程式是否重複執行，若有重複第二次執行的程式將自動結束
    //    //作者：吳居易
    //    //日期：095/12/20
    //    //參數：showMessage --> 如果有重複執行是否要提醒  ，預設為 True
    //    //     programName --> 提醒重複執行程式的中文名稱，預設為現在的執行程式檔名稱
    //    //     programExit --> 當程式重複執行時是否離開  ，預設為離開
    //    //說明：主要是以當時的執行檔名稱來處理 ktgh.exe
    //    //範例：Call SingleInstance
    //    //備註：如果要確定是否有重複執行的程式，直接 Call SingleInstance
    //    //     其他請看參數
    //    //注意：1.若將 ktgh.exe 複製一份並且改名稱為 ktgh2.exe 則表示程式沒有重複執行
    //    //       第一次執行的時候需要一秒，背景執行的程式應該沒差
    //    //     2.若 ProgramName 傳入的是 Application.ProductName
    //    //       如論如何修改檔名，第二次啟動的程式仍然可以被結束
    //    //     3.Class 裡面, 不能夠使用 Application.Exit 或 End
    //    if (string.IsNullOrEmpty(programName)) {
    //        string[] exeFileArray = Application.ExecutablePath.Split("\\");
    //        string exeFile = exeFileArray(exeFileArray.Length - 1).Split(".")(0);
    //        programName = exeFile;
    //    }

    //    System.Threading.Mutex AppInstance = new System.Threading.Mutex(false, programName);
    //    // Check for previous existance
    //    if (AppInstance.WaitOne(0, false)) {
    //    } else {
    //        // Free-up mutex
    //        if (showMessage)
    //            MessageBox.Show(programName + " 程式已經執行");
    //        if (programExit) {
    //            AppInstance.Close();
    //            Application.Exit();
    //        }
    //    }
    //}

    //public static void PrevInstance(bool showMessage = true, string programName = "", bool programExit = true, bool KillFirstProgram = false, string CHECK_TYPE = "")
    //{
    //    //功能：檢查程式是否重複執行，若有重複第二次執行的程式將自動結束
    //    //作者：吳居易
    //    //日期：094/11/12
    //    //參數：showMessage --> 如果有重複執行是否要提醒  ，預設為 True
    //    //     programName --> 提醒重複執行程式的中文名稱，預設為現在的執行程式檔名稱
    //    //     programExit --> 當程式重複執行時是否離開  ，預設為離開
    //    //     CHECK_TYPE  --> 檢查的類別，預設為""，傳入"A"只有早上0800到下午1730才執行
    //    //說明：主要是以當時的執行檔名稱來處理 ktgh.exe 
    //    //範例：Call PrevInstance
    //    //備註：如果要確定是否有重複執行的程式，直接 Call PrevInstance
    //    //     其他請看參數
    //    //注意：1.若將 ktgh.exe 複製一份並且改名稱為 ktgh2.exe 則表示程式沒有重複執行
    //    //       第一次執行的時候需要一秒，背景執行的程式應該沒差
    //    //     2.若 ProgramName 傳入的是 Application.ProductName
    //    //       如論如何修改檔名，第二次啟動的程式仍然可以被結數
    //    //Application.ExecutablePath 程式完整的檔案路徑
    //    string[] exeFileArray = Application.ExecutablePath.Split("\\");
    //    string exeFile = exeFileArray(exeFileArray.Length - 1).Split(".")(0);
    //    Process[] allProcesses = null;
    //    int x = 0;
    //    int BeforePrc = -1;

    //    //傳入"A"只有早上0800到下午1730才執行
    //    if (CHECK_TYPE == "A") {
    //        if (Get_Now_Time("C") < "0800")
    //            return;
    //        if (Get_Now_Time("C") > "1730")
    //            return;
    //    }

    //    try {
    //        allProcesses = System.Diagnostics.Process.GetProcessesByName(exeFile);
    //        //大於1表示重複執行了
    //        if (allProcesses.Length > 1) {
    //            for (x = 0; x <= allProcesses.Length - 1; x++) {
    //                if (exeFile == allProcesses(x).ProcessName) {
    //                    if (BeforePrc == -1) {
    //                        BeforePrc = x;
    //                    } else {
    //                        //時間比較大的要結束掉
    //                        if (string.IsNullOrEmpty(programName))
    //                            programName = allProcesses(x).ProcessName;

    //                        if (showMessage)
    //                            MessageBox.Show(programName + " 程式已經執行");
    //                        if (KillFirstProgram == false) {
    //                            if (allProcesses(BeforePrc).StartTime > allProcesses(x).StartTime) {
    //                                if (programExit)
    //                                    allProcesses(BeforePrc).Kill();
    //                            } else {
    //                                if (programExit)
    //                                    allProcesses(x).Kill();
    //                            }
    //                        } else if (KillFirstProgram == true) {
    //                            if (allProcesses(BeforePrc).StartTime <= allProcesses(x).StartTime) {
    //                                if (programExit)
    //                                    allProcesses(BeforePrc).Kill();
    //                            } else {
    //                                if (programExit)
    //                                    allProcesses(x).Kill();
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    } catch (Exception ex) {
    //        //0960710 我的電腦 Process 居然掛了 - 居易
    //    }

    //}

    public static bool KillProcess(string fileName = "")
    {
        //功能：以 執行檔名稱-Kill 執行中的應用程式
        //作者：吳居易
        //日期：095/01/11
        //參數：fileName --> 執行檔名稱，副檔名(*.exe)可有可無，因為不會用到
        //     showMessage --> True:提示訊息，False 不提示訊息，預設為 False
        //                     若不提示訊息 showMessage = False，會 kill 所有相同的 fileName
        //                     若使用了提示訊息，除非點選 Yes 否則不會刪除該執行程序
        //     programName --> 提醒重複執行程式的中文名稱，預設為 fileName 執行程式檔名稱
        //
        //回傳：KillProcess 會回傳 True:已經Kill 或 False:尚未Kill(不可能發生)
        //
        //說明：主要是以傳入的 fileName 執行檔名稱來處理
        //範例：KillProcess("06bz.exe")   --> 會直接將 06bz.exe kill，不會有任何提示
        //     KillProcess("06bz", True) --> 會提示是否要將 06bz.exe kill
        //備註：最慢 0.4 秒，該程式就會不見
        //重點：在 XP 的環境上測試，無法刪除 VB3 程式
        //     所以 06bz.exe 無法被 kill，上面的範例只是舉例參考
        //     因為會被 ntvdm.exe 掛載成為其附屬的子程式
        //     可以執行某支 VB3 程式，再按 Ctrl + Alt + Del 自己試看看

        string[] exefiles = fileName.Split('.');

        //要砍掉開發程式產生的執行程式 *.vshost.exe，必須把最後面的 .exe 刪除
        if (exefiles[exefiles.Length - 1].ToUpper() == "EXE")
            Array.Resize(ref exefiles, exefiles.Length - 1);

        string exeFile = String.Join(".", exefiles);

        if (string.IsNullOrEmpty(exeFile))
        {
            //string[] exeFileArray = Application.ExecutablePath.Split("\\");
            //exeFile = exeFileArray(exeFileArray.Length - 1).Split(".")(0);
            exeFile = KTEnvs.getAppProgramName();
        }


        System.Diagnostics.Process[] allProcesses = null;

        bool KillFlag = true;

        try
        {
            allProcesses = System.Diagnostics.Process.GetProcessesByName(exeFile);

            //有東西就會砍
            for (int x = 0; x <= allProcesses.Length - 1; x++)
            {
                if (exeFile.ToUpper() == allProcesses[x].ProcessName.ToUpper())
                {
                    //False 不顯示提示訊息
                    //if (showMessage) {
                    //    if (string.IsNullOrEmpty(programName))
                    //        programName = fileName;
                    //    errMessage = programName + " 執行程式正在執行" + KTConstants.vbCrLf;
                    //    errMessage = errMessage + "請確定是否刪除";
                    //    KillFlag = true;
                    //    if (MessageBox.Show(errMessage, "Kill_ExeFile", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    //        KillFlag = false;
                    //}
                    if (KillFlag == true)
                    {
                        System.Diagnostics.Process p = allProcesses[x];
                        p.Kill();
                        while (true)
                        {
                            try
                            {
                                System.Diagnostics.Process.GetProcessById(p.Id);
                            }
                            catch (Exception)
                            {
                                break;
                            }
                        }
                    }
                }
            }

            //可以由外部程式自己判斷是否真的已經刪除
            //最好的方式是，程式結束之後 Delay 0.5 或 1 秒
            //再判斷程式是否真的結束，這樣才比較正確
            allProcesses = System.Diagnostics.Process.GetProcessesByName(exeFile);
            if (allProcesses.Length > 0)
                return false;

        }
        catch (Exception)
        {
        }

        return true;
        //正常情況會是 True

    }

    //public static string getAppProgramPath()
    //{
    //    //功能：取得程式本身執行的所在路徑
    //    string ApplicationPath = Application.ExecutablePath;
    //    string ProgramName = Application.ExecutablePath.Split("\\")(Application.ExecutablePath.Split("\\").Length - 1);
    //    ApplicationPath = ApplicationPath.Replace(ProgramName, "");
    //    return ApplicationPath;
    //}

    public static string getAppProgramName()
    {
        //功能：取得程式本身的程式代碼
        //System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName
        string ProgramName = System.Diagnostics.Process.GetCurrentProcess().ProcessName.Split('.')[0];
        ProgramName = ProgramName.Trim().ToUpper();
        return ProgramName;
    }

    public static string 讀取院區電話號碼(string Hospital, string TelType)
    {
        string TELPHONE = "";
        switch (TelType)
        {
            case "1":
                //代表線
                switch (Hospital)
                {
                    case "1":
                    case "KTGH00":
                    case "KTGH03":
                        TELPHONE = "04-26625111";
                        break;

                    case "2":
                    case "HPK210":
                        TELPHONE = "04-26885599";
                        break;

                    case "3":
                    case "TSHIS":
                        TELPHONE = "037-759999";
                        break;
                }
                break;

            case "2":
                //電腦語音掛號
                switch (Hospital)
                {
                    case "1":
                    case "KTGH00":
                    case "KTGH03":
                        TELPHONE = "04-26655461";
                        break;

                    case "2":
                    case "HPK210":
                        TELPHONE = "04-26887799";
                        break;

                    case "3":
                    case "TSHIS":
                        TELPHONE = "037-759999";
                        break;
                }
                break;

            case "3":
                //人工預約
                switch (Hospital)
                {
                    case "1":
                    case "KTGH00":
                    case "KTGH03":
                        //TELPHONE = "04-26632161"    '全部都改在大甲院區人工預約
                        TELPHONE = "04-26886699";
                        break;

                    case "2":
                    case "HPK210":
                        TELPHONE = "04-26886699";
                        break;

                    case "3":
                    case "TSHIS":
                        TELPHONE = "037-759999";
                        break;
                }
                break;
        }

        return TELPHONE;
    }

    public static void 產生SYS021檔案LOG(string PGMID, string RUNAREA)
    {
        string FileNme = null;
        FileNme = "SYS021" + KTDateTime.HHMMSS() + new Random().Next(1, 10000);
        FileNme = "C:\\ap06bz\\log\\" + FileNme + ".log";

        string contents = "PGMID=" + PGMID + KTConstant.vbCrLf + "RUNAREA=" + RUNAREA;
        System.IO.File.WriteAllText(FileNme, contents, System.Text.Encoding.Default);

        System.Diagnostics.Process.Start("C:\\ap06bz\\exe\\sysp1300.exe", FileNme);
    }


    public static string 轉換院區名稱TO代碼(string HspNme)
    {
        string functionReturnValue = null;

        //陳鎮揮 105/09/16
        functionReturnValue = "ERROR";

        switch (HspNme)
        {
            case "1":
            case "KTGH00":
            case "KTGH03":
                functionReturnValue = "1";
                break;

            case "2":
            case "HPK210":
                functionReturnValue = "2";
                break;

            case "3":
            case "TSHIS":
                functionReturnValue = "3";
                break;

            case "5":
            case "KTXS00":
                functionReturnValue = "5";
                break;
        }
        return functionReturnValue;
    }
}

class KTDateTime
{
    //日期/時間處理
    //  
    //           AddDays             回傳指定的民國日期加(減)天數所得到的民國日期
    //           AddMonths           回傳指定的民國日期加(減)月數所得到的民國日期
    //           AddYears            回傳指定的民國日期加(減)年數所得到的民國日期
    //           AddHours            回傳指定的民國日期時間加(減)小時數所得到的民國日期時間
    //           AddMinutes          回傳指定的民國日期時間加(減)分鐘數所得到的民國日期時間
    //           IsCDate             檢查民國日期是否合理 yyymmdd、yyy/mm/dd、yymmdd、yy/mm/dd
    //           isCTime             檢查時間格式是否合理 hhmmss、hh:mm:ss、hhmm、hh:mm
    //           ShowDate            將民國年月日以/分開 回傳 yyy/mm/dd    See Also DateValue
    //           DateValue           回傳日期的值  yyy/mm/dd 回傳 yyymmdd  See Also ShowDate
    //           CToWDate            將民國日期年轉成西元日期 回傳 yyyy/mm/dd  See also WToCDate
    //           WToCDate            將西元日期轉成民國日期 回傳yyymmdd  See also CToWDate
    //           Today               回傳當日民國日期(yyymmdd)  See Also HHMMSS
    //           HHMMSS              回傳目前的時間 HHMMSS or HH?MM?SS (Overloads) See Also Today
    //           CWeekDay            輸入民國日期回傳星期幾1(Monday)、2、3、...、7(Sunday) 和西曆星期日為1不一樣
    //           CWeekDayC           將星期幾轉為中文 1 -> 一，2 -> 二....,7 -> 日
    //           Show_Date_Diff      輸入開始日，結束日，回傳兩時間差x年x月x日
    //           Duration_By_Second  以秒計算時間差 (OverLoads)
    //           Delay               延遲秒數
    //           SetTime             和主機對時
    //           Get_Now_Day         當日民國年日期 Today、DateValue、ShowDate，已經使用習慣的問題
    //           Change_Date_Mode    轉換民國年的日期格式 yyymmdd <--> yyy/mm/dd，已經使用習慣的問題
    //           Change_Time_Mode    轉換時間輸入的格式  hhmm <--> hh:mm、hhmmss <--> hh:mm:ss
    //           C_M_DateType        將民國年轉換成 Mysql 的西元年格式
    //           Show_Date_Diff      輸入開始日，結束日，回傳兩時間差x年x月x日
    //           DateTime24Hour      將西元日期時間的 [上午] [下午] 中文拿掉，且年月日格式轉換為 yyyy/mm/dd ，下午 + 12
    //           DiffDays            輸入兩民國日期，計算指定的日期型態，回傳指定日期型態的相差數據
    //           DiffDaysWithTimes   輸入兩民國日期、分鐘，回傳指定日期型態的相差數據
    //           ChkIn_OutObjDate    顯示不正確的日期格式, 並修改原件的背景顏色
    //           getMonth_MaxDays    取得指定 [民國年月] 或 [西元年月] 的最大日天數
    //           WeekOfMonth         輸入民國日期回傳當月份第幾週
    //           IsLastWeekOfDay     輸入民國日期回傳是否為當月的最後一週星期4,5,6(用在人事的報備支援「最後一週」的定義)
    //           IsLastWeekOfMonth   輸入民國日期回傳是否為當月的最後一週
    //           Compute_Month       輸入兩民國日期，回傳相差月數

    public static string AddDays(string BaseDate, long Days)
    {
        //功能:將指定的民國日期加(減)指定的天數,回傳加減後的日期
        //參數:BaseDate 民國日期
        //    Days     要加減的天數
        if (!IsCDate(BaseDate))
            throw new System.Exception("民國日期" + BaseDate + "錯誤，無法執行AddDays");

        BaseDate = BaseDate.Replace("/", "");

        System.DateTime WkDate = CToWDate(BaseDate);
        WkDate = WkDate.AddDays(Days);
        return WToCDate(WkDate);
    }

    public static string AddMonths(string BaseDate, int Months)
    {
        //功能:將指定的民國日期加(減)指定的月數,回傳加減後的日期
        //參數:BaseDate 民國日期
        //    Months   要加減的月數

        if (!IsCDate(BaseDate))
            throw new System.Exception("民國日期" + BaseDate + "錯誤，無法執行AddMonths");

        BaseDate = BaseDate.Replace("/", "");

        System.DateTime WkDate = CToWDate(BaseDate);
        WkDate = WkDate.AddMonths(Months);
        return WToCDate(WkDate);
    }

    public static string AddYears(string BaseDate, int Years)
    {
        //功能:將指定的民國日期加(減)指定的年數,回傳加減後的日期
        //參數:BaseDate 民國日期
        //    Years    要加減的年數

        if (!IsCDate(BaseDate))
            throw new System.Exception("民國日期" + BaseDate + "錯誤，無法執行AddYears");

        BaseDate = BaseDate.Replace("/", "");

        System.DateTime WkDate = CToWDate(BaseDate);
        WkDate = WkDate.AddYears(Years);
        return WToCDate(WkDate);
    }

    public static string AddHours(string BaseDate, string BaseTime, long Hours)
    {
        //功能：將指定的民國日期加(減)指定的小時數,回傳加減後的日期
        //日期：0950606
        //參數：BaseDate   民國日期  0950606
        //     BaseTime   民國時間  111020
        //     Hours      要加減的小時
        //回傳：0950606 171020 ，請自行回傳後再拆解
        //說明：Vsdte請傳入民國日期 : 0950606, 0950101, 0951231
        //     Vstme請傳入民國時間 : 081523, 160548, 232924
        //範例：AddHours("0950606", "111020", 6) --> 0950606 171020
        //     AddHours("0950606", "201020", 6) --> 0950607 021020  (超出今日時，會自動跨天)
        //     AddHours("0950606", "201020", -6) --> 0950606 141020
        //     AddHours("0950607", "031020", -6) --> 0950606 211020  (少於今日時，會自動減天)

        if (BaseTime.Length == 4)
            BaseTime = BaseTime + "00";   //時間補滿六位數

        if (!IsCDate(BaseDate))
            throw new System.Exception("民國日期 " + BaseDate + " 錯誤，無法執行 AddHours()");

        if (!IsCTime(BaseTime))
            throw new System.Exception("民國時間 " + BaseTime + " 錯誤，無法執行 AddHours()");

        BaseDate = BaseDate.Replace("/", "");

        System.DateTime DateTime_Base = Convert.ToDateTime(CToWDate(BaseDate).ToString("yyyy/MM/dd") + " " + Change_Time_Mode(BaseTime, "P"));
        string DateTime_Str = DateTime24Hour(DateTime_Base.AddHours(Hours).ToString(), "/").ToUpper();

        return WToCDate(Convert.ToDateTime(DateTime_Str.Split(' ')[0])) + " " + Change_Time_Mode(DateTime_Str.Split(' ')[1], "C");
    }

    public static string AddMinutes(string BaseDate, string BaseTime, long Minutes)
    {
        //功能：將指定的民國日期加(減)指定的分鐘數，回傳加減後的日期
        //日期：0961106
        //參數：BaseDate   民國日期  0950606
        //     BaseTime   民國時間  111020
        //     Minutes    要加減的分鐘數
        //回傳：0950606 171020 ，請自行回傳後再拆解
        //說明：BaseDate 請傳入民國日期 :0950606、0950101、0951231
        //     BaseTime 請傳入民國時間 :081523 、160548 、232924
        //範例：AddMinutes("0961106","134300","30") --> 0961106 141300
        //     AddMinutes("0961106","234300","30") --> 0961107 001300  (超出今日時，會自動跨天)
        //     AddMinutes("0961106","234300","-30") --> 0961106 231300
        //     AddMinutes("0961106","001300","-30") --> 0961105 234300  (少於今日時，會自動減天)

        if (BaseTime.Length == 4)
            BaseTime = BaseTime + "00";   //時間補滿六位數

        if (!IsCDate(BaseDate))
            throw new System.Exception("民國日期 " + BaseDate + " 錯誤，無法執行 AddMinutes()");

        if (!IsCTime(BaseTime))
            throw new System.Exception("民國時間 " + BaseTime + " 錯誤，無法執行 AddMinutes()");

        System.DateTime DateTime_Base = Convert.ToDateTime(CToWDate(BaseDate).ToString("yyyy/MM/dd") + " " + Change_Time_Mode(BaseTime, "P"));
        string DateTime_Str = DateTime24Hour(DateTime_Base.AddMinutes(Minutes).ToString(), "/").ToUpper();

        return WToCDate(Convert.ToDateTime(DateTime_Str.Split(' ')[0])) + " " + Change_Time_Mode(DateTime_Str.Split(' ')[1], "C");
    }

    public static bool IsCDate(string sDate)
    {
        //功能：檢查民國日期是否合理
        //參數：sDate 民國日期
        //回傳：合理的日期回傳 True，不合理的日期回傳False
        //說明：sDate 可能的格式 yyy/mm/dd、yyymmdd、yy/mm/dd、yymmdd

        try
        {
            Convert.ToInt64(sDate.Replace("/", ""));
            //有非數字的符號近來會產生 Exception
        }
        catch (Exception)
        {
            return false;
        }

        string yyy = null;
        string mm = null;
        string dd = null;

        switch (sDate.Length)
        {
            case 6:
                //yymmdd
                if (sDate.IndexOf("/") > -1)
                    return false;

                yyy = sDate.Substring(0, 2);
                mm = sDate.Substring(2, 2);
                dd = sDate.Substring(4, 2);
                break;

            case 7:
                //yyymmdd
                if (sDate.IndexOf("/") > -1)
                    return false;

                yyy = sDate.Substring(0, 3);
                mm = sDate.Substring(3, 2);
                dd = sDate.Substring(5, 2);
                break;

            case 8:
                //yy/mm/dd
                if (sDate.Substring(2, 1) != "/" | sDate.Substring(5, 1) != "/")
                    return false;

                yyy = sDate.Substring(0, 2);
                mm = sDate.Substring(3, 2);
                dd = sDate.Substring(6, 2);
                break;

            case 9:
                //yyy/mm/dd
                if (sDate.Substring(3, 1) != "/" | sDate.Substring(6, 1) != "/")
                    return false;

                yyy = sDate.Substring(0, 3);
                mm = sDate.Substring(4, 2);
                dd = sDate.Substring(7, 2);
                break;

            default:
                return false;
        }

        //轉成西元日期再用IsDate判斷
        string yyyy = (Convert.ToInt16(yyy) + 1911).ToString();
        if (IsDate(yyyy + "/" + mm + "/" + dd))
            return true;
        else
            return false;
    }

    public static bool IsCTime(string sTime)
    {
        //功能：檢查時間是否合理
        //參數：sTime 時間
        //回傳：合理的時間回傳 True
        //     不合理的時間回傳 False
        if (string.IsNullOrEmpty(sTime))
            return false;

        sTime = sTime.Replace(":", "");

        int hh = 0;
        int mm = 0;
        int ss = 0;
        int idx = 0;
        string[] atime = null;

        switch (sTime.Length)
        {
            case 4:
                sTime = sTime.Substring(0, 2) + ":" + sTime.Substring(2, 2);
                break;

            case 6:
                sTime = sTime.Substring(0, 2) + ":" + sTime.Substring(2, 2) + ":" + sTime.Substring(4, 2);
                break;

            default:
                //錯誤的格式
                return false;
        }

        atime = sTime.Split(':');

        for (idx = 0; idx <= atime.Length - 1; idx++)
        {
            switch (idx)
            {
                case 0:
                    hh = Convert.ToInt32(atime[idx]);
                    if (hh > 23 | hh < 0)
                        return false;
                    break;

                case 1:
                    mm = Convert.ToInt32(atime[idx]);
                    if (mm > 59 | mm < 0)
                        return false;
                    break;

                case 2:
                    ss = Convert.ToInt32(atime[idx]);
                    if (ss > 59 | ss < 0)
                        return false;
                    break;
            }
        }

        return true;

    }

    public static string ShowDate(string sDate)
    {
        //功能：將中文日期 YYYMMDD 顯示為 YYY/MM/DD
        switch (sDate.Length)
        {
            case 6:
                //940829
                return "0" + sDate.Substring(0, 2) + "/" + sDate.Substring(2, 2) + "/" + sDate.Substring(4, 2);

            case 7:
                //0940829
                return sDate.Substring(0, 3) + "/" + sDate.Substring(3, 2) + "/" + sDate.Substring(5, 2);

            case 8:
                //94/08/29
                return "0" + sDate;

            default:
                //094/08/29
                return sDate;
        }
    }

    public static string DateValue(string sCDate)
    {
        //功能：回傳 yyy/mm/dd 的日期值 yyymmdd

        if (string.IsNullOrEmpty(sCDate))
            return "";

        if (!IsCDate(sCDate))
            throw new System.Exception("呼叫 KTDateTime.DateValue 時發生錯誤,輸入日期格式(sCDate參數) 錯誤,正確的格式是yyy/mm/dd");

        return sCDate.Replace("/", "").Substring(0, 7);
    }

    public static string Today()
    {
        //功能：取得本日民國日期
        //回傳：yyymmdd
        //速度比套用 System.Globalization.TaiwanCalendar 取得民國年還要快。
        return WToCDate(System.DateTime.Today);
    }

    public static string HHMMSS(string Separater = "")
    {
        //功能：回傳目前的時間格式為 HH?MM?SS 共六碼 但時、分、秒以分格符號分開
        //參數：Separater 分格符號
        //return DateTime.Now.Hour.ToString().PadLeft(2, '0') + Separater + DateTime.Now.Minute.ToString().PadLeft(2, '0') + Separater + DateTime.Now.Second.ToString().PadLeft(2, '0');
        //速度比 原本 Hour、Minute、Second 還要快。
        return DateTime.Now.ToString("HH" + Separater + "mm" + Separater + "ss");
    }

    //public float Duration_By_Second()
    //{
    //    lock (static_Duration_By_Second_Date_Time_Init) {
    //        try {
    //            if (InitStaticVariableHelper(static_Duration_By_Second_Date_Time_Init)) {
    //                //功能 : 計算兩個時間點的時間差(以秒計)
    //                //不需在外部指令開始的時間以及結束的時間執行此 Function 兩次
    //                //第二次就會回傳時間差
    //                //關鍵在於 Date_Time 宣告為 Static 
    //                static_Duration_By_Second_Date_Time = new DateTime();
    //            }
    //        } finally {
    //            static_Duration_By_Second_Date_Time_Init.State = 1;
    //        }
    //    }

    //    DateTime sDateTime = default(DateTime);
    //    DateTime eDateTime = default(DateTime);

    //    if (static_Duration_By_Second_Date_Time == "#12:00:00 AM#") {
    //        static_Duration_By_Second_Date_Time = DateTime.Now;
    //        return -1;
    //    }

    //    eDateTime = DateTime.Now;
    //    sDateTime = static_Duration_By_Second_Date_Time;
    //    static_Duration_By_Second_Date_Time = new DateTime();
    //    return eDateTime.Subtract(sDateTime).TotalSeconds();

    //}

    public static double Duration_By_Second(DateTime DateTimeStart, DateTime DateTimeEnd)
    {
        //功能：計算兩個時間點的時間差(以秒計)
        //DateTimeStart：開始時間
        //DateTimeEnd：結束時間
        return DateTimeEnd.Subtract(DateTimeStart).TotalSeconds;
    }

    public static DateTime CToWDate(string sDate)
    {
        //參數：sDate --> 七碼的民國年  YYYMMDD
        //     Flag  --> D = 醫院系統使用的設定格式 MM/DD/YYYY
        //           --> C = 電腦安裝時的日期格式  YYYYMMDD
        //功能：將民國年日期轉成西元日期
        //回傳：日期型態西元日期

        sDate = sDate.Replace("/", "").Trim();

        if (sDate.Length != 7)
            throw new System.Exception("CToWDate 民國年轉成西元年" + KTConstant.vbCrLf + "民國年月日" + sDate + "轉西元年月日失敗!!");

        string yyyy = default(string);
        if (sDate.Substring(0, 1) == "-")
            yyyy = (Convert.ToInt32(sDate.Substring(0, 3)) + 1912).ToString();
        else
            yyyy = (Convert.ToInt32(sDate.Substring(0, 3)) + 1911).ToString();

        string mm = sDate.Substring(3, 2);
        string dd = sDate.Substring(5, 2);

        if (IsDate(yyyy + "/" + mm + "/" + dd))
            return Convert.ToDateTime(yyyy + "/" + mm + "/" + dd);
        else
            throw new System.Exception("CToWDate 民國年轉成西元年" + KTConstant.vbCrLf + "民國年月日" + sDate + "轉西元年月日失敗!!");
    }

    public static string WToCDate(System.DateTime dDate)
    {
        //功能：將西元日期轉成民國日期
        //回傳：民國日期 yyymmdd 

        string yyy = "";
        yyy = (dDate.Year - 1911).ToString().PadLeft(3, '0');

        if (yyy.Substring(0, 1) == "-" | yyy.Substring(0, 3) == "000")
            yyy = "-" + (1 - Convert.ToInt32(yyy)).ToString().PadLeft(2, '0');

        return yyy + dDate.ToString("MMdd");
    }

    public static int CWeekDay(string sCDate)
    {
        //功能：輸入民國日期回傳星期幾
        //參數：sCDate 民國日期yyymmdd
        //回傳：Monday:1、Tuesday:2、Wensday:3、......Sunday:7

        if (!IsCDate(sCDate))
            throw new System.Exception("呼叫 KTDateTime.CWeekDay 時發生錯誤, 輸入日期格式(sCDate參數)錯誤, 正確的格式是yyymmdd");

        System.DateTime WDate = CToWDate(sCDate);

        //set first day of the week to Monday with integer value 1 and Sunday with integer value 7
        int Day = ((int)WDate.DayOfWeek == 0) ? 7 : (int)WDate.DayOfWeek;
        return Day;

    }

    public static string CweekDayC(int iWeekDay)
    {
        // 功能：將星期幾由數字轉為中文1-> 一,2 -> 二 3 -> 三　.....7 日
        // 參數：iWeekDay 星期幾 1-7
        string CweekDayC = "";
        switch (iWeekDay)
        {
            case 1:
                CweekDayC = "一";
                break;
            case 2:
                CweekDayC = "二";
                break;
            case 3:
                CweekDayC = "三";
                break;
            case 4:
                CweekDayC = "四";
                break;
            case 5:
                CweekDayC = "五";
                break;
            case 6:
                CweekDayC = "六";
                break;
            case 7:
            case 0:
                CweekDayC = "日";
                break;
        }

        return CweekDayC;
    }

    public static string Show_Date_Diff(DateTime sDate, DateTime eDate)
    {
        //功能：輸入開始日，結束日，回傳兩時間差x年x月x日
        //說明：計算從開始日至結束日，經過x年x月x日，以整數年月計算，不算實際天數
        //參數：Start_Date , End_Date as DateTime
        //傳回：正確:「x年x月x日」 錯誤:「Input Error」
        //範例：Temp=Show_Date_Diff("2003/1/1","2005/5/10")  Return :「2年4月9日」
        //範例：Temp=Show_Date_Diff("2006/6/11","2005/5/10")  Return :「1年1月1日」
        //日期：094/05/10

        if (sDate > eDate)
        {
            DateTime TEMP = default(DateTime);
            TEMP = sDate;
            sDate = eDate;
            eDate = TEMP;
        }

        int Birth_Day = sDate.Day;
        int Now_Day = eDate.Day;

        //以現在時間計算總共年月日
        //int month_total = DateDiff(DateInterval.Month, Start_Date, End_Date);
        int month_total = (eDate.Month + eDate.Year * 12) - (sDate.Month + sDate.Year * 12);

        //宣告總共　年月日　數量
        int Year_Count = (int)Math.Floor((decimal)(month_total / 12));  //計算年數量
        int Month_Count = month_total % 12;                             //計算月數量
        int Day_Count = Now_Day - Birth_Day;                            //計算日數量

        //日小於0，月減一，日+30
        if (Day_Count < 0)
        {
            Month_Count -= 1;
            Day_Count = 30 + Day_Count;
        }

        //月小於0，年減一，月+12
        if (Month_Count < 0)
        {
            Year_Count = Year_Count - 1;
            Month_Count = 12 + Month_Count;
        }

        //回傳兩時間差x年x月x日
        return Year_Count + "歲" + Month_Count + "個月" + Day_Count + "天";
    }

    public static string Show_Date_Diff(string S_Date, string E_Date = "預設為今天", bool OnlyShowValid = false)
    {
        //功能：輸入開始日，結束日，回傳兩時間差x年x月x日
        //說明：計算從開始日至結束日，經過x年x月x日，以整數年月計算，不算實際天數
        //參數：S_Date, E_Date as DateTime
        //傳回：正確:「x年x月x日」 錯誤:「Input Error」
        //範例：Temp=Show_Date_Diff("2003/1/1","2005/5/10")  Return :「2年4月9日」
        //範例：Temp=Show_Date_Diff("2006/6/11","2005/5/10")  Return :「1年1月1日」
        //日期：094/05/10

        //如果丟入的日期非西元年， sdate = s_date 會有錯誤
        if (string.IsNullOrEmpty(S_Date))
            return "";

        DateTime sDate = Convert.ToDateTime(S_Date);
        DateTime eDate = (E_Date != "預設為今天" ? Convert.ToDateTime(E_Date) : System.DateTime.Today);
        if (eDate < sDate)
        {
            DateTime TEMP = default(DateTime);
            TEMP = sDate;
            sDate = eDate;
            eDate = TEMP;
        }

        int Birth_Day = sDate.Day;
        int Now_Day = eDate.Day;

        //以現在時間計算總共年月日
        //int month_total = DateDiff(DateInterval.Month, sDate, eDate);
        int month_total = (eDate.Month + eDate.Year * 12) - (sDate.Month + sDate.Year * 12);

        //宣告總共　年月日　數量
        int Year_Count = (int)Math.Floor((decimal)(month_total / 12));  //計算年數量
        int Month_Count = month_total % 12;                             //計算月數量
        int Day_Count = Now_Day - Birth_Day;                            //計算日數量

        //日小於0，月減一，日+30
        if (Day_Count < 0)
        {
            Month_Count -= 1;
            Day_Count = 30 + Day_Count;
        }

        //月小於0，年減一，月+12
        if (Month_Count < 0)
        {
            Year_Count = Year_Count - 1;
            Month_Count = 12 + Month_Count;
        }

        //回傳兩時間差x年x月x日
        //104/09/03 Modify By 義翔 '新增參數，如果歲或月沒有值，就只秀天吧：
        if (OnlyShowValid == true)
        {
            if (Year_Count == 0)
            {
                if (Month_Count == 0)
                {
                    return Day_Count + "天";
                }
                else
                {
                    return Month_Count + "個月" + Day_Count + "天";
                }
            }
            else
            {
                return Year_Count + "歲" + Month_Count + "個月" + Day_Count + "天";
            }
        }
        else
        {
            return Year_Count + "歲" + Month_Count + "個月" + Day_Count + "天";
        }
    }

    public static string Get_Now_Day(string now_mod)
    {
        //now_mod : C --> yyymmdd
        //now_mod : P --> yyy/mm/dd
        switch (now_mod.ToUpper())
        {
            case "C":
                return DateValue(Today());

            case "P":
                return ShowDate(Today());

            case "YYYY-MM-DD":
                return DateTime.Now.ToString("yyyy-MM-dd");

            case "YYYY/MM/DD":
                return DateTime.Now.ToString("yyyy/MM/dd");

            case "YYYYMMDD":
                return DateTime.Now.ToString("yyyyMMdd");
        }
        return "";
    }

    public static string Get_Now_DayTime(string now_mod)
    {
        //回傳目前的日期與時間
        switch (now_mod.ToUpper())
        {

            case "C10":
                //回傳到 10901010810   分的部分取 0
                return Get_Now_Day("C") + HHMMSS().Substring(0, 3) +"0";
            case "C11":
                //10909292359
                return Get_Now_Day("C") + HHMMSS().Substring(0, 4) ;
            case "P15":
                // 109/09/20 09:12
                return Get_Now_Day("P") + " " + KTDateTime.Get_Now_Time("P");
        }
        return "";

    }

    public static string Get_Now_Time(string now_mod)
    {
        //now_mod : C --> yyymmdd
        //now_mod : P --> yyy/mm/dd
        switch (now_mod.ToUpper())
        {
            case "C":
                return HHMMSS().Substring(0, 4);
          
            case "C30":
                //回傳到十分 
                return HHMMSS().Substring(0, 3) +"0";

            case "C6":
                return HHMMSS().Substring(0, 6);

            case "P":
                return HHMMSS(":").Substring(0, 5);

            case "P6":
                return HHMMSS(":");
        }
        return "";
    }

    public static string Change_Date_Mode(string input_date, string Input_Mod)
    {
        //C  -> 如果格式為 yyymmdd 則 Pass
        //      如果格式為 yyy/mm/dd 則轉換為 yyymmdd
        //P  -> 如果格式為 yyy/mm/dd 則 Pass
        //      如果格式為 yyymmdd 則轉換為 yyy/mm/dd

        if (string.IsNullOrEmpty(input_date))
            return "";

        input_date = input_date.Trim();

        if (string.IsNullOrEmpty(input_date))
            return "";

        if (!IsCDate(input_date))
            throw new System.Exception("Change_Date_Mode：" + input_date + " 輸入的日期格式錯誤");

        input_date = input_date.Replace("/", "");

        switch (Input_Mod.ToUpper())
        {
            case "C":
            case "P-C":
                return DateValue(input_date);

            case "P":
            case "C-P":
                return ShowDate(input_date);

            case "YYYY-MM-DD":
                return CToWDate(input_date).ToString("yyyy-MM-dd");

            case "YYYY/MM/DD":
                return CToWDate(input_date).ToString("yyyy/MM/dd");

            case "YYYYMMDD":
                return CToWDate(input_date).ToString("yyyyMMdd");
        }
        return "";
    }

    public static string Chang_Input_Date_To_Input_Mode(string INDTE, string Input_Mod)
    {
        INDTE = INDTE.Trim();
        if (string.IsNullOrEmpty(INDTE))
            return "";

        if (INDTE.Length < 6)
            return "";

        INDTE = INDTE.Replace("/", "");

        INDTE = INDTE.Substring(0, 3) + INDTE.Substring(3);

        if (INDTE.Length < 7)
            INDTE = INDTE.PadLeft(7, '0');

        switch (Input_Mod)
        {
            case "P":
                if (INDTE.Length == 7)
                    INDTE = Change_Date_Mode(INDTE, "C-P");
                break;

            case "C":
                if (INDTE.Length == 9)
                    INDTE = Change_Date_Mode(INDTE, "P-C");
                break;
        }

        switch (Input_Mod)
        {
            case "P":
                if (INDTE.Length != 9)
                    throw new System.Exception("輸入的日期不合邏輯");
                break;

            case "C":
                if (INDTE.Length != 7)
                    throw new System.Exception("輸入的日期不合邏輯");
                break;
        }

        return INDTE;
    }

    public static string Change_Time_Mode(string input_time, string Input_Mod)
    {
        //改變 VB6 的 輸入參數
        //Input_Time: 0800
        //Input_Mod:
        //C  -> 如果格式為 hhmm 則 Pass
        //      如果格式為 hh:mm 則轉換為 hhmm
        //P  -> 如果格式為 hh:mm 則 Pass
        //      如果格式為 hhmm 則轉換為 hh:mm

        if (string.IsNullOrEmpty(input_time))
            return "";

        input_time = input_time.Trim().Replace(":", "");
        if (string.IsNullOrEmpty(input_time))
            return "";

        if (!IsCTime(input_time))
            throw new System.Exception("Change_Time_Mode：" + input_time + " : 輸入的時間不合邏輯");

        switch (Input_Mod.ToUpper())
        {
            case "C":
            case "P-C":
                input_time = input_time.Replace(":", "");
                break;

            case "P":
            case "C-P":
                switch (input_time.Length)
                {
                    case 4:
                        input_time = input_time.Substring(0, 2) + ":" + input_time.Substring(2, 2);
                        break;

                    case 6:
                        input_time = input_time.Substring(0, 2) + ":" + input_time.Substring(2, 2) + ":" + input_time.Substring(4, 2);
                        break;
                }
                break;
        }

        return input_time;
    }

    public static string C_M_DateType(string input_date, int Input_Type)
    {
        //功能：民國年的日期時間格式轉成 Mysql 的西元日期時間格式
        //作者：W. G. E.
        //日期：0940207 重新整理
        //參數：一定只能是民國年
        //     INPUT_DATE$ --> YYYMMDD      or  YYYMMDDHHMM
        //     Input_Type% --> 1.Date       :  2003-12-31
        //                     2.DateTime   :  2003-12-31 23:59:00
        //                     3.TIMESTAMP  :  20031231235900
        //                     4.Date       :  2003/12/31
        //                     5.DateTime   :  2003/12/31 23:59:00
        //                     6.ITMESTAMP  :  20031231
        //輸出：C_M_DateType
        //備註：Mysql 的時間格式有 3 種，依照所傳入的日期時間處理
        //      第 4. 5. 的 "/" 格式 寫入 Mysql 之後都會變回 "-"
        //      為何會把第 4. 5.也放進來，因為所看過的日期函式
        //      還沒有函式處理 YYYY/MM/DD 的回傳型態
        //      Chn_ad_date --> DD/MM/YYYY

        string Wk_Date1 = "";
        string Wk_Date2 = "";
        string Wk_Time = "";
        string OutPut_Date = "";

        input_date = input_date.Trim();
        if (string.IsNullOrEmpty(input_date))
            return "";

        Wk_Date1 = input_date;
        Wk_Date1 = Wk_Date1.Replace("/", "");
        Wk_Date1 = Wk_Date1.Replace(":", "");
        Wk_Date1 = Wk_Date1.Replace(" ", "");

        if (string.IsNullOrEmpty(Wk_Date1))
            return "";

        if (!IsCDate(Wk_Date1.Substring(0, 7)))
            throw new System.Exception("呼叫 KTDateTime.C_M_DateType 時發生錯誤, 輸入日期或時間格式(input_date參數) 錯誤");

        if (Wk_Date1.Substring(0, 1) == "-")
            Wk_Date2 = (Convert.ToInt16(Wk_Date1.Substring(0, 3)) + 1912).ToString() + "-" + Wk_Date1.Substring(3, 2) + "-" + Wk_Date1.Substring(5, 2);
        else
            Wk_Date2 = (Convert.ToInt16(Wk_Date1.Substring(0, 3)) + 1911).ToString() + "-" + Wk_Date1.Substring(3, 2) + "-" + Wk_Date1.Substring(5, 2);

        //處理時間
        //包含時間
        if (Wk_Date1.Length > 7)
        {
            Wk_Time = Wk_Date1.Substring(7, Wk_Date1.Length - 7);
            if (!IsCTime(Wk_Time))
                throw new System.Exception("呼叫 KTDateTime.C_M_DateType 時發生錯誤, 輸入日期或時間格式(input_date參數) 錯誤");
        }

        if (string.IsNullOrEmpty(Wk_Time))
            Wk_Time = "000000";

        if (Wk_Time.Length == 4)
            Wk_Time = Wk_Time + "00";

        Wk_Time = Change_Time_Mode(Wk_Time, "P");

        switch (Input_Type)
        {
            case 1:
                OutPut_Date = Wk_Date2;
                break;

            case 2:
                OutPut_Date = Wk_Date2 + KTString.Space(1) + Wk_Time;
                break;

            case 3:
                Wk_Date2 = Wk_Date2.Replace("-", "");
                Wk_Time = Wk_Time.Replace(":", "");
                OutPut_Date = Wk_Date2 + Wk_Time;
                break;

            case 4:
                OutPut_Date = Wk_Date2.Replace("-", "/");
                break;

            case 5:
                OutPut_Date = Wk_Date2.Replace("-", "/") + KTString.Space(1) + Wk_Time;
                break;

            case 6:
                OutPut_Date = Wk_Date2.Replace("-", "");
                break;
        }

        return OutPut_Date;
    }

    public static long DiffDays(string BigDate, string SmlDate, DateInterval Interval = DateInterval.Day)
    {
        //功能：輸入兩民國日期，計算指定的日期型態，回傳指定日期型態的相差數據
        //日期：0950601
        //參數：BigDate, SmlDate --> 民國日期
        //備註：可計算 日期與日期 之間間隔的天數, 
        //           月 與 月  之間間隔的月份
        //           年 與 年  之間間隔的年份
        //範例：DiffDays("0950728","0950714", DateInterval.Day)   ---> Return 14
        //範例：DiffDays("0950728","0950728", DateInterval.Day)   ---> Return 0
        //範例：DiffDays("0950128","0950701", DateInterval.Month) ---> Return 6

        DateTime sBigDate = CToWDate(BigDate);
        DateTime sSmlDate = CToWDate(SmlDate);

        if (sBigDate > sSmlDate)
        {
            DateTime tmpDate = default(DateTime);
            tmpDate = sBigDate;
            sBigDate = sSmlDate;
            sSmlDate = tmpDate;
        }
        return DateDiff(Interval, sBigDate, sSmlDate);
    }

    public static long DateDiff(DateInterval Interval, System.DateTime dateOne, System.DateTime dateTwo)
    {
        //https://forums.asp.net/t/361390.aspx?DateDiff+function+in+C+

        switch (Interval)
        {
            case DateInterval.Day:
            case DateInterval.DayOfYear:
                System.TimeSpan spanForDays = dateTwo - dateOne;
                return (long)spanForDays.TotalDays;

            case DateInterval.Hour:
                System.TimeSpan spanForHours = dateTwo - dateOne;
                return (long)spanForHours.TotalHours;

            case DateInterval.Minute:
                System.TimeSpan spanForMinutes = dateTwo - dateOne;
                return (long)spanForMinutes.TotalMinutes;

            case DateInterval.Month:
                return ((dateTwo.Year - dateOne.Year) * 12) + (dateTwo.Month - dateOne.Month);

            case DateInterval.Quarter:
                long dateOneQuarter = (long)System.Math.Ceiling(dateOne.Month / 3.0);
                long dateTwoQuarter = (long)System.Math.Ceiling(dateTwo.Month / 3.0);
                return (4 * (dateTwo.Year - dateOne.Year)) + dateTwoQuarter - dateOneQuarter;

            case DateInterval.Second:
                System.TimeSpan spanForSeconds = dateTwo - dateOne;
                return (long)spanForSeconds.TotalSeconds;

            case DateInterval.Weekday:
                System.TimeSpan spanForWeekdays = dateTwo - dateOne;
                return (long)(spanForWeekdays.TotalDays / 7.0);

            case DateInterval.WeekOfYear:
                System.DateTime dateOneModified = dateOne;
                System.DateTime dateTwoModified = dateTwo;
                while (dateTwoModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                {
                    dateTwoModified = dateTwoModified.AddDays(-1);
                }
                while (dateOneModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                {
                    dateOneModified = dateOneModified.AddDays(-1);
                }
                System.TimeSpan spanForWeekOfYear = dateTwoModified - dateOneModified;
                return (long)(spanForWeekOfYear.TotalDays / 7.0);

            case DateInterval.Year:
                return dateTwo.Year - dateOne.Year;

            default:
                return 0;
        }
    }

    public static long DiffDaysWithTimes(string SDate, string STime, string EDate, string ETime, DateInterval Interval)
    {
        //功能：輸入兩民國日期、分鐘，回傳指定日期型態的相差數據
        //     此功能與 DiffDays 相同，只是多加了時間的計算
        //日期：0961106
        //參數：SDate, STime --> 民國日期，時間
        //參數：EDate, ETime --> 民國日期，時間
        //備註：主要是 日期+時間 與 日期+時間 之間間隔的指定日期型態參數
        //     自動 	swap 小日期+時間 在前面，大日期+時間 在後面
        //範例：DiffDaysWithTimes("0950728","1000", "0950714", "1100", DateInterval.Day)  ---> Return "14"
        //範例：DiffDaysWithTimes("0950728","1000", "0950728", "1100", DateInterval.Day)  ---> Return "14"
        //範例：DiffDaysWithTimes("0950714","1000", "0950714", "1200", DateInterval.Hour) ---> Return 2

        if (SDate.Length == 7 & (STime.Length == 4 | STime.Length == 6))
        {
        }
        else
            throw new System.Exception("很抱歉 參數有改變位置，請重新丟入 日期,時間,日期,時間");

        if (SDate.Length == 0 | EDate.Length == 0)
            return (long)0;

        SDate = SDate.Replace("/", "");
        EDate = EDate.Replace("/", "");

        if (Interval == DateInterval.Second)
        {
            STime = STime.Replace(":", "").PadRight(6, '0');
            ETime = ETime.Replace(":", "").PadRight(6, '0');
        }

        DateTime sDateTime = Convert.ToDateTime(C_M_DateType(SDate + STime, 5));
        DateTime eDateTime = Convert.ToDateTime(C_M_DateType(EDate + ETime, 5));

        if (sDateTime > eDateTime)
        {
            DateTime tempDateTime = default(DateTime);
            tempDateTime = sDateTime;
            sDateTime = eDateTime;
            eDateTime = tempDateTime;
        }

        return DateDiff(Interval, sDateTime, eDateTime);
    }

    public enum DateInterval
    {
        Year = 0,
        Quarter = 1,
        Month = 2,
        DayOfYear = 3,
        Day = 4,
        WeekOfYear = 5,
        Weekday = 6,
        Hour = 7,
        Minute = 8,
        Second = 9
    }

    public static long Compute_Month(string sDate, string eDate)
    {
        //輸入兩民國日期，回傳相差月數
        //InTyp(判斷輸入日期格式): C-YYYMMDD ;P-YYY/MM/DD

        return DiffDays(sDate, eDate, DateInterval.Month);

        //int Sy = 0;
        //int Sm = 0;
        //int Ey = 0;
        //int Em = 0;
        //int Rmonth = 0;

        ////沒有開始日或結束日 ，無法計算幾個月
        //if (string.IsNullOrEmpty(sDate) | string.IsNullOrEmpty(eDate)) {
        //    return 0;
        //}

        //switch (InTyp) {
        //    case "C":
        //        break;

        //    case "P":
        //        SDTE = DateValue(SDTE);
        //        EDTE = DateValue(EDTE);
        //        break;
        //}

        //Sy = Convert.ToInt32(sDate.Substring(0, 3));
        //Sm = Convert.ToInt32(sDate.Substring(3, 2));

        //Ey = Convert.ToInt32(eDate.Substring(0, 3));
        //Em = Convert.ToInt32(eDate.Substring(3, 2));

        //Rmonth = (Em - Sm) + (Ey - Sy) * 12;

        //return Rmonth;
    }

    //public void ChkIn_OutObjDate(string Input, ref object Obj, string OutPutMode = "P")
    //{
    //    //功能：顯示不正確的日期格式, 並修改原件的背景顏色
    //    //      Input 名國年 yyymmdd 
    //    //      Obj   元件   TextBox, LableBox, ComboBox 
    //    //備註：1 此功能只能夠在資料第一次載入的時候做判斷，
    //    //       判斷日期否正確, 若資料與轉換後的日期格式不符合
    //    //       將 該元件的背景 顏色改為 Rose。
    //    //     2 僅適用於資料庫的資料帶入判斷，
    //    //       不適用於使用者手動輸入資料的判斷。
    //    //     3 除非有其他較為特別的控制方式，可提出來討論

    //    Input = Input.Trim();
    //    if (string.IsNullOrEmpty(Input))
    //    {
    //        Obj.Text = ""; return;
    //    }

    //    string OutPut = "";
    //    try
    //    {
    //        OutPut = Change_Date_Mode(Input, "C");
    //    }
    //    catch (Exception ex)
    //    {
    //        //如果一開始的日期格式就不正確了
    //    }

    //    try
    //    {
    //        if (string.IsNullOrEmpty(OutPut))
    //        {
    //            Obj.Text = Input;
    //            Obj.BackColor = Color.MistyRose;
    //        }
    //        else
    //        {
    //            Obj.Text = Change_Date_Mode(OutPut, OutPutMode);
    //            if (OutPut != Input)
    //            {
    //                Obj.BackColor = Color.MistyRose;
    //            }
    //            else
    //            {
    //                Obj.BackColor = Color.White;
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}

    public static int getMonth_MaxDays(string YYYMMorYYYYMM)
    {
        //功能：取得指定 [民國年月] 或 [西元年月] 的最大日天數
        //作者：居易
        //日期：0960523
        //參數：YYYMMorYYYYMM 民國年月  09605
        //     YYYMMorYYYYMM 西元年月  200705
        //回傳：正整數 天數
        //備註：使用 try 來排除當月份輸入錯誤時，會產生錯誤時導致程式中斷

        System.DateTime maxDate = default(System.DateTime);
        string YM = YYYMMorYYYYMM;
        YM = YM.Replace("/", "");
        YM = YM.Replace("-", "");
        YM = YM.Replace(" ", "");

        YM = YM.PadLeft(5, Convert.ToChar("0"));

        switch (YM.Length)
        {
            case 5:
                YM = YM + "01";
                maxDate = CToWDate(YM);
                break;

            case 6:
                YM = YM.Substring(0, 4) + "/" + YM.Substring(4, 2) + "/01";
                maxDate = Convert.ToDateTime(YM);
                break;

            case 7:
                YM = YM.Substring(0, 5) + "01";
                maxDate = CToWDate(YM);
                break;

            case 8:
                YM = YM.Substring(0, 4) + "/" + YM.Substring(4, 2) + "/01";
                maxDate = Convert.ToDateTime(YM);
                break;

            default:
                throw new System.Exception("Function getMonth_MaxDays" + KTConstant.vbCrLf + "年月 " + YYYMMorYYYYMM + " 錯誤，無法取得月份的最大天數");
        }

        return System.DateTime.DaysInMonth(maxDate.Year, maxDate.Month);

    }

    public static int WeekOfMonth(string sCDate)
    {
        //功能：輸入民國日期回傳當月份第幾週
        //作者：珀辰
        //日期：0990705
        //參數：sCDate YYYMMDD 民國日期
        //回傳：正整數 週數
        //範例：WeekOfMonth("099/07/05") --> 2
        //     WeekOfMonth("099/07/01") --> 1
        //     WeekOfMonth("099/07/31") --> 5

        System.Globalization.GregorianCalendar culture =
    new System.Globalization.GregorianCalendar(System.Globalization.GregorianCalendarTypes.Localized);

        DateTime s = CToWDate(sCDate);
        DateTime d = CToWDate(sCDate.Substring(0, 5) + "01");
        int sW = culture.GetWeekOfYear(s, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        int dW = culture.GetWeekOfYear(d, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        return sW - dW + 1;

        //return DatePart(DateInterval.WeekOfYear, Convert.ToDateTime(C_M_DateType(sCDate, 4))) - DatePart(DateInterval.WeekOfYear, Convert.ToDateTime(C_M_DateType(Strings.Mid(sCDate, 1, 5) + "01", 4))) + 1;
    }

    //public static bool IsLastWeekOfDay(string sCDate)
    //{
    //    //功能：輸入民國日期回傳是否為當月的最後一週星期4,5,6(用在人事的報備支援「最後一週」的定義)
    //    //作者：珀辰
    //    //日期：0990720
    //    //參數：sCDate YYYMMDD 民國日期
    //    //回傳：Boolean值
    //    //範例：IsLastWeekOfDay("099/08/22") --> False
    //    //範例：IsLastWeekOfDay("099/08/23") --> False
    //    //範例：IsLastWeekOfDay("099/08/24") --> False
    //    //範例：IsLastWeekOfDay("099/08/25") --> True
    //    //範例：IsLastWeekOfDay("099/08/26") --> True
    //    //範例：IsLastWeekOfDay("099/08/27") --> True
    //    //範例：IsLastWeekOfDay("099/08/28") --> True
    //    //範例：IsLastWeekOfDay("099/08/29") --> True
    //    //範例：IsLastWeekOfDay("099/08/30") --> True
    //    //範例：IsLastWeekOfDay("099/08/31") --> True

    //    //最後一天 減 六天，如果落在這範圍，則表示為最後一星期
    //    if (AddDays(Strings.Mid(sCDate, 1, 5) + getMonth_MaxDays(Strings.Mid(sCDate, 1, 5)), -6) <= sCDate & sCDate <= Strings.Mid(sCDate, 1, 5) + getMonth_MaxDays(Strings.Mid(sCDate, 1, 5))) {
    //        return true;
    //    } else {
    //        return false;
    //    }
    //}

    public static bool IsLastWeekOfMonth(string sCDate)
    {
        //功能：輸入民國日期回傳是否為當月的最後一週
        //作者：珀辰
        //日期：0990720
        //參數：sCDate YYYMMDD 民國日期
        //回傳：Boolean值
        //範例：IsLastWeekOfMonth("099/08/22") --> False
        //範例：IsLastWeekOfMonth("099/08/23") --> False
        //範例：IsLastWeekOfMonth("099/08/24") --> False
        //範例：IsLastWeekOfMonth("099/08/25") --> False
        //範例：IsLastWeekOfMonth("099/08/26") --> False
        //範例：IsLastWeekOfMonth("099/08/27") --> False
        //範例：IsLastWeekOfMonth("099/08/28") --> False
        //範例：IsLastWeekOfMonth("099/08/29") --> True
        //範例：IsLastWeekOfMonth("099/08/30") --> True
        //範例：IsLastWeekOfMonth("099/08/31") --> True

        //用每月最後一天的週數 = 傳進來的週數，則表示為最後一週

        System.Globalization.GregorianCalendar culture =
    new System.Globalization.GregorianCalendar(System.Globalization.GregorianCalendarTypes.Localized);

        DateTime s = CToWDate(sCDate.Substring(0, 5) + getMonth_MaxDays(sCDate.Substring(0, 5)));
        DateTime d = CToWDate(sCDate);
        int sW = culture.GetWeekOfYear(s, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        int dW = culture.GetWeekOfYear(d, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        return sW == dW;

    }


    public static string Compute_Date(string Input_Date, int yy, int MM, int dd)
    {
        //'input format=yyymmdd
        System.DateTime x = CToWDate(Input_Date);
        if (yy != 0) x = x.AddYears(yy);
        if (MM != 0) x = x.AddMonths(MM);
        if (dd != 0) x = x.AddDays(dd);

        return WToCDate(x);

    }

    //public static string Computer_Hour(string SDate, string STime, string EDate, string ETime)
    //{
    //    string KeepTimeHour = null;
    //    string KeepTimeMin = null;
    //    string KeepTime = null;


    //    if (EDate == SDate)
    //    {
    //        //開始時間的分 > 結束時間的分
    //        if (Strings.Right(ETime, 2) < Strings.Right(STime, 2))
    //        {
    //            KeepTimeMin = Strings.Trim(Conversion.Str(Conversion.Val(Strings.Right(ETime, 2)) + 60 - Conversion.Val(Strings.Right(STime, 2))));
    //            KeepTimeHour = Strings.Trim(Conversion.Str(Conversion.Val(Strings.Left(ETime, 2)) - 1 - Conversion.Val(Strings.Left(STime, 2))));
    //        }
    //        else
    //        {
    //            KeepTimeMin = Strings.Trim(Conversion.Str(Conversion.Val(Strings.Right(ETime, 2)) - Conversion.Val(Strings.Right(STime, 2))));
    //            KeepTimeHour = Strings.Trim(Conversion.Str(Conversion.Val(Strings.Left(ETime, 2)) - Conversion.Val(Strings.Left(STime, 2))));
    //        }
    //        KeepTime = AddZero(KeepTimeHour, 2) + AddZero(KeepTimeMin, 2);
    //        KeepTime = Strings.Left(KeepTime, 2) + ":" + Strings.Right(KeepTime, 2);
    //    }
    //    else
    //    {
    //        //先計算差距幾天
    //        if (string.IsNullOrEmpty(EDate) | string.IsNullOrEmpty(SDate))
    //        {
    //            KeepTime = "Error";
    //        }
    //        int j = 0;
    //        while (!(compute_date(SDate, 0, 0, j) == EDate))
    //        {
    //            j = j + 1;
    //        }
    //        //開始時間的分 > 結束時間的分
    //        if (Strings.Right(ETime, 2) < Strings.Right(STime, 2))
    //        {
    //            KeepTimeMin = Strings.Trim(Conversion.Str(Conversion.Val(Strings.Right(ETime, 2)) + 60 - Conversion.Val(Strings.Right(STime, 2))));
    //            KeepTimeHour = Strings.Trim(Conversion.Str(Conversion.Val(Strings.Left(ETime, 2)) - 1 + (j * 24) - Conversion.Val(Strings.Left(STime, 2))));
    //        }
    //        else
    //        {
    //            KeepTimeMin = Strings.Trim(Conversion.Str(Conversion.Val(Strings.Right(ETime, 2)) - Conversion.Val(Strings.Right(STime, 2))));
    //            KeepTimeHour = Strings.Trim(Conversion.Str(Conversion.Val(Strings.Left(ETime, 2)) + (j * 24) - Conversion.Val(Strings.Left(STime, 2))));
    //        }
    //        KeepTime = AddZero(KeepTimeHour, 2) + AddZero(KeepTimeMin, 2);
    //        KeepTime = Strings.Left(KeepTime, 2) + ":" + Strings.Right(KeepTime, 2);
    //    }
    //    return KeepTime;
    //    //傳回HH:MM
    //}


    public static string 中華民國年月日期(string 西元年或西元年 = "", bool culturePattern = false)
    {
        //100.01.08 居易 原本是放在 電子病歷簽章
        //105.01.13 居易 掛號系統要使用此功能，故移動到共用日期

        //西元年或西元年 可傳入西元年或民國年
        //culturePattern 是否回傳 [中華民國] 的中文字 預設為 false 不回傳

        //當使用者傳入的日期錯誤時，將回傳原本傳入的日期

        if (string.IsNullOrEmpty(西元年或西元年))
        {
            西元年或西元年 = System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

        }
        else
        {
            string tmpWDate = 西元年或西元年;
            System.DateTime tmpCDate = default(System.DateTime);

            西元年或西元年 = 西元年或西元年.Split(' ')[0];
            try
            {
                tmpCDate = Convert.ToDateTime(西元年或西元年);
                //西元年或西元年 = tmpCDate.Year + "/" + tmpCDate.Month.ToString().PadLeft(2, '0') + "/" + tmpCDate.Day.ToString().PadLeft(2, '0');
                西元年或西元年 = tmpCDate.ToString("yyyy/MM/dd");
            }
            catch (Exception)
            {
            }

            西元年或西元年 = 西元年或西元年.Replace("/", "");
            西元年或西元年 = 西元年或西元年.Replace("-", "");
            try
            {
                switch (西元年或西元年.Length)
                {
                    case 7:
                        tmpCDate = CToWDate(西元年或西元年);
                        //西元年或西元年 = tmpCDate.Year + "/" + tmpCDate.Month.ToString().PadLeft(2, '0') + "/" + tmpCDate.Day.ToString().PadLeft(2, '0');
                        西元年或西元年 = tmpCDate.ToString("yyyy/MM/dd");
                        break;

                    case 8:
                        西元年或西元年 = 西元年或西元年.Substring(0, 4) + "/" + 西元年或西元年.Substring(4, 2) + "/" + 西元年或西元年.Substring(6, 2);
                        break;
                }
                tmpCDate = Convert.ToDateTime(tmpCDate);
                //確認日期格式是否正確
            }
            catch (Exception)
            {
                return tmpWDate;
            }
        }

        System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("zh-TW", true);

        culture.DateTimeFormat.Calendar = new System.Globalization.TaiwanCalendar();

        string 年月日字串 = Convert.ToDateTime(西元年或西元年).ToString(culture.DateTimeFormat.LongDatePattern, culture);
        if (!culturePattern)
            年月日字串 = 年月日字串.Replace("中華民國", "");

        return 年月日字串;

    }

    //static bool InitStaticVariableHelper(Microsoft.VisualBasic.CompilerServices.StaticLocalInitFlag flag)
    //{
    //    if (flag.State == 0) {
    //        flag.State = 2;
    //        return true;
    //    } else if (flag.State == 2) {
    //        throw new Microsoft.VisualBasic.CompilerServices.IncompleteInitialization();
    //    } else {
    //        return false;
    //    }
    //}

    public static string DateTime24Hour(DateTime sDateTime, string SplitChar = "/", bool ShowTime = false)
    {
        //功能：將西元日期時間的 [上午] [下午] 中文拿掉，且年月日格式轉換為 yyyy/mm/dd ，下午 + 12
        //     Oracle Mysql SqlServer Access 的 Date 屬性共用
        //參數：DateTime  --> 西元日期時間
        //     SplitChar --> 指定的日期區隔符號
        //     ShowTime  --> True 無論如何, 都會回傳時間
        //     ShowTime  --> False 有時間則回傳時間, 沒時間則回傳日期
        //說明：若 DateTime 為空字串..預設為當時的日期時間
        //範例：DateTime24Hour --> 2006/01/01 13:01:01
        //     DateTime24Hour("01/01/2006 下午 01:01:01") --> 2006/01/01 13:01:01
        //     DateTime24Hour("01/01/2006") --> 2006/01/01
        //     http://msdn.microsoft.com/library/cht/
        //備註：以光田的日期時間格式來說..
        //     電腦的西元年 會有包含上午(AM) 與 下午(PM) 兩種
        //     將下午(PM)的時間轉成 24 小時制

        if (!ShowTime)
            return sDateTime.ToString("yyyy" + SplitChar + "MM" + SplitChar + "dd");
        else
            return sDateTime.ToString("yyyy" + SplitChar + "MM" + SplitChar + "dd HH:mm:ss");
    }

    public static string DateTime24Hour(string DateTime = "", string SplitChar = "/", bool ShowTime = false)
    {
        //功能：將西元日期時間的 [上午] [下午] 中文拿掉，且年月日格式轉換為 yyyy/mm/dd ，下午 + 12
        //     Oracle Mysql SqlServer Access 的 Date 屬性共用
        //參數：DateTime  --> 西元日期時間
        //     SplitChar --> 指定的日期區隔符號
        //     ShowTime  --> True 無論如何, 都會回傳時間
        //     ShowTime  --> False 有時間則回傳時間, 沒時間則回傳日期
        //說明：若 DateTime 為空字串..預設為當時的日期時間
        //範例：DateTime24Hour --> 2006/01/01 13:01:01
        //     DateTime24Hour("01/01/2006 下午 01:01:01") --> 2006/01/01 13:01:01
        //     DateTime24Hour("01/01/2006") --> 2006/01/01
        //     http://msdn.microsoft.com/library/cht/
        //備註：以光田的日期時間格式來說..
        //     電腦的西元年 會有包含上午(AM) 與 下午(PM) 兩種
        //     將下午(PM)的時間轉成 24 小時制

        bool OnlyDate = false;
        DateTime = DateTime.Trim();

        if (string.IsNullOrEmpty(DateTime))
            DateTime = System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

        switch (DateTime.Length)
        {
            case 8:
                OnlyDate = true;
                if (IsDate(DateTime))
                {
                    DateTime = Convert.ToDateTime(DateTime).ToString("yyyy/MM/dd");
                }
                else
                {
                    DateTime = DateTime.Substring(0, 4) + "/" + DateTime.Substring(4, 2) + "/" + DateTime.Substring(6, 2);
                }
                break;

            case 10:
                OnlyDate = true;
                break;
        }

        //日期格式為 10 碼，
        Int16 DateTimeLength = (Int16)(DateTime.Length < 10 ? 10 : DateTime.Length);

        DateTime = Convert.ToDateTime(DateTime).ToString("yyyy/MM/dd HH:mm:ss");

        //因日期會變成 2016/2/8 01:01:01 導致擷取錯誤，重新取得正確的日期格式後，使用正確的長度
        if (DateTimeLength > 10)
            DateTimeLength = (Int16)DateTime.Length;

        string[] TimeSplit = DateTime.Split(' ');

        if ((!ShowTime & OnlyDate) | DateTimeLength <= 10)
        {
            DateTime = TimeSplit[0];
        }
        else
        {
            if (DateTime.Length > DateTimeLength)
            {
                DateTime = DateTime.Substring(0, DateTimeLength);
                //進來的時間長度有多少，回去的時間長度就多少
            }
        }
        return DateTime.Replace("/", SplitChar);
    }

    public static bool IsDate(string dateString)
    {
        DateTime d = default(DateTime);
        bool result = DateTime.TryParse(dateString, out d);
        return result;
    }

    public static void Delay(double N)
    {
        //功能：延遲秒數
        //參數：N 單位 秒 
        //傳回：無
        //範例：Delay(2)  即延遲兩秒
        //日期：094/07/15

        DateTime sTime = DateTime.Now;
        while (DateTime.Now.Subtract(sTime).TotalSeconds < N)
        {
            System.Threading.Thread.Sleep(0);
            //停止 0.001秒 避免消耗記憶體
            //System.Windows.Forms.Application.DoEvents();
        }
    }
}

class KTPicture
{
    //圖形
    //           ColorTransform      轉換顏色值 成 System.Drawing.Color 屬性
    //           PictureTxtBar       以 Picture 的色彩，模擬執行階段的 ProgressBar
    public static System.Drawing.Color ColorTransform(object Expression, int Green = 0, int Yellow = 0)
    {
        //功能：轉換顏色值 成 System.Drawing.Color 屬性
        //作者：吳居易
        //日期：094/08/08
        //說明：Expression 有三種模式 
        //     1.System.Drawing.Color --> 原本的 Color 屬性
        //     2.Integer              --> 數字值所代表的顏色值
        //     3.String               --> 系統定義的色彩名稱，也就是 Color 所定義的顏色單字
        //參數：Expression 物件有三種型態 Color、Integer、String
        //傳回：Color ARGB色彩
        //版本：
        //範例：Ex：
        //     1. Form1.BackColor = ColorTransform(Color.Reg)
        //     2. Form1.BackColor = ColorTransform(RGB(255, 0 ,0))    '紅色
        //        Form1.BackColor = ColorTransform(65535)             '黃色
        //        Form1.BackColor = ColorTransform(0, 255, 0)
        //     3. Form1.BackColor = ColorTransform("Blue")
        //備註：如果不是此三種型態，什麼事情也不會發生
        //     圖片就是要處理顏色，所以放在這裡 @@
        //參考：ReturnColor.ToKnownColor  可列出參數結構
        System.Drawing.Color ReturnColor = default(System.Drawing.Color);

        if (Expression.GetType().ToString() == "System.Drawing.Color")
            return (System.Drawing.Color)Expression;

        if (Expression.GetType().IsValueType)
        {
            //由 RGB() 轉換而成的 Integer
            ReturnColor = System.Drawing.ColorTranslator.FromWin32((int)Expression);
            if ((int)Expression <= 255 & Green <= 255 & Yellow <= 255)
            {
                //三種顏色值若 小於 255 才屬於 RGB or FromArgb 的顏色範圍
                //若 Expression <= 255 此處的答案與 ColorTranslator.FromWin32(Expression) 相同
                ReturnColor = System.Drawing.Color.FromArgb((int)Expression, Green, Yellow);
            }
        }
        else
        {
            //顏色名稱 
            ReturnColor = System.Drawing.Color.FromName((string)Expression);
        }

        return ReturnColor;
    }

    public static void PictureTxtBar(dynamic PicBox, long ValueInt64, long ValueCount, string Text = "", object FontColor = null, object StartColor = null, object EndColor = null, System.Drawing.Drawing2D.LinearGradientMode LineMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal)
    {
        //功能：以 Picture 的色彩，模擬執行階段的 ProgressBar
        //作者：吳居易
        //日期：094/08/10
        //說明：以 PictureBox 模擬 ProgressBar 的執行進度
        //參數：PicBox     --> PictureBox 圖片元件(System.Windows.Forms.PictureBox)
        //     ValueInt64 --> 執行進度的分子
        //     ValueCount --> 執行進度的分母
        //     Text       --> 目前的進度說明
        //     FontColor  --> 顯示的文字顏色      
        //     StartColor --> 漸層的開始顏色色彩  
        //     EndColor   --> 漸層的結束顏色色彩  
        //     LineMode   --> 漸層顏色的模式
        //傳回：無
        //版本：
        //範例：Ex：PictureTxtBar(PictureBox1, 500, 1000, "第 500 個")
        //備註：請在表單上拉出一個適當長度的 PictureBox 元件，將該元件丟入此 Sub
        //參考：顏色的使用方式 請參考 Function ColorTransform

        PicBox.Refresh();
        //############### 定義字型顏色 圖片線條顏色 ###############
        System.Drawing.Color FontColor_ = default(System.Drawing.Color);
        System.Drawing.Color StartColor_ = default(System.Drawing.Color);
        System.Drawing.Color EndCOlor_ = default(System.Drawing.Color);

        if ((FontColor != null))
            FontColor_ = ColorTransform(FontColor);
        if (FontColor_.Name == "0")
            FontColor_ = System.Drawing.Color.DarkSlateBlue;
        if ((StartColor != null))
            StartColor_ = ColorTransform(StartColor);
        if (StartColor_.Name == "0")
            StartColor_ = System.Drawing.Color.LightSkyBlue;
        if ((EndColor != null))
            EndCOlor_ = ColorTransform(EndColor);
        if (EndCOlor_.Name == "0")
            EndCOlor_ = System.Drawing.Color.White;
        //#########################################################


        //############## 繪製線條顏色 #############################
        System.Drawing.Graphics G = PicBox.CreateGraphics();
        decimal percent = ((decimal)ValueInt64 / ValueCount);
        int percentint = Convert.ToInt32(percent * 100);

        System.Drawing.Rectangle rect = PicBox.ClientRectangle;
        //計算將要描繪的區塊進度
        rect.Width = (int)(rect.Width * percent) + 1;

        System.Drawing.Drawing2D.LinearGradientBrush brush = default(System.Drawing.Drawing2D.LinearGradientBrush);
        brush = new System.Drawing.Drawing2D.LinearGradientBrush(rect, StartColor_, EndCOlor_, LineMode);
        //Draw the progress meter.
        G.FillRectangle(brush, rect);
        brush.Dispose();
        //#########################################################

        //############## 繪製文字模式 #############################
        string percentstr = null;
        percentstr = "";
        percentstr = percentstr + ValueInt64.ToString().Insert(ValueInt64.ToString().Length, " / ");
        percentstr = percentstr + ValueCount.ToString().Insert(ValueCount.ToString().Length, " , ");
        percentstr = percentstr + percentint.ToString().Insert(percentint.ToString().Length, "%");
        percentstr = percentstr.Insert(percentstr.Length, " " + Text);

        System.Drawing.PointF pF = default(System.Drawing.PointF);
        System.Drawing.Font ft = new System.Drawing.Font("細明體", 11);
        int PWidth = PicBox.Width - (KTString.LenB(percentstr)) * (ft.Height / 3);
        //計算起始位置
        if (PWidth < 0)
            PWidth = 0;
        pF = new System.Drawing.PointF(PWidth / 2, (PicBox.Height / 2) - (ft.Height / 3));
        PicBox.CreateGraphics().DrawString(percentstr, ft, new System.Drawing.SolidBrush(FontColor_), pF.X, pF.Y);
        //#########################################################
    }
}

class KTHtmlPage
{
#if !noDapper
    //1081019 居易
    //        故意在這裡特別使用 noDapper 條件式編譯符號的原因
    //        不想用 Dapper 的，一定不是寫網頁
    //        就算是寫網頁，既然會使用 Dapper 為何不能再多引用一個 System.Web 呢 ? 
    public static string ClientIPAddress()
    {
        string ip = System.Web.HttpContext.Current.Request.ServerVariables["X_FORWARDED_FOR"] ?? System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        if (ip == "::1") ip = "127.0.0.1";
        return ip;
    }
#endif
}

public class KTDataBase : IDisposable
{
    public KTDataBase(KTDB.KTConnectionControler KTCC_)
    {
        KTCC = KTCC_;
    }

    private KTDB.KTConnectionControler KTCC { get; set; }

    private class HISProgramer_Model
    {
        public string Opid { get; set; }
        public string Opname { get; set; }
        public string Costdpt { get; set; }
        public string Branch { get; set; }
    }

    public string HISProgramer(string ProgramID = "", string DBLink = "", string TransHost = "")
    {
        //功能：取得 HIS主機 上的程式負責人姓名
        //參數：ProgramID      --> 程式代碼 : 執行程式的執行檔名稱 
        //                         若為獨立程式可以不需要傳入參數
        //                         若非獨立程式(該程式表單並非主要啟動程式)
        //                         需填入原本主程式的程式代碼
        //
        //     DBLink 與 Host --> 保留給部分啟動程式，不是直接連線到 KTGH00 或 HPK210
        //                         也能夠取得程式負責人
        //
        //範例：1.門診預約掛號_95 --> PRSP1000.Exe
        //       HISProgramer()  --> 這樣就可以取得程式負責人
        //     2.門診批價結帳    --> OPBP1000.Exe
        //       HISProgramer()  --> 這樣就可以取得程式負責人
        //       但是 原本的 PRSP1000 就會被取代成 OPBP1000 的程式負責人
        //       故需要傳入參數 HISProgramer("PRSP1000") 才能取得原先的程式負責人
        //        
        //備註：1.SYS001 須建檔 PGMID.程式代碼 PGMER.負責人員工編號
        //     2.不傳入 ProgramName 參數的情況之下
        //       如果修改成執行程式的執行檔名稱，將抓不到 SYS001 的資料

        string sqlstr = "";

        if (string.IsNullOrEmpty(ProgramID))
            ProgramID = KTEnvs.getAppProgramName();

        string OracleHostDSN = KTDB.KTConnectionControler.OracleHostDSN;

        System.Data.DataTable tblPRS003 = new System.Data.DataTable();
        List<HISProgramer_Model> lstPRS003 = null;
        HISProgramer_Model rowPRS003 = null;

        sqlstr = "";
        sqlstr = sqlstr + " Select Opid, Opname, costdpt, (Select BRANCH From " + KTCC.DataBaseLink(DBLink, "HRE_ORGBAS", TransHost: TransHost) + " WHERE COSTDPT = DEPT_NO) As Branch ";
        sqlstr = sqlstr + "   From " + KTCC.DataBaseLink(DBLink, "PRS003", TransHost: TransHost);
        sqlstr = sqlstr + "  Where OPID In (SELECT PGMER FROM " + KTCC.DataBaseLink(DBLink, "SYS001", TransHost: TransHost) + " WHERE PGMID = '" + ProgramID + "') ";
        sqlstr = sqlstr + "  Order By Opid ";
#if !noDapper
        lstPRS003 = new List<HISProgramer_Model>();
        lstPRS003 = KTCC.DoSql<HISProgramer_Model>(sqlstr, TransHost: TransHost).ToList();
#else
        KTCC.DoSql(sqlstr, tblPRS003, TransHost: TransHost);
        lstPRS003 = KTCC.DataTableToList<HISProgramer_Model>(tblPRS003).ToList();
#endif

        string wkstr = "";
        if (lstPRS003.Count() == 0) { wkstr = ProgramID; }

        else
        {
            rowPRS003 = lstPRS003[0];

            switch (KTDB.KTConnectionControler.OracleHostDSN)
            {
                case "KTGH00":
                case "KTGH01":
                case "SAKTHIS":
                case "KTGH03":
                    if (rowPRS003.Branch.ToString() == "A")
                        wkstr = "程式維護:" + rowPRS003.Opname + " 分機:2017,2018";
                    else
                        wkstr = "程式維護:" + rowPRS003.Opname + " 分機:821306,821307";
                    break;

                case "HPK210":
                case "KTGHTA01":
                case "NTSERTA":
                    if (rowPRS003.Branch.ToString() == "A")
                        wkstr = "程式維護:" + rowPRS003.Opname + " 分機:812017,812018";
                    else
                        wkstr = "程式維護:" + rowPRS003.Opname + " 分機:1306,1307";
                    break;

                case "TSHIS":
                    if (rowPRS003.Branch.ToString() == "A")
                        wkstr = "程式維護:" + rowPRS003.Opname + "(負責人在沙鹿)";
                    else
                        wkstr = "程式維護:" + rowPRS003.Opname + "(負責人在大甲)";
                    break;

                case "KTXS00":
                case "KTXS01":
                    if (rowPRS003.Branch.ToString() == "A")
                        wkstr = "程式維護:" + rowPRS003.Opname + " 分機:2017,2018";
                    else
                        wkstr = "程式維護:" + rowPRS003.Opname + " 分機:821306,821307";
                    break;
            }
        }

        if (wkstr == ProgramID)
        {
            wkstr = wkstr + ":無程式負責人 需要時請聯絡:";
            switch (OracleHostDSN)
            {
                case "KTGH00":
                case "KTGH01":
                case "SAKTHIS":
                case "KTGH03":
                    wkstr = wkstr + "2017,2018";
                    break;
                case "HPK210":
                case "KTGHTA01":
                case "NTSERTA":
                    wkstr = wkstr + "1306,1307";
                    break;
                case "TSHIS":
                    wkstr = wkstr + "沙鹿資訊部";
                    break;
            }
        }

        return wkstr;
    }

    public string 取得患者最新測量值(string CRTNO, string DTATYP)
    {
        //104/08/19 Add By 義翔 
        //DTATYP:'01.身高(cm)
        //        02.體重(kg)
        //        03.腰圍(cm)
        //        04.血壓(mmHg)
        //        05.脈搏(次 / 分)
        //        06.呼吸(次 / 分)
        //        07.血糖
        //        08.頭圍(cm)
        //        09.疼痛指數
        //        10.體溫(℃)

        string functionReturnValue = null;

        KTCC.getDBLinkState();

        string TheSameHost = "";
        string[] TheSameHosts = KTDB.KTConnectionControler.getTheSameHosts();

        string sqlstr = "";
        System.Data.DataTable tblPRS039 = new System.Data.DataTable();

        sqlstr = "Select * From ( ";

        for (int x = 0; x <= TheSameHosts.Length - 1; x++)
        {
            TheSameHost = TheSameHosts[x];
            if (KTCC.getDBLinkState(TheSameHost))
            {
                if (x != 0) sqlstr = sqlstr + "  Union All ";

                sqlstr = sqlstr + " Select REC_1, REC_2, UPDTE, UPTME From " + KTCC.DataBaseLink(TheSameHost, "PRS039");
                sqlstr = sqlstr + "  Where Crtno = '" + CRTNO + "' ";
                sqlstr = sqlstr + "    And DTATYP = '" + DTATYP + "' ";
            }
        }

        sqlstr = sqlstr + " ) ";
        sqlstr = sqlstr + " Order By UPDTE Desc, UPTME Desc ";

        KTCC.DoSql(sqlstr, tblPRS039);

        if (KTCC.DoSqlOK == true)
        {
            if (tblPRS039.Rows.Count > 0)
            {
                switch (DTATYP)
                {
                    case "01":
                    case "02":
                    case "03":
                    case "05":
                    case "06":
                    case "07":
                    case "08":
                    case "09":
                    case "10":
                        functionReturnValue = tblPRS039.Rows[0]["REC_1"] + "";
                        break;

                    case "04":
                        functionReturnValue = tblPRS039.Rows[0]["REC_1"] + "" + " / " + tblPRS039.Rows[0]["REC_2"] + "";
                        break;
                }
            }
        }
        return functionReturnValue;
    }


    public void 資訊系統功能使用記錄(string HspNme, string Itemid, string ItemNme, string Opid)
    {
        //資料存放在IPF009D
        //ITEMID 的代碼資料放在 M:\文件\軟體課\程式相關\蚊子系統統計.DOCX
        //OPID   請傳入程式開發負責人員

        string TransHost = "IPF009D";
        if (!KTCC.AlreadyConnectOK(TransHost))
            KTCC.Connect(HspNme, UseCnKey: TransHost);

        KTCC.BeginTransaction(TransHost);

        string ExeName = (System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

        string sqlstr;
        sqlstr = " INSERT INTO IPF009D ";
        sqlstr = sqlstr + " ( DTATYP,CRTNO,PARA1,PARA2";
        sqlstr = sqlstr + ",ITEMID1,ITEMNME1,UPOP,UPDTE,UPTME) ";
        sqlstr = sqlstr + " VALUES ('55'";
        sqlstr = sqlstr + ",'" + Itemid + "'";
        sqlstr = sqlstr + ",'" + ExeName + "'";
        sqlstr = sqlstr + ",'" + Opid + "'";
        sqlstr = sqlstr + ",'" + Itemid + "'";
        sqlstr = sqlstr + ",'" + ItemNme + "'";
        sqlstr = sqlstr + ",'" + KTString.GetUserID() + "'";
        sqlstr = sqlstr + ",'" + KTDateTime.Get_Now_Day("C") + "'";
        sqlstr = sqlstr + ",'" + KTDateTime.Get_Now_Time("C") + "'";
        sqlstr = sqlstr + ")";
        KTCC.DoSql(sqlstr, TransHost: TransHost);

        if (KTCC.DoSqlOK == false)
        {
            KTCC.Rollback(TransHost);
            KTCC.Close(TransHost);
            //Interaction.MsgBox("新增系統使用功能次數有問題");
            return;
        }

        KTCC.Commit(TransHost);
        KTCC.Close(TransHost);
    }

    //設定Form_Caption 一致性
    //public static string Set_Form_Caption(string Form_Name, string Application_Name)
    //{
    //    //Form_Name = "看診"，Application_Name= "ORDP1000.EXE"
    //    string Text = "[" + Form_Name + "-" + Application_Name.ToString().Replace(".EXE", "") + "-版本:" + KTString.Get_Application_Data(檔案內容.修改日期, Application.StartupPath + "\\" + Application_Name);
    //    switch (WhichHost())
    //    {
    //        case "HPK210":
    //            Text = Text + "-大甲]";
    //            break;

    //        case "KTGH00":
    //            Text = Text + "-沙鹿]";
    //            break;

    //        case "TSHIS":
    //            Text = Text + "-通霄]";
    //            break;

    //        case "KTGH03":
    //            Text = Text + "-測試]";
    //            break;
    //    }
    //    return Text;
    //}


    //public bool Display_Prg(string PrgName, string Mode, string upFilePath = "C:\\AP06BZ\\EXE\\")
    //{
    //    //回傳 True 需自行執行程式更新，False 完成
    //    //Mode: "A" 自動更新、"M" 只顯示訊息、"N" 自動更新，但由外部程式自行做KillProcess或End
    //    //PrgName: "欲檢查的程式" 請給全名:例:"ordp1000" or "opbp1000"
    //    if (KTEnvs.Get_IPAddress().Substring(0, 11) == "192.168.160")
    //    {
    //        int nowTime = Convert.ToInt32(KTDateTime.Get_Now_Time("C6"));
    //        if (nowTime <= 160000 || nowTime > 200000)
    //            return false;
    //    }

    //    PrgName = PrgName.ToUpper();
    //    //轉大寫
    //    string[] PrgNameAry = PrgName.Split('.');
    //    if (PrgNameAry.Length == 1)
    //        PrgName = PrgName + ".EXE";

    //    string SqlStr = "";
    //    string PGMName = "";
    //    string strPathName = upFilePath + PrgName;

    //    //Application.StartupPath & "\" & PrgName & ".EXE"
    //    bool Need_Update = false;
    //    //是否需程式更新
    //    string StrShowOracleDate = "";
    //    string StrShowFileDate = "";

    //    //若當地的檔案找不到，表示確定要更新
    //    if (!System.IO.File.Exists(strPathName))
    //        Need_Update = true;

    //    else
    //    {
    //        IDataReader rdrPROG010 = null;
    //        try
    //        {
    //            SqlStr = "Select PRGNAME,FILEDATE,FILETIME,DLPATH,ACTIVE From PROG010 ";
    //            SqlStr = SqlStr + " Where UPPER(prgname) = '" + PrgName + "'";
    //            SqlStr = SqlStr + "   AND ( ACTIVE IS NULL OR  ACTIVE <> 'B' OR UPDTE < '" + KTDateTime.Get_Now_Day("C") + "' ) ";

    //            KTCC.DoSql(SqlStr, rdrPROG010);
    //            if (rdrPROG010.Read())
    //            {
    //                string FILEDATE = rdrPROG010["FILEDATE"].ToString();
    //                string FILETIME = rdrPROG010["FILETIME"].ToString();

    //                StrShowOracleDate = "20" + FILEDATE.Substring(0, 2) + "/" + FILEDATE.Substring(2, 2) + "/" + FILEDATE.Substring(4, 2) + " " + FILETIME.Substring(0, 2) + ":" + FILETIME.Substring(2, 2) + ":" + FILETIME.Substring(4, 2);

    //                //如果檔案存在，要判斷是否需要更新

    //                //0970705 居易
    //                //時常發生 主機上的日期與 Client 上的日期時間不相同，會都多個兩秒，導致一直要程式更新
    //                //判斷程式預設 + 3 秒，如果真的有人 make 到上傳的時間 小於 3 秒，那麼他也只好認了，只能說他的速度太快
    //                StrShowFileDate = KTDateTime.DateTime24Hour(System.IO.File.GetLastWriteTime(strPathName).AddSeconds(3));

    //                //100/01/07 Modify by 珀辰
    //                //原時間是判斷到「年/月/日 時:分:秒」，但因系統時間有時會被改成 HH:MM 造成程式錯誤，
    //                //所以改為判斷到「年/月/日 時:分」，不判斷秒了，所以增加以下判斷「全部截至分」
    //                if (StrShowOracleDate.Length == 19)
    //                {
    //                    StrShowOracleDate = StrShowOracleDate.Substring(0, 16);
    //                }
    //                if (StrShowOracleDate.Length == 19)
    //                {
    //                    StrShowFileDate = StrShowOracleDate.Substring(0, 16);
    //                }
    //                if (Convert.ToInt32(StrShowOracleDate) > Convert.ToInt32(StrShowFileDate))
    //                {
    //                    Need_Update = true;
    //                    //ElseIf StrShowOracleDate < StrShowFileDate Then
    //                    //    '100/05/06 Add by 珀辰，因有時會回覆版本，但樓下又以為進入診間後會自已強制更新，但其實是不更新的「資料庫上比client還新」
    //                    //    '但因有強制更新註記上去，所以必需再去判別如果「資料庫上比client還舊」，那就詭異了，所以強制更新使資料庫跟client一樣的版本。
    //                    //    If tblPROG010.Rows(0)("ACTIVE") & "" = "D" Then  '有強制更新註記
    //                    //        Need_Update = True
    //                    //    End If
    //                }
    //            }
    //        }
    //        catch
    //        { }

    //        if (rdrPROG010 != null) rdrPROG010.Close();
    //    }

    //    if (Need_Update == true)
    //    {
    //        //要更新時才去抓取 SYS001 程式名稱
    //        IDataReader rdrSYS001 = null;

    //        try
    //        {
    //            SqlStr = " SELECT * FROM SYS001 ";
    //            SqlStr = SqlStr + " WHERE PGMID = '" + PrgNameAry[0] + "'";
    //            SqlStr = SqlStr + "   AND Updte is not null And Uptme is Not null ";
    //            KTCC.DoSql(SqlStr, rdrSYS001);
    //            if (rdrSYS001.Read())
    //            {
    //                PGMName = "　" + rdrSYS001["PGMNAME"];
    //            }
    //        }
    //        catch { }

    //        if (rdrSYS001 != null) rdrSYS001.Close();


    //        SqlStr = "發現此 <" + PrgNameAry[0] + PGMName + "> 為較舊之版本" + KTConstant.vbCrLf + KTConstant.vbCrLf;
    //        if (!string.IsNullOrEmpty(StrShowFileDate))
    //            SqlStr = SqlStr + "目前版本: " + StrShowFileDate + KTConstant.vbCrLf + KTConstant.vbCrLf;

    //        SqlStr = SqlStr + "最新版本: " + StrShowOracleDate + KTConstant.vbCrLf + KTConstant.vbCrLf;
    //        SqlStr = SqlStr + "將自動執行新程式更新" + KTConstant.vbCrLf + KTConstant.vbCrLf;
    //        SqlStr = SqlStr + "更新程式之後請重新啟動 <" + (!string.IsNullOrEmpty(PGMName) ? PGMName : PrgNameAry[0]) + "> 謝謝";

    //        if (Mode.ToUpper() == "A" | Mode.ToUpper() == "N")
    //        {
    //            Interaction.MsgBox(SqlStr, 64, "光田綜合醫院 資訊室 <重要訊息>");
    //            switch (PrgName) {
    //                case "NURP5000":
    //                case "NURP5000.EXE":
    //                    //護理系統先使用 DISPLAYA 並且不要再執行 Silent的BAT

    //                    string FileNme = "";
    //                    FileNme = "DISPLAYA" + KTDateTime.HHMMSS() + KTString.GetUserID() + System.Guid.NewGuid();
    //                    FileNme = "C:\\ap06bz\\log\\" + FileNme + ".log";

    //                    var wkStr = new System.Text.StringBuilder();
    //                    wkStr.AppendLine("USERID=" + KTString.GetUserID());
    //                    wkStr.AppendLine("USERNAME=" + KTString.GetUserName());
    //                    wkStr.AppendLine("PGMID=" + PrgName);
    //                    System.IO.File.WriteAllText(FileNme, wkStr.ToString(), System.Text.Encoding.Default);
    //                    System.Diagnostics.Process.Start("C:\\ap06bz\\exe\\DISPLAYA.exe", FileNme);
    //                    break;

    //                default:
    //                    //Dim Prg_Str As String = System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName
    //                    System.Diagnostics.Process.Start("C:\\AP06BZ\\EXE\\DISPLAYN.EXE ", PrgName);
    //                    break;
    //            }

    //            if (Mode.ToUpper() == "A")
    //                KTEnvs.KillProcess();

    //            return true;
    //        }
    //        else
    //        {
    //            Interaction.MsgBox(SqlStr, 64, "光田綜合醫院 資訊室<重要訊息>");
    //            return true;
    //        }
    //    }

    //    return false;
    //}

    //public static void OpenEbmPacsImage(wk_user, wk_pwd, wk_accno = "", wk_crtno = "", wk_STUDYDATE1 = "", string 預設開啟院區 = "")
    //{
    //    try {
    //        string PacsArea = "";
    //        string PacsUrl = "";
    //        bool bShow = KTCC.ShowDosqlMsg;
    //        try {
    //            KTCC.ShowDosqlMsg = false;
    //            string SQLSTR = "";
    //            switch (預設開啟院區) {
    //                case "":
    //                    SQLSTR = "SELECT PARAMETER,REMARK FROM ORD3010 WHERE CODE='202' AND TYPE='1'";
    //                    //PACS開啟院區存檔TABLE
    //                    break;
    //                default:
    //                    SQLSTR = "SELECT PARAMETER,REMARK FROM ORD3010@" + 預設開啟院區 + " WHERE CODE='202' AND TYPE='1'";
    //                    //PACS開啟院區存檔TABLE
    //                    break;
    //            }
    //            DataTable TMP = new DataTable();
    //            KTCC.DoSql(SQLSTR, TMP);
    //            ReplaceDBNull(TMP);
    //            if (TMP.Rows.Count > 0) {
    //                PacsArea = TMP.Rows(0)("PARAMETER") + "";
    //                PacsUrl = TMP.Rows(0)("REMARK") + "";
    //            }
    //        } catch (Exception ex2) {
    //        }

    //        KTCC.ShowDosqlMsg = bShow;

    //        //OpenArea = 1 開啟沙鹿PACS
    //        //OpenArea = 2 開啟大甲PACS
    //        //OpenArea = 3 開啟通霄PACS
    //        if (System.IO.Directory.Exists("C:\\AP06BZ\\LOG\\"))
    //            System.IO.Directory.CreateDirectory("C:\\AP06BZ\\LOG\\");
    //        try {
    //            System.IO.File.Delete("C:\\AP06BZ\\LOG\\PACS.TXT");
    //        } catch (Exception ex) {
    //        }
    //        if (System.IO.File.Exists("C:\\AP06BZ\\LOG\\PACS.TXT") == false) {
    //            StreamWriter sw = File.CreateText("C:\\AP06BZ\\LOG\\PACS.TXT");
    //            sw.Close();
    //        }
    //        System.IO.FileStream Target_File = new System.IO.FileStream("C:\\AP06BZ\\LOG\\PACS.TXT", IO.FileMode.Append);
    //        System.IO.StreamWriter Target_Writer_File = new System.IO.StreamWriter(Target_File, System.Text.Encoding.Default);
    //        string Line_Text = "";
    //        Line_Text = "USER=" + wk_user;
    //        Target_Writer_File.WriteLine(Line_Text);
    //        Line_Text = "PASSWORD=" + wk_pwd;
    //        Target_Writer_File.WriteLine(Line_Text);
    //        Line_Text = "CRTNO=" + wk_crtno;
    //        Target_Writer_File.WriteLine(Line_Text);
    //        if (string.IsNullOrEmpty(PacsArea)) {
    //            Line_Text = "WHEREAMI=" + WhereAmI();
    //            Target_Writer_File.WriteLine(Line_Text);
    //        } else {
    //            Line_Text = "WHEREAMI=" + PacsArea;
    //            Target_Writer_File.WriteLine(Line_Text);
    //        }
    //        Line_Text = "ACCNO=" + wk_accno;
    //        Target_Writer_File.WriteLine(Line_Text);
    //        Line_Text = "STUDYDATE1=" + wk_STUDYDATE1;
    //        Target_Writer_File.WriteLine(Line_Text);
    //        Line_Text = "PACSURL=" + PacsUrl;
    //        Target_Writer_File.WriteLine(Line_Text);
    //        Target_Writer_File.Close();
    //    } catch (Exception ex) {
    //    }

    //    Interaction.Shell("C:\\AP06BZ\\EXE\\ORDP1070.EXE", AppWinStyle.Hide);

    //    //'PACS用的Function
    //    //Dim strEbmUrl As String

    //    //If WhereAmI() = 1 Then
    //    //    strEbmUrl = "http://192.73.141.15/DicomWeb/DicomWeb.dll/Login?User=" & wk_user$ & "&Password=" & wk_pwd$ & "&PTNID=" & wk_crtno$
    //    //    Process.Start(strEbmUrl)  '.NET的用法
    //    //    'Dim InBuffer As String = "User=" & wk_user$ & "_Password=" & wk_pwd$ & "_PTNID=" & wk_crtno$
    //    //    'Dim startInfo As New ProcessStartInfo("IExplore.exe")
    //    //    'startInfo.Arguments = "http://192.73.141.34/ASP/DEFAULT.asp?url=http://192.73.141.15/DicomWeb/DicomWeb.dll/Login?" & InBuffer
    //    //    'Process.Start(startInfo)
    //    //Else
    //    //    strEbmUrl = "http://192.73.144.31/DicomWeb/DicomWeb.dll/Login?User=" & wk_user$ & "&Password=" & wk_pwd$ & "&PTNID=" & wk_crtno$
    //    //    Process.Start(strEbmUrl)  '.NET的用法
    //    //End If

    //    //'Try
    //    //'    Dim iBufferLen As Integer
    //    //'    Dim InBuffer As String = "User=" & wk_user$ & "&Password=" & wk_pwd$ & "&PTNID=" & wk_crtno$
    //    //'    Dim OutBuffer As String
    //    //'    Dim OutString As String = ""

    //    //'    iBufferLen = EncodeURLParamStr(InBuffer, Nothing)
    //    //'    OutBuffer = Space(iBufferLen - 1)

    //    //'    Call EncodeURLParamStr(InBuffer, OutBuffer)

    //    //'    If WhereAmI() = 1 Then
    //    //'        strEbmUrl = "http://192.73.141.15/DicomWeb/DicomWeb.dll/Login?" & OutBuffer
    //    //'    Else
    //    //'        strEbmUrl = "http://192.73.144.31/DicomWeb/DicomWeb.dll/Login?" & OutBuffer
    //    //'    End If
    //    //'    Process.Start(strEbmUrl)  '.NET的用法
    //    //'Catch ex As Exception
    //    //'    MsgBox(ex.Message)
    //    //'End Try

    //    //'Process.Start("IExplore.exe", strEbmUrl)
    //    //'ShellExecute(1, "open", strEbmUrl, "", "", SW_SHOWNORMAL)
    //}

    public static bool OpenURLWithGoogleChrome(string URL, bool 仍接受以IE開啟 = false)
    {
        string[] GoogleChrome = new string[3];

        GoogleChrome[0] = "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe";
        GoogleChrome[1] = "C:\\ChromeV34\\GoogleChromePortable.exe";
        GoogleChrome[2] = "C:\\GoogleChromePortable32 v34\\GoogleChromePortable.exe";

        int i = 0;
        for (i = 0; i <= GoogleChrome.Length - 1; i++)
        {
            if (System.IO.File.Exists(GoogleChrome[i]) == true)
            {
                System.Diagnostics.Process.Start(GoogleChrome[i], URL);
                return true;
            }
        }

        if (仍接受以IE開啟 == true)
        {
            //以上Google的路徑都不在的話，則以IE開啟，但網頁會錯誤，無法正常顯示
            //MsgBox("本台電腦尚未安裝GoogleChrome瀏覽器，會造成填寫手術前護理紀錄錯誤", MsgBoxStyle.Critical)
            System.Diagnostics.Process.Start(URL);
            return true;
        }

        return false;
    }

    public void Dispose()
    {
        GC.SuppressFinalize(this); //Hey, GC: don't bother calling finalize later
    }
}
//}