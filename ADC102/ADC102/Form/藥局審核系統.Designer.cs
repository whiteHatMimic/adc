﻿namespace ADC102
{
    partial class 藥局審核系統
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(藥局審核系統));
            this.btnClose = new System.Windows.Forms.Button();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.cboStkcod = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ctdbgADC102List = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
            this.rtbReason = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCrtno = new System.Windows.Forms.Label();
            this.lblPtname = new System.Windows.Forms.Label();
            this.lblBrdte = new System.Windows.Forms.Label();
            this.lblPtsex = new System.Windows.Forms.Label();
            this.lblVsdte = new System.Windows.Forms.Label();
            this.lblVsapn = new System.Windows.Forms.Label();
            this.lblDptname = new System.Windows.Forms.Label();
            this.lblVsdr = new System.Windows.Forms.Label();
            this.btnArgee = new System.Windows.Forms.Button();
            this.btnDisArgee = new System.Windows.Forms.Button();
            this.picDrug = new System.Windows.Forms.PictureBox();
            this.panelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctdbgADC102List)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDrug)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnClose.Location = new System.Drawing.Point(889, 571);
            this.btnClose.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 40);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "離開";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.panelMenu.Controls.Add(this.cboStkcod);
            this.panelMenu.Controls.Add(this.label2);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMenu.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(1008, 47);
            this.panelMenu.TabIndex = 6;
            // 
            // cboStkcod
            // 
            this.cboStkcod.BackColor = System.Drawing.Color.White;
            this.cboStkcod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStkcod.FormattingEnabled = true;
            this.cboStkcod.ItemHeight = 16;
            this.cboStkcod.Items.AddRange(new object[] {
            "全部"});
            this.cboStkcod.Location = new System.Drawing.Point(99, 10);
            this.cboStkcod.Margin = new System.Windows.Forms.Padding(10);
            this.cboStkcod.Name = "cboStkcod";
            this.cboStkcod.Size = new System.Drawing.Size(220, 24);
            this.cboStkcod.TabIndex = 2;
            this.cboStkcod.SelectedIndexChanged += new System.EventHandler(this.cboStkcod_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(19, 13);
            this.label2.Margin = new System.Windows.Forms.Padding(10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "護理站:";
            // 
            // ctdbgADC102List
            // 
            this.ctdbgADC102List.AllowSort = false;
            this.ctdbgADC102List.AllowUpdate = false;
            this.ctdbgADC102List.AllowUpdateOnBlur = false;
            this.ctdbgADC102List.AlternatingRows = true;
            this.ctdbgADC102List.CaptionHeight = 19;
            this.ctdbgADC102List.EmptyRows = true;
            this.ctdbgADC102List.FlatStyle = C1.Win.C1TrueDBGrid.FlatModeEnum.Flat;
            this.ctdbgADC102List.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ctdbgADC102List.GroupByCaption = "Drag a column header here to group by that column";
            this.ctdbgADC102List.Images.Add(((System.Drawing.Image)(resources.GetObject("ctdbgADC102List.Images"))));
            this.ctdbgADC102List.Location = new System.Drawing.Point(22, 60);
            this.ctdbgADC102List.Margin = new System.Windows.Forms.Padding(10);
            this.ctdbgADC102List.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow;
            this.ctdbgADC102List.Name = "ctdbgADC102List";
            this.ctdbgADC102List.PreviewInfo.Location = new System.Drawing.Point(0, 0);
            this.ctdbgADC102List.PreviewInfo.Size = new System.Drawing.Size(0, 0);
            this.ctdbgADC102List.PreviewInfo.ZoomFactor = 75D;
            this.ctdbgADC102List.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("ctdbgADC102List.PrintInfo.PageSettings")));
            this.ctdbgADC102List.RowHeight = 24;
            this.ctdbgADC102List.Size = new System.Drawing.Size(970, 300);
            this.ctdbgADC102List.TabIndex = 7;
            this.ctdbgADC102List.Text = "審核清單";
            this.ctdbgADC102List.DoubleClick += new System.EventHandler(this.ctdbgADC102List_DoubleClick);
            this.ctdbgADC102List.PropBag = resources.GetString("ctdbgADC102List.PropBag");
            // 
            // rtbReason
            // 
            this.rtbReason.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtbReason.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.rtbReason.Location = new System.Drawing.Point(22, 462);
            this.rtbReason.Margin = new System.Windows.Forms.Padding(10);
            this.rtbReason.MaxLength = 50;
            this.rtbReason.Name = "rtbReason";
            this.rtbReason.Size = new System.Drawing.Size(759, 102);
            this.rtbReason.TabIndex = 8;
            this.rtbReason.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(19, 441);
            this.label1.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 16);
            this.label1.TabIndex = 9;
            this.label1.Text = "原因:";
            // 
            // lblCrtno
            // 
            this.lblCrtno.AutoSize = true;
            this.lblCrtno.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblCrtno.Location = new System.Drawing.Point(19, 370);
            this.lblCrtno.Name = "lblCrtno";
            this.lblCrtno.Size = new System.Drawing.Size(64, 16);
            this.lblCrtno.TabIndex = 10;
            this.lblCrtno.Text = "病歷號: ";
            // 
            // lblPtname
            // 
            this.lblPtname.AutoSize = true;
            this.lblPtname.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblPtname.Location = new System.Drawing.Point(216, 370);
            this.lblPtname.Name = "lblPtname";
            this.lblPtname.Size = new System.Drawing.Size(80, 16);
            this.lblPtname.TabIndex = 10;
            this.lblPtname.Text = "患者姓名: ";
            // 
            // lblBrdte
            // 
            this.lblBrdte.AutoSize = true;
            this.lblBrdte.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblBrdte.Location = new System.Drawing.Point(422, 370);
            this.lblBrdte.Name = "lblBrdte";
            this.lblBrdte.Size = new System.Drawing.Size(44, 16);
            this.lblBrdte.TabIndex = 11;
            this.lblBrdte.Text = "生日:";
            // 
            // lblPtsex
            // 
            this.lblPtsex.AutoSize = true;
            this.lblPtsex.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblPtsex.Location = new System.Drawing.Point(603, 370);
            this.lblPtsex.Name = "lblPtsex";
            this.lblPtsex.Size = new System.Drawing.Size(48, 16);
            this.lblPtsex.TabIndex = 12;
            this.lblPtsex.Text = "性別: ";
            // 
            // lblVsdte
            // 
            this.lblVsdte.AutoSize = true;
            this.lblVsdte.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblVsdte.Location = new System.Drawing.Point(22, 406);
            this.lblVsdte.Name = "lblVsdte";
            this.lblVsdte.Size = new System.Drawing.Size(76, 16);
            this.lblVsdte.TabIndex = 13;
            this.lblVsdte.Text = "門診日期:";
            // 
            // lblVsapn
            // 
            this.lblVsapn.AutoSize = true;
            this.lblVsapn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblVsapn.Location = new System.Drawing.Point(216, 406);
            this.lblVsapn.Name = "lblVsapn";
            this.lblVsapn.Size = new System.Drawing.Size(44, 16);
            this.lblVsapn.TabIndex = 13;
            this.lblVsapn.Text = "時段:";
            // 
            // lblDptname
            // 
            this.lblDptname.AutoSize = true;
            this.lblDptname.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblDptname.Location = new System.Drawing.Point(422, 406);
            this.lblDptname.Name = "lblDptname";
            this.lblDptname.Size = new System.Drawing.Size(76, 16);
            this.lblDptname.TabIndex = 13;
            this.lblDptname.Text = "醫師名稱:";
            // 
            // lblVsdr
            // 
            this.lblVsdr.AutoSize = true;
            this.lblVsdr.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lblVsdr.Location = new System.Drawing.Point(603, 406);
            this.lblVsdr.Name = "lblVsdr";
            this.lblVsdr.Size = new System.Drawing.Size(76, 16);
            this.lblVsdr.TabIndex = 13;
            this.lblVsdr.Text = "醫師代碼:";
            // 
            // btnArgee
            // 
            this.btnArgee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.btnArgee.FlatAppearance.BorderSize = 0;
            this.btnArgee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnArgee.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnArgee.Location = new System.Drawing.Point(19, 574);
            this.btnArgee.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.btnArgee.Name = "btnArgee";
            this.btnArgee.Size = new System.Drawing.Size(100, 40);
            this.btnArgee.TabIndex = 15;
            this.btnArgee.Text = "同意";
            this.btnArgee.UseVisualStyleBackColor = false;
            this.btnArgee.Click += new System.EventHandler(this.btnArgee_Click);
            // 
            // btnDisArgee
            // 
            this.btnDisArgee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.btnDisArgee.FlatAppearance.BorderSize = 0;
            this.btnDisArgee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDisArgee.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnDisArgee.Location = new System.Drawing.Point(139, 574);
            this.btnDisArgee.Margin = new System.Windows.Forms.Padding(10, 5, 10, 5);
            this.btnDisArgee.Name = "btnDisArgee";
            this.btnDisArgee.Size = new System.Drawing.Size(100, 40);
            this.btnDisArgee.TabIndex = 16;
            this.btnDisArgee.Text = "不同意";
            this.btnDisArgee.UseVisualStyleBackColor = false;
            this.btnDisArgee.Click += new System.EventHandler(this.btnDisArgee_Click);
            // 
            // picDrug
            // 
            this.picDrug.Location = new System.Drawing.Point(794, 370);
            this.picDrug.Name = "picDrug";
            this.picDrug.Size = new System.Drawing.Size(198, 192);
            this.picDrug.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picDrug.TabIndex = 17;
            this.picDrug.TabStop = false;
            // 
            // 藥局審核系統
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1008, 628);
            this.ControlBox = false;
            this.Controls.Add(this.picDrug);
            this.Controls.Add(this.btnDisArgee);
            this.Controls.Add(this.btnArgee);
            this.Controls.Add(this.lblVsdr);
            this.Controls.Add(this.lblDptname);
            this.Controls.Add(this.lblVsapn);
            this.Controls.Add(this.lblVsdte);
            this.Controls.Add(this.lblPtsex);
            this.Controls.Add(this.lblBrdte);
            this.Controls.Add(this.lblPtname);
            this.Controls.Add(this.lblCrtno);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rtbReason);
            this.Controls.Add(this.ctdbgADC102List);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Name = "藥局審核系統";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "藥局審核系統";
            this.Load += new System.EventHandler(this.藥局審核系統_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.藥局審核系統_KeyDown);
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctdbgADC102List)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDrug)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.ComboBox cboStkcod;
        private System.Windows.Forms.Label label2;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid ctdbgADC102List;
        private System.Windows.Forms.RichTextBox rtbReason;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCrtno;
        private System.Windows.Forms.Label lblPtname;
        private System.Windows.Forms.Label lblBrdte;
        private System.Windows.Forms.Label lblPtsex;
        private System.Windows.Forms.Label lblVsdte;
        private System.Windows.Forms.Label lblVsapn;
        private System.Windows.Forms.Label lblDptname;
        private System.Windows.Forms.Label lblVsdr;
        private System.Windows.Forms.Button btnArgee;
        private System.Windows.Forms.Button btnDisArgee;
        private System.Windows.Forms.PictureBox picDrug;
    }
}

