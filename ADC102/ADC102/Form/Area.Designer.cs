﻿namespace ADC102
{
    partial class Area
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbKTGH03 = new System.Windows.Forms.RadioButton();
            this.rbTSHIS = new System.Windows.Forms.RadioButton();
            this.rbHPK210 = new System.Windows.Forms.RadioButton();
            this.rbKTGH00 = new System.Windows.Forms.RadioButton();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rbKTGH03
            // 
            this.rbKTGH03.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.rbKTGH03.Location = new System.Drawing.Point(443, 48);
            this.rbKTGH03.Margin = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.rbKTGH03.Name = "rbKTGH03";
            this.rbKTGH03.Padding = new System.Windows.Forms.Padding(5);
            this.rbKTGH03.Size = new System.Drawing.Size(100, 30);
            this.rbKTGH03.TabIndex = 8;
            this.rbKTGH03.Text = "測試院區";
            this.rbKTGH03.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbKTGH03.UseVisualStyleBackColor = true;
            // 
            // rbTSHIS
            // 
            this.rbTSHIS.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.rbTSHIS.Location = new System.Drawing.Point(313, 48);
            this.rbTSHIS.Margin = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.rbTSHIS.Name = "rbTSHIS";
            this.rbTSHIS.Padding = new System.Windows.Forms.Padding(5);
            this.rbTSHIS.Size = new System.Drawing.Size(100, 30);
            this.rbTSHIS.TabIndex = 7;
            this.rbTSHIS.Text = "通霄院區";
            this.rbTSHIS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbTSHIS.UseVisualStyleBackColor = true;
            // 
            // rbHPK210
            // 
            this.rbHPK210.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.rbHPK210.Location = new System.Drawing.Point(183, 48);
            this.rbHPK210.Margin = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.rbHPK210.Name = "rbHPK210";
            this.rbHPK210.Padding = new System.Windows.Forms.Padding(5);
            this.rbHPK210.Size = new System.Drawing.Size(100, 30);
            this.rbHPK210.TabIndex = 6;
            this.rbHPK210.Text = "大甲院區";
            this.rbHPK210.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbHPK210.UseVisualStyleBackColor = true;
            // 
            // rbKTGH00
            // 
            this.rbKTGH00.Checked = true;
            this.rbKTGH00.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.rbKTGH00.Location = new System.Drawing.Point(53, 48);
            this.rbKTGH00.Margin = new System.Windows.Forms.Padding(15, 10, 15, 10);
            this.rbKTGH00.Name = "rbKTGH00";
            this.rbKTGH00.Padding = new System.Windows.Forms.Padding(5);
            this.rbKTGH00.Size = new System.Drawing.Size(100, 30);
            this.rbKTGH00.TabIndex = 5;
            this.rbKTGH00.TabStop = true;
            this.rbKTGH00.Text = "沙鹿院區";
            this.rbKTGH00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbKTGH00.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Location = new System.Drawing.Point(443, 103);
            this.btnClose.Margin = new System.Windows.Forms.Padding(10, 15, 10, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Padding = new System.Windows.Forms.Padding(5);
            this.btnClose.Size = new System.Drawing.Size(100, 40);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "離開";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(84)))), ((int)(((byte)(124)))));
            this.btnConfirm.FlatAppearance.BorderSize = 0;
            this.btnConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirm.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnConfirm.ForeColor = System.Drawing.Color.White;
            this.btnConfirm.Location = new System.Drawing.Point(323, 103);
            this.btnConfirm.Margin = new System.Windows.Forms.Padding(10, 15, 10, 15);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Padding = new System.Windows.Forms.Padding(5);
            this.btnConfirm.Size = new System.Drawing.Size(100, 40);
            this.btnConfirm.TabIndex = 9;
            this.btnConfirm.Text = "確認";
            this.btnConfirm.UseVisualStyleBackColor = false;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // Area
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 171);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.rbKTGH03);
            this.Controls.Add(this.rbTSHIS);
            this.Controls.Add(this.rbHPK210);
            this.Controls.Add(this.rbKTGH00);
            this.Name = "Area";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "院區選項";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton rbKTGH03;
        private System.Windows.Forms.RadioButton rbTSHIS;
        private System.Windows.Forms.RadioButton rbHPK210;
        private System.Windows.Forms.RadioButton rbKTGH00;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnConfirm;
    }
}