﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;

using ADC102.VM;
using ADC102.Models;
using KTDB;

namespace ADC102
{
    public partial class 藥局審核系統 : Form
    {
        private KTConnectionControler KTCC; //連線 class
        private string stkcod = "全部"; //護理站
        private string hsp = mdArea.hsp;
        private string userId = mdUser.userId; //使用者
        private string userName = mdUser.userName; //使用者名稱
        private Button currentButton;   //儲存同意、不同意按鈕
        private static List<mdPicDrugs> picDrugList = new List<mdPicDrugs>(); //儲存圖片清單，完整路徑、圖片名稱
        //private enum Area: int
        //{
        //    KTGH00=1,
        //    HPK210=2,
        //    TSHIS=3,
        //    KTGH03=4
        //}

        //圖片資訊類型
        private class mdPicDrugs
        {
            public string path { get; set; }
            public string fileName { get; set; }
        }

        public 藥局審核系統()
        {
            InitializeComponent();
            userId = KTString.GetUserID();
            userName = KTString.GetUserName();
        }

        //重抓未審核過的資料
        private void ReloadADC102Data(string stkcod)
        {
            DataTable dtADC102 = new mdADC102Service(KTCC).GetADC102Drug(stkcod);
            //其他方法 Class
            mdOtherFun mdOtherFun = new mdOtherFun();
            
            foreach (DataRow item in dtADC102.Rows)
            {
                item["ORDDTE"] = item["ORDDTE"].ToString().Substring(0, 3) + "/" + item["ORDDTE"].ToString().Substring(3, 2) + "/" + item["ORDDTE"].ToString().Substring(5, 2);
                item["ORDTME"] = item["ORDTME"].ToString().Substring(0, 2) + ":" + item["ORDTME"].ToString().Substring(2, 2);

                //呼叫其他方法，轉換來源轉換，數字轉換中文
                item["SRFLG"] = mdOtherFun.ConvertNumberToChineseSrflg(item["SRFLG"].ToString());
            }

            ctdbgADC102List.SetDataBinding(dtADC102, "", true);
        }

        //確認圖片路徑是否存在
        private bool checkTbDrugStatus()
        {
            string chkPath = @"C:\ap06bz\PICTURE\tbDrug";

            if (Directory.Exists(chkPath) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //取得所有藥品圖片位置
        private void GetAllDrugPicturePath()
        {
            DirectoryInfo di = new DirectoryInfo(@"C:\AP06BZ\PICTURE\tbDrug");

            foreach (var fi in di.GetFiles())
            {
                picDrugList.Add(new mdPicDrugs
                {
                    path = fi.FullName,   //完整檔案路徑
                    fileName = fi.Name    //檔案名稱
                });
            }
        }

        //取得唯一藥品圖片
        private void GetOnlyDrugPic(string ordid)
        {
            int idx = 0;

            for (int i = 0; i < picDrugList.Count; i++)
            {
                string str = picDrugList[i].fileName.Split('-')[0];
                
                if (str == ordid)
                {
                    idx = i;

                    picDrug.Image = Image.FromFile(picDrugList[i].path);
                    break;
                }
            }
        }

        //更新審核資料
        private void UpdateADC102(object btnSender)
        {
            //其他方法 Class
            mdOtherFun mdOtherFun = new mdOtherFun();
            ADC102Key ADC102Key = new ADC102Key()
            {
                CRTNO = ctdbgADC102List.Columns["CRTNO"].Text,
                ACTNO = ctdbgADC102List.Columns["ACTNO"].Text,
                SRFLG = mdOtherFun.ConvertCheineseToNumberSrflg(ctdbgADC102List.Columns["SRFLG"].Text),
                ORDID = ctdbgADC102List.Columns["ORDID"].Text,
                ORDDTE = ctdbgADC102List.Columns["ORDDTE"].Text.Replace("/", ""),
                ORDTME = ctdbgADC102List.Columns["ORDTME"].Text.Replace(":", ""),
                SCRNO = ctdbgADC102List.Columns["SCRNO"].Text,
                STKCOD = ctdbgADC102List.Columns["STKCOD"].Text
            };

            //確認是同意按鈕、不同意按鈕
            if (btnSender != null)
            {
                if (currentButton != (Button)btnSender)
                {
                    currentButton = (Button)btnSender;
                }
            }

            //新更新審核物件
            newUpdateADC102VM newObj = new newUpdateADC102VM();
            newObj.REASON = rtbReason.Text;
            newObj.AGREEUPOP = userId;
            newObj.AGREEFLG = string.Empty;
            newObj.AGREEDTE = KTDateTime.Today();
            newObj.AGREETME = KTDateTime.HHMMSS();

            //同意按鈕註記為 'Y'，不同意按鈕注意為 'N'
            if (currentButton.Text == "同意")
            {
                newObj.AGREEFLG = "Y";
            }
            else
            {
                newObj.AGREEFLG = "N";
            }

            //檢查不同意時，是否有寫原因
            if (newObj.AGREEFLG == "N")
            {
                if (newObj.REASON == "")
                {
                    MessageBox.Show("不同意時需填寫原因", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else
            {
                newObj.REASON = "";
                rtbReason.Text = "";
            }

            /*
             * 開始執行資料處理
             */
            try
            {
                using (KTCC = new KTConnectionControler())
                {
                    KTCC.Connect(hsp, CnHomeFlg: true);
                    KTCC.BeginTransaction();
                    KTCC.RunExecute = false;

                    //檢查審核資料存不存在
                    var oldObj = new mdADC102Service(KTCC).InspactADC102<oldUpdateADC102VM>(ADC102Key);
                    if (oldObj == null)
                    {
                        MessageBox.Show("查無此筆審核資料", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    var prs014RegisterInfo = new mdPRS014Service(KTCC).GetPrs014RegisterInfo<PRS014VM>(ADC102Key.CRTNO, ADC102Key.ACTNO);
                    if (prs014RegisterInfo == null)
                    {
                        MessageBox.Show("查無掛號資訊", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    //傳送給廠商欄位
                    ADCSupport ADCSupport = new ADCSupport()
                    {
                        //處方日期
                        ORDDTE = ctdbgADC102List.Columns["ORDDTE"].Text.Replace("/", ""),
                        //處方時間
                        ORDTME = ctdbgADC102List.Columns["ORDTME"].Text.Replace(":", ""),
                        //單位/庫別
                        STKCOD = ctdbgADC102List.Columns["STKCOD"].Text,
                        //病歷號
                        CRTNO = ctdbgADC102List.Columns["CRTNO"].Text,
                        //性別
                        PTSEX = prs014RegisterInfo.PTSEX,
                        //患者姓名
                        PTNAME = prs014RegisterInfo.PTNAME,
                        //生日
                        BRDTE = prs014RegisterInfo.BRDTE,
                        //床號
                        VSLIN = ctdbgADC102List.Columns["VSCLIN"].Text,
                        //流水號
                        ACTNO = ctdbgADC102List.Columns["ACTNO"].Text,
                        //來源
                        SRFLG = mdOtherFun.ConvertCheineseToNumberSrflg(ctdbgADC102List.Columns["SRFLG"].Text),
                        //序號
                        SCRNO = ctdbgADC102List.Columns["SCRNO"].Text,
                        //醫令碼
                        ORDID = ctdbgADC102List.Columns["ORDID"].Text,
                        //劑量
                        DOSAGE = ctdbgADC102List.Columns["DOSAGE"].Text,
                        //開藥單位
                        PKUNT = ctdbgADC102List.Columns["PKUNT"].Text,
                        //給藥量
                        ORDAMT = ctdbgADC102List.Columns["ORDAMT"].Text,
                        //給藥單位
                        BIUNT = ctdbgADC102List.Columns["BIUNT"].Text,
                        //頻率
                        FREQ = ctdbgADC102List.Columns["FREQ"].Text,
                        //審核藥師員編
                        AGREEUPOP = userId
                    };
                    
                    NameValueCollection data = new NameValueCollection();
                    data["data"] = JsonConvert.SerializeObject(ADCSupport);

                    string url = "http://192.73.144.96/miniSERV_ADC/foo/websvc/psc/?data=[" + data["data"] + "]";
                    string r_txt = "";
                    string[] arrSplit = new string[] { };

                    r_txt = new WebClient().DownloadString(url);
                    r_txt = r_txt.Replace("{", "").Replace("}", "").Replace("\"", "");
                    arrSplit = r_txt.Split(':');

                    if (arrSplit[1].ToString() == "true")
                    {
                        MessageBox.Show("回傳廠商成功", "資訊", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        //更新藥師是否同意、不同意
                        new mdADC102Service(KTCC).UpdateADC102(ADC102Key, newObj, oldObj);

                        if (KTCC.Rowindicator == 1)
                        {
                            KTCC.Commit();
                            //重抓須審核資料
                            MessageBox.Show("更新資料成功", "資訊", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ReloadADC102Data(stkcod);
                        }
                        else
                        {
                            KTCC.Rollback();
                            MessageBox.Show("更新資料失敗", "警告", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("回傳廠商失敗", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                rtbReason.Text = "";
                newObj.REASON = "";
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //初始化執行
        private void 藥局審核系統_Load(object sender, EventArgs e)
        {
            //切換護理站切換預設 '全部' 選項
            cboStkcod.SelectedIndex = 0;

            //確認圖片路徑是否存在，如果存在執行批次下在圖片
            if (checkTbDrugStatus() == true)
            {
                Process.Start(@"C:\AP06BZ\EXE\ORDP1051.EXE");
            }
            else
            {
                Process.Start(@"C:\AP06BZ\PICTURE\drugzip.exe");
            }

            //取得所有圖片位置及名稱
            GetAllDrugPicturePath();

            try
            {
                using (KTCC = new KTConnectionControler())
                {
                    KTCC.RunExecute = false;
                    KTCC.Connect(hsp, CnHomeFlg: true);
                    this.Text = this.Text + " ";
                    this.Text = this.Text + KTEnvs.GetLocationInChinese(hsp) + " ";
                    this.Text = this.Text + new KTDataBase(KTCC).HISProgramer() + " ";
                    this.Text = this.Text + "登入人員: " + userId + "  " + userName;

                    //抓取單位代碼及名稱
                    var prs002List = new mdPRS002Service(KTCC).GetPrs002Stkcod<PRS002VM>();

                    foreach(var obj in prs002List)
                    {
                        cboStkcod.Items.Add(obj.DPTID + "  " + obj.FNAME);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 點擊同意按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnArgee_Click(object sender, EventArgs e)
        {
            UpdateADC102(sender);
        }

        /// <summary>
        /// 點擊不同意按鈕
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDisArgee_Click(object sender, EventArgs e)
        {
            UpdateADC102(sender);
        }

        /// <summary>
        /// 連點兩下尋找掛號資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ctdbgADC102List_DoubleClick(object sender, EventArgs e)
        {
            string crtno = ctdbgADC102List.Columns["CRTNO"].Text;
            string actno = ctdbgADC102List.Columns["ACTNO"].Text;
            string ordid = ctdbgADC102List.Columns["ORDID"].Text;

            //取得藥品圖片
            GetOnlyDrugPic(ordid);

            try
            {
                using (KTCC = new KTConnectionControler())
                {
                    KTCC.RunExecute = false;
                    KTCC.Connect(hsp, CnHomeFlg: true);

                    //尋找此患者掛號資訊
                    var prs014RegisterInfo = new mdPRS014Service(KTCC).GetPrs014RegisterInfo<PRS014VM>(crtno, actno);

                    if (prs014RegisterInfo != null)
                    {
                        lblCrtno.Text = "病歷號: " + prs014RegisterInfo.CRTNO;
                        lblPtname.Text = "患者姓名: " + prs014RegisterInfo.PTNAME;
                        lblBrdte.Text = "生日: " + prs014RegisterInfo.BRDTE.Substring(0, 3) + "/" + prs014RegisterInfo.BRDTE.Substring(3, 2) + "/" + prs014RegisterInfo.BRDTE.Substring(5, 2);
                        lblPtsex.Text = "性別: " + prs014RegisterInfo.PTSEX;
                        lblVsdte.Text = "門診日: " + prs014RegisterInfo.VSDTE.Substring(0, 3) + "/" + prs014RegisterInfo.VSDTE.Substring(3, 2) + "/" + prs014RegisterInfo.VSDTE.Substring(5, 2);
                        lblVsapn.Text = "時段: " + prs014RegisterInfo.VSAPN;
                        lblDptname.Text = "科別: " + prs014RegisterInfo.DPTNAME;
                        lblVsdr.Text = "醫師: " + prs014RegisterInfo.VSDR;
                    }
                    else
                    {
                        MessageBox.Show("查無患者掛號資料", "資訊", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //選擇單位時，抓出單位未審核資料
        private void cboStkcod_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                stkcod = (cboStkcod.Text == "全部") ? cboStkcod.Text: cboStkcod.Text.Substring(0, 4);

                using (KTCC = new KTConnectionControler())
                {
                    KTCC.RunExecute = false;
                    KTCC.Connect(hsp, CnHomeFlg: true);
                    ReloadADC102Data(stkcod);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 模擬 tab key
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 藥局審核系統_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("${Tab}");
            }
        }

        /// <summary>
        /// 離開畫面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
