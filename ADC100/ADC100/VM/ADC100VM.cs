﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ADC100.VM
{
    /// <summary>
    /// 查詢 ADC100 設備清單
    /// </summary>
    /*public class ADC100VM
    {
        // ADC 機器編號
        public string ADC_NO { get; set; }
        // 單位代碼/庫別
        public string STKCOD { get; set; }
        // 位置描述
        public string LOCATION_DESCRIPTION { get; set; }
        // ADC 狀態
        public string STATUS { get; set; }
        // 故意多綁維護欄位，要綁定 GridViews
        public string MODIFY_ADC { get; set; }
    }*/

    /// <summary>
    /// 新增 ADC100 資料
    /// </summary>
    public class InsertADC100VM
    {
        // ADC 機器編號
        public string ADC_NO { get; set; }
        // 單位代碼/庫別
        public string STKCOD { get; set; }
        // 位置描述
        public string LOCATION_DESCRIPTION { get; set; }
        // ADC 狀態
        public int STATUS { get; set; }
        // 建立人員
        public string SUID { get; set; }
        // 起始建立日期
        public string SDTE { get; set; }
        // 起始建立時間
        public string STME { get; set; }
        // 更新人員
        public string UPOP { get; set; }
        // 更新日期
        public string UPDTE { get; set; }
        // 更新時間
        public string UPTME { get; set; }
    }

    /// <summary>
    /// 舊 ADC100 資料
    /// </summary>
    public class oldUpdateADC100VM
    {
        // 單位代碼
        public string STKCOD { get; set; }
        // 位置描述
        public string LOCATION_DESCRIPTION { get; set; }
        // ADC 狀態
        public int STATUS { get; set; }
        // 更新人員
        public string UPOP { get; set; }
        // 更新日期
        public string UPDTE { get; set; }
        // 更新時間
        public string UPTME { get; set; }
    }

    /// <summary>
    /// 更新 ADC100 設備資料
    /// </summary>
    public class newUpdateADC100VM
    {
        // 單位代碼
        public string STKCOD { get; set; }
        // 位置描述
        public string LOCATION_DESCRIPTION { get; set; }
        // ADC 狀態
        public int STATUS { get; set; }
        // 更新人員
        public string UPOP { get; set; }
        // 更新日期
        public string UPDTE { get; set; }
        // 更新時間
        public string UPTME { get; set; }
    }

    /// <summary>
    /// 刪除 ADC 資料
    /// </summary>
    public class DeleteADC100VM
    {
        // ADC 機器編號
        public string ADC_NO { get; set; }
        // 單位代碼/庫別
        public string STKCOD { get; set; }
        // 位置描述
        public string LOCATION_DESCRIPTION { get; set; }
        // ADC 狀態
        public int STATUS { get; set; }
        // 更新人員
        public string UPOP { get; set; }
        // 更新日期
        public string UPDTE { get; set; }
        // 更新時間
        public string UPTME { get; set; }
    }
}
