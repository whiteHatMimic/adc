﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using Dapper;
using KTDB;
using ADC100.VM;


namespace ADC100.Models
{
    public class mdADC100Service
    {
        private KTConnectionControler KTCC;

        public string sqlStr = string.Empty;

        public mdADC100Service(KTConnectionControler _KTCC)
        {
            this.KTCC = _KTCC;
        }

        /// <summary>
        /// 取得 ADC 設備清單
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public DataTable GetADC100DeviceList()
        {
            DataTable dtADC100 = new DataTable("ADC100");
            sqlStr = @"SELECT ADC100.ADC_NO,
                              ADC100.STKCOD || '  ' || PRS002.FNAME AS STKCOD,
                              ADC100.LOCATION_DESCRIPTION,
                              CASE ADC100.STATUS
                                WHEN 0 THEN '停用'
                                       ELSE '啟用'
                              END AS STATUS,
                              '藥品維護' AS MODIFY_ADC
                         FROM ADC100
                              LEFT OUTER JOIN (SELECT DPTID,
                                                      FNAME
                                                 FROM PRS002
                                                   WHERE     DTFLG      = '2' 
                                                         AND VISIBLEFLG = 'Y' ) PRS002
                                  ON ( PRS002.DPTID = ADC100.STKCOD ) ORDER BY ADC_NO ASC
            ";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            KTCC.DoSql(cmd, dtADC100);

            return dtADC100;
        }

        /// <summary>
        /// 檢查有無重複 ADC 資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="adcNo">ADC 設備編號</param>
        /// <returns></returns>
        public T InspactADC100<T>(string adcNo)
        {
            sqlStr = @"SELECT ADC_NO,
                              STKCOD,
                              LOCATION_DESCRIPTION,
                              STATUS,
                              UPOP,
                              UPDTE,
                              UPTME
                       FROM ADC100 WHERE ADC_NO = :ADC_NO
            ";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            cmd.Parameters("ADC_NO", adcNo);
            var result = KTCC.DoSql<T>(cmd).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增 ADC 設備資料
        /// </summary>
        /// <param name="insertADC100">ADC 新增資料</param>
        public void InsertADC100Device(InsertADC100VM insertADC100)
        {
            DynamicParameters param;
            KTCC.CreateInsertSql(insertADC100, "ADC100", out sqlStr, out param);
            KTCC.DoSql(sqlStr, param);
        }

        /// <summary>
        /// 更新 ADC100 設備資料
        /// </summary>
        /// <param name="adcNo">ADC 設備編號</param>
        /// <param name="newObj">新 ADC 資料</param>
        /// <param name="oldObj">舊 ADC 資料</param>
        public void UpdateADC100Device(string adcNo, newUpdateADC100VM newObj, oldUpdateADC100VM oldObj)
        {
            DynamicParameters param;
            KTCC.CreateUpdateSql(newObj, oldObj, "ADC100", out sqlStr, out param, " WHERE ADC_NO = :ADC_NO", new { ADC_NO = adcNo });
            KTCC.DoSql(sqlStr, param);
        }

        /// <summary>
        /// 刪除 ADC 設備資料
        /// </summary>
        /// <param name="adcNo">ADC 設備編號</param>
        public void DeleteADC100Device(string adcNo)
        {
            sqlStr = "DELETE FROM ADC100 WHERE ADC_NO = :ADC_NO";
            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            cmd.Parameters("ADC_NO", adcNo);
            KTCC.DoSql(cmd);
        }
    }
}
