﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KTDB;
using ADC100.VM;
using Dapper;

namespace ADC100.Models
{
    public class mdADC101LOGService
    {
        private KTConnectionControler KTCC;

        public string sqlStr = string.Empty;

        public mdADC101LOGService(KTConnectionControler _KTCC)
        {
            KTCC = _KTCC;
        }

        /// <summary>
        /// 紀錄誰刪除藥品
        /// </summary>
        public void InsertADC101DelLog(InsertADC101LOGVM insertADC101)
        {
            DynamicParameters param;
            KTCC.CreateInsertSql(insertADC101, "ADC101_LOG", out sqlStr, out param);
            KTCC.DoSql(sqlStr, param);
        }
    }
}
