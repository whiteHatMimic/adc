﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KTDB;

namespace ADC100.Models
{
    class mdPrs006Service
    {
        private KTConnectionControler KTCC;

        public string sqlSql = string.Empty;

        public mdPrs006Service(KTConnectionControler _KTCC)
        {
            KTCC = _KTCC;
        }

        /// <summary>
        /// 取得醫令碼之藥品名稱
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ordid"></param>
        /// <returns></returns>
        public T GetPrs006OrdId<T>(string ordid)
        {
            sqlSql = @"SELECT ORDID,
                              ORDNME,
                              SDATE,
                              EDATE,
                              ACTION,
                              BIUNT,
                              PKUNT
                         FROM PRS006
                           WHERE     ORDID = :ORDID
                                 AND NO = '0'";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlSql;
            cmd.Parameters("ORDID", ordid);
            var result = KTCC.DoSql<T>(cmd).SingleOrDefault();

            return result;
        }
    }
}
