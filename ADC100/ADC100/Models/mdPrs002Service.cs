﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KTDB;
using ADC100.VM;

namespace ADC100.Models
{
    public class mdPrs002Service
    {
        private KTConnectionControler KTCC;
        public string sqlStr = string.Empty;

        public mdPrs002Service(KTConnectionControler _KTCC)
        {
            KTCC = _KTCC;
        }

        /// <summary>
        /// 取得單位對照表
        /// </summary>
        /// <param name="DBlink"></param>
        /// <returns></returns>
        public IEnumerable<T> GetPrs002<T>()
        {
            sqlStr = "SELECT DPTID, FNAME FROM PRS002 WHERE DTFLG = '2' AND VISIBLEFLG='Y' ORDER BY DPTID ASC";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            var result = KTCC.DoSql<T>(cmd).ToList();

            return result;
        }
    }
}
