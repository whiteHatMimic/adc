﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KTDB;

namespace ADC100.Models
{
    public class mdPub002Service
    {
        private KTConnectionControler KTCC;

        private string sqlStr = string.Empty;

        public mdPub002Service(KTConnectionControler _KTCC)
        {
            KTCC = _KTCC;
        }

        public IEnumerable<T> GetPub002<T>()
        {
            sqlStr = "SELECT CODE AS DPTID, CNAME AS FNAME FROM PUB002 WHERE DTATYP='GAD'";

            KTDBCommand cmd = new KTDBCommand();
            cmd.CommandText = sqlStr;
            var result = KTCC.DoSql<T>(cmd).ToList();

            return result;
        }
    }
}
