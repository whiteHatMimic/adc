﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using ADC100.Models;
using ADC100.VM;
using KTDB;

namespace ADC100
{
    public partial class ADC_設備維護 : Form
    {
        private KTConnectionControler KTCC;
        private string hsp = mdArea._hsp;
        private string userId = mdUser.userId;
        private string userName = mdUser.userName;

        public ADC_設備維護()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 重抓 ADC100 設備資料
        /// </summary>
        public void ReLoadADC100()
        {
            var ADC100DeviceList = new mdADC100Service(KTCC).GetADC100DeviceList();
        
            ctdbgADCDevice.SetDataBinding(ADC100DeviceList, "", true);
        }

        /// <summary>
        /// 控制元件狀態
        /// </summary>
        /// <param name="isRead">ADC 編號是否唯獨</param>
        /// <param name="isBtnAddADCVis">新增 按鈕是否隱藏</param>
        /// <param name="isBtnUpADCVis">更新 按鈕是否隱藏</param>
        /// <param name="isBtnDelVis">刪除 按鈕是否隱藏</param>
        public void SetControlStatus(bool isRead, bool isBtnAddADCVis, bool isBtnUpADCVis, bool isBtnDelVis)
        {   
            txtADCNo.ReadOnly = isRead;
            btnAddADC100.Visible = isBtnAddADCVis;
            btnUpADC100.Visible = isBtnUpADCVis;
            btnDelADC100.Visible = isBtnDelVis;
        }

        /// <summary>
        /// 取得 ADC 設備資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ADC_設備維護_Load(object sender, EventArgs e)
        {
            cboStkcod.Text = cboStkcod.Items[0].ToString();
            SetControlStatus(false, true, false, false);

            try
            {
                using (KTCC = new KTConnectionControler())
                {
                    KTCC.RunExecute = false;
                    KTCC.Connect(hsp, CnHomeFlg: true);

                    this.Text = this.Text + " ";
                    this.Text = this.Text + KTEnvs.GetLocationInChinese(hsp) + " ";
                    this.Text = this.Text + new KTDataBase(KTCC).HISProgramer() + " ";
                    this.Text = this.Text + "登入人員: " + userId + "  " + userName;

                    // ADC 設備清單
                    ReLoadADC100();

                    //單位代碼清單
                    var PRS002List = new mdPrs002Service(KTCC).GetPrs002<PRS002VM>();

                    //加入護理站清單
                    foreach (var obj in PRS002List)
                    {
                        cboStkcod.Items.Add(obj.DPTID + "  " + obj.FNAME);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 新增 ADC100 設備資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddADC100_Click(object sender, EventArgs e)
        {
            //驗證 ADC 編號、護理站名稱、位置描述誰為空白並提醒
            string isVaild = new mdADC100Validate().VerifyADCDriveData(txtADCNo.Text, cboStkcod.Text, txtLocationDescription.Text);

            if (!string.IsNullOrEmpty(isVaild))
            {
                MessageBox.Show(isVaild, "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                InsertADC100VM insertADCDevice = new InsertADC100VM();
                insertADCDevice.ADC_NO = txtADCNo.Text.ToUpper();
                insertADCDevice.STKCOD = cboStkcod.Text.Substring(0, 4);
                insertADCDevice.LOCATION_DESCRIPTION = txtLocationDescription.Text;
                insertADCDevice.STATUS = chkADCStatsus.Checked ? 1 : 0;
                insertADCDevice.SUID = userId;
                insertADCDevice.SDTE = KTDateTime.Today();
                insertADCDevice.STME = KTDateTime.HHMMSS();
                insertADCDevice.UPOP = userId;
                insertADCDevice.UPDTE = KTDateTime.Today();
                insertADCDevice.UPTME = KTDateTime.HHMMSS();

                using (KTCC = new KTConnectionControler())
                {
                    KTCC.RunExecute = false;
                    KTCC.Connect(hsp, CnHomeFlg: true);
                    KTCC.BeginTransaction();

                    //檢查有無重複資料
                    var isRepeat = new mdADC100Service(KTCC).InspactADC100<InsertADC100VM>(insertADCDevice.ADC_NO);

                    if (isRepeat != null)
                    {
                        MessageBox.Show("輸入的 ADC 編號已重複，請更換別種編號", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        new mdADC100Service(KTCC).InsertADC100Device(insertADCDevice);

                        if (KTCC.Rowindicator == 1)
                        {
                            KTCC.Commit();
                            MessageBox.Show("新增 ADC 設備資料成功", "資訊", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ReLoadADC100();
                            txtADCNo.Clear();
                            cboStkcod.Text = cboStkcod.Items[0].ToString();
                            txtLocationDescription.Clear();
                        }
                        else
                        {
                            KTCC.Rollback();
                            MessageBox.Show("新增 ADC 設備資料失敗", "資訊", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 更新 ADC100 設備
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpADC100_Click(object sender, EventArgs e)
        {
            //驗證 ADC 編號、護理站名稱、位置描述誰為空白並提醒
            string isVaild = new mdADC100Validate().VerifyADCDriveData(txtADCNo.Text, cboStkcod.Text, txtLocationDescription.Text);

            if (!string.IsNullOrEmpty(isVaild))
            {
                MessageBox.Show(isVaild, "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                string adcNo = txtADCNo.Text;
                newUpdateADC100VM newUpdateADC100 = new newUpdateADC100VM();
                newUpdateADC100.STKCOD = cboStkcod.Text.Substring(0, 4);
                newUpdateADC100.LOCATION_DESCRIPTION = txtLocationDescription.Text;
                newUpdateADC100.STATUS = Convert.ToInt32(chkADCStatsus.Checked);
                newUpdateADC100.UPOP = userId;
                newUpdateADC100.UPDTE = KTDateTime.Today();
                newUpdateADC100.UPTME = KTDateTime.HHMMSS();

                using (KTCC = new KTConnectionControler())
                {
                    KTCC.RunExecute = false;
                    KTCC.Connect(hsp, CnHomeFlg: true);
                    KTCC.BeginTransaction();

                    var oldUpdateADC100 = new mdADC100Service(KTCC).InspactADC100<oldUpdateADC100VM>(adcNo);

                    if (oldUpdateADC100 == null)
                    {
                        MessageBox.Show("查無此筆資料", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        new mdADC100Service(KTCC).UpdateADC100Device(adcNo, newUpdateADC100, oldUpdateADC100);

                        if (KTCC.Rowindicator == 1)
                        {
                            KTCC.Commit();
                            MessageBox.Show("修改 ADC 設備資料成功", "資訊", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ReLoadADC100();
                        }
                        else
                        {
                            KTCC.Rollback();
                            MessageBox.Show("修改 ADC 設備成功", "警告", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //刪除 ADC100 設備資料
        private void btnDelADC100_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("您確定要刪除這筆 ADC 資料", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            try
            {
                string adcNo = txtADCNo.Text;

                if (dr == DialogResult.Yes)
                {
                    using (KTCC = new KTConnectionControler())
                    {
                        KTCC.RunExecute = false;
                        KTCC.Connect(hsp, CnHomeFlg: true);
                        KTCC.BeginTransaction();

                        var isEmpty = new mdADC100Service(KTCC).InspactADC100<DeleteADC100VM>(adcNo);

                        if (isEmpty == null)
                        {
                            MessageBox.Show("資料已被刪除", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            new mdADC100Service(KTCC).DeleteADC100Device(adcNo);

                            if (KTCC.Rowindicator == 1)
                            {
                                KTCC.Commit();
                                MessageBox.Show("刪除 ADC 資料成功", "資訊", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ReLoadADC100();
                            }
                            else
                            {
                                KTCC.Rollback();
                                MessageBox.Show("刪除 ADC 資料失敗", "資訊", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // 餵資料至上面 menu
        private void ctdbgADCDevice_DoubleClick(object sender, EventArgs e)
        {
            txtADCNo.Text = ctdbgADCDevice.Columns["ADC_NO"].Text;
            cboStkcod.Text = ctdbgADCDevice.Columns["STKCOD"].Text;
            txtLocationDescription.Text = ctdbgADCDevice.Columns["LOCATION_DESCRIPTION"].Text;
            chkADCStatsus.Checked = (ctdbgADCDevice.Columns["STATUS"].Text == "啟用") ? true : false;
            SetControlStatus(true, false, true, true);
        }

        //點選維護按鈕進入藥品維護畫面
        private void ctdbgADCDevice_ButtonClick(object sender, C1.Win.C1TrueDBGrid.ColEventArgs e)
        {
            if (e.ColIndex == 4)
            {
                string adcNo = ctdbgADCDevice.Columns["ADC_NO"].Value.ToString();
                string nurStationName = ctdbgADCDevice.Columns["STKCOD"].Value.ToString();
                string locationDescription = ctdbgADCDevice.Columns["LOCATION_DESCRIPTION"].Value.ToString();
                string status = ctdbgADCDevice.Columns["STATUS"].Value.ToString();
                ADC_護理站藥品維護 ADC_護理站藥品維護 = new ADC_護理站藥品維護();
                ADC_護理站藥品維護.withADCDevice(adcNo, nurStationName, locationDescription);
                ADC_護理站藥品維護.ShowDialog();
            }
        }

        //清空 menu
        private void btnClear_Click(object sender, EventArgs e)
        {
            SetControlStatus(false, true, false, false);
            txtADCNo.Clear();
            cboStkcod.Text = cboStkcod.Items[0].ToString();
            chkADCStatsus.Checked = false;
            txtLocationDescription.Clear();
        }

        // 關閉畫面
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // 模擬 key
        private void ADC_設備維護_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }
    }
}
