﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using KTDB;
using ADC100.Models;
using ADC100.VM;

namespace ADC100
{
    public partial class ADC_護理站藥品維護 : Form
    {
        private KTConnectionControler KTCC;
        private string hsp = mdArea._hsp;
        private string userId = mdUser.userId;
        private string userName = mdUser.userName;
        private string tmpADCNo = string.Empty; //暫存 ADC 編號
        private string tmpStkcod = string.Empty; //單位代碼
        private bool isEffective = false;  //儲放是否為有效醫令

        public ADC_護理站藥品維護()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 抓取現在 ADC 為哪台，被 ADC 設備維護的 ctdbgADCDevice_ButtonClick 方法呼叫
        /// </summary>
        /// <param name="adcNo">ADC 編號</param>
        /// <param name="stationId">護理站名稱</param>
        /// <param name="locationDescription">位置描述</param>
        public void withADCDevice(string adcNo, string stkcod, string locationDescription)
        {
            lblADCNo.Text = lblADCNo.Text + " " + adcNo;
            lblStkcod.Text = lblStkcod.Text + " " + stkcod;
            lblLocationDescription.Text = lblLocationDescription.Text + " " + locationDescription;
            tmpADCNo = adcNo;
            tmpStkcod = stkcod.Substring(0, 4);
        }

        //重抓某台 ADC 藥品
        private void ReLoadADC101()
        {
            var result = new mdADC101Service(KTCC).GetADC100DrugList(tmpADCNo, tmpStkcod);

            ctdbgADCDrug.SetDataBinding(result, "", true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ADC_護理站藥品維護_Load(object sender, EventArgs e)
        {
            btnUpADC101.Visible = false;
            btnDelADC101.Visible = false;

            try
            {
                using (KTCC = new KTConnectionControler())
                {
                    KTCC.RunExecute = false;
                    KTCC.Connect(hsp, CnHomeFlg: true);

                    this.Text = this.Text + " ";
                    this.Text = this.Text + KTEnvs.GetLocationInChinese(hsp) + " ";
                    this.Text = this.Text + new KTDataBase(KTCC).HISProgramer() + " ";
                    this.Text = this.Text + "登入人員: " + userId + "  " + userName;

                    ReLoadADC101();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 新增 ADC101 table 藥品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddADC101_Click(object sender, EventArgs e)
        {
            var isVaild = new mdADC101Validate().VerifyADC101Data(txtOrdId.Text, txtStoragePos.Text, txtStorageNo.Text);

            if (!string.IsNullOrEmpty(isVaild))
            {
                MessageBox.Show(isVaild, "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                InsertADC101VM obj = new InsertADC101VM();
                obj.ADC_NO = tmpADCNo;
                obj.STKCOD = tmpStkcod;
                obj.ORDID = txtOrdId.Text;
                obj.STORAGE_POS = txtStoragePos.Text;
                obj.STORAGE_NO = txtStorageNo.Text;
                obj.SAFE_AMOUNT = txtSafeAmount.Text;
                obj.STANDARD_AMOUNT = txtStandardAmount.Text;
                obj.BIUNT = lblBiunt.Text;
                obj.PKUNT = lblPkunt.Text;
                obj.SUID = userId;
                obj.SDTE = KTDateTime.Today();
                obj.STME = KTDateTime.HHMMSS();
                obj.UPOP = userId;
                obj.UPDTE = KTDateTime.Today();
                obj.UPTME = KTDateTime.HHMMSS();

                if (isEffective == true)
                {
                    using (KTCC = new KTConnectionControler())
                    {
                        KTCC.RunExecute = false;
                        KTCC.Connect(hsp, CnHomeFlg: true);
                        KTCC.BeginTransaction();

                        var isRepact = new mdADC101Service(KTCC).InspactADC101<InsertADC100VM>(obj.ADC_NO, obj.STKCOD, obj.ORDID);

                        if (isRepact != null)
                        {
                            MessageBox.Show("此藥品已新增過無法新增", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            new mdADC101Service(KTCC).InsertADC101(obj);

                            if (KTCC.Rowindicator == 1)
                            {
                                KTCC.Commit();
                                MessageBox.Show("新增藥品成功", "資訊", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ReLoadADC101();
                                txtOrdId.Clear();
                                lblOrdName.Text = "醫令名稱: ";
                                txtStoragePos.Clear();
                                txtStorageNo.Clear();
                                txtSafeAmount.Clear();
                                txtStandardAmount.Clear();
                                txtOrdId.ReadOnly = false;
                                isEffective = false;
                            }
                            else
                            {
                                KTCC.Rollback();
                                MessageBox.Show("新增藥品失敗", "警告", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("無效醫令", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 更新藥品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpADC101_Click(object sender, EventArgs e)
        {
            var isVaild = new mdADC101Validate().VerifyADC101Data(txtOrdId.Text, txtStoragePos.Text, txtStorageNo.Text);

            if (!string.IsNullOrEmpty(isVaild))
            {
                MessageBox.Show(isVaild, "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                string adcNo = tmpADCNo;
                string stationId = tmpStkcod;
                string ordid = txtOrdId.Text;
                newUpdateADC101VM newObj = new newUpdateADC101VM();
                newObj.STORAGE_POS = txtStoragePos.Text;
                newObj.STORAGE_NO = Convert.ToInt32(txtStorageNo.Text);
                newObj.SAFE_AMOUNT = txtSafeAmount.Text;
                newObj.STANDARD_AMOUNT = txtStandardAmount.Text;
                newObj.UPOP = userId;
                newObj.UPDTE = KTDateTime.Today();
                newObj.UPTME = KTDateTime.HHMMSS();

                using (KTCC = new KTConnectionControler())
                {
                    KTCC.RunExecute = false;
                    KTCC.Connect(hsp, CnHomeFlg: true);
                    KTCC.BeginTransaction();

                    var oldObj = new mdADC101Service(KTCC).InspactADC101<oldUpdateADC101VM>(adcNo, stationId, ordid);

                    if (oldObj == null)
                    {
                        MessageBox.Show("查無藥品資料", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        new mdADC101Service(KTCC).UpdateADC101(adcNo, stationId, ordid, newObj, oldObj);

                        if (KTCC.Rowindicator == 1)
                        {
                            KTCC.Commit();
                            MessageBox.Show("更新藥品成功", "資訊", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ReLoadADC101();
                        }
                        else
                        {
                            KTCC.Rollback();
                            MessageBox.Show("更新藥品失敗", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 刪除 ADC 藥品
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelADC101_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("您確定要刪除此藥品", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            try
            {
                if (dr == DialogResult.Yes)
                {
                    InsertADC101LOGVM obj = new InsertADC101LOGVM();
                    obj.ADC_NO = tmpADCNo;
                    obj.STKCOD = tmpStkcod;
                    obj.ORDID = txtOrdId.Text;
                    obj.STORAGE_POS = txtStoragePos.Text;
                    obj.STORAGE_NO = txtStorageNo.Text;
                    obj.SAFE_AMOUNT = txtSafeAmount.Text;
                    obj.STANDARD_AMOUNT = txtStandardAmount.Text;
                    obj.DELPOP = userId;
                    obj.DELDTE = KTDateTime.Today();
                    obj.DELTME = KTDateTime.HHMMSS();

                    using (KTCC = new KTConnectionControler())
                    {
                        KTCC.RunExecute = false;
                        KTCC.Connect(hsp, CnHomeFlg: true);
                        KTCC.BeginTransaction();

                        var isEmptyADC101 = new mdADC101Service(KTCC).InspactADC101<DeleteADC100VM>(obj.ADC_NO, obj.STKCOD, obj.ORDID);

                        if (isEmptyADC101 == null)
                        {
                            MessageBox.Show("此藥品已刪除", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            new mdADC101Service(KTCC).DeleteADC101(obj.ADC_NO, obj.STKCOD, obj.ORDID);
                            new mdADC101LOGService(KTCC).InsertADC101DelLog(obj);

                            if (KTCC.Rowindicator == 1)
                            {
                                KTCC.Commit();
                                MessageBox.Show("刪除藥品成功", "資訊", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ReLoadADC101();
                            }
                            else
                            {
                                KTCC.Rollback();
                                MessageBox.Show("刪除藥品失敗", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //清除 menu
        private void btnClear_Click(object sender, EventArgs e)
        {
            txtOrdId.Clear();
            lblOrdName.Text = "醫令名稱: ";
            txtStoragePos.Clear();
            txtStorageNo.Clear();
            txtSafeAmount.Clear();
            txtStandardAmount.Clear();
            txtOrdId.ReadOnly = false;
            btnAddADC101.Visible = true;
            btnUpADC101.Visible = false;
            btnDelADC101.Visible = false;
            isEffective = false;
        }

        //餵資料至上面 menu
        private void ctdbgADCDrug_DoubleClick(object sender, EventArgs e)
        {
            txtOrdId.Text = ctdbgADCDrug.Columns["ORDID"].Text;
            lblOrdName.Text = "醫令名稱: " + ctdbgADCDrug.Columns["ORDNME"].Text;
            txtStoragePos.Text = ctdbgADCDrug.Columns["STORAGE_POS"].Text;
            txtStorageNo.Text = ctdbgADCDrug.Columns["STORAGE_NO"].Text;
            txtSafeAmount.Text = ctdbgADCDrug.Columns["SAFE_AMOUNT"].Text;
            txtStandardAmount.Text = ctdbgADCDrug.Columns["STANDARD_AMOUNT"].Text;
            lblBiunt.Text = ctdbgADCDrug.Columns["BIUNT"].Text;
            lblPkunt.Text = ctdbgADCDrug.Columns["PKUNT"].Text;
            txtOrdId.ReadOnly = true;
            btnAddADC101.Visible = false;
            btnUpADC101.Visible = true;
            btnDelADC101.Visible = true;
        }

        // 模擬 key
        private void ADC_護理站藥品維護_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.SendWait("{Tab}");
            }
        }

        /// <summary>
        /// 醫令在輸入時，醫令名稱清單
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtOrdId_TextChanged(object sender, EventArgs e)
        {
            lblOrdName.Text = "醫令名稱: ";
        }

        /// <summary>
        /// 驗證是否為有效醫令
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtOrdId_Validating(object sender, CancelEventArgs e)
        {
            string ordId = txtOrdId.Text;

            if (!isEffective)
            {
                if (!string.IsNullOrEmpty(ordId))
                {
                    try
                    {
                        using (KTCC = new KTConnectionControler())
                        {
                            KTCC.RunExecute = false;
                            KTCC.Connect(hsp, CnHomeFlg: true);

                            //抓取有無此醫令
                            var prs006OrdId = new mdPrs006Service(KTCC).GetPrs006OrdId<PRS006VM>(ordId);

                            if (prs006OrdId == null)
                            {
                                isEffective = false;
                                lblOrdName.Text = "醫令名稱: ";
                                MessageBox.Show("查無此醫令", "警告", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                            else
                            {
                                isEffective = true;
                                lblOrdName.Text = "醫令名稱: " + prs006OrdId.ORDNME;
                                lblBiunt.Text = prs006OrdId.BIUNT;
                                lblPkunt.Text = prs006OrdId.PKUNT;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }

            e.Cancel = false;
            txtOrdId.ReadOnly = true;
        }

        //離開畫面
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
