﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using ADC100.Models;

namespace ADC100
{
    public partial class Area : Form
    {
        public Area()
        {
            InitializeComponent();
            mdUser.userId = KTString.GetUserID();
            mdUser.userName = KTString.GetUserName();
        }

        // 確認院區
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            string hsp = string.Empty;

            if (rbKTGH00.Checked == true)
            {
                hsp = "KTGH00";
            }
            else if (rbHPK210.Checked == true)
            {
                hsp = "HPK210";
            }
            else if (rbTSHIS.Checked == true)
            {
                hsp = "TSHIS";
            }
            else
            {
                MessageBox.Show("您尚未選擇院區", "警告", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.Visible = false;
            mdArea._hsp = hsp;
            ADC_設備維護 adc = new ADC_設備維護();
            adc.Show();
        }

        // 離開程式
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
