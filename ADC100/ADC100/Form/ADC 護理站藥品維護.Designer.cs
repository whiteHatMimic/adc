﻿namespace ADC100
{
    partial class ADC_護理站藥品維護
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ADC_護理站藥品維護));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLocationDescription = new System.Windows.Forms.Label();
            this.lblStkcod = new System.Windows.Forms.Label();
            this.lblADCNo = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblPkunt = new System.Windows.Forms.Label();
            this.lblBiunt = new System.Windows.Forms.Label();
            this.txtStandardAmount = new System.Windows.Forms.TextBox();
            this.txtSafeAmount = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtStoragePos = new System.Windows.Forms.TextBox();
            this.btnDelADC101 = new System.Windows.Forms.Button();
            this.txtStorageNo = new System.Windows.Forms.TextBox();
            this.btnUpADC101 = new System.Windows.Forms.Button();
            this.btnAddADC101 = new System.Windows.Forms.Button();
            this.txtOrdId = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblOrdName = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.ctdbgADCDrug = new C1.Win.C1TrueDBGrid.C1TrueDBGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctdbgADCDrug)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblLocationDescription);
            this.panel1.Controls.Add(this.lblStkcod);
            this.panel1.Controls.Add(this.lblADCNo);
            this.panel1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.panel1.Location = new System.Drawing.Point(14, 14);
            this.panel1.Margin = new System.Windows.Forms.Padding(5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(980, 50);
            this.panel1.TabIndex = 0;
            // 
            // lblLocationDescription
            // 
            this.lblLocationDescription.AutoSize = true;
            this.lblLocationDescription.Location = new System.Drawing.Point(651, 10);
            this.lblLocationDescription.Margin = new System.Windows.Forms.Padding(10);
            this.lblLocationDescription.Name = "lblLocationDescription";
            this.lblLocationDescription.Padding = new System.Windows.Forms.Padding(5);
            this.lblLocationDescription.Size = new System.Drawing.Size(90, 26);
            this.lblLocationDescription.TabIndex = 2;
            this.lblLocationDescription.Text = "位置描述: ";
            // 
            // lblStkcod
            // 
            this.lblStkcod.AutoSize = true;
            this.lblStkcod.Location = new System.Drawing.Point(351, 10);
            this.lblStkcod.Margin = new System.Windows.Forms.Padding(10);
            this.lblStkcod.Name = "lblStkcod";
            this.lblStkcod.Padding = new System.Windows.Forms.Padding(5);
            this.lblStkcod.Size = new System.Drawing.Size(74, 26);
            this.lblStkcod.TabIndex = 2;
            this.lblStkcod.Text = "護理站: ";
            // 
            // lblADCNo
            // 
            this.lblADCNo.AutoSize = true;
            this.lblADCNo.Location = new System.Drawing.Point(10, 10);
            this.lblADCNo.Margin = new System.Windows.Forms.Padding(10);
            this.lblADCNo.Name = "lblADCNo";
            this.lblADCNo.Padding = new System.Windows.Forms.Padding(5);
            this.lblADCNo.Size = new System.Drawing.Size(126, 26);
            this.lblADCNo.TabIndex = 0;
            this.lblADCNo.Text = "ADC 設備編號: ";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblPkunt);
            this.panel2.Controls.Add(this.lblBiunt);
            this.panel2.Controls.Add(this.txtStandardAmount);
            this.panel2.Controls.Add(this.txtSafeAmount);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.btnClear);
            this.panel2.Controls.Add(this.txtStoragePos);
            this.panel2.Controls.Add(this.btnDelADC101);
            this.panel2.Controls.Add(this.txtStorageNo);
            this.panel2.Controls.Add(this.btnUpADC101);
            this.panel2.Controls.Add(this.btnAddADC101);
            this.panel2.Controls.Add(this.txtOrdId);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.lblOrdName);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.panel2.Location = new System.Drawing.Point(14, 74);
            this.panel2.Margin = new System.Windows.Forms.Padding(5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(980, 152);
            this.panel2.TabIndex = 3;
            // 
            // lblPkunt
            // 
            this.lblPkunt.AutoSize = true;
            this.lblPkunt.Location = new System.Drawing.Point(912, 62);
            this.lblPkunt.Name = "lblPkunt";
            this.lblPkunt.Size = new System.Drawing.Size(40, 16);
            this.lblPkunt.TabIndex = 13;
            this.lblPkunt.Text = "AMP";
            // 
            // lblBiunt
            // 
            this.lblBiunt.AutoSize = true;
            this.lblBiunt.Location = new System.Drawing.Point(913, 14);
            this.lblBiunt.Name = "lblBiunt";
            this.lblBiunt.Size = new System.Drawing.Size(40, 16);
            this.lblBiunt.TabIndex = 12;
            this.lblBiunt.Text = "AMP";
            // 
            // txtStandardAmount
            // 
            this.txtStandardAmount.Location = new System.Drawing.Point(815, 56);
            this.txtStandardAmount.MaxLength = 5;
            this.txtStandardAmount.Name = "txtStandardAmount";
            this.txtStandardAmount.Size = new System.Drawing.Size(86, 27);
            this.txtStandardAmount.TabIndex = 11;
            // 
            // txtSafeAmount
            // 
            this.txtSafeAmount.Location = new System.Drawing.Point(813, 7);
            this.txtSafeAmount.MaxLength = 2;
            this.txtSafeAmount.Name = "txtSafeAmount";
            this.txtSafeAmount.Size = new System.Drawing.Size(86, 27);
            this.txtSafeAmount.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(738, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "標準量:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(742, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "安全量:";
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.btnClear.FlatAppearance.BorderSize = 0;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnClear.Location = new System.Drawing.Point(343, 97);
            this.btnClear.Margin = new System.Windows.Forms.Padding(5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Padding = new System.Windows.Forms.Padding(5);
            this.btnClear.Size = new System.Drawing.Size(100, 40);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "清除";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtStoragePos
            // 
            this.txtStoragePos.Location = new System.Drawing.Point(123, 53);
            this.txtStoragePos.Margin = new System.Windows.Forms.Padding(5);
            this.txtStoragePos.Name = "txtStoragePos";
            this.txtStoragePos.Size = new System.Drawing.Size(150, 27);
            this.txtStoragePos.TabIndex = 2;
            // 
            // btnDelADC101
            // 
            this.btnDelADC101.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(34)))), ((int)(((byte)(68)))));
            this.btnDelADC101.FlatAppearance.BorderSize = 0;
            this.btnDelADC101.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelADC101.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnDelADC101.ForeColor = System.Drawing.Color.White;
            this.btnDelADC101.Location = new System.Drawing.Point(233, 97);
            this.btnDelADC101.Margin = new System.Windows.Forms.Padding(5);
            this.btnDelADC101.Name = "btnDelADC101";
            this.btnDelADC101.Padding = new System.Windows.Forms.Padding(5);
            this.btnDelADC101.Size = new System.Drawing.Size(100, 40);
            this.btnDelADC101.TabIndex = 6;
            this.btnDelADC101.Text = "刪除";
            this.btnDelADC101.UseVisualStyleBackColor = false;
            this.btnDelADC101.Click += new System.EventHandler(this.btnDelADC101_Click);
            // 
            // txtStorageNo
            // 
            this.txtStorageNo.Location = new System.Drawing.Point(451, 56);
            this.txtStorageNo.Margin = new System.Windows.Forms.Padding(5);
            this.txtStorageNo.Name = "txtStorageNo";
            this.txtStorageNo.Size = new System.Drawing.Size(150, 27);
            this.txtStorageNo.TabIndex = 3;
            // 
            // btnUpADC101
            // 
            this.btnUpADC101.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(84)))), ((int)(((byte)(124)))));
            this.btnUpADC101.FlatAppearance.BorderSize = 0;
            this.btnUpADC101.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpADC101.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnUpADC101.ForeColor = System.Drawing.Color.White;
            this.btnUpADC101.Location = new System.Drawing.Point(123, 97);
            this.btnUpADC101.Margin = new System.Windows.Forms.Padding(5);
            this.btnUpADC101.Name = "btnUpADC101";
            this.btnUpADC101.Padding = new System.Windows.Forms.Padding(5);
            this.btnUpADC101.Size = new System.Drawing.Size(100, 40);
            this.btnUpADC101.TabIndex = 5;
            this.btnUpADC101.Text = "修改";
            this.btnUpADC101.UseVisualStyleBackColor = false;
            this.btnUpADC101.Click += new System.EventHandler(this.btnUpADC101_Click);
            // 
            // btnAddADC101
            // 
            this.btnAddADC101.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(189)))), ((int)(((byte)(157)))));
            this.btnAddADC101.FlatAppearance.BorderSize = 0;
            this.btnAddADC101.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddADC101.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnAddADC101.ForeColor = System.Drawing.Color.White;
            this.btnAddADC101.Location = new System.Drawing.Point(13, 97);
            this.btnAddADC101.Margin = new System.Windows.Forms.Padding(5);
            this.btnAddADC101.Name = "btnAddADC101";
            this.btnAddADC101.Padding = new System.Windows.Forms.Padding(5);
            this.btnAddADC101.Size = new System.Drawing.Size(100, 40);
            this.btnAddADC101.TabIndex = 4;
            this.btnAddADC101.Text = "新增";
            this.btnAddADC101.UseVisualStyleBackColor = false;
            this.btnAddADC101.Click += new System.EventHandler(this.btnAddADC101_Click);
            // 
            // txtOrdId
            // 
            this.txtOrdId.Location = new System.Drawing.Point(123, 7);
            this.txtOrdId.Margin = new System.Windows.Forms.Padding(5);
            this.txtOrdId.Name = "txtOrdId";
            this.txtOrdId.Size = new System.Drawing.Size(150, 27);
            this.txtOrdId.TabIndex = 1;
            this.txtOrdId.TextChanged += new System.EventHandler(this.txtOrdId_TextChanged);
            this.txtOrdId.Validating += new System.ComponentModel.CancelEventHandler(this.txtOrdId_Validating);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(366, 56);
            this.label8.Margin = new System.Windows.Forms.Padding(10);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(5);
            this.label8.Size = new System.Drawing.Size(70, 26);
            this.label8.TabIndex = 2;
            this.label8.Text = "儲位號:";
            // 
            // lblOrdName
            // 
            this.lblOrdName.AutoSize = true;
            this.lblOrdName.Location = new System.Drawing.Point(331, 10);
            this.lblOrdName.Margin = new System.Windows.Forms.Padding(10);
            this.lblOrdName.Name = "lblOrdName";
            this.lblOrdName.Padding = new System.Windows.Forms.Padding(5);
            this.lblOrdName.Size = new System.Drawing.Size(94, 26);
            this.lblOrdName.TabIndex = 2;
            this.lblOrdName.Text = "醫令名稱:  ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 53);
            this.label7.Margin = new System.Windows.Forms.Padding(10);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(5);
            this.label7.Size = new System.Drawing.Size(86, 26);
            this.label7.TabIndex = 2;
            this.label7.Text = "藥品位置:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(38, 7);
            this.label9.Margin = new System.Windows.Forms.Padding(10);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(5);
            this.label9.Size = new System.Drawing.Size(70, 26);
            this.label9.TabIndex = 0;
            this.label9.Text = "醫令碼:";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(191)))), ((int)(((byte)(191)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnClose.Location = new System.Drawing.Point(894, 627);
            this.btnClose.Margin = new System.Windows.Forms.Padding(10);
            this.btnClose.Name = "btnClose";
            this.btnClose.Padding = new System.Windows.Forms.Padding(5);
            this.btnClose.Size = new System.Drawing.Size(100, 40);
            this.btnClose.TabIndex = 12;
            this.btnClose.Text = "離開";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // ctdbgADCDrug
            // 
            this.ctdbgADCDrug.AllowUpdate = false;
            this.ctdbgADCDrug.AllowUpdateOnBlur = false;
            this.ctdbgADCDrug.AlternatingRows = true;
            this.ctdbgADCDrug.CaptionHeight = 19;
            this.ctdbgADCDrug.EmptyRows = true;
            this.ctdbgADCDrug.FlatStyle = C1.Win.C1TrueDBGrid.FlatModeEnum.Popup;
            this.ctdbgADCDrug.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ctdbgADCDrug.GroupByCaption = "Drag a column header here to group by that column";
            this.ctdbgADCDrug.Images.Add(((System.Drawing.Image)(resources.GetObject("ctdbgADCDrug.Images"))));
            this.ctdbgADCDrug.Location = new System.Drawing.Point(14, 241);
            this.ctdbgADCDrug.Margin = new System.Windows.Forms.Padding(5, 10, 5, 10);
            this.ctdbgADCDrug.MarqueeStyle = C1.Win.C1TrueDBGrid.MarqueeEnum.HighlightRow;
            this.ctdbgADCDrug.Name = "ctdbgADCDrug";
            this.ctdbgADCDrug.PreviewInfo.Location = new System.Drawing.Point(0, 0);
            this.ctdbgADCDrug.PreviewInfo.Size = new System.Drawing.Size(0, 0);
            this.ctdbgADCDrug.PreviewInfo.ZoomFactor = 75D;
            this.ctdbgADCDrug.PrintInfo.PageSettings = ((System.Drawing.Printing.PageSettings)(resources.GetObject("ctdbgADCDrug.PrintInfo.PageSettings")));
            this.ctdbgADCDrug.RowHeight = 24;
            this.ctdbgADCDrug.Size = new System.Drawing.Size(980, 366);
            this.ctdbgADCDrug.TabIndex = 8;
            this.ctdbgADCDrug.Text = "護理站藥品維護";
            this.ctdbgADCDrug.DoubleClick += new System.EventHandler(this.ctdbgADCDrug_DoubleClick);
            this.ctdbgADCDrug.PropBag = resources.GetString("ctdbgADCDrug.PropBag");
            // 
            // ADC_護理站藥品維護
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1008, 682);
            this.ControlBox = false;
            this.Controls.Add(this.ctdbgADCDrug);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ADC_護理站藥品維護";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ADC_護理站藥品維護";
            this.Load += new System.EventHandler(this.ADC_護理站藥品維護_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ADC_護理站藥品維護_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctdbgADCDrug)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblStkcod;
        private System.Windows.Forms.Label lblADCNo;
        private System.Windows.Forms.Label lblLocationDescription;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtStoragePos;
        private System.Windows.Forms.TextBox txtOrdId;
        private System.Windows.Forms.Label lblOrdName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtStorageNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnDelADC101;
        private System.Windows.Forms.Button btnUpADC101;
        private System.Windows.Forms.Button btnAddADC101;
        private System.Windows.Forms.Button btnClose;
        private C1.Win.C1TrueDBGrid.C1TrueDBGrid ctdbgADCDrug;
        private System.Windows.Forms.TextBox txtStandardAmount;
        private System.Windows.Forms.TextBox txtSafeAmount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPkunt;
        private System.Windows.Forms.Label lblBiunt;
    }
}